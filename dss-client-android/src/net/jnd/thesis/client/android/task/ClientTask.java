/**
 * 
 */
package net.jnd.thesis.client.android.task;

import net.jnd.thesis.common.bean.AuthenticationBean;
import net.jnd.thesis.common.bean.WSConnectionBean;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;
import android.os.AsyncTask;
import android.util.Log;

/**
 * @author João Domingos
 * @since Jan 19, 2013
 */
class ClientTask extends AsyncTask<String, String, Boolean> {

	@Override
	protected Boolean doInBackground(String... uri) {
		WebSocketServices services;
		AuthenticationBean auth = new AuthenticationBean("usera", "usera");
		WSConnectionBean wsConn = new WSConnectionBean();
		wsConn.setUrl(uri[0]);
		wsConn.setTimeout(100000L);
		if (ClientWebSocketContext.connect(wsConn)) {
			Log.d(getClass().getName(), "connection successfull!");
			if ((services = ClientWebSocketContext.getServices()) != null) {
				Log.d(getClass().getName(),
						"services initialization successfull!");
				String echoReq = "Hello world!";
				Log.d(getClass().getName(), "echo request: " + echoReq);
				try {
					Log.d(getClass().getName(),
							"echo response: " + services.echo(echoReq));
					services.authenticate(auth);
				} catch (WSException e) {
					Log.d(getClass().getName(), e.getMessage(), e);
				}
				if (services.isAuthenticated()) {
					Log.d(getClass().getName(), "authentication successfull!");
				} else {
					Log.d(getClass().getName(), "unable to authenticate...");
				}
			} else {
				Log.d(getClass().getName(), "unable to initialize services...");
			}
		} else {
			Log.d(getClass().getName(), "unable to connect...");
		}
		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		// Do anything with response..
	}
}