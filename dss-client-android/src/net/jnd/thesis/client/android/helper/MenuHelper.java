/**
 * 
 */
package net.jnd.thesis.client.android.helper;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.jnd.thesis.client.android.activity.EchoActivity;
import net.jnd.thesis.client.android.activity.ListsActivity;
import net.jnd.thesis.client.android.activity.LoginActivity;
import net.jnd.thesis.client.android.activity.SessionActivity;
import net.jnd.thesis.client.android.activity.SessionReplayActivity;
import net.jnd.thesis.client.android.activity.TestDataSourceActivity;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;

/**
 * @author João Domingos
 * @since Feb 3, 2013
 */
public class MenuHelper {

	private static Map<String, Class<? extends Activity>> appMenu = new LinkedHashMap<String, Class<? extends Activity>>();

	static {
		appMenu.put("Echo", EchoActivity.class);
		appMenu.put("Listings", ListsActivity.class);
		appMenu.put("Data Sources", TestDataSourceActivity.class);
		appMenu.put("Sessions", SessionActivity.class);
		appMenu.put("Replay", SessionReplayActivity.class);
		appMenu.put("Logout", LoginActivity.class);
	}

	/**
	 * @return
	 */
	public static List<String> getMenuKeys() {
		return new LinkedList<String>(appMenu.keySet());
	}

	/**
	 * @param relativePos
	 * @return
	 */
	public static Class<? extends Activity> getMenuActivity(int relativePos) {
		String key = getMenuKeys().get(relativePos);
		return appMenu.get(key);
	}

	/**
	 * @param l
	 * @param v
	 * @param position
	 * @param id
	 */
	public static void handleClick(Activity activity, ListView l, View v,
			int position, long id) {

		Class<? extends Activity> clazz = getMenuActivity(position);
		if (clazz != null) {
			Intent intent = new Intent(activity.getApplicationContext(), clazz);
			activity.startActivity(intent);
		} else {
			new LogoutTask(activity).execute((Object) null);
		}
	}

	/**
	 * 
	 */
	public static class LogoutTask extends AsyncTask<Object, Void, Boolean> {

		private Activity activity;

		/**
		 * 
		 */
		public LogoutTask(Activity activity) {
			this.activity = activity;
		}

		@Override
		protected Boolean doInBackground(final Object... params) {
			ClientWebSocketContext.disconnect();
			return true;
		}

		/**
		 * 
		 */
		private void startLoginActivity() {
			Intent intent = new Intent(activity.getApplicationContext(),
					LoginActivity.class);
			activity.startActivity(intent);
		}

		@Override
		protected void onPostExecute(final Boolean resp) {
			activity.finish();
			startLoginActivity();
		}

		@Override
		protected void onCancelled() {
			startLoginActivity();
		}

	}

}
