package net.jnd.thesis.client.android.activity;

import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.enums.Pattern;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.SessionEventHandler;
import net.jnd.thesis.ws.services.exception.WSException;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * @author João Domingos
 * @since Feb 3, 2013
 */
public class TestDataSourceActivity extends Activity {

	private BackgroundTask backgroundTask;

	private DisconnectTask disconnectTask;

	private DSSUserSessionConnection currentConnection;

	private Map<Pattern, DSSInteractionModel> interactionModels = new TreeMap<Pattern, DSSInteractionModel>();

	private Map<String, DSSDataSource> dataSourcesByName = new TreeMap<String, DSSDataSource>();
	
	private Map<Long, DSSDataSource> dataSourcesById = new TreeMap<Long, DSSDataSource>();

	private ToggleButton button;
	private Spinner spinner;
	private EditText text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cleanup();
		setContentView(R.layout.activity_test_data_source);
		setupComponents();
		ClientWebSocketContext.setEventHandler(new TestDataSourceEventsHandler(
				text));
	}

	@Override
	public void finish() {
		super.finish();
		new DisconnectTask().execute(currentConnection);
	}

	/**
	 * 
	 */
	private void cleanup() {
		this.backgroundTask = null;
		this.disconnectTask = null;
		this.currentConnection = null;
		this.interactionModels.clear();
		this.dataSourcesById.clear();
		this.dataSourcesByName.clear();
	}

	/**
	 * 
	 */
	private void setupComponents() {
		this.button = (ToggleButton) findViewById(R.id.testDataSourceButton);
		this.text = (EditText) findViewById(R.id.testDataSourceText);
		this.text.setEnabled(false);
		this.spinner = (Spinner) findViewById(R.id.testDataSourceSpinner);
		populate();
	}

	/**
	 * 
	 */
	private void populate() {
		populateDateSources();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.spinner_item,
				new LinkedList<String>(dataSourcesByName.keySet())) {
		};
		this.spinner.setAdapter(adapter);

		try {
			for (DSSInteractionModel bean : ClientWebSocketContext
					.getServices().listInteractionModels()) {
				interactionModels.put(bean.getPattern(), bean);
			}
		} catch (WSException e) {
			Log.e(getClass().getName(), e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	private void populateDateSources() {
		try {
			for (DSSDataSource bean : ClientWebSocketContext.getServices()
					.listDataSources()) {
				dataSourcesById.put(bean.getId(), bean);
				dataSourcesByName.put(bean.getName(), bean);
			}
		} catch (WSException e) {
			Log.e(getClass().getName(), e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	private class TestDataSourceEventsHandler implements SessionEventHandler {

		private TextView textView;

		/**
		 * @param textView
		 */
		TestDataSourceEventsHandler(TextView textView) {
			this.textView = textView;
		}

		@Override
		public void handleEvent(final EventBean evt) {
			runOnUiThread(new Runnable() {
				public void run() {
					// String dataSourceName =
					// dataSourcesById.get(evt.getDataSourceId()).getName();
					// String msg = String.format("[%s] %s\n", dataSourceName,
					// evt.getValue());
					// textView.append(msg);
					textView.append(evt.getValue() + "\n");
				}
			});
		}

	}

	/**
	 * @param view
	 */
	public void onClearButton(View view) {
		this.text.setText("");
	}
	
	/**
	 * @param view
	 */
	public void onExitButton(View view) {
		finish();
	}
	
	/**
	 * @param view
	 */
	public void onToggleClicked(View view) {
		boolean on = ((ToggleButton) view).isChecked();
		if (on) {
			String selected = String.valueOf(spinner.getSelectedItem());
			boolean cancel = false;
			View focusView = null;
			if (TextUtils.isEmpty(selected)) {
				button.setError(getString(R.string.error_field_required));
				focusView = spinner;
				cancel = true;
			}
			// if ok proceed
			if (cancel) {
				focusView.requestFocus();
			} else {
				button.setEnabled(false);
				backgroundTask = new BackgroundTask();
				backgroundTask.execute(dataSourcesByName.get(selected));
			}
		} else {
			if (currentConnection != null) {
				button.setEnabled(false);
				disconnectTask = new DisconnectTask();
				disconnectTask.execute(currentConnection);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_test_data_source, menu);
		return true;
	}

	/**
	 * 
	 */
	public class BackgroundTask extends AsyncTask<DSSDataSource, Void, Boolean> {

		@Override
		protected Boolean doInBackground(DSSDataSource... params) {
			boolean res = true;
			DSSInteractionModel streaming = interactionModels
					.get(Pattern.STREAMING);
			Long streamingId = streaming != null ? streaming.getId() : -1;
			try {
				currentConnection = ClientWebSocketContext.getServices()
						.startTestDataSourceSession(params[0].getId(),
								streamingId);
			} catch (WSException e) {
				Log.e(getClass().getName(), e.getMessage(), e);
				res = false;
			}
			return res;
		}

		@Override
		protected void onPostExecute(final Boolean resp) {
			backgroundTask = null;
			runOnUiThread(new Runnable() {
				public void run() {
					text.append("test session started: " + resp);
					text.append("\n");
					button.setEnabled(true);
				}
			});
		}

		@Override
		protected void onCancelled() {
			backgroundTask = null;
		}

	}

	/**
	 * 
	 */
	public class DisconnectTask extends
			AsyncTask<DSSUserSessionConnection, Void, Boolean> {

		@Override
		protected Boolean doInBackground(DSSUserSessionConnection... params) {
			boolean res = true;
			if (params[0] != null) {
				try {
					res = ClientWebSocketContext.getServices().stopSession(
							params[0].getId());
				} catch (WSException e) {
					Log.e(getClass().getName(), e.getMessage(), e);
					res = false;
				}
			}
			return res;
		}

		@Override
		protected void onPostExecute(final Boolean resp) {
			disconnectTask = null;
			runOnUiThread(new Runnable() {
				public void run() {
					text.append("test session stopped: " + resp);
					text.append("\n");
					button.setEnabled(true);
					if (resp) {
						currentConnection = null;
					}
				}
			});
		}

		@Override
		protected void onCancelled() {
			disconnectTask = null;
		}

	}

}
