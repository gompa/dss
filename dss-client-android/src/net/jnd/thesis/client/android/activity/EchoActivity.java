package net.jnd.thesis.client.android.activity;

import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.exception.WSException;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author João Domingos
 * @since Jan 19, 2013
 */
public class EchoActivity extends Activity {

	private EchoTask echoTask;

	private EditText echoInput;
	private TextView echoOutput;
	private Button echoButton;

	private View echoProgressContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_echo);
		echoInput = (EditText) findViewById(R.id.echoInput);
		echoOutput = (TextView) findViewById(R.id.echoOutput);
		echoOutput.setEnabled(false);
		echoButton = (Button) findViewById(R.id.echoButton);
		echoProgressContainer = findViewById(R.id.echo_progress);
		echoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				boolean cancel = false;
				View focusView = null;
				String echoText = echoInput.getText().toString();
				if (TextUtils.isEmpty(echoText)) {
					echoInput
							.setError(getString(R.string.error_field_required));
					focusView = echoInput;
					cancel = true;
				}
				// if ok proceed
				if (cancel) {
					focusView.requestFocus();
				} else {
					showProgress(true);
					echoTask = new EchoTask();
					echoTask.execute(echoText);
				}
			}
		});
	}

	/**
	 * @param view
	 */
	public void onClearButton(View view) {
		this.echoOutput.setText("");
	}

	/**
	 * @param view
	 */
	public void onExitButton(View view) {
		finish();
	}

	/**
	 * 
	 */
	public class EchoTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			String res = "";
			String echoText = params[0];
			try {
				res = ClientWebSocketContext.getServices().echo(echoText);
			} catch (WSException e) {
				Log.d(getClass().getName(), e.getMessage(), e);
			}
			return res;
		}

		@Override
		protected void onPostExecute(final String resp) {
			echoTask = null;
			showProgress(false);

			runOnUiThread(new Runnable() {
				public void run() {

					echoOutput.setText(resp);

				}
			});

		}

		@Override
		protected void onCancelled() {
			echoTask = null;
			showProgress(false);
		}

	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);
			echoProgressContainer.setVisibility(View.VISIBLE);
			echoProgressContainer.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							echoProgressContainer
									.setVisibility(show ? View.VISIBLE
											: View.GONE);
						}
					});
			//echoOutput.setVisibility(View.VISIBLE);
			/*echoOutput.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							echoOutput.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});*/
		} else {
			echoProgressContainer
					.setVisibility(show ? View.VISIBLE : View.GONE);
			//echoOutput.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

}
