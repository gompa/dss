package net.jnd.thesis.client.android.activity;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import net.jnd.thesis.common.interfaces.DSSBaseBean;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.exception.WSException;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * @author João Domingos
 * @since Feb 3, 2013
 */
public class ListsActivity extends Activity {

	private BackgroundTask backgroundTask;

	private static enum Listing {
		listDataSources, listDynamicReconfigurations, listEsperExpressions, listInteractionModels, listOwnedSessions, listSessionConditions, listSessions, listUserSessionConnections;

		/**
		 * @param listingName
		 * @return
		 */
		static Listing get(String listingName) {
			Listing res = null;
			for (Listing listing : values()) {
				if (listing.name().equals(listingName)) {
					res = listing;
					break;
				}
			}
			return res;
		}
	};

	private Button button;
	private Spinner spinner;
	private EditText text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lists);
		setupComponents();
	}

	/**
	 * @param view
	 */
	public void onClearButton(View view) {
		this.text.setText("");
	}
	
	/**
	 * @param view
	 */
	public void onExitButton(View view) {
		finish();
	}
	
	/**
	 * 
	 */
	private void setupComponents() {
		this.button = (Button) findViewById(R.id.listsButton);

		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String listingName = String.valueOf(spinner.getSelectedItem());
				boolean cancel = false;
				View focusView = null;
				if (TextUtils.isEmpty(listingName)) {
					button.setError(getString(R.string.error_field_required));
					focusView = spinner;
					cancel = true;
				}
				// if ok proceed
				if (cancel) {
					focusView.requestFocus();
				} else {
					backgroundTask = new BackgroundTask();
					backgroundTask.execute(Listing.get(listingName));
				}
			}
		});

		this.text = (EditText) findViewById(R.id.listsText);
		this.text.setText("");
		this.text.setEnabled(false);

		this.spinner = (Spinner) findViewById(R.id.listsSpinner);

		Set<String> spinnerItems = new TreeSet<String>();
		for (Listing listing : Listing.values()) {
			spinnerItems.add(listing.name());
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.spinner_item,
				new LinkedList<String>(spinnerItems)) {
		};
		this.spinner.setAdapter(adapter);

	}

	/**
	 * 
	 */
	public class BackgroundTask extends
			AsyncTask<Listing, Void, Collection<? extends DSSBaseBean>> {

		@Override
		protected Collection<? extends DSSBaseBean> doInBackground(
				Listing... params) {
			final Listing selectedListing = params[0];
			runOnUiThread(new Runnable() {
				public void run() {
					button.setEnabled(false);
					text.setText("fetching list \"" + selectedListing + "\"...");
				}
			});
			Collection<? extends DSSBaseBean> res = null;
			try {
				switch (selectedListing) {
				case listDataSources:
					res = ClientWebSocketContext.getServices()
							.listDataSources();
					break;
				case listDynamicReconfigurations:
					res = ClientWebSocketContext.getServices()
							.listDynamicReconfigurations();
					break;
				case listEsperExpressions:
					res = ClientWebSocketContext.getServices()
							.listEsperExpressions();
					break;
				case listInteractionModels:
					res = ClientWebSocketContext.getServices()
							.listInteractionModels();
					break;
				case listOwnedSessions:
					res = ClientWebSocketContext.getServices()
							.listOwnedSessions();
					break;
				case listSessionConditions:
					res = ClientWebSocketContext.getServices()
							.listSessionConditions();
					break;
				case listSessions:
					res = ClientWebSocketContext.getServices().listSessions();
					break;
				case listUserSessionConnections:
					res = ClientWebSocketContext.getServices()
							.listUserSessionConnections();
					break;
				default:
					break;
				}
			} catch (WSException e) {
				Log.d(getClass().getName(), e.getMessage(), e);
			}
			return res;
		}

		@Override
		protected void onPostExecute(
				final Collection<? extends DSSBaseBean> resp) {
			backgroundTask = null;
			runOnUiThread(new Runnable() {
				public void run() {
					text.setText("");
					for (DSSBaseBean baseBean : resp) {
						text.append(String.valueOf(baseBean));
						text.append("\n");
						button.setEnabled(true);
					}

				}
			});
		}

		@Override
		protected void onCancelled() {
			backgroundTask = null;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_lists, menu);
		return true;
	}

}
