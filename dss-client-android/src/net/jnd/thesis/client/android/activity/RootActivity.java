/**
 * 
 */
package net.jnd.thesis.client.android.activity;

import net.jnd.thesis.common.bean.InteractionModelBean;
import net.jnd.thesis.common.enums.Pattern;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.exception.WSException;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * @author João Domingos
 * @since Jan 29, 2013
 */
public class RootActivity extends Activity {

	protected enum ActivityResult {
		RESULT_CLOSE_ALL(1);

		private Integer code;

		private ActivityResult(Integer code) {
			this.code = code;
		}

		public static ActivityResult get(Integer code) {
			ActivityResult res = null;
			for (ActivityResult a : values()) {
				if (a.code == code) {
					res = a;
					break;
				}
			}
			return res;
		}

	}

	protected static final int LOW_BATTERY_THRESHOLD = 15;

	@Override
	public final void startActivityForResult(Intent intent, int requestCode) {
		super.startActivityForResult(intent, requestCode);
	}

	/**
	 * @param activity
	 * @param intent
	 * @param requestCode
	 */
	public static void startActivityForResult(Activity activity, Intent intent,
			ActivityResult requestCode) {
		activity.startActivityForResult(intent, requestCode.code);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		ActivityResult a = ActivityResult.get(resultCode);
		boolean execDefault = false;
		//
		if (a != null) {
			switch (a) {
			case RESULT_CLOSE_ALL:
				setResult(a.code);
				finish();
			default:
				execDefault = true;
			}
		}
		//
		if (execDefault) {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public final boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public final boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public final boolean onMenuOpened(int featureId, Menu menu) {
		// TODO Auto-generated method stub
		return super.onMenuOpened(featureId, menu);
	}

	@Override
	public final void onLowMemory() {
		// TODO Auto-generated method stub
		super.onLowMemory();
	}

	@Override
	public boolean onSearchRequested() {
		// TODO Auto-generated method stub
		return super.onSearchRequested();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	private void batteryLevelUpdate() {
		BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
			public	 void onReceive(Context context, Intent intent) {
				// get battery level
				int level = intent.getIntExtra("level", 0);
				// if the battery low
				if(level < LOW_BATTERY_THRESHOLD) {
					// get the current model
					InteractionModelBean currentModel = getCurrentInteractionModel();
					// modify the model to a PRODUCER-CONSUMER 
					// that consumes events evety 5 minutes
					currentModel.setPattern(Pattern.PRODUCER_CONSUMER);
					currentModel.setEventDelay(5L*60*1000);
					// update the interaction model object on server side
					try {
						ClientWebSocketContext.getServices().updateInteractionModel(currentModel);
					} catch (WSException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				context.unregisterReceiver(this);
				int rawlevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,
						-1);
				int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
				level = -1;
				if (rawlevel >= 0 && scale > 0) {
					level = (rawlevel * 100) / scale;
				}
				int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,
						-1);
				int status = intent
						.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
				int onplug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,
						-1);
				boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING
						|| status == BatteryManager.BATTERY_STATUS_FULL;
				boolean onUSB = onplug == BatteryManager.BATTERY_PLUGGED_USB;
				boolean onAC = onplug == BatteryManager.BATTERY_PLUGGED_AC;
			}

			private InteractionModelBean getCurrentInteractionModel() {
				// TODO Auto-generated method stub
				return null;
			}

			private InteractionModelBean getInteractionModel(String string) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		IntentFilter batteryLevelFilter = new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED);
		registerReceiver(batteryLevelReceiver, batteryLevelFilter);
	}

}
