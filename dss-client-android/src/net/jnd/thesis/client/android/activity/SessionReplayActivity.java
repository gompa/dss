package net.jnd.thesis.client.android.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

/**
 * @author João Domingos
 * @since Feb 3, 2013
 */
public class SessionReplayActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_session_replay);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_session_replay, menu);
		return true;
	}

}
