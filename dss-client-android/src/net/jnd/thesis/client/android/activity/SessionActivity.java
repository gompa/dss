package net.jnd.thesis.client.android.activity;

import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.SessionEventHandler;
import net.jnd.thesis.ws.services.exception.WSException;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * @author João Domingos
 * @since Feb 3, 2013
 */
public class SessionActivity extends Activity {

	private BackgroundTask backgroundTask;

	private DisconnectTask disconnectTask;

	private DSSUserSessionConnection currentConnection;

	private Map<String, DSSUserSessionConnection> connectionsByName = new TreeMap<String, DSSUserSessionConnection>();

	private Map<Long, DSSUserSessionConnection> connectionsById = new TreeMap<Long, DSSUserSessionConnection>();

	private ToggleButton button;
	private Spinner spinner;
	private EditText text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cleanup();
		setContentView(R.layout.activity_session);
		setupComponents();
		ClientWebSocketContext
				.setEventHandler(new SessionActivityEventsHandler(text));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_session, menu);
		return true;
	}

	@Override
	public void finish() {
		super.finish();
		new DisconnectTask().execute(currentConnection);
	}

	/**
	 * 
	 */
	private void cleanup() {
		this.backgroundTask = null;
		this.disconnectTask = null;
		this.currentConnection = null;
		this.connectionsById.clear();
		this.connectionsByName.clear();
	}

	/**
	 * 
	 */
	private void setupComponents() {
		this.button = (ToggleButton) findViewById(R.id.sessionButton);
		this.text = (EditText) findViewById(R.id.sessionText);
		this.text.setEnabled(false);
		this.spinner = (Spinner) findViewById(R.id.sessionSpinner);
		populate();
	}

	/**
	 * 
	 */
	private void populate() {
		populateConnections();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.spinner_item,
				new LinkedList<String>(connectionsByName.keySet())) {
		};
		this.spinner.setAdapter(adapter);
	}

	/**
	 * 
	 */
	private void populateConnections() {
		try {
			for (DSSUserSessionConnection bean : ClientWebSocketContext
					.getServices().listUserSessionConnections()) {
				connectionsById.put(bean.getId(), bean);
				connectionsByName.put(bean.getDescription(), bean);
			}
		} catch (WSException e) {
			Log.e(getClass().getName(), e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	private class SessionActivityEventsHandler implements SessionEventHandler {

		private TextView textView;

		/**
		 * @param textView
		 */
		SessionActivityEventsHandler(TextView textView) {
			this.textView = textView;
		}

		@Override
		public void handleEvent(final EventBean evt) {
			runOnUiThread(new Runnable() {
				public void run() {
					textView.append(String.valueOf(evt) + "\n");
				}
			});
		}

	}

	/**
	 * @param view
	 */
	public void onClearButton(View view) {
		this.text.setText("");
	}

	/**
	 * @param view
	 */
	public void onExitButton(View view) {
		finish();
	}

	/**
	 * @param view
	 */
	public void onToggleClicked(View view) {
		boolean on = ((ToggleButton) view).isChecked();
		if (on) {
			String selected = String.valueOf(spinner.getSelectedItem());
			boolean cancel = false;
			View focusView = null;
			if (TextUtils.isEmpty(selected)) {
				button.setError(getString(R.string.error_field_required));
				focusView = spinner;
				cancel = true;
			}
			// if ok proceed
			if (cancel) {
				focusView.requestFocus();
			} else {
				button.setEnabled(false);
				backgroundTask = new BackgroundTask();
				backgroundTask.execute(connectionsByName.get(selected));
			}
		} else {
			if (currentConnection != null) {
				button.setEnabled(false);
				disconnectTask = new DisconnectTask();
				disconnectTask.execute(currentConnection);
			}
		}
	}

	/**
	 * 
	 */
	public class BackgroundTask extends
			AsyncTask<DSSUserSessionConnection, Void, Boolean> {

		@Override
		protected Boolean doInBackground(DSSUserSessionConnection... params) {
			boolean res = true;
			try {
				if (ClientWebSocketContext.getServices().connectToSession(
						params[0])) {
					currentConnection = params[0];
				}
			} catch (WSException e) {
				Log.e(getClass().getName(), e.getMessage(), e);
				res = false;
			}
			return res;
		}

		@Override
		protected void onPostExecute(final Boolean resp) {
			backgroundTask = null;
			runOnUiThread(new Runnable() {
				public void run() {
					text.append("connected to session: " + resp);
					text.append("\n");
					button.setEnabled(true);
				}
			});
		}

		@Override
		protected void onCancelled() {
			backgroundTask = null;
		}

	}

	/**
	 * 
	 */
	public class DisconnectTask extends
			AsyncTask<DSSUserSessionConnection, Void, Boolean> {

		@Override
		protected Boolean doInBackground(DSSUserSessionConnection... params) {
			boolean res = true;
			if (params[0] != null) {
				try {
					res = ClientWebSocketContext.getServices()
							.disconnectFromSession(params[0].getId());
				} catch (WSException e) {
					Log.e(getClass().getName(), e.getMessage(), e);
					res = false;
				}
			}
			return res;
		}

		@Override
		protected void onPostExecute(final Boolean resp) {
			disconnectTask = null;
			runOnUiThread(new Runnable() {
				public void run() {
					text.append("disconnected from session: " + resp);
					text.append("\n");
					button.setEnabled(true);
					if (resp) {
						currentConnection = null;
					}
				}
			});
		}

		@Override
		protected void onCancelled() {
			disconnectTask = null;
		}

	}

}
