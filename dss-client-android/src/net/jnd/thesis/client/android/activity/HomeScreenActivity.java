package net.jnd.thesis.client.android.activity;

import net.jnd.thesis.client.android.helper.MenuHelper;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * @author João Domingos
 * @since Feb 3, 2013
 */
public class HomeScreenActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_home_screen);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, MenuHelper.getMenuKeys());
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		MenuHelper.handleClick(this, l, v, position, id);
	}

	@Override
	public void onBackPressed() {
		// do nothing, disabling back button
	}

}
