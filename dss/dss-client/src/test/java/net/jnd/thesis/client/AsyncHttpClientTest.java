/**
 * 
 */
package net.jnd.thesis.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.FluentCaseInsensitiveStringsMap;
import com.ning.http.client.HttpResponseBodyPart;
import com.ning.http.client.HttpResponseHeaders;
import com.ning.http.client.HttpResponseStatus;
import com.ning.http.client.Response;
import com.ning.http.client.websocket.WebSocket;
import com.ning.http.client.websocket.WebSocketTextListener;
import com.ning.http.client.websocket.WebSocketUpgradeHandler;

/**
 * @author João Domingos
 * @since Dec 3, 2012
 */
public class AsyncHttpClientTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AsyncHttpClientTest test = new AsyncHttpClientTest();
		test.asyncHttpTest();
	}

	/**
	 * Async Http Client library purpose is to allow Java applications to easily
	 * execute HTTP requests and asynchronously process the HTTP responses. The
	 * library also supports the WebSocket Protocol.
	 */
	public void asyncHttpTest() {
		try {
			basicSyncTest();
			asyncTest();
			mixedApproach();
			fullLifeCycleTest();
			websocketTest();
			customWebsocketTest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * 
	 */
	private void customWebsocketTest() throws InterruptedException,
			ExecutionException, IOException {
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		WebSocket websocket = asyncHttpClient
				.prepareGet("ws://echo.websocket.org")
				.execute(
						new WebSocketUpgradeHandler.Builder()
								.addWebSocketListener(
										new WebSocketTextListener() {

											@Override
											public void onOpen(
													WebSocket websocket) {
												System.out.println(websocket
														.toString());
											}

											@Override
											public void onClose(
													WebSocket websocket) {
												System.out.println(websocket
														.toString());
											}

											@Override
											public void onError(Throwable t) {
												System.out.println(t
														.getMessage());
											}

											@Override
											public void onMessage(String message) {
												System.out.println(message);
											}

											@Override
											public void onFragment(
													String fragment,
													boolean last) {
												System.out.println(fragment);
												System.out.println(last);
											}
										}).build()).get();

		websocket.sendMessage("trecos pecos!".getBytes("UTF-8"));
	}

	/**
	 * you can also configure the AsyncHttpClient via it's AsyncHttpClientConfig
	 * object:
	 * 
	 * Async Http Client also support WebSocket by simply doing:
	 * 
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	private void websocketTest() throws InterruptedException,
			ExecutionException, IOException {
		AsyncHttpClient c = new AsyncHttpClient();
		WebSocket websocket = c
				.prepareGet("ws://echo.websocket.org")
				.execute(
						new WebSocketUpgradeHandler.Builder()
								.addWebSocketListener(
										new WebSocketTextListener() {

											@Override
											public void onOpen(
													WebSocket websocket) {
												System.out.println(websocket
														.toString());
											}

											@Override
											public void onClose(
													WebSocket websocket) {
												System.out.println(websocket
														.toString());
											}

											@Override
											public void onError(Throwable t) {
												System.out.println(t
														.getMessage());
											}

											@Override
											public void onMessage(String message) {
												System.out.println(message);
											}

											@Override
											public void onFragment(
													String fragment,
													boolean last) {
												System.out.println(fragment);
												System.out.println(last);
											}
										}).build()).get();
		System.out.println(websocket);
	}

	/**
	 * You have full control on the Response life cycle, so you can decide at
	 * any moment to stop processing what the server is sending back:
	 * 
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * 
	 */
	private void fullLifeCycleTest() throws IOException, InterruptedException,
			ExecutionException {
		AsyncHttpClient c = new AsyncHttpClient();
		Future<String> f = c.prepareGet("http://www.ning.com/").execute(
				new AsyncHandler<String>() {
					private ByteArrayOutputStream bytes = new ByteArrayOutputStream();

					@Override
					public STATE onStatusReceived(HttpResponseStatus status)
							throws Exception {
						int statusCode = status.getStatusCode();
						// The Status have been read
						// If you don't want to read the headers,body or stop
						// processing the response
						if (statusCode >= 500) {
							return STATE.ABORT;
						}
						return STATE.CONTINUE;
					}

					@Override
					public STATE onHeadersReceived(HttpResponseHeaders h)
							throws Exception {
						FluentCaseInsensitiveStringsMap headers = h
								.getHeaders();
						System.out.println(headers);
						// The headers have been read
						// If you don't want to read the body, or stop
						// processing the response
						return STATE.ABORT;
					}

					@Override
					public STATE onBodyPartReceived(
							HttpResponseBodyPart bodyPart) throws Exception {
						bytes.write(bodyPart.getBodyPartBytes());
						return STATE.CONTINUE;
					}

					@Override
					public String onCompleted() throws Exception {
						// Will be invoked once the response has been fully read
						// or a ResponseComplete exception
						// has been thrown.
						// NOTE: should probably use Content-Encoding from
						// headers
						return bytes.toString("UTF-8");
					}

					@Override
					public void onThrowable(Throwable t) {
					}
				});

		String bodyResponse = f.get();
		System.out.println(bodyResponse);
	}

	/**
	 * You can also mix Future with AsyncHandler to only retrieve part of the
	 * asynchronous response
	 * 
	 * which is something you want to do for large responses: this way you can
	 * process content as soon as it becomes available, piece by piece, without
	 * having to buffer it all in memory.
	 * 
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	private void mixedApproach() throws IOException, InterruptedException,
			ExecutionException {
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		Future<Integer> f = asyncHttpClient.prepareGet("http://www.ning.com/")
				.execute(new AsyncCompletionHandler<Integer>() {

					@Override
					public Integer onCompleted(Response response)
							throws Exception {
						// Do something with the Response
						return response.getStatusCode();
					}

					@Override
					public void onThrowable(Throwable t) {
						// Something wrong happened.
					}
				});
		int statusCode = f.get();
		System.out.println(statusCode);
	}

	/**
	 * You can also accomplish asynchronous (non-blocking) operation without
	 * using a Future if you want to receive and process the response in your
	 * handler:
	 * 
	 * (this will also fully read Response in memory before calling onCompleted)
	 * 
	 * @throws IOException
	 */
	private void asyncTest() throws IOException {
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		asyncHttpClient.prepareGet("http://www.ning.com/").execute(
				new AsyncCompletionHandler<Response>() {

					@Override
					public Response onCompleted(Response response)
							throws Exception {
						// Do something with the Response
						// ...
						return response;
					}

					@Override
					public void onThrowable(Throwable t) {
						// Something wrong happened.
						System.out.println("fck...");
					}
				});
	}

	/**
	 * Note that in this case all the content must be read fully in memory, even
	 * if you used getResponseBodyAsStream()' method on returnedResponse`
	 * object.
	 * 
	 * @throws IOException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	private void basicSyncTest() throws IOException, InterruptedException,
			ExecutionException {
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		Future<Response> f = asyncHttpClient.prepareGet("http://www.ning.com/")
				.execute();
		Response r = f.get();
		System.out.println(r);
	}

}
