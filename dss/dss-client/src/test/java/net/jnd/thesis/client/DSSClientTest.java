/**
 *	@author João Domingos
 *	@author Pedro Martins
 *	@updated Nov 24, 2013
 *	@version 1.1
 *
 *	This example tests the client connection to the server. To run it, you must 
 *	first launch the server as indicated in the README.txt file. 
 *
 *	This test allows you to test the authentication, print the session 
 *	connections together with their conditions and allows you to make a session
 *	test using XMPP.
 *
 */
package net.jnd.thesis.client;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import net.jnd.thesis.common.bean.AuthenticationBean;
import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.bean.WSConnectionBean;
import net.jnd.thesis.common.enums.DataSourceType;
import net.jnd.thesis.common.enums.Pattern;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.DefaultSessionEventHandler;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

public class DSSClientTest {
	
	/*
	 * Variables 
	 */

	public static final String CONNECTION_URL ="ws://localhost:8080/dss-server/ws/client";
	
	private static final String USERNAME = "admin";
	private static final String PASSWORD = "admin_pwd";
	
	protected static WebSocketServices services;
	protected Collection<DSSDataSource> dataSources;
	protected Collection<DSSSessionCondition> sessionConditions;
	protected Collection<DSSInteractionModel> interactionModels;
	protected Collection<DSSDynamicReconfiguration> dynamicReconfigurations;
	protected Collection<DSSSession> sessions;
	protected Collection<DSSSession> ownedSessions;
	protected DSSSession singleSession;
	protected DSSSession testSession;

	private DSSUserSessionConnection testConnection;
	private Collection<DSSUserSessionConnection> userSessionConnections;

	/*
	 * Misc
	 */
	
	/**
	 * Allows you to test authentication and sessions. Uncomment the desired 
	 * methods to use.
	 * 
	 * @throws Exception 
	 * */
	public static void main(String[] args) throws Exception {
		DSSClientTest client = new DSSClientTest();
		client.connectAndAuthenticate();
		client.basicListings();
		client.basicSessionTests();
		System.exit(0);
	}

	/**
	 * Tests the user authentication by using the Administrator account.
	 * 
	 * @throws WSException
	 */
	private void connectAndAuthenticate() throws WSException {
		AuthenticationBean auth = new AuthenticationBean(USERNAME, PASSWORD);
		WSConnectionBean wsConn = new WSConnectionBean();
		wsConn.setUrl(CONNECTION_URL);
		
		assertTrue(ClientWebSocketContext.connect(wsConn));
		assertNotNull(services = ClientWebSocketContext.getServices());
		/*
		 * String echoReq = "Hello world!";
		 * assertTrue(echoReq.equals(services.echo(echoReq))); // list data
		 * sources without authentication try { services.listDataSources(); }
		 * catch (WSException e) { assertTrue(e instanceof
		 * UserNotAuthenticatedException); }
		 */
		services.authenticate(auth);
		assertTrue(services.isAuthenticated());
		
		// set custom event handler
		ClientWebSocketContext
				.setEventHandler(new DefaultSessionEventHandler() {

					@Override
					public void handleEvent(EventBean evt) {
						System.out.print("CUSTOM EVENT HANDLER!!!: ");
						super.handleEvent(evt);
					}

				});
	}
	
	
	/**
	 * Tests a basic XMPP source session. If there is no XMPP session, an error 
	 * occurs. 
	 * 
	 * @throws WSException
	 * @throws InterruptedException
	 * 
	 */
	private void basicSessionTests() throws WSException, InterruptedException {
		ownedSessions = services.listOwnedSessions();
		assertTrue(!ownedSessions.isEmpty());
		singleSession = services.getSession(ownedSessions.iterator().next()
				.getId());
		assertNotNull(singleSession);
		// build data source
		DSSDataSource xmppDataSource = getXmppDataSource();
		assertNotNull(xmppDataSource);
		// build interaction model
		DSSInteractionModel model = getStreamingInteractionModel();
		assertNotNull(model);
		try {
			// start test session
			DSSUserSessionConnection connection = services
					.startTestDataSourceSession(xmppDataSource.getId(), model.getId());
			testSession = services.getSession(connection.getConnectionSession()
					.getId());
			testConnection = services.getConnection(connection.getId());
			// wait 1 minute
			Thread.sleep(180000);
			assertTrue(connection != null);
			assertTrue(connection.getId() > 0);
			assertTrue(connection.getConnectionSession() != null);
			assertTrue(connection.getConnectionSession().getId() > 0);
			assertNotNull(testSession);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Boolean connectionStopped = false;
			Boolean sessionStopped = false;
			// stop test session
			try {
				connectionStopped = services
						.disconnectFromSession(testConnection.getId());

			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				sessionStopped = services.stopSession(testSession.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			assertTrue(connectionStopped);
			assertTrue(sessionStopped);
		}
	}

	/**
	 * Prints the user sessions, conditions and additional information.
	 * 
	 * @throws WSException
	 */
	private void basicListings() throws WSException {
		for (DSSDataSource bean : dataSources = services.listDataSources()) {
			System.out.println(bean);
		}
		assertNotNull(dataSources);
		for (DSSSessionCondition bean : sessionConditions = services
				.listSessionConditions()) {
			System.out.println(bean);
		}
		assertNotNull(sessionConditions);
		for (DSSInteractionModel bean : interactionModels = services
				.listInteractionModels()) {
			System.out.println(bean);
		}
		assertNotNull(interactionModels);
		for (DSSDynamicReconfiguration bean : dynamicReconfigurations = services
				.listDynamicReconfigurations()) {
			System.out.println(bean);
		}
		assertNotNull(dynamicReconfigurations);
		for (DSSSession bean : sessions = services.listSessions()) {
			System.out.println(bean);
		}
		assertNotNull(sessions);
		for (DSSUserSessionConnection bean : userSessionConnections = services
				.listUserSessionConnections()) {
			System.out.println(bean);
		}
		assertNotNull(dynamicReconfigurations);
	}

	/*
	 * Private helper Methods
	 */
	
	private DSSDataSource getXmppDataSource() {
		DSSDataSource res = null;
		for (DSSDataSource ds : dataSources) {
			if (DataSourceType.XMPP.equals(ds.getType())) {
				res = ds;
				break;
			}
		}
		return res;
	}

	private DSSInteractionModel getStreamingInteractionModel() {
		DSSInteractionModel model = null;
		for (DSSInteractionModel m : interactionModels) {
			if (Pattern.STREAMING.equals(m.getPattern())) {
				model = m;
				break;
			}
		}
		return model;
	}

}
