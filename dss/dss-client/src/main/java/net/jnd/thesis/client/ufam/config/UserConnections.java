/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.DynamicReconfigurations.Reconfiguration;
import net.jnd.thesis.client.ufam.config.EsperExpressions.Expression;
import net.jnd.thesis.client.ufam.config.InteractionModels.Model;
import net.jnd.thesis.client.ufam.config.Sessions.Session;
import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.UserSessionConnectionBean;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class UserConnections extends
		AbstractData<User, UserSessionConnectionBean> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = false;

		for (User usr : User.values()) {
			try {
				UserSessionConnectionBean tmp;
				switch (usr) {
				case LOCAL_AUTHORITY:
				case ADMIN:
					tmp = buildDefaultBean(usr);
					res &= buildConnection(usr, tmp);
					break;
				case FIRE_DEPARTMENT:
					tmp = buildDefaultBean(usr);
					Collection<DSSDynamicReconfiguration> reconfs = new ArrayList<DSSDynamicReconfiguration>();
					reconfs.add(Data.reconfs.get(
							Reconfiguration.ALERT_FIRE_DEP,
							DSSDynamicReconfiguration.class));
					tmp.setDynamicReconfs(reconfs);
					res &= buildConnection(usr, tmp);
					break;
				case EMERGENCY_TEAM:
					tmp = buildDefaultBean(usr);
					res &= buildConnection(usr, tmp);
					break;
				case FIREMAN:
					tmp = buildDefaultBean(usr);
					res &= buildConnection(usr, tmp);
					break;
				case FLOOD_SIMULATION:
					tmp = buildDefaultBean(usr);
					res &= buildConnection(usr, tmp);
					break;
				default:
					break;
				}

			} catch (WSException e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	/**
	 * @param usr
	 * @return
	 */
	private UserSessionConnectionBean buildDefaultBean(User usr) {
		UserSessionConnectionBean tmp;
		tmp = new UserSessionConnectionBean();
		tmp.setConnectionEsperExpression(Data.expressions
				.get(Expression.PASS_THROUGH));
		tmp.setConnectionInteractionModel(Data.models.get(Model.STREAM_ALL));
		tmp.setConnectionSession(Data.sessions
				.get(Session.URBAN_FLOODING_ANALYSIS_AND_MODELLING));
		tmp.setDescription(usr.name());
		return tmp;
	}

	/**
	 * @param usr
	 * @param tmp
	 * @return
	 * @throws WSException
	 */
	private boolean buildConnection(User usr, UserSessionConnectionBean tmp)
			throws WSException {
		boolean res;
		Users.authenticate(services, usr);
		//
		Map<String, UserSessionConnectionBean> existing = new HashMap<String, UserSessionConnectionBean>();
		for (DSSUserSessionConnection e : services.listUserSessionConnections()) {
			existing.put(e.getDescription(), (UserSessionConnectionBean) e);
		}
		//
		UserSessionConnectionBean current = existing.get(usr.name());
		if (current != null) {
			data.put(usr, current);
			res = true;
		} else {
			current = tmp;
			services.createUserSessionConnection(current);
			if (current.getId() == null) {
				res = false;
			} else {
				data.put(usr, current);
				res = true;
			}
		}
		return res;
	}
}
