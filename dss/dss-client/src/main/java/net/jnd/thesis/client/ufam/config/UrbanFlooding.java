/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.WSConnectionBean;
import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class UrbanFlooding implements Runnable, Initializable {

	/**
	 * cloud: "ws://ec2-46-51-142-33.eu-west-1.compute.amazonaws.com/dss-server/ws/client"
	 * default: "ws://vw-l-u6:8080/dss-server/ws/client"
	 */
	private static String url;

	private WebSocketServices services;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args == null || args.length != 1) {
			throw new IllegalArgumentException(
					"need to specify 1 parameter: url...");
		} else {
			url = args[0];
		}
		Thread t = new Thread(new UrbanFlooding());
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		init();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.Initializable#init()
	 */
	@Override
	public boolean init() {
		try {
			return config();
		} catch (WSException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @return
	 * @throws WSException
	 */
	private boolean config() throws WSException {
		boolean res = connect();
		res = res && Users.authenticate(services, User.ADMIN);
		res = res && Data.initialize();
		return res;
	}

	/**
	 * @return
	 * @throws WSException
	 */
	private boolean connect() throws WSException {
		WSConnectionBean wsConn = new WSConnectionBean();
		wsConn.setUrl(url);
		wsConn.setTimeout(30000L);
		if (ClientWebSocketContext.connect(wsConn)) {
			this.services = ClientWebSocketContext.getServices();
		}
		String echoReq = "Hello world!";
		return services != null && echoReq.equals(services.echo(echoReq));
	}

}
