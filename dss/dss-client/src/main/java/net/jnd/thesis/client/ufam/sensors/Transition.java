/**
 * 
 */
package net.jnd.thesis.client.ufam.sensors;

/**
 * @author João Domingos
 * @since Jun 2, 2013
 */
public class Transition {

	private long offset;

	private Config config;

	/**
	 * @param offset
	 * @param config
	 */
	public Transition(long offset, Config config) {
		super();
		this.offset = offset;
		this.config = config;
	}

	/**
	 * @param offset
	 * @param config
	 * @return
	 */
	public static Transition get(long offset, Config config) {
		return new Transition(offset, config);
	}

	/**
	 * @return the offset
	 */
	public long getOffset() {
		return offset;
	}

	/**
	 * @param offset
	 *            the offset to set
	 */
	public void setOffset(long offset) {
		this.offset = offset;
	}

	/**
	 * @return the config
	 */
	public Config getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(Config config) {
		this.config = config;
	}

}
