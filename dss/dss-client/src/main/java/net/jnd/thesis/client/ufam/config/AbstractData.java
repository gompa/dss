/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.ws.services.ClientWebSocketContext;
import net.jnd.thesis.ws.services.WebSocketServices;

/**
 * @author João Domingos
 * @since Apr 12, 2013
 */
public abstract class AbstractData<K, V> implements Initializable,
		GettableMap<K, V> {

	protected WebSocketServices services;

	protected Map<K, V> data = new HashMap<K, V>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.Initializable#init()
	 */
	@Override
	public final boolean init() {
		this.services = ClientWebSocketContext.getServices();
		return this.services != null && initCustom();
	}

	/**
	 * @return
	 */
	protected abstract boolean initCustom();

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.GettableMap#get(java.lang.Object)
	 */
	@Override
	public V get(K key) {
		return data.get(key);
	}

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	<T> Collection<T> getAll(Class<T> clazz, K... exclusions) {
		Collection<K> exc = new ArrayList<K>();
		if (exclusions != null) {
			exc.addAll(Arrays.asList(exclusions));
		}
		//
		Collection<T> res = new ArrayList<T>();
		for (Map.Entry<K, V> entry : data.entrySet()) {
			if (!exc.contains(entry.getKey())) {
				res.add((T) entry.getValue());
			}
		}
		return res;
	}

	/**
	 * @return
	 */
	<T> Collection<T> getAll(Class<T> clazz) {
		return getAll(clazz, (K[]) null);
	}

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	<T> T get(K key, Class<T> clazz) {
		return (T) get(key);
	}

}
