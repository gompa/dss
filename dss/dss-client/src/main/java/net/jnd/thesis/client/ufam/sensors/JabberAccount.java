/**
 * 
 */
package net.jnd.thesis.client.ufam.sensors;


/**
 * @author João Domingos
 * @since Jun 2, 2013
 */

public enum JabberAccount {
	//INFO: here we change the accounts of who is receiving the sensor information and who is sending it
//	SENSORS(JabberServer.GTALK, "admin", "admin"),
	SENSORS(JabberServer.GTALK, "thesis.dimfccs.sensors@gmail.com", "thesis.dimfccs.sensors.pwd"),
	DIMFCCS(JabberServer.GTALK, "thesis.dimfccs@gmail.com", "thesis.dimfccs.pwd");

	private JabberServer server;

	private String user;

	private String pwd;

	/**
	 * @param user
	 * @param pwd
	 */
	private JabberAccount(JabberServer server, String user, String pwd) {
		this.server = server;
		this.user = user;
		this.pwd = pwd;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @return the pwd
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * @return the server
	 */
	public JabberServer getServer() {
		return server;
	}

}
