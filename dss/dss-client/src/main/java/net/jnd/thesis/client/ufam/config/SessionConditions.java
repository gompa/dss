/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.DataSources.DataSource;
import net.jnd.thesis.client.ufam.config.DataSources.Xmpp;
import net.jnd.thesis.client.ufam.config.SessionConditions.Condition;
import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.SessionConditionBean;
import net.jnd.thesis.common.enums.ConditionOperator;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class SessionConditions extends
		AbstractData<Condition, SessionConditionBean> {

	enum Condition {
		HUMIDITY_NORMAL_SCENARIO(User.LOCAL_AUTHORITY, Xmpp.HUMIDITY,
				ConditionOperator.GREATER, 60), HUMIDITY_ABOVE_NORMAL(
				User.LOCAL_AUTHORITY, Xmpp.HUMIDITY, ConditionOperator.GREATER,
				70), HUMIDITY_FIRE_DEPARTMENT(User.FIRE_DEPARTMENT,
				Xmpp.HUMIDITY, ConditionOperator.GREATER, 65), PRECIPITATION_EMERGENCY(
				User.LOCAL_AUTHORITY, Xmpp.PRECIPITATION,
				ConditionOperator.GREATER, 130), GROUND_WATER_LEVEL_DISASTER(
				User.LOCAL_AUTHORITY, Xmpp.GROUND_WATER_LEVEL,
				ConditionOperator.GREATER, 200), GROUND_WATER_LEVEL_LOW(
				User.LOCAL_AUTHORITY, Xmpp.GROUND_WATER_LEVEL,
				ConditionOperator.SMALLER, 50), NEAR_ZERO_PRECIPITATION(
				User.LOCAL_AUTHORITY, Xmpp.PRECIPITATION,
				ConditionOperator.SMALLER, 5);

		private DataSource ds;

		private ConditionOperator op;

		private String val;

		private User usr;

		Condition(User usr, DataSource ds, ConditionOperator op, Integer val) {
			this.usr = usr;
			this.ds = ds;
			this.op = op;
			this.val = String.valueOf(val);
		}

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public SessionConditionBean create(WebSocketServices services)
				throws WSException {
			SessionConditionBean b = new SessionConditionBean();
			b.setDescription(name());
			b.setConditionOperator(op);
			b.setConditionValue(val);
			b.setDssDataSource(Data.dataSources.get(ds));
			//
			b.setId(services.createSessionCondition(b));
			return b;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = true;
		try {
			for (Condition required : Condition.values()) {
				if (required.usr == null) {
					for (User usr : User.values()) {
						res &= create(required, usr);
					}
				} else {
					res &= create(required, required.usr);
				}
			}
		} catch (WSException e) {
			res = false;
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * @param required
	 * @param usr
	 * @return
	 * @throws WSException
	 */
	private boolean create(Condition required, User usr) throws WSException {
		boolean res = false;
		Users.authenticate(services, usr);
		//
		Map<String, SessionConditionBean> existing = new HashMap<String, SessionConditionBean>();
		for (DSSSessionCondition e : services.listSessionConditions()) {
			existing.put(e.getDescription(), (SessionConditionBean) e);
		}
		//
		SessionConditionBean current = existing.get(required.name());
		if (current != null) {
			data.put(required, current);
			res = true;
		} else {
			current = required.create(services);
			if (current.getId() == null) {
				res = false;
			} else {
				data.put(required, current);
				res = true;
			}
		}
		return res;
	}

}
