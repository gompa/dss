/**
 * 
 */
package net.jnd.thesis.client.ufam.sensors;

/**
 * @author João Domingos
 * @since Jun 2, 2013
 */
public class Config {

	private long interval = 5 * 1000;
	private int min;
	private int max;

	/**
	 * @param interval
	 * @param min
	 * @param max
	 */
	public Config(long interval, int min, int max) {
		this.interval = interval;
		this.min = min;
		this.max = max;
	}

	/**
	 * @param min
	 * @param max
	 */
	public Config(int min, int max) {
		this.min = min;
		this.max = max;
	}

	/**
	 * @param min
	 * @param max
	 */
	public static Config get(int min, int max) {
		return new Config(min, max);
	}

	/**
	 * @return the interval
	 */
	public long getInterval() {
		return interval;
	}

	/**
	 * @param interval
	 *            the interval to set
	 */
	public void setInterval(long interval) {
		this.interval = interval;
	}

	/**
	 * @return the min
	 */
	public int getMin() {
		return min;
	}

	/**
	 * @param min
	 *            the min to set
	 */
	public void setMin(int min) {
		this.min = min;
	}

	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * @param max
	 *            the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	@Override
	public String toString() {
		return String.format("(%s -> %s)", min, max);
	}

}
