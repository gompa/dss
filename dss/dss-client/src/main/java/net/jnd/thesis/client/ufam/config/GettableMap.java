/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public interface GettableMap<K, V> {

	V get(K key);

}
