/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.DataSources.DataSource;
import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.DataSourceBean;
import net.jnd.thesis.common.bean.DataSourceBeanEventTwitter;
import net.jnd.thesis.common.bean.DataSourceBeanEventXmpp;
import net.jnd.thesis.common.bean.DataSourceBeanPollingRss;
import net.jnd.thesis.common.enums.DataSourceType;
import net.jnd.thesis.common.enums.DataSourceValueType;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class DataSources extends AbstractData<DataSource, DataSourceBean> {

	interface DataSource {

		String name();

		DataSourceBean create(WebSocketServices services) throws WSException;

	}

	public enum Xmpp implements DataSource {
		HUMIDITY, PRECIPITATION, WIND_SPEED, WIND_DIRECTION, GROUND_WATER_LEVEL, DAM_WATER_LEVEL, RIVER_WATER_LEVEL;

		//INFO: user and pwd on datasource client
//		private String usr = "thesis.dimfccs@gmail.com";
		private String usr = "thesis.dimfccs";
		private String pwd = "thesis.dimfccs.pwd";

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public DataSourceBean create(WebSocketServices services)
				throws WSException {
			DataSourceBeanEventXmpp b = new DataSourceBeanEventXmpp();
			b.setType(DataSourceType.XMPP);
			b.setDataSourceValueType(DataSourceValueType.NUMERIC);
			b.setName(name());
			
			b.setPassword(pwd);
			
			b.setRegex(getSanitizedName() + "\\d*");
			
			b.setUsername(usr);
			
			b.setId(services.createDataSource(b));
			return b;
		}

		/**
		 * @return
		 */
		public String getSanitizedName() {
			return name().toLowerCase() + ":";
		}

	}

	enum Twitter implements DataSource {
		FIELD_INFORMATION;

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public DataSourceBean create(WebSocketServices services)
				throws WSException {
			DataSourceBeanEventTwitter b = new DataSourceBeanEventTwitter();
			//
			b.setType(DataSourceType.TWITTER);
			b.setDataSourceValueType(DataSourceValueType.STRING);
			b.setName(name());
			//
			b.setScreenNames("thesisdimfccs");
			//
			b.setId(services.createDataSource(b));
			return b;
		}
	}

	enum Rss implements DataSource {
		LOCAL_NEWS;

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public DataSourceBean create(WebSocketServices services)
				throws WSException {
			DataSourceBeanPollingRss b = new DataSourceBeanPollingRss();
			//
			b.setType(DataSourceType.RSS);
			b.setDataSourceValueType(DataSourceValueType.STRING);
			b.setName(name());
			//
			b.setUrl("http://feeds.feedburner.com/PublicoRSS");
			b.setInterval(10000L);
			//
			b.setId(services.createDataSource(b));
			return b;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = false;
		try {
			Users.authenticate(services, User.LOCAL_AUTHORITY);
			Map<String, DataSourceBean> existing = new HashMap<String, DataSourceBean>();
			for (DSSDataSource e : services.listDataSources()) {
				existing.put(e.getName(), (DataSourceBean) e);
			}
			//
			Collection<DataSource> dataSources = new LinkedList<DataSource>();
			dataSources.addAll(Arrays.asList(Xmpp.values()));
			dataSources.addAll(Arrays.asList(Twitter.values()));
			dataSources.addAll(Arrays.asList(Rss.values()));
			//
			for (DataSource required : dataSources) {
				DataSourceBean current = existing.get(required.name());
				if (current != null) {
					data.put(required, current);
					res = true;
				} else {
					current = required.create(services);
					if (current.getId() == null) {
						res = false;
						break;
					} else {
						data.put(required, current);
						res = true;
					}
				}
			}
		} catch (WSException e) {
			res = false;
			e.printStackTrace();
		}
		return res;
	}
}
