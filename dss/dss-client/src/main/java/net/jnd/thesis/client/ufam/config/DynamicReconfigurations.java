/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.DynamicReconfigurations.Reconfiguration;
import net.jnd.thesis.client.ufam.config.EsperExpressions.Expression;
import net.jnd.thesis.client.ufam.config.InteractionModels.Model;
import net.jnd.thesis.client.ufam.config.SessionConditions.Condition;
import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.DynamicReconfigurationBean;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class DynamicReconfigurations extends
		AbstractData<Reconfiguration, DynamicReconfigurationBean> {

	enum Reconfiguration {

		ALERT(null, Condition.HUMIDITY_ABOVE_NORMAL, null,
				Expression.HUMIDITY_PRECIPITATION_WIND), 
		EMERGENCY(null,
				Condition.PRECIPITATION_EMERGENCY, null,
				Expression.HUMIDITY_PRECIPITATION_WIND_WATERLEVEL), 
		DISASTER(null,
				Condition.GROUND_WATER_LEVEL_DISASTER, null,
				Expression.HUMIDITY_PRECIPITATION_WATERLEVEL_TWITTER), 
		AFTERMATH(null,
				Condition.GROUND_WATER_LEVEL_LOW, null,
				Expression.PRECIPITATION_WATERLEVEL_TWITTER_LOCALNEWS), 
		NORMAL(null,
				Condition.NEAR_ZERO_PRECIPITATION,
				Model.HUMIDITY_NORMAL_SCENARIO, Expression.HUMIDITY), 
		ALERT_FIRE_DEP(
				Reconfiguration.ALERT, null, Model.STREAM_ALL,
				Expression.PASS_THROUGH);

		Condition condition;

		Model model;

		Expression exp;

		Reconfiguration reconf;

		private Reconfiguration(Reconfiguration reconf, Condition condition,
				Model model, Expression exp) {
			this.reconf = reconf;
			this.condition = condition;
			this.model = model;
			this.exp = exp;
		}

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public DynamicReconfigurationBean create(WebSocketServices services)
				throws WSException {
			DynamicReconfigurationBean b = new DynamicReconfigurationBean();
			b.setDescription(name());
			//
			if (reconf != null) {
				b.setEventNameEnabled(true);
				b.setEventName(reconf.name());
			}
			//
			if (condition != null) {
				b.setSessionConditionEnabled(true);
				b.setDssSessionCondition(Data.conditions.get(condition));
			}
			//
			if (model != null) {
				b.setInteractionModelEnabled(true);
				b.setDssInteractionModel(Data.models.get(model));
			}
			//
			if (exp != null) {
				b.setEsperExpressionEnabled(true);
				b.setDssEsperExpression(Data.expressions.get(exp));
			}
			//
			b.setId(services.createDynamicReconfiguration(b));
			return b;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = true;
		try {
			Users.authenticate(services, User.LOCAL_AUTHORITY);
			//
			Map<String, DynamicReconfigurationBean> existing = new HashMap<String, DynamicReconfigurationBean>();
			for (DSSDynamicReconfiguration e : services
					.listDynamicReconfigurations()) {
				existing.put(e.getDescription(), (DynamicReconfigurationBean) e);
			}
			//
			for (Reconfiguration required : Reconfiguration.values()) {
				DynamicReconfigurationBean current = existing.get(required
						.name());
				if (current != null) {
					data.put(required, current);
					res = true;
				} else {
					current = required.create(services);
					if (current.getId() == null) {
						res = false;
						break;
					} else {
						data.put(required, current);
						res = true;
					}
				}
			}
		} catch (WSException e) {
			res = false;
			e.printStackTrace();
		}
		return res;
	}

}
