/**
 * 
 */
package net.jnd.thesis.client.ufam.sensors;

import java.util.HashMap;
import java.util.Map;

/**
 * @author João Domingos
 * @since Jun 2, 2013
 */
public class SensorTransitions {

	private static final long NORMAL = 1;

	private static final long ALERT = 5;

	private static final long EMERGENCY = 10;

	private static final long DISASTER = 15;

	private static final long AFTERMATH = 20;

	private static final long RETURN_TO_NORMAL = 25;

	private static final Map<Sensor, Map<Long, Config>> transitions = new HashMap<Sensor, Map<Long, Config>>();

	static {
		transitions.put(Sensor.HUMIDITY, new HashMap<Long, Config>());
		transitions.put(Sensor.PRECIPITATION, new HashMap<Long, Config>());
		transitions.put(Sensor.WIND_SPEED, new HashMap<Long, Config>());
		transitions.put(Sensor.WIND_DIRECTION, new HashMap<Long, Config>());
		transitions.put(Sensor.GROUND_WATER_LEVEL, new HashMap<Long, Config>());
		transitions.put(Sensor.DAM_WATER_LEVEL, new HashMap<Long, Config>());
		transitions.put(Sensor.RIVER_WATER_LEVEL, new HashMap<Long, Config>());
	}

	// BAU
	static {
		transitions.get(Sensor.HUMIDITY).put(NORMAL, Config.get(61, 69)); // OK
		transitions.get(Sensor.PRECIPITATION).put(NORMAL, Config.get(121, 129)); // OK
		transitions.get(Sensor.WIND_SPEED).put(NORMAL, Config.get(11, 19)); // OK
		transitions.get(Sensor.WIND_DIRECTION).put(NORMAL, Config.get(91, 99)); // OK
		transitions.get(Sensor.GROUND_WATER_LEVEL).put(NORMAL,
				Config.get(55, 59)); // OK
		transitions.get(Sensor.DAM_WATER_LEVEL).put(NORMAL, Config.get(55, 59)); // OK
		transitions.get(Sensor.RIVER_WATER_LEVEL).put(NORMAL,
				Config.get(55, 59)); // OK
	}

	// HUMIDITY > 70
	static {
		transitions.get(Sensor.HUMIDITY).put(ALERT, Config.get(71, 79)); // OK
		transitions.get(Sensor.PRECIPITATION).put(ALERT, Config.get(120, 130)); // OK
		transitions.get(Sensor.WIND_SPEED).put(ALERT, Config.get(21, 29));
		transitions.get(Sensor.WIND_DIRECTION).put(ALERT, Config.get(101, 109)); // OK
		transitions.get(Sensor.GROUND_WATER_LEVEL).put(ALERT,
				Config.get(111, 131)); // OK
		transitions.get(Sensor.DAM_WATER_LEVEL).put(ALERT, Config.get(91, 99)); // OK
		transitions.get(Sensor.RIVER_WATER_LEVEL)
				.put(ALERT, Config.get(91, 99)); // OK
	}

	// PRECIPITATION > 130
	static {
		transitions.get(Sensor.HUMIDITY).put(EMERGENCY, Config.get(81, 89)); // OK
		transitions.get(Sensor.PRECIPITATION).put(EMERGENCY,
				Config.get(130, 199)); // OK
		transitions.get(Sensor.WIND_SPEED).put(EMERGENCY, Config.get(31, 49)); // OK
		transitions.get(Sensor.WIND_DIRECTION).put(EMERGENCY,
				Config.get(111, 119)); // OK
		transitions.get(Sensor.GROUND_WATER_LEVEL).put(EMERGENCY,
				Config.get(161, 189)); // OK
		transitions.get(Sensor.DAM_WATER_LEVEL).put(EMERGENCY,
				Config.get(101, 109)); // OK
		transitions.get(Sensor.RIVER_WATER_LEVEL).put(EMERGENCY,
				Config.get(101, 109)); // OK
	}

	// GROUND_WATER_LEVEL > 200
	static {
		transitions.get(Sensor.HUMIDITY).put(DISASTER, Config.get(91, 99)); // OK
		transitions.get(Sensor.PRECIPITATION).put(DISASTER,
				Config.get(201, 209)); // OK
		transitions.get(Sensor.WIND_SPEED).put(DISASTER, Config.get(51, 59)); // OK
		transitions.get(Sensor.WIND_DIRECTION).put(DISASTER,
				Config.get(121, 129)); // OK
		transitions.get(Sensor.GROUND_WATER_LEVEL).put(DISASTER,
				Config.get(201, 211)); // OK
		transitions.get(Sensor.DAM_WATER_LEVEL).put(DISASTER,
				Config.get(121, 129)); // OK
		transitions.get(Sensor.RIVER_WATER_LEVEL).put(DISASTER,
				Config.get(121, 129)); // OK
	}

	// GROUND_WATER_LEVEL < 50
	static {
		transitions.get(Sensor.HUMIDITY).put(AFTERMATH, Config.get(71, 79)); // OK
		transitions.get(Sensor.PRECIPITATION).put(AFTERMATH,
				Config.get(131, 169)); // OK
		transitions.get(Sensor.WIND_SPEED).put(AFTERMATH, Config.get(31, 39)); // OK
		transitions.get(Sensor.WIND_DIRECTION).put(AFTERMATH,
				Config.get(131, 139)); // OK
		transitions.get(Sensor.GROUND_WATER_LEVEL).put(AFTERMATH,
				Config.get(41, 49)); // OK
		transitions.get(Sensor.DAM_WATER_LEVEL).put(AFTERMATH,
				Config.get(91, 99)); // OK
		transitions.get(Sensor.RIVER_WATER_LEVEL).put(AFTERMATH,
				Config.get(91, 99)); // OK
	}

	// PRECIPITATION < 5
	static {
		transitions.get(Sensor.HUMIDITY).put(RETURN_TO_NORMAL,
				Config.get(61, 69)); // OK
		transitions.get(Sensor.PRECIPITATION).put(RETURN_TO_NORMAL,
				Config.get(1, 4)); // OK
		transitions.get(Sensor.WIND_SPEED).put(RETURN_TO_NORMAL,
				Config.get(10, 20)); // OK
		transitions.get(Sensor.WIND_DIRECTION).put(RETURN_TO_NORMAL,
				Config.get(141, 149)); // OK
		transitions.get(Sensor.GROUND_WATER_LEVEL).put(RETURN_TO_NORMAL,
				Config.get(55, 59)); // OK
		transitions.get(Sensor.DAM_WATER_LEVEL).put(RETURN_TO_NORMAL,
				Config.get(55, 59)); // OK
		transitions.get(Sensor.RIVER_WATER_LEVEL).put(RETURN_TO_NORMAL,
				Config.get(55, 59)); // OK
	}

	/**
	 * @param sensor
	 * @return
	 */
	public static Map<Long, Config> getTransitions(Sensor sensor) {
		return transitions.get(sensor);
	}

}
