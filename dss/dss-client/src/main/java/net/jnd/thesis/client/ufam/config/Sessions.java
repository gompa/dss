/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.DynamicReconfigurations.Reconfiguration;
import net.jnd.thesis.client.ufam.config.EsperExpressions.Expression;
import net.jnd.thesis.client.ufam.config.InteractionModels.Model;
import net.jnd.thesis.client.ufam.config.Sessions.Session;
import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.SessionBean;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class Sessions extends AbstractData<Session, SessionBean> {

	enum Session {
		URBAN_FLOODING_ANALYSIS_AND_MODELLING;

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public SessionBean create(WebSocketServices services)
				throws WSException {
			SessionBean b = new SessionBean();
			b.setClientModeAllowed(true);
			b.setDataSources(Data.dataSources.getAll(DSSDataSource.class));
			b.setDynamicReconfs(Data.reconfs.getAll(
					DSSDynamicReconfiguration.class,
					Reconfiguration.ALERT_FIRE_DEP));
			b.setName(name());
			b.setRepositoryEnabled(true);
			b.setSessionEsperExpression(Data.expressions
					.get(Expression.HUMIDITY));
			b.setSessionInteractionModel(Data.models.get(Model.STREAM_ALL));
			b.setUsers(Data.users.getAll(DSSUser.class));
			//
			b.setId(services.createSession(b));
			return b;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = false;
		try {
			Users.authenticate(services, User.LOCAL_AUTHORITY);
			//
			Map<String, SessionBean> existing = new HashMap<String, SessionBean>();
			for (DSSSession e : services.listSessions()) {
				existing.put(e.getName(), (SessionBean) e);
			}
			//
			for (Session required : Session.values()) {
				SessionBean current = existing.get(required.name());
				if (current != null) {
					data.put(required, current);
					res = true;
				} else {
					current = required.create(services);
					if (current.getId() == null) {
						res = false;
						break;
					} else {
						data.put(required, current);
						res = true;
					}
				}
			}
		} catch (WSException e) {
			res = false;
			e.printStackTrace();
		}
		return res;
	}

}
