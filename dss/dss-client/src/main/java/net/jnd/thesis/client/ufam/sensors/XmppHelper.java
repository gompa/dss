/**
 * 
 */
package net.jnd.thesis.client.ufam.sensors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Random;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

/**
 * @author João Domingos
 * @since Jun 2, 2013
 */
public class XmppHelper implements Runnable, MessageListener, ChatManagerListener{

	private static final JabberAccount accountFrom = JabberAccount.SENSORS;
	private static final JabberAccount accountTo = JabberAccount.DIMFCCS;
	private static final JabberServer server = accountFrom.getServer();
	private static final Random random = new Random();
	private static final XmppHelper instance = new XmppHelper();

	private XMPPConnection connection;
	private Chat chat;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jivesoftware.smack.ChatManagerListener#chatCreated(org.jivesoftware
	 * .smack.Chat, boolean)
	 */
	@Override
	public void chatCreated(Chat chat, boolean createdLocally) {
		chat.addMessageListener(this);
	}

	@Override
	public void processMessage(Chat chat, Message message) {
		if (message.getType() == Message.Type.chat) {
			System.out.println(chat.getParticipant() + " says: "
					+ message.getBody());
		} else if (message.getType() == Message.Type.error) {
			System.out.println("error: " + message.getError());
		}
	}

	/**
	 * @param userName
	 * @param password
	 * @throws XMPPException
	 */
	public void login(String userName, String password) throws XMPPException {

		ConnectionConfiguration config = new ConnectionConfiguration(
				server.getUrl(), server.getPort(), server.getServiceName());

		connection = new XMPPConnection(config);
		connection.connect();
		connection.login(userName, password);

		chat = connection.getChatManager()
				.createChat(accountTo.getUser(), this);
	}

	/**
	 * @param message
	 * @param to
	 * @throws XMPPException
	 */
	public void sendMessage(String message) throws XMPPException {
		chat.sendMessage(message);
	}

	/**
	 * 
	 */
	public void displayBuddyList() {
		Roster roster = connection.getRoster();
		Collection<RosterEntry> entries = roster.getEntries();
		System.out.println("\n\n" + entries.size() + " buddy(ies):");
		for (RosterEntry r : entries) {
			System.out.println(r.getUser());
		}
	}

	/**
	 * 
	 */
	public static void disconnect() {
		if (instance.connection != null) {
			instance.connection.disconnect();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			XMPPConnection.DEBUG_ENABLED = true;
			login(accountFrom.getUser(), accountFrom.getPwd());
		} catch (XMPPException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param sensor
	 */
	public static void generateEvent(Sensor sensor) {

		//TESTING: change back, testing
		//		double genValue = generateValue(sensor.getConfig());
		double genValue = getRandomDouble(0, 100);

		BigDecimal bigDecimal = new BigDecimal(genValue);
		bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);

		//============================

		//TESTING: change back, testing
		String genValueStr = bigDecimal.toString().replace(".", ",");
		//		String genEvent = String.format("%s%s", sensor.getDs()
		//				.getSanitizedName(), genValueStr);

		//		String genValueStr = bigDecimal.toString();
		String genEvent = genValueStr;
		//============================

		try {
			instance.sendMessage(genEvent);
		} catch (XMPPException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param config
	 * @return
	 */
	private static double generateValue(Config config) {
		return getRandomDouble(config.getMin(), config.getMax());
	}

	/**
	 * @param min
	 * @param max
	 * @return
	 */
	private static double getRandomDouble(double min, double max) {
		return min + random.nextDouble() * (max - min);
	}

	/**
	 * @param args
	 */
	public static void launch() {
		new Thread(instance).start();
	}

}
