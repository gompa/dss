/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public interface Initializable {

	boolean init();

}
