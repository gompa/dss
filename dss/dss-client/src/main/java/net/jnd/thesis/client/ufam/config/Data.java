/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class Data {

	/**
	 * 
	 */
	static Users users = new Users();

	/**
	 * 
	 */
	static DataSources dataSources = new DataSources();

	/**
	 * 
	 */
	static EsperExpressions expressions = new EsperExpressions();
	static SessionConditions conditions = new SessionConditions();
	static InteractionModels models = new InteractionModels();
	static DynamicReconfigurations reconfs = new DynamicReconfigurations();;

	/**
	 * 
	 */
	static Sessions sessions = new Sessions();
	static UserConnections connections = new UserConnections();

	/**
	 * @return
	 */
	public static boolean initialize() {
		boolean res = true;
		//
		res = res && users.init();
		//
		res = res && dataSources.init();
		//
		res = res && conditions.init();
		res = res && models.init();
		res = res && expressions.init();
		res = res && reconfs.init();
		//
		res = res && sessions.init();
		res = res && connections.init();
		//
		return res;
	}

}
