package net.jnd.thesis.client.ufam.sensors;

/**
 * @author João Domingos
 * @since Jun 2, 2013
 */
public enum JabberServer {

	
	//INFO: url and port of the server that this client will connect to in order to send messages. 
	GTALK("talk.google.com", 5222, "gmail.com");
//	GTALK("localhost", 5222, "gmail.com");
	
	private String url;
	private int port;
	private String serviceName;

	/**
	 * @param url
	 * @param port
	 */
	private JabberServer(String url, int port, String serviceName) {
		this.url = url;
		this.port = port;
		this.serviceName = serviceName;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return url + ":" + port;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

}
