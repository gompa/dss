/**
 * @author João Domingos, Pedro Martins
 * @version 1.1
 * 
 * Client launcher. This class is responsible for launching everything in the
 * java client. By default it connects to the "talk.google.com" server on port
 * 5222, but you can change that by checking the JabberServer class. 
 * 
 * By default, the sender of the message is the account 
 * thesis.dimfccs.sensors@gmail.com and the receiver is the account 
 * thesis.dimfccs@gmail.com. You can change this by checking out the class
 * JabberAccount.
 * 
 * This examples is very strict and has zero modularity and zero adaptation.
 * It is made to only work for this specific case. If you need something 
 * remotely or even slightly different, I strongly recommend you create your 
 * own example apart, by checking out the home website of the smack API at
 * http://www.igniterealtime.org/downloads/index.jsp and joining the forums
 * to ask for help.
 * 
 * @see JabberAccount
 * @see JabberServer
 * @see @see <a href="http://www.igniterealtime.org/downloads/index.jsp">igniterealtime</a>
 */
package net.jnd.thesis.client.ufam.sensors;

public class SensorEventGeneration {

	public static void main(String[] args) {
		XmppHelper.launch();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				XmppHelper.disconnect();
			}
		});
		
		ThreadGroup tg = new ThreadGroup("SENSORS");
		Thread t = null;
		for (Sensor sensor : Sensor.values()) {
			t = new Thread(tg, sensor);
			t.start();
			try {
				Thread.sleep(sensor.getConfig().getInterval() / 2);
//				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.exit(0);
	}
}
