/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.BeanUtils;

/**
 * @author João Domingos
 * @since May 19, 2013
 */
public class ClientHelper {

	/**
	 * @param obj
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("rawtypes")
	public static void dump(Object obj) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		for (Object entryObj : BeanUtils.describe(obj).entrySet()) {
			Entry entry = (Map.Entry) entryObj;
			System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}

}
