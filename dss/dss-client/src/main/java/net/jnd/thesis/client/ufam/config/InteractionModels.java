/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.InteractionModels.Model;
import net.jnd.thesis.client.ufam.config.SessionConditions.Condition;
import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.InteractionModelBean;
import net.jnd.thesis.common.enums.Pattern;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class InteractionModels extends
		AbstractData<Model, InteractionModelBean> {

	enum Model {

		STREAM_ALL(null, Pattern.STREAMING, null, null), HUMIDITY_NORMAL_SCENARIO(
				User.LOCAL_AUTHORITY, Pattern.PUBLISHER_SUBSCRIBER,
				Condition.HUMIDITY_ABOVE_NORMAL, null), HUMIDITY_FIRE_DEPARTMENT(
				User.FIRE_DEPARTMENT, Pattern.PUBLISHER_SUBSCRIBER,
				Condition.HUMIDITY_FIRE_DEPARTMENT, null), PRODUCER_CONSUMER_LARGE(
				null, Pattern.PRODUCER_CONSUMER, null, 1800000L), PRODUCER_CONSUMER_FAST(
				null, Pattern.PRODUCER_CONSUMER, null, 10000L);

		private Pattern pattern;

		private Condition topic;

		private Long polling;

		private User usr;

		private Model(User usr, Pattern pattern, Condition topic, Long polling) {
			this.pattern = pattern;
			this.topic = topic;
			this.polling = polling;
			this.usr = usr;
		}

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public InteractionModelBean create(WebSocketServices services)
				throws WSException {
			InteractionModelBean b = new InteractionModelBean();
			b.setDescription(name());
			b.setDssSessionCondition(Data.conditions.get(topic));
			b.setEventDelay(polling);
			b.setPattern(pattern);
			//
			b.setId(services.createInteractionModel(b));
			return b;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = true;
		try {
			for (Model required : Model.values()) {
				if (required.usr == null) {
					for (User usr : User.values()) {
						res &= create(required, usr);
					}
				} else {
					res &= create(required, required.usr);
				}
			}
		} catch (WSException e) {
			res = false;
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * @param required
	 * @param usr
	 * @return
	 * @throws WSException
	 */
	public boolean create(Model required, User usr) throws WSException {
		boolean res = false;
		if (Users.authenticate(services, usr)) {
			//
			Map<String, InteractionModelBean> existing = new HashMap<String, InteractionModelBean>();
			for (DSSInteractionModel e : services.listInteractionModels()) {
				existing.put(e.getDescription(), (InteractionModelBean) e);
			}
			//
			InteractionModelBean current = existing.get(required.name());
			if (current != null) {
				data.put(required, current);
				res = true;
			} else {
				current = required.create(services);
				if (current.getId() != null) {
					data.put(required, current);
					res = true;
				}
			}
		}
		return res;
	}

}
