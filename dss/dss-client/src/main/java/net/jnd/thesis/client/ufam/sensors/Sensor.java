/**
 * 
 */
package net.jnd.thesis.client.ufam.sensors;

import java.util.Map;

import net.jnd.thesis.client.ufam.config.DataSources.Xmpp;

/**
 * @author João Domingos
 * @since Jun 2, 2013
 */
public enum Sensor implements Runnable {

	HUMIDITY(Xmpp.HUMIDITY, Config.get(-10, 0)), 
	PRECIPITATION(Xmpp.PRECIPITATION, Config.get(-10, 0)), 
	WIND_SPEED(Xmpp.WIND_SPEED, Config.get(-10, 0)), 
	WIND_DIRECTION(Xmpp.WIND_DIRECTION, Config.get(-10, 0)), 
	GROUND_WATER_LEVEL(Xmpp.GROUND_WATER_LEVEL, Config.get(-10, 0)), 
	DAM_WATER_LEVEL(Xmpp.DAM_WATER_LEVEL, Config.get(-10, 0)), 
	RIVER_WATER_LEVEL(Xmpp.RIVER_WATER_LEVEL, Config.get(-10, 0));

	private static final long INITIAL_DELAY = 5 * 1000;

	//INFO: lifetime of Sensor
	private long lifetime = Long.MAX_VALUE;
//	private long lifetime = 50;//Long.MAX_VALUE;

	private long cycle = 0;

	private Xmpp ds;

	private Config config;

	private Map<Long, Config> transitions;

	/**
	 * 
	 */
	private Sensor(Xmpp ds, Config config) {
		this.ds = ds;
		this.config = config;
	}

	/**
	 * @return the lifetime
	 */
	public long getLifetime() {
		return lifetime;
	}

	/**
	 * @param lifetime
	 *            the lifetime to set
	 */
	public void setLifetime(long lifetime) {
		this.lifetime = lifetime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		this.transitions = SensorTransitions.getTransitions(this);
		//
		try {
			Thread.sleep(INITIAL_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//TODO: bugfix !
//		while (cycle < lifetime) {
		for (;;) {
			//
			Config newConfig = transitions.get(cycle);
			cycle++;
			// System.out.println(String.format("[%s] new cycle: %s", this, cycle));
			if (newConfig != null) {
				System.out.println("---------------");
				System.out.println(String.format("[%s][%s] new configuration: %s",
						this, cycle, newConfig));
				this.config = newConfig;
			}
			//
			XmppHelper.generateEvent(this);
			//
			try {
				//TESTING: sleep time!
				Thread.sleep(this.config.getInterval() / 2);
//				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the cycle
	 */
	public long getCycle() {
		return cycle;
	}

	/**
	 * @return the ds
	 */
	public Xmpp getDs() {
		return ds;
	}

	/**
	 * @return the config
	 */
	public Config getConfig() {
		return config;
	}

	/**
	 * @return the transitions
	 */
	public Map<Long, Config> getTransitions() {
		return transitions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name();
	}

}
