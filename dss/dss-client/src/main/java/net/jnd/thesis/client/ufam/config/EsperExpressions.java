/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.DataSources.DataSource;
import net.jnd.thesis.client.ufam.config.DataSources.Rss;
import net.jnd.thesis.client.ufam.config.DataSources.Twitter;
import net.jnd.thesis.client.ufam.config.DataSources.Xmpp;
import net.jnd.thesis.client.ufam.config.EsperExpressions.Expression;
import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.EsperExpressionBean;
import net.jnd.thesis.common.enums.EsperExpressionType;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class EsperExpressions extends
		AbstractData<Expression, EsperExpressionBean> {

	enum Expression {
		PASS_THROUGH(null), 
		/**
		 * NORMAL
		 */
		HUMIDITY( 
				User.LOCAL_AUTHORITY, 
				Xmpp.HUMIDITY), 
		/**
		 * ALERT
		 */
		HUMIDITY_PRECIPITATION_WIND(
				User.LOCAL_AUTHORITY, 
				Xmpp.HUMIDITY, 
				Xmpp.PRECIPITATION, Xmpp.WIND_DIRECTION, Xmpp.WIND_SPEED), 
		/**
		 * EMERGENCY
		 */
		HUMIDITY_PRECIPITATION_WIND_WATERLEVEL(
				User.LOCAL_AUTHORITY, 
				Xmpp.HUMIDITY, 
				Xmpp.PRECIPITATION, Xmpp.WIND_DIRECTION, Xmpp.WIND_SPEED, 
				Xmpp.GROUND_WATER_LEVEL), 
		/**
		 * DISASTER
		 */
		HUMIDITY_PRECIPITATION_WATERLEVEL_TWITTER(
				User.LOCAL_AUTHORITY, 
				Xmpp.HUMIDITY, 
				Xmpp.PRECIPITATION,
				Xmpp.GROUND_WATER_LEVEL, 
				Twitter.FIELD_INFORMATION),
		/**
		 * AFTERMATH
		 */
		PRECIPITATION_WATERLEVEL_TWITTER_LOCALNEWS(
				User.LOCAL_AUTHORITY, 
				Xmpp.PRECIPITATION,
				Xmpp.GROUND_WATER_LEVEL, 
				Twitter.FIELD_INFORMATION,
				Rss.LOCAL_NEWS);

		Collection<DataSource> dataSources = new HashSet<DataSources.DataSource>();

		User usr;

		static final String BASE_QUERY = "select * from pattern [every e=net.jnd.thesis.domain.Event%s]";

		Expression(User usr, DataSource... dataSources) {
			for (DataSource dataSource : dataSources) {
				if (dataSource != null) {
					this.dataSources.add(dataSource);
				}
			}
		}

		String getQuery() {
			StringBuilder sb = new StringBuilder();
			if (!dataSources.isEmpty()) {
				sb.append("(");
				Iterator<DataSource> itr = dataSources.iterator();
				while (itr.hasNext()) {
					sb.append(String.format("name='%s'", itr.next().name()));
					sb.append(itr.hasNext() ? " or " : ")");
				}
			}
			return String.format(BASE_QUERY, sb.toString());
		}

		/**
		 * @param services
		 * @return
		 * @throws WSException
		 */
		public EsperExpressionBean create(WebSocketServices services)
				throws WSException {
			EsperExpressionBean b = new EsperExpressionBean();
			b.setDescription(name());
			b.setEsperExpressionType(EsperExpressionType.EQL);
			b.setExpressionQuery(getQuery());
			//
			b.setId(services.createEsperExpression(b));
			return b;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = true;
		try {
			for (Expression required : Expression.values()) {
				if (required.usr == null) {
					for (User usr : User.values()) {
						res &= create(required, usr);
					}
				} else {
					res &= create(required, required.usr);
				}
			}
		} catch (WSException e) {
			res = false;
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * @param required
	 * @param usr
	 * @return
	 * @throws WSException
	 */
	private boolean create(Expression required, User usr) throws WSException {
		boolean res = false;
		//
		Map<String, EsperExpressionBean> existing = new HashMap<String, EsperExpressionBean>();
		for (DSSEsperExpression e : services.listEsperExpressions()) {
			existing.put(e.getDescription(), (EsperExpressionBean) e);
		}
		//
		EsperExpressionBean current = existing.get(required.name());
		if (current != null) {
			data.put(required, current);
			res = true;
		} else {
			current = required.create(services);
			if (current.getId() == null) {
				res = false;
			} else {
				data.put(required, current);
				res = true;
			}
		}
		return res;
	}

}
