/**
 * 
 */
package net.jnd.thesis.client.ufam.config;

import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.client.ufam.config.Users.User;
import net.jnd.thesis.common.bean.AuthenticationBean;
import net.jnd.thesis.common.bean.UserBean;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Apr 11, 2013
 */
public class Users extends AbstractData<User, UserBean> {

	static String adminUser = "admin";
	static String adminPwd = adminUser + "_pwd";

	enum User {
		ADMIN, LOCAL_AUTHORITY, FIRE_DEPARTMENT, FIREMAN, EMERGENCY_TEAM, FLOOD_SIMULATION;

		boolean isAdmin() {
			return ADMIN.getName().equalsIgnoreCase(getName());
		}

		String getUsername() {
			return name().toLowerCase();
		}

		String getName() {
			return getUsername().replace("_", " ").toUpperCase();
		}

		String getPwd() {
			return getUsername() + "_pwd";
		}

		/**
		 * @param username
		 * @return
		 * @throws WSException
		 */
		UserBean create(WebSocketServices services) throws WSException {
			UserBean value;
			value = new UserBean();
			value.setUsername(getUsername());
			value.setName(getName());
			value.setPassword(getPwd());
			value.setAdmin(isAdmin());
			value.setId(services.createUser(value));
			return value;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.client.AbstractData#initCustom()
	 */
	@Override
	public boolean initCustom() {
		boolean res = false;
		try {
			Users.authenticate(services, User.ADMIN);
			Map<String, UserBean> existing = new HashMap<String, UserBean>();
			for (DSSUser e : services.listUsers()) {
				existing.put(e.getUsername().toLowerCase(), (UserBean) e);
			}
			//
			for (User required : User.values()) {
				UserBean usr = existing.get(required.getUsername());
				if (usr != null) {
					data.put(required, usr);
					res = true;
				} else {
					usr = required.create(services);
					if (usr.getId() == null) {
						res = false;
						break;
					} else {
						data.put(required, usr);
						res = true;
					}
				}
			}
		} catch (WSException e) {
			res = false;
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * @param services
	 * @param usr
	 * @return
	 * @throws WSException
	 */
	public static boolean authenticate(WebSocketServices services, User usr)
			throws WSException {
		AuthenticationBean auth = new AuthenticationBean(usr.getUsername(),
				usr.getPwd());
		services.authenticate(auth);
		return services.isAuthenticated();
	}

}
