package net.jnd.thesis.domain;

import java.util.List;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.interfaces.DSSDataSourcePollingRss;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
@RooJavaBean
@RooJpaActiveRecord
@RooJson
@Table(name = "DSS_DATASOURCE_RSS")
public class RssDataSource extends PollingDataSource implements
		DSSDataSourcePollingRss {

	@NotNull
	@Size(max = 400)
	private String url = "http://feeds.feedburner.com/publicoRSS";

	public DataSourceType getDataSourceType() {
		return DataSourceType.RSS;
	}

	public net.jnd.thesis.common.enums.DataSourceType getType() {
		return net.jnd.thesis.common.enums.DataSourceType
				.valueOf(getDataSourceType().name());
	}

	/**
	 * @return
	 */
	//public static long countRssDataSources() {
	//	return count(entityManager(), RssDataSource.class);
	//}

	/**
	 * @return
	 */
	//public static List<RssDataSource> findAllRssDataSources() {
	//	return findAll(entityManager(), RssDataSource.class);
	//}

	/**
	 * @param id
	 * @return
	 */
	//public static RssDataSource findRssDataSource(Long id) {
	//	return find(entityManager(), RssDataSource.class, id);
	//}

	/**
	 * @param firstResult
	 * @param maxResults
	 * @return
	 */
	//public static List<RssDataSource> findRssDataSourceEntries(int firstResult,
	//		int maxResults) {
	//	return findEntries(entityManager(), RssDataSource.class, firstResult,
	//			maxResults);
	//}

}
