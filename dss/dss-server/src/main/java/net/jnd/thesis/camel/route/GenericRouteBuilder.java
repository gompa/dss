package net.jnd.thesis.camel.route;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.helper.LogHelper;

import org.apache.camel.CamelContext;
import org.apache.camel.Processor;
import org.apache.camel.ServiceStatus;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.ExpressionNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * 
 * 
 * @see		EsperEventProducer
 * @see		EsperEventConsumer
 * @since 	1.2
 */
public abstract class GenericRouteBuilder extends RouteBuilder implements
		DSSRoute {

	private static final Logger LOG = LoggerFactory
			.getLogger(GenericRouteBuilder.class);

	protected static final String HEADER_DATASOURCE_NAME = "ds";
	protected static final String HEADER_REGEX_NAME = "regex";
	protected static final String HEADER_DELAY = "customDelay";
	protected static final String FILTER_METHOD_NAME = "filter";
	protected static final String PRE_FILTER_METHOD_NAME = "preFilter";
	protected static final String POS_FILTER_METHOD_NAME = "posFilter";
	protected static final String HEADER_USER_NAME = "user";
	protected static final String HEADER_SESSION_NAME = "session";

	private Long sessionId;
	private boolean test = Boolean.FALSE;
	private String id;

	/**
	 * @param ctx
	 * @param sessionId
	 * @param test
	 */
	public GenericRouteBuilder(CamelContext ctx, Long sessionId, boolean test) {
		super(ctx);
		this.id = UUID.randomUUID().toString();
		this.sessionId = sessionId;
		this.test = test;
	}

	/**
	 * @return
	 */
	public final boolean isTest() {
		return test;
	}

	/**
	 * @param node
	 */
	protected final void appendToRoute(ExpressionNode route) {
		route.routeId(getId());
		appendToRouteSpecific(route);
	}

	/**
	 * @param node
	 */
	protected void appendToRouteSpecific(ExpressionNode route) {
		// do nothing, override if necessary
	}

	/**
	 * Returns the type of pre-filter to be applied before the Camel processor.
	 * It is overwritten by the child classes that implement this abstract class
	 * or that implement any of its children.
	 * 
	 * @return	the pre-filter class type to be applied before the Camel 
	 * 			processor.
	 */
	protected Class<?> getPreFilterClass() {
		return null;
	}

	/**
	 * Returns the type of pre-filter to be applied after the Camel processor.
	 * It is overwritten by the child classes that implement this abstract class
	 * or that implement any of its children.
	 * 
	 * @return	the pre-filter class type to be applied after the Camel 
	 * 			processor.
	 */
	protected Class<?> getPosFilterClass() {
		return null;
	}

	/**
	 * Define and configure the {@link Processor} class to be applied to each 
	 * received event.
	 * 
	 * @return	the Processor to be applied.
	 */
	protected abstract Processor getProcessor();

	/**
	 * @return
	 */
	protected abstract ComponentHelper getFrom();

	/**
	 * Returns the Id of this route.
	 * 
	 * @return	the id of this route.
	 */
	public final String getId() {
		return id;
	}

	@Override
	public final boolean start(boolean resume) {
		boolean res = true;
		try {
			ServiceStatus routeStatus = getStatus();
			if (resume && routeStatus != null && routeStatus.isSuspended()) {
				getContext().resumeRoute(getId());
			} else if (routeStatus != null && routeStatus.isStartable()) {
				getContext().startRoute(getId());
			} else {
				res = routeStatus == null
						|| ServiceStatus.Started.equals(routeStatus)
						|| ServiceStatus.Starting.equals(routeStatus);
			}
		} catch (Exception e) {
			res = false;
			LogHelper.error(LOG, e);
		}
		return res;
	}

	@Override
	public final boolean stop(boolean end) {
		boolean res = Boolean.TRUE;
		int stopTimeout = 60;
		try {
			ServiceStatus routeStatus = getStatus();
			if (!end && routeStatus != null && routeStatus.isSuspendable()) {
				getContext().suspendRoute(getId(), stopTimeout,
						TimeUnit.SECONDS);
			} else if (routeStatus != null && routeStatus.isStoppable()) {
				getContext().stopRoute(getId(), stopTimeout, TimeUnit.SECONDS);
			} else {
				res = ServiceStatus.Stopped.equals(routeStatus)
						|| ServiceStatus.Stopping.equals(routeStatus)
						|| ServiceStatus.Suspended.equals(routeStatus)
						|| ServiceStatus.Suspending.equals(routeStatus);
			}
		} catch (Exception e) {
			res = Boolean.FALSE;
			LogHelper.error(LOG, e);
		}
		return res;
	}

	/**
	 * @return
	 */
	public ServiceStatus getStatus() {
		return getContext().getRouteStatus(getId());
	}

	/**
	 * @return
	 */
	public Long getSessionId() {
		return sessionId;
	}

	/**
	 * @return
	 */
	private String getExpression() {
		return String.valueOf(getContext().getRouteDefinition(getId()));
	}

	@Override
	public String toString() {
		return String.format(
				"-- [ROUTE][TEST? %s][STATUS: %s][EXPRESSION: %s]", isTest(),
				getSessionId(), getExpression(), getStatus());
	}


	@Override
	public final int hashCode() {
		return (this.id != null) ? this.id.hashCode() : -1;
	}

	@Override
	public final boolean equals(Object obj) {
		return (this.id != null) && (obj != null)
				&& obj instanceof GenericRouteBuilder
				&& ((GenericRouteBuilder) obj).id.equals(this.id);
	}
}
