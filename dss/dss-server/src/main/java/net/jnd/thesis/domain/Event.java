package net.jnd.thesis.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.ToStringHelper;
import net.jnd.thesis.common.interfaces.DSSEvent;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
@RooJavaBean
@RooJpaActiveRecord
@Table(name = "DSS_EVENT")
public class Event extends DomainEntity implements DSSEvent {

	@NotNull
	@Size(max = 400)
	private String valueRaw;

	@Size(max = 400)
	private String value;

	private Float floatValue;

	@ManyToOne(fetch = FetchType.EAGER)
	private DataSource dataSource;

	@ManyToOne(fetch = FetchType.EAGER)
	@NotNull
	private Session session;

	@Enumerated
	@NotNull
	private EventType eventType;

	/**
	 * 
	 */
	public void setDate(Date dt) {
		setCreationDate(dt);
	}

	@Override
	public Date getDate() {
		return getCreationDate();
	}

	@Override
	public Long getSessionId() {
		return getSession() != null ? getSession().getId() : null;
	}

	@Override
	public Long getDataSourceId() {
		return getDataSource() != null ? getDataSource().getId() : null;
	}

	@Override
	public String toString() {
		return ToStringHelper.build(this);
	}

	/**
	 * @return
	 */
	public static long countEvents() {
		return count(entityManager(), Event.class);
	}

	/**
	 * @return
	 */
	public static List<Event> findAllEvents() {
		return findAll(entityManager(), Event.class);
	}

	/**
	 * @param id
	 * @return
	 */
	public static Event findEvent(Long id) {
		return find(entityManager(), Event.class, id);
	}

	/**
	 * @param firstResult
	 * @param maxResults
	 * @return
	 */
	public static List<Event> findEventEntries(int firstResult, int maxResults) {
		return findEntries(entityManager(), Event.class, firstResult,
				maxResults);
	}

	@Override
	public net.jnd.thesis.common.enums.EventType getType() {
		net.jnd.thesis.common.enums.EventType res = null;
		if (getEventType() != null) {
			res = net.jnd.thesis.common.enums.EventType.valueOf(getEventType()
					.name());
		}
		return res;
	}

	@Override
	public String getName() {
		return dataSource != null ? dataSource.getName() : "";
	}

}
