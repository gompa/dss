package net.jnd.thesis.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.helper.RequestHelper;
import net.jnd.thesis.service.CacheDataService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooJson
@Table(name = "DSS_SESSION")
@RooJpaActiveRecord(finders = { "findSessionsByCreationUser",
		"findSessionsByAllowedUsers" })
public class Session extends DomainEntity implements DSSSession {

	private static final Logger LOG = LoggerFactory.getLogger(Session.class);

	@NotNull
	@Size(max = 40)
	private String name = "Default session";

	private String description;

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<AuthUser> allowedUsers = new HashSet<AuthUser>();

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<DataSource> sessionDataSources = new HashSet<DataSource>();

	@NotNull
	@Enumerated
	private SessionStatus sessionStatus = SessionStatus.NEW;

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<DynamicReconfiguration> dynamicReconfigurations = new HashSet<DynamicReconfiguration>();

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	private InteractionModel interactionModel;

	@ManyToOne(fetch = FetchType.EAGER)
	private InteractionModel modifiedInteractionModel;

	private Boolean clientModelAllowed = true;

	private Boolean enableRepository = true;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	private EsperExpression esperExpression;

	private Boolean replaying = false;
	
	private Long replayDelay = 10000L;
	
	@Override
	public Collection<net.jnd.thesis.common.interfaces.DSSUser> getUsers() {
		Collection<DSSUser> c = new ArrayList<DSSUser>();
		for (AuthUser u : getAllowedUsers()) {
			c.add(u);
		}
		return c;
	}

	@Override
	public DSSUser getOwner() {
		return getCreationUser();
	}

	@Override
	public Collection<net.jnd.thesis.common.interfaces.DSSDataSource> getDataSources() {
		Collection<DSSDataSource> c = new ArrayList<DSSDataSource>();
		for (DataSource ds : getSessionDataSources()) {
			c.add(ds);
		}
		return c;
	}

	@Override
	public DSSInteractionModel getSessionInteractionModel() {
		return getModifiedInteractionModel() != null ? getModifiedInteractionModel()
				: getInteractionModel();
	}

	@Override
	public Collection<net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration> getDynamicReconfs() {
		Collection<DSSDynamicReconfiguration> c = new ArrayList<DSSDynamicReconfiguration>();
		for (DynamicReconfiguration dr : getDynamicReconfigurations()) {
			c.add(dr);
		}
		return c;
	}

	@Override
	public String toString() {
		String owner = (getCreationUser() != null) ? getCreationUser()
				.getName() : StringUtils.EMPTY;
		return String.format("%s (owner: %s)", getName(), owner);
	}

	@PostPersist
	@PostUpdate
	protected void updateCache() {
		LOG.debug(Session.class.getName() + " -> void updateCache()");
		CacheDataService.update(this);
	}

	@Override
	public boolean isClientModeAllowed() {
		return getClientModelAllowed();
	}

	@Override
	public boolean isRepositoryEnabled() {
		return getEnableRepository();
	}

	@Override
	public net.jnd.thesis.common.enums.SessionStatus getStatus() {
		net.jnd.thesis.common.enums.SessionStatus res = null;
		if (getSessionStatus() != null) {
			res = net.jnd.thesis.common.enums.SessionStatus
					.valueOf(getSessionStatus().name());
		}
		return res;
	}

	@Override
	public DSSEsperExpression getSessionEsperExpression() {
		return getEsperExpression();
	}

	/**
	 * @return
	 */
	public boolean isReplaying() {
		return replaying;
	}
	
	public static long countSessions() {
		AuthUser usr = RequestHelper.getCurrentUser(false);
		TypedQuery<Long> q = buildCountQuery(usr);
		return q.getSingleResult();
	}

	public static List<Session> findAllSessions() {
		AuthUser usr = RequestHelper.getCurrentUser(false);
		return findAllSessionsByUser(usr);
	}

	public static List<Session> findAllSessionsByUser(AuthUser usr) {
		TypedQuery<Session> q = buildQuery(usr);
		return q.getResultList();
	}

	public static Session findSession(Long id) {
		Session res = null;
		for (Session session : findAllSessions()) {
			if (session.getId().equals(id)) {
				res = session;
				break;
			}
		}
		return res;
	}

	public static List<Session> findSessionEntries(int firstResult,
			int maxResults) {
		AuthUser usr = RequestHelper.getCurrentUser(false);
		TypedQuery<Session> q = buildQuery(usr);
		return q.setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();
	}

	/**
	 * @param usr
	 * @return
	 */
	private static TypedQuery<Session> buildQuery(AuthUser usr) {
		TypedQuery<Session> q = null;
		if (usr == null || usr.isAdministrator()) {
			q = entityManager().createQuery("SELECT o FROM Session AS o ",
					Session.class);
		} else {
			q = entityManager()
					.createQuery(
							"SELECT o FROM Session AS o WHERE o.creationUser = :creationUser OR :allowedUser MEMBER OF o.allowedUsers ",
							Session.class);
			q.setParameter("creationUser", usr);
			q.setParameter("allowedUser", usr);
		}
		return q;
	}

	/**
	 * @param usr
	 * @return
	 */
	private static TypedQuery<Long> buildCountQuery(AuthUser usr) {
		TypedQuery<Long> q = null;
		if (usr == null || usr.isAdministrator()) {
			q = entityManager().createQuery(
					"SELECT count(o) FROM Session AS o ", Long.class);
		} else {
			q = entityManager()
					.createQuery(
							"SELECT count(o) FROM Session AS o WHERE o.creationUser = :creationUser OR :allowedUser MEMBER OF o.allowedUsers ",
							Long.class);
			q.setParameter("creationUser", usr);
			q.setParameter("allowedUser", usr);
		}
		return q;
	}

}
