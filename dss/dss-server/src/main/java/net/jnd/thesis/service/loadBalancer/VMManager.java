package net.jnd.thesis.service.loadBalancer;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pedro Martins
 * 
 * The not thread-safe, Singleton class, VMManager. 
 * <p>
 * This singleton class is responsible for all 
 * the interaction with the Singleton object {@link Requests Requests}, which 
 * in turn realizes all the communications needed with the EC2 AWS. Therefore, 
 * any requests for creating, deleting, or checking content of EC2 instances 
 * must pass through this class.
 * <p>
 * This class also sets up the RMI registry, depending if this EC2 instance is 
 * Dad, or one of his sons. If this EC2 instance is Dad, then this singleton 
 * will have an RMI server that will receive requests from sons, and deliver 
 * them to the Requests singleton. If however, this EC2 instance is a son, then
 * this VMManager will create an RMI client, which will make use of an 
 * {@link ICommunicator ICommunicator} implementation, which will communicate 
 * with Dad and ask him to perform the operations needed in the AWS EC2 cloud. 
 * Currently, the class that implements the ICommunicator interface is the 
 * {@link RMICommunicator RMICommunicator} class.
 * <p>
 * Last but not least, this class also uses the {@link CpuHistory CpuHistory} 
 * class to decided whether or not the creation of a new VM is needed or not, or
 * if this EC2 instance should still be alive or not.
 * <p> 
 * As the other singletons, this class is not thread-safe. The instantiation of 
 * the class poses no problem because it is initialized by a single thread that
 * runs in an orderly manner, executing the 
 * {@link LoadBalancerService LoadBalancerService} service. This service is the 
 * first to call the {@link #getInstance() getInstance()} method of this class
 * and so we know for sure that it is initialized safely.
 * <p>
 * However, the other methods are not atomic, and do not implement any 
 * thread-safety mechanism, so caution is advised. However, thread problem with 
 * this class are only like to happen in scenarios where multiple, 
 * possibly hundreds of, sessions are being created at the same exact time by 
 * many sons, and where many sons are asking to commit suicide. Because so far 
 * it is currently impossible to test such a scenario with multiple sons, it is 
 * therefore not expected to have thread-safety problems. For more information 
 * on thread-safety see: 
 * <a href="http://www.javaworld.com/article/2073352/core-java/simply-singleton.html">Simply Singleton</a>
 *  
 *
 * @see		Requests
 * @see		ICommunicator
 * @see		RMICommunicator
 * @see		CpuHistory
 * @see		<a href="http://www.javaworld.com/article/2073352/core-java/simply-singleton.html">Simply Singleton</a>
 * @since 	2.0.1
 */
public class VMManager {

	/*
	 * Public variables
	 * */
	/** CPU usage upper limit to cross when asking to create a new VM. */
	public final static double INNITIAL_ADD_VM_THRESHOLD = 0.7; 	

	/** CPU usage lower limit cross when asking to kill this VM. */
	public final static double INNITIAL_KILL_VM_THRESHOLD = 0.1;	

	/** RMI port used to which out registry will listen to.*/
	public static final int RMI_REGISTRY_PORT = 1099;
	
	/**
	 * Public IP of the God server, aka Dad server. This is the IP of the
	 * master, the server that can never go down, the server that decided if
	 * new VMs should be created and that kills VMs that are no longer needed.
	 * */
	public static final String DAD_PUBLIC_IP = "54.72.144.95";
	
	/*
	 * Private variables
	 * */
	private static final Logger LOG = LoggerFactory
			.getLogger(VMManager.class);

	/** Name of the {@link RMICommunicator RMICommunicator} object stored
	 * in the God server.*/
	private static final String SERVER_BIND_NAME = "internalComms";
	
	private double addVMThreshold, killVMThreshold;
	private String myPublicIP, dadPublicIp;
	private Registry registry;
	private ICommunicator communicator;

	/*
	 * Singletons used
	 * */
	private static VMManager localManager;	//this classe's singleton
	private static Requests requests;
	private static CpuHistory history; 
	
	/**
	 * Constructor of the VMManager singleton. 
	 * <p>
	 * Creates a VMManager object, with the publicly defined thresholds for 
	 * creation and deletion of VMs.  It also initializes the Requests singleton
	 * and it is the only class that makes use of it. Last but not least, it
	 * also sets up the registry depending if this machine is Dad or not. If it
	 * is Dad, then it will have an RMI server, to receive requests from his 
	 * sons, otherwise it will have an RMI client to contact Dad and ask him to
	 * perform operations on the AWS cloud. 
	 */
	private VMManager() {
		addVMThreshold = INNITIAL_ADD_VM_THRESHOLD;
		killVMThreshold = INNITIAL_KILL_VM_THRESHOLD;

		history = CpuHistory.getInstance();
		requests =  Requests.getInstance();

		myPublicIP = requests.getMyPublicIP();
		dadPublicIp = DAD_PUBLIC_IP;

		LOG.debug("dadPublicIp: " + myPublicIP);
		LOG.debug("myPublicIp: " + myPublicIP);
		LOG.debug("myInstanceId: " + requests.getMyInstanceId());

		//setting up RMI Registries
		LOG.info(VMManager.class.getName() + ", setting up RMI Registries");

		try{
			/*
			 * If we are the father, we set up our registry to listen to 
			 * everyone, otherwise it means we are a child and we simply try
			 * to connect to our father.
			 * */
			if(amIDad()){
				communicator = new RMICommunicator();

				// Bind the remote object's stub in the registry
				registry = LocateRegistry.createRegistry(RMI_REGISTRY_PORT);
				registry.rebind(SERVER_BIND_NAME, communicator);

				LOG.info("local RMICommunicator server ready.");

			}else{
				registry = LocateRegistry.getRegistry(dadPublicIp);
				communicator = (ICommunicator) registry.lookup(SERVER_BIND_NAME);
			}
		}catch(RemoteException rex){
			LOG.error("Exception in RMIServer.start " + rex);
		} catch (NotBoundException e) {
			LOG.error("Could not find RMICommunicator object on dad: " + e);
		}
	}

	
	/**
	 * Returns the VMManager singleton that already exists, or it initializes 
	 * one in case it is called by the first time. It is not thread-safe. 
	 * 
	 * @return	the VMManager singleton
	 * */
	public static VMManager getInstance(){
		if(localManager == null)
			localManager = new VMManager();

		return localManager;
	}

	/**
	 * If this EC2 instance is Dad, then it creates a spot request for a new VM, 
	 * otherwise it asks Dad to create a new spot request for a new VM. Dad will
	 * not create a new spot request for VM if there is already one VM instance
	 * active, or if there is already a spot request for one. 
	 * <p>
	 * Currently, dad create multiple instances, but he can only do it one at 
	 * a time. This is to prevent a burst of VMs.
	 * 
	 * @return					<code>true</code> if a new spot request for a 
	 * 							new VM was created, <code>false</code> otherwise
	 * @throws	RemoteException	if this EC2 instance is a client, and there was
	 * 							and error trying to communicate with Dad
	 */
	public boolean launchNewVM() throws RemoteException{

		if(amIDad()){
			requests.updateLists();
			
			
			if(!requests.areAnyOpen()){
				LOG.warn("requesting a new VM!");
				requests.submitRequests();
				return true;
			}else if(requests.areAnyOpen()){
				LOG.info("No VM requests because there is already one or"
						+ " more spot requests:");
				List<String> requestsList = requests.getOpenRequests();
				for(String req : requestsList)
					LOG.info(req);
			}
			
//			if(!requests.areAnyOpen() && !requests.areAnyActive()){
//				LOG.warn("requesting a new VM!");
//				requests.submitRequests();
//				return true;
//			}else if(requests.areAnyActive()){
//				LOG.info("No VM requests because there is already one or"
//						+ " more instances running:");
//
//				List<String> instances = requests.getActiveInstances();
//				for(String instance : instances)
//					LOG.info(instance);
//
//				LOG.info("And their public ips are:");
//				List<String> publicIps = requests.getPublicIPs();
//				for(String ip : publicIps)
//					LOG.info(ip);
//
//			}else if(requests.areAnyOpen()){
//				LOG.info("No VM requests because there is already one or"
//						+ " more spot requests:");
//				List<String> requestsList = requests.getOpenRequests();
//				for(String req : requestsList)
//					LOG.info(req);
//			}

		}else{
			return communicator.askNewVM();
		}
		return false;
	}

	/**
	 * Kills the target VM. Can only be invoked by Dad, for only Dad decides who
	 * lives or dies.
	 *
	 * @param					instanceId, the instance id of the machine 
	 * 							we wish to terminate
	 * @return					<code>true</code>, if Dad killed the target 
	 * 							VM successfully, or <code>false</false> if 
	 * 							the request was denied by Dad or failed due 
	 * 							to some reason
	 * @throws RemoteException	if this EC2 instance is a son, is asking 
	 * 							dad to kill it and there was a communication 
	 * 							error when trying to do that
	 */
	public boolean killVM(String instanceId) throws RemoteException{
		return requests.updateLists() && requests.killVM(instanceId);
	}

	/**
	 * Commit suicide. If I am not Dad, and Dad approves of suicide, then he 
	 * kills me. Otherwise, it means I am dad, and I just wait around doing 
	 * nothing.
	 *
	 * @return					<code>true</code>, if Dad approves and is able 
	 * 							to execute my request for death
	 * @throws RemoteException	if I tried communicating to Dad my request, and 
	 * 							the communication failed for some reason
	 */
	public boolean commitSuicide() throws RemoteException{
		return !amIDad() && communicator.killVM(requests.getMyInstanceId());
	}

	/**
	 * Returns <code>true</code> if I am Dad, or <code>false</false> if I am one
	 * of his sons.	
	 *
	 * @return	<code>true</code> if I am Dad, or <code>false</false> otherwise.
	 */
	public boolean amIDad(){
		return myPublicIP.equals(dadPublicIp);
	}

	/**
	 * Checks if is new VM needed. A new VM is needed if the average CPU usage 
	 * of all the CPU usages kept in history is greater than the 
	 * {@link #addVMThreshold addVMThreshold} variable.
	 *
	 * @return	<code>true</code>, if is a new VM is needed, or 
	 * 			<code>false</code> otherwise
	 */
	public boolean isNewVMNeeded(){
		LOG.debug("Do we really need a new VM?");
		return history.getResultsAvg() > addVMThreshold;
	}

	/**
	 * Checks if this specific VM instance needed. A VM is needed only if it is 
	 * working. We see this by comparing the the average CPU usage of all the 
	 * CPU usages kept in history and checking if the average is greater than 
	 * the {@link #killVMThreshold killVMThreshold} variable. If it is greater
	 * then it means this instance is working hard and therefore we need it.
	 * Otherwise it means the instance is wasting resources we we should kill 
	 * it.
	 *
	 * @return	<code>true</code>, if this VM is needed, or <code>false</code> 
	 * 			otherwise
	 */
	public boolean isThisVMNeeded(){
		LOG.debug("Should I live?");
		return history.getResultsAvg() > killVMThreshold;
	}

	/*
	 * GETTERS and SETTERS
	 * */

	/**
	 * Returns the current lower limit that the CPU usage must cross in 
	 * order for this VM to ask to be killed.
	 * 
	 * @return	the lower boundary of CPU usage require for this VM to ask 
	 * 			for suicide.
	 * */
	public double getKillVMThreshold() {
		return killVMThreshold;
	}

	/**
	 * Sets the lower boundary of CPU usage that must be crossed for this VM
	 * to ask for suicide.
	 * 
	 * @param	killVMThreshold	the new lower boundary of CPU usage that 
	 * 							must be crossed for this VM to ask for 
	 * 							suicide.
	 * */
	public void setKillVMThreshold(double killVMThreshold) {
		this.killVMThreshold = killVMThreshold;
	}

	/**
	 * Returns the current upper boundary of CPU usage that must be crossed
	 * for this VM to ask for the creation of a new VM.
	 * 
	 * @return	the upper boundary of CPU usage that must be crossed for 
	 * 			this VM to ask for a new brother.
	 * */
	public double getAddVMThreshold() {
		return addVMThreshold;
	}

	/**
	 * Sets the upper boundary of CPU usage that must be crossed for this VM
	 * to ask for the creation of a new brother.
	 * 
	 * @param	addVMThreshold	the new upper boundary of CPU usage that 
	 * 			must be crossed for this VM to ask for a new brother. 
	 * */
	public void setAddVMThreshold(double addVMThreshold) {
		this.addVMThreshold = addVMThreshold;
	}
}
