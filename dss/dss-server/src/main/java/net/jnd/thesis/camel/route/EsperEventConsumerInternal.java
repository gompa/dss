package net.jnd.thesis.camel.route;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import net.jnd.thesis.camel.bean.CamelInternalEvent;
import net.jnd.thesis.camel.component.EsperComponentHelper;
import net.jnd.thesis.common.enums.EsperExpressionType;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.domain.DynamicReconfiguration;
import net.jnd.thesis.domain.EsperExpression;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.EventType;
import net.jnd.thesis.domain.InteractionModel;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionCondition;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.helper.RoutesWrapper;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.service.DSSDataService;
import net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocketHelper;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.model.ExpressionNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.event.map.MapEventBean;

/**
 * @author João Domingos
 * @since Nov 4, 2012
 */
class EsperEventConsumerInternal extends EsperEventConsumer {

	private static final Logger LOG = LoggerFactory
			.getLogger(EsperEventConsumerInternal.class);

	private static final Map<Long, Set<Long>> dynaReconfsBlackList = new HashMap<Long, Set<Long>>();

	/**
	 * 
	 */
	private EsperExpressionType esperExpressionType = EsperExpressionType.EQL;

	/**
	 * 
	 */
	private String esperExpressionQuery = "select * from pattern [every e="
			+ CamelInternalEvent.class.getName() + "]";

	/**
	 * @param sessionId
	 * @param reconf
	 * @return
	 */
	private static boolean check(Long sessionId, DynamicReconfiguration reconf) {
		Set<Long> bannedIds = dynaReconfsBlackList.get(sessionId);
		//
		if (bannedIds == null) {
			dynaReconfsBlackList
			.put(sessionId, bannedIds = new HashSet<Long>());
		}
		//
		return !bannedIds.contains(reconf.getId());
	}

	/**
	 * @param ctx
	 * @param sessionId
	 * @param test
	 */
	public EsperEventConsumerInternal(CamelContext ctx, Long sessionId,
			boolean test) {
		super(ctx, sessionId, test);
		Session session = CacheDataService.getSession(sessionId);
		EsperExpression exp = session.getEsperExpression();
		this.esperExpressionQuery = exp.getExpressionQuery();
		this.esperExpressionType = exp.getEsperExpressionType();
	}

	@Override
	protected void appendToRouteSpecific(ExpressionNode route) {
		route.recipientList(header(EsperEventConsumer.HEADER_RECIPIENT_LIST),
				EsperEventConsumer.DELIMITER_RECIPIENT_LIST)
				.parallelProcessing().ignoreInvalidEndpoints();
	}

	@Override
	protected final EsperExpressionType getEsperExpressionType() {
		return esperExpressionType;
	}

	@Override
	protected final String getEsperExpressionQuery() {
		String query = esperExpressionQuery;
		query = query.replaceAll(Event.class.getName(),
				CamelInternalEvent.class.getName());
		if (esperExpressionQuery.contains(")]")) {
			query = query.replace("(", "((").replace(")", "))");
			query = query.replace("))]", ") and sid=0 and sid<%s)]");
		} else {
			query = query.replace("]", "(sid=0 and sid<%s)]");
		}
		query = String.format(query, new Date().getTime());
		return query;
	}

	@Override
	protected String getEsperName() {
		return EsperComponentHelper.getEsperNameInternal(getSessionId(),
				isTest());
	}

	@Override
	protected Processor getProcessor() {
		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				
				boolean eventOk = true;
				DefaultMessage msg = (DefaultMessage) exchange.getIn();
				
				// build recipient list header
				msg.setHeader(HEADER_DELAY, 0L);
				final Session session = CacheDataService
						.getSession(getSessionId());
				// build recipient list header
				msg.setHeader(HEADER_RECIPIENT_LIST,
						EsperEventConsumerHelper.buildRecipientList(session));
				MapEventBean body = (MapEventBean) msg.getBody();
				
				final CamelInternalEvent event = buildInternalEventFromProperties(body);
				LOG.debug("TIME_CHECK_P2_START " + event.getValue() + " " + System.currentTimeMillis() + " " + getSessionId());
				
				// get session interaction model
				InteractionModel interactionModel = session
						.getInteractionModel();
				//				Collection<DynamicReconfiguration> reconfs = session
				//						.getDynamicReconfigurations();
				// process pattern related operations
				
				switch (interactionModel.getPattern()) {
				case PUBLISHER_SUBSCRIBER:
					DSSSessionCondition condition = interactionModel
					.getDssSessionCondition();
					eventOk = EsperEventConsumerHelper.checkCondition(
							condition, event);
					// if not ok, clean recipient list
					if (!eventOk) {
						msg.setHeader(HEADER_RECIPIENT_LIST,
								org.apache.commons.lang3.StringUtils.EMPTY);
					}
					break;
				case PRODUCER_CONSUMER:
					msg.setHeader(HEADER_DELAY,
							interactionModel.getEventDelay());
					break;
				default:
					break;
				}
				
				// persist event
				if (eventOk) {
					
					event.setSid(session.getId());
//					event.setSessionId(session.getId());
					
					// persist event
					Event evt = buildEvent(event);
					
					
					msg.setBody(evt, evt.getClass());
					if (evt != null && session.isRepositoryEnabled()
							/*
							 * && !SessionStatus.REPLAYING.name().equals(
							 * session.getStatus())
							 */) {
						try {
							evt.persist();
						} catch (Exception e) {
							LOG.error("Failed to persist event: " + e.getMessage(), e);
						}
					}
					
					LOG.info("Persisted event");
					
					// launch dynamic reconfigurations
					new Thread(new Runnable() {
						@Override
						public void run() {
							// process dynamic reconfigurations
							handleDynamicReconfigurations(session, event);
						}
					}).start();

				}

				LOG.debug("TIME_CHECK_P2_END " + event.getValue() + " " + System.currentTimeMillis() + " " + getSessionId());

				//DEBUG printing EsperEventConsumerInternal info
				try {
					LOG.debug("DEBUG_PATH EsperEventConsumerInternal, process(), from route: " + getFrom().from());
					LOG.debug("DEBUG_PATH EsperEventConsumerInternal, process(), to route: " + msg.getHeader(HEADER_RECIPIENT_LIST));
					LOG.debug("DEBUG_PATH EsperEventConsumerInternal, process(), threadId: " + Thread.currentThread().getId());
					LOG.debug("DEBUG_PATH EsperEventConsumerInternal, process(), messageId: " + msg.getMessageId());
					LOG.debug("DEBUG_PATH EsperEventConsumerInternal, process(), eventId: " + event.getId());
					LOG.debug("DEBUG_PATH EsperEventConsumerInternal, process(), eventCreationDate: " + event.getCreationDate());
				} catch (OperationNotSupportedException e) {
					e.printStackTrace();
				}
			}

		};

	}

	/**
	 * @param session
	 * @param event
	 */
	private void handleDynamicReconfigurations(Session session,
			CamelInternalEvent event) {
		DynamicReconfiguration dynRec = null;
		Collection<DynamicReconfiguration> reconfs = session
				.getDynamicReconfigurations();
		for (DynamicReconfiguration reconf : reconfs) {
			SessionCondition condition = reconf.getSessionCondition();
			if (check(session.getId(), reconf)
					&& reconf.getSessionConditionEnabled()
					&& EsperEventConsumerHelper
					.checkCondition(condition, event)) {
				Long newInteractionModelId = null;
				Long newEsperExpressionId = null;
				//
				if (reconf.getInteractionModelEnabled()) {
					if (session.getInteractionModel().getId() != reconf
							.getInteractionModel().getId()) {
						newInteractionModelId = reconf.getInteractionModel()
								.getId();
						dynRec = reconf;

					}

				}
				//
				if (reconf.getEsperExpressionEnabled()) {
					if (session.getEsperExpression().getId() != reconf
							.getEsperExpression().getId()) {
						newEsperExpressionId = reconf.getEsperExpression()
								.getId();
						dynRec = reconf;
					}
				}
				//
				if (newInteractionModelId != null
						|| newEsperExpressionId != null) {
					dynaReconfsBlackList.get(session.getId()).add(reconf.getId());
					CacheDataService.updateSessionInteractionModel(session,
							newInteractionModelId, newEsperExpressionId);
					break;
				}

			}

		}
		// apply dynamic reconfiguration client rules
		if (dynRec != null) {

			// build event
			Event e = new Event();
			e.setSession(session);
			e.setEventType(EventType.SESSION_DYNAMIC_RECONFIGURATION);
			e.setValue(dynRec.getDescription());
			e.setValueRaw(dynRec.getDescription());
			e.persist();
			
			// broadcast event
			for (DSSUser dssUser : session.getUsers()) {
				ServerSideWebSocketHelper.broadcastEvent(dssUser.getId(), e);
			}

			// modify clients connections
			for (DSSUser dssUser : session.getUsers()) {
				RoutesWrapper sessionRoutes = DSSDataService.getSessionRoutes()
						.get(session.getId());
				List<UserSessionConnection> conns = sessionRoutes
						.getUserConnections();
				// for all session client connections
				for (UserSessionConnection conn : conns) {
					// check all dynamic reconfigurations
					for (DynamicReconfiguration reconf : conn
							.getDynamicReconfigurations()) {
						// only if the dynamic reconfiguration is event enabled
						if (reconf.getEventNameEnabled()
								&& String.valueOf(reconf.getEventName())
								.equals(dynRec.getDescription())) {
							Long newInteractionModelId = null;
							Long newEsperExpressionId = null;
							// interaction model modifications
							if (reconf.getInteractionModelEnabled()
									&& reconf.getInteractionModel() != null) {
								newInteractionModelId = reconf
										.getInteractionModel().getId();
							}
							// esper expression modifications
							if (reconf.getEsperExpressionEnabled()
									&& reconf.getEsperExpression() != null) {
								newEsperExpressionId = reconf
										.getEsperExpression().getId();
							}
							// update connection route
							if (newInteractionModelId != null
									|| newEsperExpressionId != null) {
								CacheDataService
								.updateConnectionInteractionModel(conn,
										newInteractionModelId,
										newEsperExpressionId);
								break;
							}
						}
					}
				}

			}
		}
	}
}
