package net.jnd.thesis.camel.component;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import net.jnd.thesis.camel.util.TwitterUtil;

import org.apache.camel.CamelContext;
import org.apache.camel.component.twitter.TwitterComponent;
import org.apache.camel.impl.DefaultComponent;
import org.apache.commons.lang3.StringUtils;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 * 
 *        TODO: exclude re-tweets if necessary override
 *        org.apache.camel.component.twitter.consumer.streaming.FilterConsumer ?
 * 
 */
public class TwitterComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "twitter";
	private static final String COMMON_AUTH_URI = "&consumerKey=%s&consumerSecret=%s&accessToken=%s&accessTokenSecret=%s";
	private static final String URI_PREFIX = COMPONENT_NAME + "://";

	private String consumerKey = StringUtils.EMPTY;
	private String consumerSecret = StringUtils.EMPTY;
	private String accessToken = StringUtils.EMPTY;
	private String accessTokenSecret = StringUtils.EMPTY;
	private String userIds = StringUtils.EMPTY;

	/*
	 * 
	 */

	public TwitterComponentHelper(CamelContext ctx, String consumerKey,
			String consumerSecret, String accessToken,
			String accessTokenSecret, String... screenNames) {
		super(ctx);
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		this.accessToken = accessToken;
		this.accessTokenSecret = accessTokenSecret;
		this.userIds = buildUsers(screenNames);
	}

	// /**
	// * @param ctx
	// */
	// public TwitterComponentHelper(CamelContext ctx, String consumerKey,
	// String consumerSecret, String accessToken,
	// String accessTokenSecret, String... screenNames) {
	// super(ctx);
	// this.consumerKey = consumerKey;
	// this.consumerSecret = consumerSecret;
	// this.accessToken = accessToken;
	// this.accessTokenSecret = accessTokenSecret;
	// this.userIds = buildUsers(screenNames);
	// }

	/**
	 * @param screenNames
	 * @return
	 */
	private String buildUsers(String... screenNames) {
		Set<String> userIds = new HashSet<String>();
		for (String user : screenNames) {
			if (StringUtils.isNotBlank(user)) {
				String screenName = String.valueOf(user).trim();
				Long userId = TwitterUtil.getUserIdFromScreenName(screenName);
				if (userId != null) {
					userIds.add(String.valueOf(userId));
				}
			}
		}
		StringBuilder sb = new StringBuilder();
		for (Iterator<String> itr = userIds.iterator(); itr.hasNext();) {
			sb.append(itr.next());
			if (itr.hasNext()) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

	@Override
	protected DefaultComponent buildComponent(CamelContext ctx) {
		TwitterComponent component = ctx.getComponent(COMPONENT_NAME,
				TwitterComponent.class);
		component.setAccessToken(accessToken);
		component.setAccessTokenSecret(accessTokenSecret);
		component.setConsumerKey(consumerKey);
		component.setConsumerSecret(consumerSecret);
		return component;
	}

	@Override
	protected String getFromUri() throws OperationNotSupportedException {
		String from = buildUri("streaming/filter?type=event&userIds=%s",
				this.userIds);
		// from = buildUri("search?type=polling&delay=%s&keywords=%s",
		// this.delay, this.searchTerm);
		// from = buildUri("streaming/filter?type=event&keywords=%s",
		// this.searchTerm);
		// from = buildUri("timeline/home?type=polling&delay=%s",
		// this.delay);
		// from = buildUri("timeline/home?type=direct");
		return from;
	}

	/**
	 * @param uri
	 * @param params
	 * @return
	 */
	private String buildUri(String uri, Object... params) {
		String auth = String.format(COMMON_AUTH_URI, this.consumerKey,
				this.consumerSecret, this.accessToken, this.accessTokenSecret);
		StringBuilder sb = new StringBuilder(URI_PREFIX);
		sb.append(String.format(uri, params)).append(auth);
		return sb.toString();
	}

	@Override
	protected String getToUri() throws OperationNotSupportedException {
		throw new OperationNotSupportedException();
	}

}
