package net.jnd.thesis.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
@RooJavaBean
@RooJpaActiveRecord(mappedSuperclass = true)
public abstract class EventDrivenDataSource extends DataSource {

}
