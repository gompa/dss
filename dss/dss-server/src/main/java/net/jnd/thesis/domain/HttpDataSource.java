package net.jnd.thesis.domain;

import java.util.List;

import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.interfaces.DSSDataSourcePollingHttp;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
@RooJavaBean
@RooJpaActiveRecord
@RooJson
@Table(name = "DSS_DATASOURCE_HTTP")
public class HttpDataSource extends PollingDataSource implements
		DSSDataSourcePollingHttp {

	@NotNull
	@Size(max = 400)
	private String url = "http://www.psi20.net/";

	@NotNull
	@Size(max = 400)
	private String query = "//html/body/table/tbody/tr[6]/td[3]";

	@NotNull
	@Enumerated
	private HttpQueryType queryType = HttpQueryType.XPATH;

	public DataSourceType getDataSourceType() {
		return DataSourceType.HTTP;
	}

	public net.jnd.thesis.common.enums.DataSourceType getType() {
		return net.jnd.thesis.common.enums.DataSourceType
				.valueOf(getDataSourceType().name());
	}
	
	@Override
	public net.jnd.thesis.common.enums.HttpQueryType getHttpQueryType() {
		net.jnd.thesis.common.enums.HttpQueryType res = null;
		if (getQueryType() != null) {
			res = net.jnd.thesis.common.enums.HttpQueryType
					.valueOf(getQueryType().name());
		}
		return res;
	}

//	/**
//	 * @return
//	 */
//	public static long countHttpDataSources() {
//		return count(entityManager(), HttpDataSource.class);
//	}
//
//	/**
//	 * @return
//	 */
//	public static List<HttpDataSource> findAllHttpDataSources() {
//		return findAll(entityManager(), HttpDataSource.class);
//	}
//
//	/**
//	 * @param id
//	 * @return
//	 */
//	public static HttpDataSource findHttpDataSource(Long id) {
//		return find(entityManager(), HttpDataSource.class, id);
//	}
//
//	/**
//	 * @param firstResult
//	 * @param maxResults
//	 * @return
//	 */
//	public static List<HttpDataSource> findHttpDataSourceEntries(
//			int firstResult, int maxResults) {
//		return findEntries(entityManager(), HttpDataSource.class, firstResult,
//				maxResults);
//	}
//

}
