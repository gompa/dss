/**
 * 
 */
package net.jnd.thesis.servlet.jetty;

import net.jnd.thesis.servlet.ws.DSSWebSocket;

import org.eclipse.jetty.websocket.WebSocket.OnTextMessage;

/**
 * @author João Domingos
 * @since Dec 4, 2012
 */
public interface CustomJettyWebSocket extends DSSWebSocket, OnTextMessage {


}
