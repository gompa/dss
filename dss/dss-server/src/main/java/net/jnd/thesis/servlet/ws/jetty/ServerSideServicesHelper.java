/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import net.jnd.thesis.common.bean.BaseBean;
import net.jnd.thesis.common.bean.DataSourceBean;
import net.jnd.thesis.common.bean.DataSourceBeanEventTwitter;
import net.jnd.thesis.common.bean.DataSourceBeanEventXmpp;
import net.jnd.thesis.common.bean.DataSourceBeanPollingHttp;
import net.jnd.thesis.common.bean.DataSourceBeanPollingRss;
import net.jnd.thesis.common.bean.DynamicReconfigurationBean;
import net.jnd.thesis.common.bean.EsperExpressionBean;
import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.bean.InteractionModelBean;
import net.jnd.thesis.common.bean.SessionBean;
import net.jnd.thesis.common.bean.SessionConditionBean;
import net.jnd.thesis.common.bean.UserBean;
import net.jnd.thesis.common.bean.UserSessionConnectionBean;
import net.jnd.thesis.common.enums.EventType;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.domain.AuthRole;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.ConditionOperator;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.DomainEntity;
import net.jnd.thesis.domain.DynamicReconfiguration;
import net.jnd.thesis.domain.EsperExpression;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.HttpDataSource;
import net.jnd.thesis.domain.HttpQueryType;
import net.jnd.thesis.domain.InteractionModel;
import net.jnd.thesis.domain.PatternTemplate;
import net.jnd.thesis.domain.RssDataSource;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionCondition;
import net.jnd.thesis.domain.SessionStatus;
import net.jnd.thesis.domain.TwitterDataSource;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.domain.XmppDataSource;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Dec 30, 2012
 */
public class ServerSideServicesHelper {

	/**
	 * @param elems
	 * @return
	 */
	static <T> Set<T> buildSet(T... elems) {
		if (elems == null) {
			return null;
		}
		Set<T> s = new HashSet<T>();
		for (T elem : elems) {
			s.add(elem);
		}
		return s;
	}

	/**
	 * @param origin
	 * @param target
	 */
	private static void copyAudit(BaseBean origin, DomainEntity target) {
		target.setId(origin.getId());
		target.setCreationDate(origin.getCreationDate());
		target.setUpdateDate(origin.getUpdateDate());
		target.setCreationUser(buildUser((UserBean) origin.getCreationUser()));
		target.setUpdateUser(buildUser((UserBean) origin.getUpdateUser()));
	}

	/**
	 * @param origin
	 * @param target
	 */
	private static void copyAudit(DomainEntity origin, BaseBean target) {
		target.setId(origin.getId());
		target.setCreationDate(origin.getCreationDate());
		target.setUpdateDate(origin.getUpdateDate());
		target.setCreationUser(buildUserBean(origin.getCreationUser()));
		target.setUpdateUser(buildUserBean(origin.getUpdateUser()));
	}

	/**
	 * @param model
	 * @return
	 * @throws WSException
	 */
	static InteractionModelBean buildInteractionModelBean(InteractionModel model)
			throws WSException {
		if (model == null) {
			return null;
		}
		//
		InteractionModelBean bean = new InteractionModelBean();
		copyAudit(model, bean);
		//
		bean.setEventDelay(model.getEventDelay());
		bean.setPattern(model.getPattern());
		bean.setDssSessionCondition(buildSessionConditionBean(model.getTopic()));
		bean.setDescription(model.getDescription());
		return bean;
	}

	/**
	 * @param model
	 * @return
	 * @throws WSException
	 */
	static InteractionModel buildInteractionModel(InteractionModelBean model)
			throws WSException {
		return buildInteractionModel(model, false);
	}

	/**
	 * @param model
	 * @return
	 * @throws WSException
	 */
	static InteractionModel buildInteractionModel(InteractionModelBean model,
			boolean update) throws WSException {
		if (model == null) {
			return null;
		}
		//
		InteractionModel bean = new InteractionModel();
		if (model.getId() != null) {
			bean = InteractionModel.findInteractionModel(model.getId());
			if (!update) {
				return bean;
			}
		}
		copyAudit(model, bean);
		//
		bean.setDescription(model.getDescription());
		bean.setEventDelay(model.getEventDelay());
		if (model.getPattern() != null) {
			bean.setPatternTemplate(PatternTemplate.valueOf(model.getPattern()
					.name()));
		}
		bean.setTopic(buildSessionCondition((SessionConditionBean) model
				.getDssSessionCondition()));
		return bean;
	}

	/**
	 * @param ds
	 * @return
	 * @throws WSException
	 */
	static DataSourceBean buildDataSourceBean(DataSource ds) throws WSException {
		if (ds == null) {
			return null;
		}
		//
		DataSourceBean res = null;
		switch (ds.getType()) {
		case HTTP:
			HttpDataSource inHttp = (HttpDataSource) ds;
			DataSourceBeanPollingHttp outHttp = new DataSourceBeanPollingHttp();
			outHttp.setInterval(inHttp.getInterval());
			outHttp.setQuery(inHttp.getQuery());
			outHttp.setUrl(inHttp.getUrl());
			outHttp.setHttpQueryType(inHttp.getHttpQueryType());
			res = outHttp;
			break;
		case RSS:
			RssDataSource inRss = (RssDataSource) ds;
			DataSourceBeanPollingRss outRss = new DataSourceBeanPollingRss();
			outRss.setInterval(inRss.getInterval());
			outRss.setUrl(inRss.getUrl());
			res = outRss;
			break;
		case TWITTER:
			TwitterDataSource inTwitter = (TwitterDataSource) ds;
			DataSourceBeanEventTwitter outTwitter = new DataSourceBeanEventTwitter();
			outTwitter.setScreenNames(inTwitter.getScreenNames());
			res = outTwitter;
			break;
		case XMPP:
			XmppDataSource inXmpp = (XmppDataSource) ds;
			DataSourceBeanEventXmpp outXmpp = new DataSourceBeanEventXmpp();
			outXmpp.setFromUser(inXmpp.getFromUser());
			outXmpp.setPassword(inXmpp.getPassword());
			outXmpp.setUsername(inXmpp.getUsername());
			res = outXmpp;
			break;
		default:
			break;
		}
		//
		if (res != null) {
			copyAudit(ds, res);
			//
			res.setEnabled(ds.isEnabled());
			res.setName(ds.getName());
			res.setRegex(ds.getRegex());
			res.setDataSourceValueType(ds.getDataSourceValueType());
			res.setType(ds.getType());
		}
		//
		return res;
	}

	/**
	 * @param ds
	 * @return
	 */
	static DataSource buildDataSource(DataSourceBean ds) {
		return buildDataSource(ds, false);
	}

	/**
	 * @param ds
	 * @param update
	 * @return
	 */
	static DataSource buildDataSource(DataSourceBean ds, boolean update) {
		if (ds == null) {
			return null;
		}
		DataSource res = null;
		if (ds.getId() != null) {
			res = DataSource.findDataSource(ds.getId());
			if (!update) {
				return res;
			}
		}
		switch (ds.getType()) {
		case HTTP:
			DataSourceBeanPollingHttp inHttp = (DataSourceBeanPollingHttp) ds;
			HttpDataSource outHttp = new HttpDataSource();
			if (res != null) {
				outHttp = (HttpDataSource) res;
			}
			outHttp.setPollingInterval(inHttp.getInterval());
			outHttp.setQuery(inHttp.getQuery());
			outHttp.setUrl(inHttp.getUrl());
			if (inHttp.getHttpQueryType() != null) {
				outHttp.setQueryType(HttpQueryType.valueOf(inHttp
						.getHttpQueryType().name()));
			}
			res = outHttp;
			break;
		case RSS:
			DataSourceBeanPollingRss inRss = (DataSourceBeanPollingRss) ds;
			RssDataSource outRss = new RssDataSource();
			if (res != null) {
				outRss = (RssDataSource) res;
			}
			outRss.setPollingInterval(inRss.getInterval());
			outRss.setUrl(inRss.getUrl());
			res = outRss;
			break;
		case TWITTER:
			DataSourceBeanEventTwitter inTwitter = (DataSourceBeanEventTwitter) ds;
			TwitterDataSource outTwitter = new TwitterDataSource();
			if (res != null) {
				outTwitter = (TwitterDataSource) res;
			}
			outTwitter.setScreenNames(inTwitter.getScreenNames());
			res = outTwitter;
			break;
		case XMPP:
			DataSourceBeanEventXmpp inXmpp = (DataSourceBeanEventXmpp) ds;
			XmppDataSource outXmpp = new XmppDataSource();
			if (res != null) {
				outXmpp = (XmppDataSource) res;
			}
			outXmpp.setFromUser(inXmpp.getFromUser());
			outXmpp.setPassword(inXmpp.getPassword());
			outXmpp.setUsername(inXmpp.getUsername());
			res = outXmpp;
			break;
		default:
			break;
		}
		//
		if (res != null) {
			copyAudit(ds, res);
			//
			res.setEnabled(ds.isEnabled());
			res.setName(ds.getName());
			res.setRegex(ds.getRegex());
			if (ds.getDataSourceValueType() != null) {
				res.setValueType(net.jnd.thesis.domain.DataSourceValueType
						.valueOf(ds.getDataSourceValueType().name()));
			}
		}
		//
		return res;
	}

	/**
	 * @param condition
	 * @return
	 * @throws WSException
	 */
	static SessionConditionBean buildSessionConditionBean(
			SessionCondition condition) throws WSException {
		if (condition == null) {
			return null;
		}
		//
		SessionConditionBean bean = new SessionConditionBean();
		copyAudit(condition, bean);
		//
		bean.setDescription(condition.getDescription());
		bean.setConditionOperator(condition.getConditionOperator());
		bean.setConditionValue(condition.getConditionValue());
		bean.setDssDataSource(buildDataSourceBean(condition.getDataSource()));
		return bean;
	}

	/**
	 * @param condition
	 * @param update
	 * @return
	 * @throws WSException
	 */
	static SessionCondition buildSessionCondition(SessionConditionBean condition)
			throws WSException {
		return buildSessionCondition(condition, false);
	}

	/**
	 * @param condition
	 * @return
	 * @throws WSException
	 */
	static SessionCondition buildSessionCondition(
			SessionConditionBean condition, boolean update) throws WSException {
		if (condition == null) {
			return null;
		}
		//
		SessionCondition bean = new SessionCondition();
		if (condition.getId() != null) {
			bean = SessionCondition.findSessionCondition(condition.getId());
			if (!update) {
				return bean;
			}
		}
		copyAudit(condition, bean);
		//
		bean.setValue(condition.getConditionValue());
		if (condition.getConditionOperator() != null) {
			bean.setOperator(ConditionOperator.valueOf(condition
					.getConditionOperator().name()));
		}
		bean.setDescription(condition.getDescription());
		bean.setDataSource(buildDataSource((DataSourceBean) condition
				.getDssDataSource()));
		return bean;
	}

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	static DynamicReconfiguration buildDynamicReconfiguration(
			DynamicReconfigurationBean bean) throws WSException {
		return buildDynamicReconfiguration(bean, false);
	}

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	static DynamicReconfiguration buildDynamicReconfiguration(
			DynamicReconfigurationBean bean, boolean update) throws WSException {
		if (bean == null) {
			return null;
		}
		//
		DynamicReconfiguration reconf = new DynamicReconfiguration();
		if (bean.getId() != null) {
			reconf = DynamicReconfiguration.findDynamicReconfiguration(bean
					.getId());
			if (!update) {
				return reconf;
			}
		}
		copyAudit(bean, reconf);
		//
		reconf.setInteractionModel(buildInteractionModel((InteractionModelBean) bean
				.getDssInteractionModel()));
		reconf.setSessionCondition(buildSessionCondition((SessionConditionBean) bean
				.getDssSessionCondition()));
		reconf.setEsperExpression(buildEsperExpression((EsperExpressionBean) bean
				.getDssEsperExpression()));
		reconf.setEventName(bean.getEventName());
		//
		reconf.setDescription(bean.getDescription());
		//
		reconf.setInteractionModelEnabled(bean.isInteractionModelEnabled());
		reconf.setEsperExpressionEnabled(bean.isEsperExpressionEnabled());
		reconf.setEventNameEnabled(bean.isEventNameEnabled());
		reconf.setSessionConditionEnabled(bean.isSessionConditionEnabled());
		//
		return reconf;
	}

	/**
	 * @param reconf
	 * @return
	 * @throws WSException
	 */
	static DynamicReconfigurationBean buildDynamicReconfigurationBean(
			DynamicReconfiguration reconf) throws WSException {
		if (reconf == null) {
			return null;
		}
		//
		DynamicReconfigurationBean bean = new DynamicReconfigurationBean();
		copyAudit(reconf, bean);
		//
		bean.setDssInteractionModel(buildInteractionModelBean(reconf
				.getInteractionModel()));
		bean.setDssSessionCondition(buildSessionConditionBean(reconf
				.getSessionCondition()));
		bean.setDssEsperExpression(buildEsperExpressionBean(reconf
				.getEsperExpression()));
		//
		bean.setDescription(reconf.getDescription());
		//
		bean.setEventNameEnabled(reconf.isEventNameEnabled());
		bean.setInteractionModelEnabled(reconf.isInteractionModelEnabled());
		bean.setEsperExpressionEnabled(reconf.isEsperExpressionEnabled());
		bean.setSessionConditionEnabled(reconf.isSessionConditionEnabled());
		//
		return bean;
	}

	/**
	 * @param session
	 * @return
	 * @throws WSException
	 */
	static SessionBean buildSessionBean(Session session) throws WSException {
		if (session == null) {
			return null;
		}
		//
		SessionBean bean = new SessionBean();
		copyAudit(session, bean);
		//
		bean.setName(session.getName());
		bean.setRepositoryEnabled(session.isRepositoryEnabled());
		bean.setClientModeAllowed(session.isClientModeAllowed());
		bean.setStatus(session.getStatus());
		bean.setSessionInteractionModel(buildInteractionModelBean(session
				.getInteractionModel()));
		bean.setSessionEsperExpression(buildEsperExpressionBean(session
				.getEsperExpression()));
		// build data sources
		Set<DSSDataSource> dataSources = new LinkedHashSet<DSSDataSource>();
		for (DataSource ds : session.getSessionDataSources()) {
			dataSources.add(buildDataSourceBean(ds));
		}
		bean.setDataSources(dataSources);
		// build dynamic reconfigurations
		Set<DSSDynamicReconfiguration> reconfs = new LinkedHashSet<DSSDynamicReconfiguration>();
		for (DynamicReconfiguration reconf : session
				.getDynamicReconfigurations()) {
			reconfs.add(buildDynamicReconfigurationBean(reconf));
		}
		bean.setDynamicReconfs(reconfs);
		// build session users
		Set<DSSUser> users = new LinkedHashSet<DSSUser>();
		for (AuthUser usr : session.getAllowedUsers()) {
			users.add(buildUserBean(usr));
		}
		bean.setUsers(users);
		return bean;
	}

	/**
	 * @param session
	 * @return
	 * @throws WSException
	 */
	static Session buildSession(SessionBean session) throws WSException {
		return buildSession(session, false);
	}

	/**
	 * @param session
	 * @return
	 * @throws WSException
	 */
	static Session buildSession(SessionBean session, boolean update)
			throws WSException {
		if (session == null) {
			return null;
		}
		//
		Session bean = new Session();
		if (session.getId() != null) {
			bean = Session.findSession(session.getId());
			if (!update) {
				return bean;
			}
		}
		copyAudit(session, bean);
		//
		bean.setName(session.getName());
		bean.setEnableRepository(session.isRepositoryEnabled());
		bean.setClientModelAllowed(Boolean.valueOf(session
				.isClientModeAllowed()));
		//
		if (session.getStatus() != null) {
			bean.setSessionStatus(SessionStatus.valueOf(session.getStatus()
					.name()));
		}
		//
		bean.setInteractionModel(buildInteractionModel((InteractionModelBean) session
				.getSessionInteractionModel()));
		bean.setEsperExpression(buildEsperExpression((EsperExpressionBean) session
				.getSessionEsperExpression()));
		// build data sources
		Set<DataSource> dataSources = new LinkedHashSet<DataSource>();
		for (DSSDataSource ds : session.getDataSources()) {
			dataSources.add(buildDataSource((DataSourceBean) ds));
		}
		bean.setSessionDataSources(dataSources);
		// build dynamic reconfigurations
		Set<DynamicReconfiguration> reconfs = new LinkedHashSet<DynamicReconfiguration>();
		for (DSSDynamicReconfiguration reconf : session.getDynamicReconfs()) {
			reconfs.add(buildDynamicReconfiguration((DynamicReconfigurationBean) reconf));
		}
		bean.setDynamicReconfigurations(reconfs);
		// build session users
		Set<AuthUser> users = new LinkedHashSet<AuthUser>();
		for (DSSUser usr : session.getUsers()) {
			users.add(buildUser((UserBean) usr));
		}
		bean.setAllowedUsers(users);
		return bean;
	}

	/**
	 * @param usr
	 * @return
	 */
	static AuthUser buildUser(UserBean usr) {
		return buildUser(usr, false);
	}

	/**
	 * @param usr
	 * @return
	 */
	static AuthUser buildUser(UserBean usr, boolean update) {
		if (usr == null) {
			return null;
		}
		//
		AuthUser bean = new AuthUser();
		if (usr.getId() != null) {
			bean = AuthUser.findAuthUser(usr.getId());
			if (!update) {
				return bean;
			}
		}
		copyAudit(usr, bean);
		//
		bean.setEnabled(usr.isEnabled());
		bean.setName(usr.getName());
		bean.setUsername(usr.getUsername());
		bean.setPassword(usr.getPassword());
		//
		List<AuthRole> roles = AuthRole.findAllAuthRoles();
		if (usr.isAdmin()) {
			bean.setAuthorities(new HashSet<AuthRole>(roles));
		} else {
			Set<AuthRole> tmp = new HashSet<AuthRole>();
			for (AuthRole authRole : roles) {
				if (!authRole.getAuthority().equals("ROLE_ADMIN")) {
					tmp.add(authRole);
				}
			}
			bean.setAuthorities(tmp);
		}
		//
		return bean;
	}

	/**
	 * @param usr
	 * @return
	 */
	static UserBean buildUserBean(AuthUser usr) {
		if (usr == null) {
			return null;
		}
		//
		UserBean bean = new UserBean();
		copyAudit(usr, bean);
		//
		bean.setEnabled(usr.isEnabled());
		bean.setName(usr.getName());
		bean.setUsername(usr.getUsername());
		bean.setPassword(usr.getPassword());
		bean.setAdmin(usr.getAuthorities().contains("ROLE_ADMIN"));
		return bean;
	}

	/**
	 * @param connection
	 * @return
	 * @throws WSException
	 */
	static UserSessionConnection buildUserSessionConnection(
			UserSessionConnectionBean connection) throws WSException {
		return buildUserSessionConnection(connection, false);
	}

	/**
	 * @param connection
	 * @return
	 * @throws WSException
	 */
	static UserSessionConnection buildUserSessionConnection(
			UserSessionConnectionBean connection, boolean update)
			throws WSException {
		if (connection == null) {
			return null;
		}
		//
		UserSessionConnection conn = new UserSessionConnection();
		if (connection.getId() != null) {
			conn = UserSessionConnection.findUserSessionConnection(connection
					.getId());
			if (!update) {
				return conn;
			}
		}
		copyAudit(connection, conn);
		//
		conn.setUsr(buildUser((UserBean) connection.getConnectionUser()));
		conn.setDescription(connection.getDescription());
		conn.setSession(buildSession((SessionBean) connection
				.getConnectionSession()));
		conn.setConnectionStatus(connection.getConnectionStatus());
		//
		conn.setEsperExpression(buildEsperExpression((EsperExpressionBean) connection
				.getConnectionEsperExpression()));
		//
		conn.setInteractionModel(buildInteractionModel((InteractionModelBean) connection
				.getConnectionInteractionModel()));
		// build dynamic reconfigurations
		if(connection.getDynamicReconfs() != null) {
			Set<DynamicReconfiguration> reconfs = new LinkedHashSet<DynamicReconfiguration>();
			for (DSSDynamicReconfiguration reconf : connection.getDynamicReconfs()) {
				reconfs.add(buildDynamicReconfiguration((DynamicReconfigurationBean) reconf));
			}
			conn.setDynamicReconfigurations(reconfs);
		}
		//
		return conn;
	}

	/**
	 * @param user
	 * @param session
	 * @return
	 */
	static UserSessionConnectionBean buildUserSessionConnectionBean(
			UserSessionConnection connection) throws WSException {
		if (connection == null) {
			return null;
		}
		//
		UserSessionConnectionBean conn = new UserSessionConnectionBean();
		copyAudit(connection, conn);
		//
		conn.setConnectionUser(buildUserBean(connection.getUsr()));
		conn.setDescription(connection.getDescription());
		conn.setConnectionSession(buildSessionBean(connection.getSession()));
		conn.setConnectionStatus(connection.getConnectionStatus());
		//
		conn.setConnectionEsperExpression(buildEsperExpressionBean(connection
				.getEsperExpression()));
		//
		conn.setConnectionInteractionModel(buildInteractionModelBean(connection
				.getInteractionModel()));
		// build dynamic reconfigurations
		Set<DSSDynamicReconfiguration> reconfs = new LinkedHashSet<DSSDynamicReconfiguration>();
		for (DynamicReconfiguration reconf : connection
				.getDynamicReconfigurations()) {
			reconfs.add(buildDynamicReconfigurationBean(reconf));
		}
		conn.setDynamicReconfs(reconfs);
		//
		return conn;
	}

	/**
	 * @param bean
	 * @return
	 */
	static EsperExpressionBean buildEsperExpressionBean(EsperExpression bean) {
		if (bean == null) {
			return null;
		}
		EsperExpressionBean res = new EsperExpressionBean();
		res.setDescription(bean.getDescription());
		res.setEsperExpressionType(bean.getEsperExpressionType());
		res.setExpressionQuery(bean.getExpressionQuery());
		res.setId(bean.getId());
		return res;
	}

	/**
	 * @param bean
	 * @return
	 */
	static EsperExpression buildEsperExpression(EsperExpressionBean bean) {
		return buildEsperExpression(bean, false);
	}

	/**
	 * @param bean
	 * @return
	 */
	static EsperExpression buildEsperExpression(EsperExpressionBean bean,
			boolean update) {
		if (bean == null) {
			return null;
		}
		//
		EsperExpression res = new EsperExpression();
		if (bean.getId() != null) {
			res = EsperExpression.findEsperExpression(bean.getId());
			if (!update) {
				return res;
			}
		}
		copyAudit(bean, res);
		//
		res.setDescription(bean.getDescription());
		res.setExpressionType(net.jnd.thesis.domain.EsperExpressionType
				.valueOf(bean.getEsperExpressionType().name()));
		res.setExpressionQuery(bean.getExpressionQuery());
		return res;
	}

	/**
	 * @param evt
	 * @return
	 */
	static EventBean buildEventBean(Event evt) {
		if (evt == null) {
			return null;
		}
		EventBean res = new EventBean();
		res.setCreationDate(evt.getCreationDate());
		res.setDataSourceId(evt.getDataSourceId());
		res.setDate(evt.getDate());
		if (evt.getType() != null) {
			res.setEventType(EventType.valueOf(evt.getType().name()));
		}
		res.setFloatValue(evt.getFloatValue());
		res.setId(evt.getId());
		res.setSessionId(evt.getSessionId());
		res.setUpdateDate(evt.getUpdateDate());
		res.setValue(evt.getValue());
		res.setValueRaw(evt.getValueRaw());
		return res;
	}

	static Event buildEvent(EventBean evt) {
		// TODO
		return null;
	}

	/**
	 * @param dataSourceId
	 * @param interactionModelId
	 * @param user
	 * @return
	 */
	static Session buildTestDataSourceSession(Long dataSourceId,
			Long interactionModelId, AuthUser user) throws WSException {
		if (user == null) {
			return null;
		}
		// build interaction model
		InteractionModel model = InteractionModel
				.findInteractionModel(interactionModelId);
		if (model == null) {
			throw new WSException("interaction model does not exist...");
		}
		// build data source
		DataSource ds = (DataSource) CacheDataService
				.getDataSource(dataSourceId);
		if (ds == null) {
			throw new WSException("data source does not exist...");
		}
		// build session
		Session s = new Session();
		s.setAllowedUsers(buildSet(user));
		s.setClientModelAllowed(false);
		s.setCreationUser(user);
		s.setInteractionModel(model);
		//
		String sessionName = String.format("USR_%s_DS_%s_%s", user.getId(),
				dataSourceId, Calendar.getInstance().getTimeInMillis());
		s.setName(sessionName);
		s.setDescription(sessionName);
		s.setEnableRepository(false);
		s.setSessionDataSources(buildSet(ds));
		s.setUpdateUser(user);
		//
		EsperExpression exp = new EsperExpression();
		exp.setDescription(sessionName);
		exp.setCreationUser(user);
		exp.setUpdateUser(user);
		exp.persist();
		//
		s.setEsperExpression(exp);
		//
		return s;
	}

	/**
	 * @param user
	 * @param session
	 * @return
	 */
	static UserSessionConnection buildTestUserSessionConnection(AuthUser user,
			Session session) {
		if (session == null) {
			return null;
		}
		UserSessionConnection conn = new UserSessionConnection();
		conn.setUsr(user);
		conn.setSession(session);
		conn.setCreationUser(user);
		conn.setUpdateUser(user);
		return conn;
	}

}
