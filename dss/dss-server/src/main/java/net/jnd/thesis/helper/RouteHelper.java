package net.jnd.thesis.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.TypedQuery;

import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.domain.ConnectionStatus;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.ReplaySession;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionStatus;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.service.DSSDataService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public final class RouteHelper {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(RouteHelper.class);

	/**
	 * @return
	 */
	private static Map<Long, RoutesWrapper> getSessionRoutes() {
		return DSSDataService.getSessionRoutes();
	}

	/**
	 * @return
	 */
	public static Set<Long> getSessionIds() {
		return getSessionRoutes().keySet();
	}

	/**
	 * @param user
	 * @param session
	 * @param test
	 */
	private static boolean initializeRoutes(Session session, boolean test) {
		LOG.debug("initializeRoutes(" + session.getId() + ")" );
		Long sessionId = session.getId();
		// clear existing routes
		clearRoutes(session, true);
		// build session routes
		RoutesWrapper wrapper = new RoutesWrapper(sessionId);
		// build internal consumer
		wrapper.setInternalConsumer(Boolean.TRUE);
		// build producers
		if (session.isReplaying()) {
			ReplaySession replay = null;
			//
			try {
				TypedQuery<ReplaySession> res = ReplaySession
						.findReplaySessionsBySessionIdEquals(session.getId());
				replay = res.getSingleResult();
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			//
			if (replay == null) {
				replay = new ReplaySession();
				replay.setSessionId(session.getId());
				replay.merge();
			} else {
				replay.setEventId(0L);
				replay.merge();
			}
			//
			wrapper.setProducer(0L, Boolean.TRUE);
		} else {
			for (DataSource ds : session.getSessionDataSources()) {
				wrapper.setProducer(ds.getId(), Boolean.TRUE);
			}
		}
		// update the session routes map
		setRoutes(session, wrapper, test);
		// log session start
		DSSLogHelper.sessionStart(session);
		// start the routes
		return wrapper.startContext(true);
	}

	/**
	 * @return
	 */
	public static List<String> dumpRoutes() {
		List<String> routesList = new ArrayList<String>();
		routesList.add("-- ROUTES DUMP START --");
		routesList.add(StringUtils.EMPTY);
		for (RoutesWrapper routes : getSessionRoutes().values()) {
			routesList.add(". SESSION #" + routes.getSessionId());
			routesList.add(StringUtils.EMPTY);
			for (String route : routes.dumpRoutes()) {
				routesList.add(route);
				LOG.debug(String.format("dumpRoutes(): %s", route));
			}
			routesList.add(StringUtils.EMPTY);
		}
		routesList.add(StringUtils.EMPTY);
		routesList.add("-- ROUTES DUMP END --");
		return routesList;
	}

	/**
	 * @param session
	 * @param routesWrapper
	 * @param test
	 */
	private static void setRoutes(Session session, RoutesWrapper routesWrapper,
			boolean test) {
		LOG.debug("setRoutes(" + session.getId() + ", " + routesWrapper + ")");
		clearRoutes(session, true);
		getSessionRoutes().put(session.getId(), routesWrapper);
	}

	/**
	 * @param session
	 */
	private static boolean clearRoutes(Session session, boolean end) {
		LOG.debug(String.format("clearRoutes(%s, %s)", session.getId(), end));
		boolean res = false;
		RoutesWrapper currentRoutes = getSessionRoutes()
				.remove(session.getId());
		if (currentRoutes != null) {
			res = currentRoutes.stopContext(end);
			DSSLogHelper.sessionEnd(session);
			res = true;
		}
		return res;
	}

	/**
	 * @param session
	 * @param test
	 */
	public static String startSession(Long sessionId) {
		LOG.debug(String.format("startSession(%s)", sessionId));
		String res = StringUtils.EMPTY;
		Session session = CacheDataService.getSession(sessionId);
		switch (session.getStatus()) {
		case STARTED:
			res = "The session has already started...";
			break;
		case ENDED:
			res = "The session has already ended...";
			break;
		case NEW:
			if (StringUtils.isBlank(res)) {
				CacheDataService.updateSessionStatus(session,
						SessionStatus.STARTED);
			}
			if (!initializeRoutes(session, Boolean.FALSE)) {
				res = "An error ocurred when trying to initialize the session...";
				// CacheDataService
				// .updateSessionStatus(session, SessionStatus.NEW);
			}
			break;
		case PAUSED:
			if (StringUtils.isBlank(res)) {
				CacheDataService.updateSessionStatus(session,
						SessionStatus.STARTED);
			}
			RoutesWrapper routesWrapper = getSessionRoutes().get(sessionId);
			boolean ok = Boolean.FALSE;
			if (routesWrapper == null) {
				LOG.debug(String.format("startSession(%s) - RESUMING...",
						sessionId));
				ok = initializeRoutes(session, Boolean.FALSE);
			} /*
			 * else { LOG.debug(String.format("startSession(%s) - STARTING...",
			 * sessionId)); ok = routesWrapper.startContext(true); }
			 */
			//
			if (!ok) {
				res = "An error ocurred when trying to resume the session...";
				// CacheDataService.updateSessionStatus(session,
				// SessionStatus.PAUSED);
			}
			break;
		default:
			res = "The session has an invalid status...";
			break;
		}
		// if successful, update session status
		if (StringUtils.isBlank(res)) {
			CacheDataService
					.updateSessionStatus(session, SessionStatus.STARTED);
		}
		return res;
	}

	/**
	 * @param session
	 * @param test
	 */
	public static String stopSession(Long sessionId) {
		LOG.debug(String.format("stopSession(%s)", sessionId));
		String res = StringUtils.EMPTY;
		Session session = CacheDataService.getSession(sessionId);
		// switch (session.getStatus()) {
		// case STARTED:
		// case ENDED:
		/*
		 * case ENDED: res = "The session has ended..."; break; case PAUSED: res
		 * = "The session is already stopped..."; break;
		 */
		// case PAUSED:
		if (!clearRoutes(session, true)) {
			res = "An error occurred while trying to stop the session...";
		}
		// break;
		// case NEW:
		// res = "The session was not yet initialized...";
		// break;
		// default:
		// res = "The session has an invalid status...";
		// break;
		// }
		// if successful, update session status
		// if (StringUtils.isBlank(res)) {
		CacheDataService.updateSessionStatus(session, SessionStatus.PAUSED);
		// }
		return res;
	}

	/**
	 * @param connectionId
	 */
	public static String openUserSessionConnection(Long connectionId) {
		LOG.debug(String.format("openUserSessionConnection(%s)", connectionId));
		String res = StringUtils.EMPTY;
		UserSessionConnection connection = CacheDataService
				.getConnection(connectionId);
		//
		boolean hasAccess = false;
		Session session = connection.getSession();
		for (DSSUser user : session.getUsers()) {
			if (user.getId() == connection.getConnectionUser().getId()) {
				hasAccess = true;
				break;
			}
		}
		//
		if (hasAccess) {
			RoutesWrapper routesWrapper = getSessionRoutes().get(
					session.getId());
			// if the session has been initialized, proceed
			if (routesWrapper != null
					&& session.getStatus() != net.jnd.thesis.common.enums.SessionStatus.NEW) {
				// create user consumer
				if (!routesWrapper.setUserConsumer(connection, Boolean.TRUE)) {
					res = "An error occurred while trying to open the user connection...";
				}
			} else {
				res = "The session is not initialized...";
			}
		} else {
			res = "The user does not belong to this session...";
		}
		// update connection status
		if (StringUtils.isBlank(res)) {
			CacheDataService.updateConnectionStatus(connection,
					ConnectionStatus.OPEN);
		}
		return res;
	}

	/**
	 * @param connectionId
	 */
	public static String closeUserSessionConnection(Long connectionId) {
		LOG.debug(String.format("closeUserSessionConnection(%s)", connectionId));
		String res = StringUtils.EMPTY;
		UserSessionConnection connection = CacheDataService
				.getConnection(connectionId);
		RoutesWrapper routesWrapper = getSessionRoutes().get(
				connection.getConnectionSession().getId());
		// if (routesWrapper != null
		// && connection.getConnectionSession().getStatus() !=
		// net.jnd.thesis.common.enums.SessionStatus.NEW) {
		if (routesWrapper == null
				|| !routesWrapper.stopUserConsumer(connection
						.getConnectionUser().getId(), true)) {
			res = "An error occurred while trying to close the user connection...";
		}
		// } else {
		// res = "The session is not initialized...";
		// }
		// update connection status
		// if (StringUtils.isBlank(res)) {
		CacheDataService.updateConnectionStatus(connection,
				ConnectionStatus.CLOSED);
		// }
		return res;
	}

	/**
	 * @param session
	 */
	public static void updateSessionRoutes(Session session, boolean dataSources,
			boolean users, boolean internal) {
		
		LOG.debug( RouteHelper.class.getName() +  " -> "
				+ "void updateSessionRoutes(" + session.getId() + ", " 
				+ dataSources + ", " + users + ", " + internal +")");
		
		if (SessionStatus.STARTED.name().equals(session.getStatus().name())) {
			RoutesWrapper sessionRoutes = getSessionRoutes().get(
					session.getId());
			if (sessionRoutes != null) {
				// update the sessions if necessary
				sessionRoutes.updateSessionRoutes(session, dataSources, users,
						internal);
			}
		}
	}

	/**
	 * @param connection
	 */
	public static void updateConnectionRoutes(UserSessionConnection connection) {
		LOG.info(String.format("updateConnectionRoutes(%s)",
				connection.getId()));
		if (ConnectionStatus.OPEN.equals(connection.getConnectionStatus())) {
			RoutesWrapper routes = RouteHelper.getSessionRoutes().get(
					connection.getId());
			if (routes != null) {
				// update the sessions if necessary
				routes.updateConnectionRoutes(connection);
			}
		}
	}

}
