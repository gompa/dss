package net.jnd.thesis.helper;

import net.jnd.thesis.camel.bean.CamelInternalEvent;
import net.jnd.thesis.common.enums.EsperExpressionType;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public interface Constants {

	public static final EsperExpressionType DEFAULT_TYPE = EsperExpressionType.EQL;
	public static final String DEFAULT_QUERY = "select e.dataSource, e.rawValue, e.processedValue, e.floatValue from pattern [every e="
			+ CamelInternalEvent.class.getName() + "]";

}
