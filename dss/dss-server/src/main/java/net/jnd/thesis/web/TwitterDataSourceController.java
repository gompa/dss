package net.jnd.thesis.web;

import net.jnd.thesis.domain.TwitterDataSource;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/twitterdatasources")
@Controller
@RooWebScaffold(path = "twitterdatasources", formBackingObject = TwitterDataSource.class)
public class TwitterDataSourceController {
}
