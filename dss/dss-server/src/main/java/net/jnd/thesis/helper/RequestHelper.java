/**
 * 
 */
package net.jnd.thesis.helper;

import javax.servlet.http.HttpServletRequest;

import net.jnd.thesis.common.enums.RequestParams;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.service.CacheDataService;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public final class RequestHelper {

	/**
	 * @param fromCache
	 * @return
	 */
	public static AuthUser getCurrentUser(boolean fromCache) {
		AuthUser res = null;
		SecurityContext ctx = SecurityContextHolder.getContext();
		if (ctx != null) {
			Authentication authentication = ctx.getAuthentication();
			if (authentication != null) {
				Object principal = authentication.getPrincipal();
				if(principal instanceof AuthUser) {
					res = (AuthUser) principal;
					if (fromCache) {
						res = (AuthUser) CacheDataService.getUser(res.getId());
					}
				} else {
					System.out.println(principal);
				}
			}
		}
		return res;
	}

	/**
	 * @param request
	 * @return
	 */
	public static Long getSessionId(HttpServletRequest request) {
		Long res = null;
		String id = request.getParameter(RequestParams.SESSION_ID.name());
		if (StringUtils.isNotBlank(id) && StringUtil.isNumeric(id)) {
			res = new Long(id);
		}
		return res;
	}

	/**
	 * @param request
	 * @return
	 */
	public static Long getUserSessionConnectionId(HttpServletRequest request) {
		Long res = null;
		String id = request
				.getParameter(RequestParams.USER_SESSION_CONNECTION_ID.name());
		if (StringUtils.isNotBlank(id) && StringUtil.isNumeric(id)) {
			res = new Long(id);
		}
		return res;
	}

	/**
	 * @param uiModel
	 * @param msg
	 */
	public static void saveUserMessage(Model uiModel, String msg) {
		String currentMsg = (String) uiModel.asMap().get(
				RequestParams.USER_MESSAGE.name());
		if (StringUtils.isNotEmpty(currentMsg)) {
			msg = currentMsg + "<br/>" + msg;
		}
		uiModel.addAttribute(RequestParams.USER_MESSAGE.name(), msg);
	}

}
