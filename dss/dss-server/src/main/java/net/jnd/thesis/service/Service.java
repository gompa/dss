package net.jnd.thesis.service;

import net.jnd.thesis.common.exception.ServiceException;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * Represents a service that can be run by the server. 
 * <p>
 * All services must be specified in the 
 * "dss/dss-server/src/main/resources/META-INF/spring/applicationContext.xml" 
 * file. 
 * <p>
 * A special service worth mentioning is the 
 * {@link InitializationService InitializationService}, which is responsible for
 * launching all the startup services. If you wish to add a start-up service, 
 * you have to add it to the InitializationService class and call its 
 * {@link #start() start()} method there.
 * 
 * @see		InitializationService
 * @since 	1.1.1
 * */
public interface Service {
	
	/**
	 * Starts a service or throws {@link ServiceException ServiceException} if 
	 * a problem that prevents its start arises.
	 * 
	 * @throws	ServiceException	If a critical error has occurred and this 
	 * 								Service cannot start.
	 * */
	public void start() throws ServiceException;
	
	/**
	 * Stops a service or throws {@link ServiceException ServiceException} if 
	 * a problem that prevents it from stopping gracefully arises. Please notice
	 * that due the nature of exceptions, the service will still stop, but since 
	 * that is not going to happen naturally, there may be cleaning up problems 
	 * and inconsistent states.
	 * 
	 * @throws	ServiceException	If a critical error has occurred and this 
	 * 								Service cannot stop gracefully.
	 * */
	public void stop() throws ServiceException;
	
}
