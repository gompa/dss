/**
 * 
 */
package net.jnd.thesis.domain;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
public enum EsperExpressionType {
	PATTERN, EQL;
}
