/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.DynamicReconfiguration;
import net.jnd.thesis.domain.EsperExpression;
import net.jnd.thesis.domain.InteractionModel;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionCondition;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.ws.services.exception.UserNotAuthenticatedException;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author João Domingos
 * @since Jan 27, 2013
 */
public abstract class ServerSideListGetServices extends ServerSideCoreServices {

	@SuppressWarnings("unused")
	private static final Log LOG = LogFactory
			.getLog(ServerSideListGetServices.class);

	@Override
	public Collection<DSSUser> listUsers() throws WSException {
		if (isAuthenticated()) {
			Collection<DSSUser> res = new LinkedHashSet<DSSUser>();
			if (this.user.isAdministrator()) {
				List<AuthUser> authUsers = AuthUser.findAllAuthUsers();
				for (AuthUser usr : authUsers) {
					res.add(ServerSideServicesHelper.buildUserBean(usr));
				}
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Collection<DSSDataSource> listDataSources() throws WSException {
		if (isAuthenticated()) {
			Collection<DSSDataSource> res = new LinkedHashSet<DSSDataSource>();
			List<DataSource> dataSources = DataSource
					.findDataSourcesByCreationUser(this.user).getResultList();
			for (DataSource ds : dataSources) {
				res.add(ServerSideServicesHelper.buildDataSourceBean(ds));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Collection<DSSSessionCondition> listSessionConditions()
			throws WSException {
		if (isAuthenticated()) {
			List<SessionCondition> sessionConditions = SessionCondition
					.findSessionConditionsByCreationUser(this.user)
					.getResultList();
			Collection<DSSSessionCondition> res = new LinkedHashSet<DSSSessionCondition>();
			for (SessionCondition condition : sessionConditions) {
				res.add(ServerSideServicesHelper
						.buildSessionConditionBean(condition));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Collection<DSSInteractionModel> listInteractionModels()
			throws WSException {
		if (isAuthenticated()) {
			Collection<DSSInteractionModel> res = new LinkedHashSet<DSSInteractionModel>();
			for (InteractionModel model : InteractionModel
					.findInteractionModelsByCreationUser(this.user)
					.getResultList()) {
				res.add(ServerSideServicesHelper
						.buildInteractionModelBean(model));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Collection<DSSDynamicReconfiguration> listDynamicReconfigurations()
			throws WSException {
		if (isAuthenticated()) {
			Collection<DSSDynamicReconfiguration> res = new LinkedHashSet<DSSDynamicReconfiguration>();
			for (DynamicReconfiguration reconf : DynamicReconfiguration
					.findDynamicReconfigurationsByCreationUser(this.user)
					.getResultList()) {
				res.add(ServerSideServicesHelper
						.buildDynamicReconfigurationBean(reconf));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.ws.services.WSServer#listSessions()
	 */
	@Override
	public Collection<DSSSession> listSessions() throws WSException {
		if (isAuthenticated()) {
			Collection<DSSSession> res = new LinkedHashSet<DSSSession>();
			for (Session session : Session.findAllSessionsByUser(this.user)) {
				res.add(ServerSideServicesHelper.buildSessionBean(session));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.ws.services.WebSocketServices#listEsperExpressions()
	 */
	@Override
	public Collection<DSSEsperExpression> listEsperExpressions()
			throws WSException {
		if (isAuthenticated()) {
			Collection<DSSEsperExpression> res = new LinkedHashSet<DSSEsperExpression>();
			for (EsperExpression bean : EsperExpression
					.findEsperExpressionsByCreationUser(user).getResultList()) {
				res.add(ServerSideServicesHelper.buildEsperExpressionBean(bean));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Collection<DSSUserSessionConnection> listUserSessionConnections()
			throws WSException {
		if (isAuthenticated()) {
			Collection<DSSUserSessionConnection> res = new LinkedHashSet<DSSUserSessionConnection>();
			for (UserSessionConnection bean : UserSessionConnection
					.findUserSessionConnectionsByCreationUser(user)
					.getResultList()) {
				res.add(ServerSideServicesHelper
						.buildUserSessionConnectionBean(bean));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Collection<DSSSession> listOwnedSessions() throws WSException {
		if (isAuthenticated()) {
			Collection<DSSSession> res = new LinkedHashSet<DSSSession>();
			for (Session session : Session
					.findSessionsByCreationUser(this.user).getResultList()) {
				res.add(ServerSideServicesHelper.buildSessionBean(session));
			}
			return res;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public DSSSession getSession(Long sessionId) throws WSException {
		if (isAuthenticated()) {
			Session session = Session.findSession(sessionId);
			if (session == null) {
				throw new WSException("session does not exist...");
			}
			return ServerSideServicesHelper.buildSessionBean(session);
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public DSSUserSessionConnection getConnection(Long connectionId)
			throws WSException {
		if (isAuthenticated()) {
			UserSessionConnection connection = UserSessionConnection
					.findUserSessionConnection(connectionId);
			if (connection == null) {
				throw new WSException("connection does not exist...");
			}
			return (DSSUserSessionConnection) ServerSideServicesHelper
					.buildUserSessionConnectionBean(connection);
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

}
