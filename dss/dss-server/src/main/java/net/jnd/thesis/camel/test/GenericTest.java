/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.jnd.thesis.camel.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.main.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public abstract class GenericTest extends RouteBuilder implements Runnable {

	private static final Logger LOG = LoggerFactory
			.getLogger(GenericTest.class);

	private boolean runTests = Boolean.FALSE;

	@Override
	public void run() {
		printHello();
		final Main main = new Main();
		main.enableHangupSupport();
		main.addRouteBuilder(this);
		try {
			runCamel(main);
			if (this.runTests) {
				runTests(main);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * @param main
	 */
	private void runCamel(final Main main) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					main.run();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					System.exit(-1);
				}
			}
		}, "CAMEL_TEST_RUN_CAMEL").start();
	}

	/**
	 * @param main
	 */
	private void runTests(final Main main) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!main.isStarted()) {
					LOG.debug("main not started yet, going to sleep");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						LOG.error(e.getMessage(), e);
					}
				}
				runCustomTests(getContext());
			}
		}, "CAMEL_TEST_RUN_TESTS").start();
	}

	/**
	 * 
	 */
	private void printHello() {
		System.out.println("\n\n\n\n");
		System.out.println("===============================================");
		System.out.println("Open your web browser on http://localhost:9090");
		System.out.println("Press ctrl+c to stop this example");
		System.out.println("===============================================");
		System.out.println("\n\n\n\n");
	}

	/**
	 * @param testClass
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends GenericTest> void start(Class<T> testClass)
			throws InstantiationException, IllegalAccessException {
		new Thread(testClass.newInstance(), "CAMEL_TEST_MAIN").start();
	}

	/**
	 * 
	 */
	protected void runCustomTests(CamelContext ctx) {
		// default, do nothing
	}

	/**
	 * @param runTests
	 */
	public void setRunTests(boolean runTests) {
		this.runTests = runTests;
	}

	/**
	 * @return
	 */
	public static String getFormattedTime() {
		return new SimpleDateFormat("'['yyyy-MM-dd HH:MM:ss'] '")
				.format(new Date());
	}

}
