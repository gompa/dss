/**
 * 
 */
package net.jnd.thesis.camel.route;

import java.util.Collection;
import java.util.Date;

import net.jnd.thesis.camel.bean.CamelInternalEvent;
import net.jnd.thesis.camel.component.EsperComponentHelper;
import net.jnd.thesis.common.enums.EsperExpressionType;
import net.jnd.thesis.common.interfaces.DSSEvent;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.domain.DynamicReconfiguration;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.InteractionModel;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionCondition;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.helper.UserSessionConnectionHelper;
import net.jnd.thesis.service.CacheDataService;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.model.ExpressionNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.event.map.MapEventBean;

/**
 * @author João Domingos
 * @since Nov 4, 2012
 */
public class EsperEventConsumerUser extends EsperEventConsumer {

	private static final Logger LOG = LoggerFactory
			.getLogger(EsperEventConsumerUser.class);

	private Long userSessionConnectionId;

	private Long userId;

	private Long sessionId;

	/**
	 * 
	 */
	private EsperExpressionType esperExpressionType = EsperExpressionType.EQL;

	/**
	 * 
	 */
	private String esperExpressionQuery = "select * from pattern [every e="
			+ CamelInternalEvent.class.getName() + "]";

	/**
	 * @param ctx
	 * @param connection
	 * @param test
	 */
	public EsperEventConsumerUser(CamelContext ctx,
			DSSUserSessionConnection connection, boolean test) {
		super(ctx, connection.getConnectionSession().getId(), test);
		this.userSessionConnectionId = connection.getId();
		this.userId = connection.getConnectionUser().getId();
		this.sessionId = connection.getConnectionSession().getId();
		this.esperExpressionQuery = connection.getConnectionEsperExpression()
				.getExpressionQuery();
		this.esperExpressionType = connection.getConnectionEsperExpression()
				.getEsperExpressionType();
	}

	/**
	 * @return the userSessionConnectionId
	 */
	public Long getUserSessionConnectionId() {
		return userSessionConnectionId;
	}

	@Override
	protected void appendToRouteSpecific(ExpressionNode route) {
		super.appendToRouteSpecific(route);
	}

	@Override
	protected final EsperExpressionType getEsperExpressionType() {
		return esperExpressionType;
	}

	@Override
	protected final String getEsperExpressionQuery() {
		String query = esperExpressionQuery;
		if (esperExpressionQuery.contains(")]")) {
			query = query.replace("(", "((").replace(")", "))");
			query = query.replace("))]", String.format(
					") and session.id=%s and session.id<%s)]", sessionId,
					new Date().getTime()));
		} else {
			query = query.replace("]", String.format(
					"(session.id=%s and session.id<%s)]", sessionId,
					new Date().getTime()));
		}
		return query;
	}

	@Override
	protected String getEsperName() {
		return EsperComponentHelper.getEsperNameUser(getSessionId(),
				this.userId, isTest());
	}

	@Override
	protected Processor getProcessor() {
		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				
				DefaultMessage msg = (DefaultMessage) exchange.getIn();
				MapEventBean body = (MapEventBean) msg.getBody();
				
				// build event
				final Event event = buildEventFromProperties(body);
				msg.setBody(event, event.getClass());
				
				LOG.debug("TIME_CHECK_P3_START " +  event.getValue() + " " + System.currentTimeMillis() + " " + event.getSessionId());
				// build recipient list header
				msg.setHeader(HEADER_DELAY, 0L);
				
				//DEBUG: delete tracker 
				try{
					LOG.debug("DEBUG_PATH EsperEventConsumerUser, process(), from route: " + getFrom().from());
					LOG.debug("DEBUG_PATH EsperEventConsumerUser, process(), threadId: " + Thread.currentThread().getId());
					LOG.debug("DEBUG_PATH EsperEventConsumerUser, process(), messageId: " + msg.getMessageId());
					LOG.debug("DEBUG_PATH EsperEventConsumerUser, process(), eventId: " + event.getId());
					LOG.debug("DEBUG_PATH EsperEventConsumerUser, process(), eventCreationDate: " + event.getCreationDate());
				}catch(Exception e){
					e.printStackTrace();
				}
				
				//DEBUG debug
				LOG.debug("msg.getBOdy()= " + body);
				
				// get session
				Session session = CacheDataService.getSession(getSessionId());
				// get connection
				final UserSessionConnection conn = CacheDataService
						.getConnection(getUserSessionConnectionId());
				// validate event & interaction model
				boolean eventOk = true;
				// fetch interaction model
				InteractionModel interactionModel = (session
						.isClientModeAllowed() && (conn.getInteractionModel() != null)) ? conn
						.getInteractionModel() : session.getInteractionModel();
				
				// process pattern related operations
				switch (interactionModel.getPattern()) {
				case PUBLISHER_SUBSCRIBER:
					DSSSessionCondition condition = interactionModel
							.getDssSessionCondition();
					eventOk = EsperEventConsumerHelper.checkCondition(
							condition, event);
					break;
				case PRODUCER_CONSUMER:
					msg.setHeader(HEADER_DELAY,
							interactionModel.getEventDelay());
					break;
				default:
					break;
				}
				// if the event is ok, proceed
				if (eventOk) {
					// broadcast event
					// Event evt = buildEvent(event);
					UserSessionConnectionHelper.broadcast(conn, event);

					// launch dynamic reconfigurations
					new Thread(new Runnable() {
						@Override
						public void run() {
							// process dynamic reconfigurations
							handleDynamicReconfigurations(conn, event);
						}
					});
				}
				LOG.debug("TIME_CHECK_P3_END " +  event.getValue() + " " + System.currentTimeMillis() + " " + event.getSessionId());
			}
			
		};

	}

	/**
	 * @param session
	 * @param event
	 */
	private void handleDynamicReconfigurations(UserSessionConnection conn,
			DSSEvent event) {
		Collection<DynamicReconfiguration> reconfs = conn
				.getDynamicReconfigurations();
		// check all dynamic reconfigurations
		for (DynamicReconfiguration reconf : reconfs) {
			SessionCondition condition = reconf.getSessionCondition();
			// check if dynamic reconfiguration is triggered
			if (reconf.getSessionConditionEnabled()
					&& EsperEventConsumerHelper
							.checkCondition(condition, event)) {
				Long newInteractionModelId = null;
				Long newEsperExpressionId = null;
				// check model modifications
				if (reconf.getInteractionModelEnabled()) {
					if (!conn.getInteractionModel().getId()
							.equals(reconf.getInteractionModel().getId())) {
						newInteractionModelId = reconf.getInteractionModel()
								.getId();
					}
				}
				// check esper modifications
				if (reconf.getEsperExpressionEnabled()) {
					if (!conn.getEsperExpression().equals(
							reconf.getEsperExpression().getId())) {
						newEsperExpressionId = reconf.getEsperExpression()
								.getId();
					}
				}
				// update connection route
				if (newInteractionModelId != null
						|| newEsperExpressionId != null) {
					CacheDataService.updateConnectionInteractionModel(conn,
							newInteractionModelId, newEsperExpressionId);
					break;
				}

			}

		}

	}

}
