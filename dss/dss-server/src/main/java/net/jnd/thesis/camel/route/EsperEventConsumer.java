package net.jnd.thesis.camel.route;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import net.jnd.thesis.camel.bean.CamelInternalEvent;
import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.camel.component.EsperComponentHelper;
import net.jnd.thesis.common.enums.EsperExpressionType;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.EventType;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.helper.LogHelper;
import net.jnd.thesis.service.CacheDataService;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.model.DelayDefinition;
import org.apache.camel.model.ExpressionNode;
import org.apache.camel.model.FilterDefinition;
import org.apache.camel.model.RouteDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.client.PropertyAccessException;
import com.espertech.esper.event.map.MapEventBean;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * Helps define routes that will consume data from Esper queues, and then send 
 * it to other Esper queues. These routes are used in two different situations:
 * <ol>
 * <li>	
 * 		To consume data from the session messaging layer and then send it to the
 *  	client messaging layer. This is done by consuming data from 
 * 		the session queues, which are configured by the 
 * 		{@link EsperEventProducer} abstract class and its concrete children, and 
 * 		then sending it to the specific client Esper queues.
 * </li>
 * <li>
 * 		To consume data from the client messaging layer and then send it to the 
 * 		client interface for dissemination. This is done by consuming data from 
 * 		the specific client Esper queues and sending it to the clients through 
 * 		the concrete class {@link EsperEventConsumerInternal}, which then uses 
 * 		the {@link net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocket} class to
 * 		actually broadcast the message to all the clients themselves.
 * </li>
 * </ol>
 * <p>
 * The configuration of the routes is done in the {@link #configure()} method.
 * <p>
 * This class is extended by EsperEventConsumerInternal and 
 * {@link EsperEventConsumerUser}. It is also used by 
 * {@link EsperEventConsumerFactory} to create EsperEventConsumerInternal and 
 * EsperEventConsumerUser instances as well.
 * 
 * @see		#configure()
 * @see		EsperEventProducer
 * @see		EsperEventConsumerInternal
 * @see		EsperEventConsumerUser
 * @see		EsperEventConsumerFactory 
 * @since	1.2.1
 */
public abstract class EsperEventConsumer extends GenericRouteBuilder {

	private static final Logger LOG = LoggerFactory
			.getLogger(EsperEventConsumer.class);

	public static final String HEADER_RECIPIENT_LIST = "HEADER_RECIPIENT_LIST";

	public static final String DELIMITER_RECIPIENT_LIST = ",";

	/**
	 * @param ctx
	 * @param session
	 * @param test
	 */
	public EsperEventConsumer(CamelContext ctx, Long sessionId, boolean test) {
		/*
		 * TODO: remove this constructor and use the more complete constructor
		 * passing the session expression type and expression query
		 */
		super(ctx, sessionId, test);
		//
	}

	@Override
	public final void configure() throws Exception {
		// start route definition
		RouteDefinition route = from(getFrom().from());

		LOG.debug("DEBUG EsperEventConsumer, configure(),from route: " + getFrom().from());
		LOG.debug("DEBUG EsperEventConsumer, configure(), threadId: " + Thread.currentThread().getId());
		
		// apply pre filter
		FilterDefinition filterDefinition = route.filter().method(
				getPreFilterClass() != null ? getPreFilterClass()
						: EsperEventConsumerFilter.class, FILTER_METHOD_NAME);
		
		// apply main processor
		ExpressionNode expressionNode = filterDefinition.process(getProcessor());

		// process delay if necessary
		DelayDefinition delayDefinition = expressionNode
				.delay(header(HEADER_DELAY));

		// apply pos filter
		filterDefinition = delayDefinition.filter().method(
				getPosFilterClass() != null ? getPosFilterClass()
						: EsperEventConsumerFilter.class, FILTER_METHOD_NAME);

		// add empty processor at the end of the route, camel needs this...
		expressionNode = filterDefinition.process(new Processor() {
			@Override
			public void process(Exchange paramExchange) throws Exception {
				// empty processor, do nothing
				// a processor is required after a filter
			}
		});

		// append specific route rules if necessary
		appendToRoute(expressionNode);
	}

	/**
	 * 
	 */
	public class EsperEventDelayer {

		/**
		 * @return
		 */
		public long customDelay() {
			return 0;
		}

	}

	/**
	 * 
	 */
	public static class EsperEventConsumerFilter {

		/**
		 * @param feed
		 * @return
		 */
		public boolean filter(Object event) {
			return Boolean.TRUE;
		}
	}

	/**
	 * Specify the Esper expression used to consume events.
	 * 
	 * @return	the esperExpressionQuery
	 */
	protected abstract String getEsperExpressionQuery();

	/**
	 * Specify the Esper queue name from which this route will consume events.
	 * 
	 * @return	the full path of the Esper queue from which this route will
	 * 			consume events.
	 */
	protected abstract String getEsperName();

	/**
	 * Specify the Esper expression that will be used to filter the events 
	 * consumed from the Esper queue returned by {@link #getEsperName() getEsper}
	 * method.
	 * 
	 * @see	#getEsperName()
	 */
	protected abstract EsperExpressionType getEsperExpressionType();

	//INFO: Esper Filter applied by this method
	@Override
	protected final ComponentHelper getFrom() {
		return new EsperComponentHelper(getContext(), getEsperName(),
				getEsperName(), getEsperExpressionType(),
				getEsperExpressionQuery());
	}

	/**
	 * @param body
	 * @return
	 */
	protected CamelInternalEvent buildInternalEventFromProperties(MapEventBean body) {

		CamelInternalEvent res = null;

		try {

			Object e = null;
			Object datasource = null;
			Object exp = null;

			try {
				e = body.get("e");

				LogHelper.debug(LOG, "got \"e\" just fine: " + e);
			} catch (PropertyAccessException ex) {
				LOG.error("Exception in buildInternalEventFromProperties(MapEventBean body) at e = body.get(\"e\"): " + ex.getMessage(), e);
			}

			try {
				datasource = body.get("datasource");

				LogHelper.debug(LOG, "got datasource just fine: " + datasource);
			} catch (PropertyAccessException ex) {
				LOG.error("Exception in buildInternalEventFromProperties(MapEventBean body) at datasource = body.get(\"datasource\"): " + ex.getMessage(), datasource);
			}

			try {
				exp = body.get("exp");

				LogHelper.debug(LOG, "got \"exp\" just fine: " + exp);
			} catch (PropertyAccessException ex) {
				LOG.error("Exception in buildInternalEventFromProperties(MapEventBean body) at exp = body.get(\"exp\"): " + ex.getMessage(), exp);
			}


			if (e != null && e instanceof CamelInternalEvent) {
				res = (CamelInternalEvent) e;
			} else if (datasource != null) {

				// process other aggregations
				String val = null;
				String desc = null;
				Map<String, Object> props = body.getProperties();

				net.jnd.thesis.common.enums.EventType eventType = net.jnd.thesis.common.enums.EventType.DATA;

				LogHelper.debug(LOG, "entering for loop:");

				for (Entry<String, Object> entry : props.entrySet()) {
					LogHelper.debug(LOG, "objetc: " + entry);


					if (entry.getKey().contains("avg(")) {
						eventType = net.jnd.thesis.common.enums.EventType.DATA_AVG;
					} else if (entry.getKey().contains("count(")) {
						eventType = net.jnd.thesis.common.enums.EventType.DATA_COUNT;
					} else if (entry.getKey().contains("min(")) {
						eventType = net.jnd.thesis.common.enums.EventType.DATA_MIN;
					} else if (entry.getKey().contains("max(")) {
						eventType = net.jnd.thesis.common.enums.EventType.DATA_MAX;
					} else if (entry.getKey().contains("sum(")) {
						eventType = net.jnd.thesis.common.enums.EventType.DATA_SUM;
					}
					//
					if (eventType != net.jnd.thesis.common.enums.EventType.DATA) {
						val = String.valueOf(entry.getValue());
						desc = String.valueOf(entry.getKey());
						break;
					}
				}

				if (val != null && desc != null) {

					String dataSourceName = String.valueOf(datasource);
					DataSource ds = DataSource.findDataSourcesByNameEquals(
							dataSourceName).getSingleResult();

					LOG.info("printing ds " + ds);

					if (ds != null) {
						Date d = new Date();
						CamelInternalEvent evt = new CamelInternalEvent();
						evt.setCreationDate(d);
						evt.setDataSourceId(ds.getId());

						LOG.info("setting camel evt " + evt);

						try {
							evt.setFloatValue(Float.valueOf(val));
						} catch (Exception ex) {
							LOG.error("Cannot set float value: "+ 
									ex.getMessage(), e);
						}

						evt.setDate(new Date());
						evt.setName(ds.getName());
						evt.setUpdateDate(d);
						evt.setType(eventType);
						evt.setValue(val);
						evt.setValueRaw(desc);
						res = evt;
					}

				}
				/* If it is not and event 'e' nor a 'datasource' then we treat 
				 * it like simple data*/
			}else if(exp != null){
				LOG.info("setting exp event with exp: " + exp);

				net.jnd.thesis.common.enums.EventType eventType = 
						net.jnd.thesis.common.enums.EventType.DATA;

				Date d = new Date();
				CamelInternalEvent evt = new CamelInternalEvent();
				evt.setCreationDate(d);

				LOG.info("setting camel evt " + evt);

				try {
					evt.setFloatValue(((Double) exp).floatValue());
				} catch (Exception ex) {
					LOG.error("Cannot set float value: "+ 
							ex.getMessage());
				}

				evt.setDate(new Date());
				evt.setUpdateDate(d);
				evt.setType(eventType);
				evt.setValue(String.valueOf(exp));
				evt.setValueRaw(String.valueOf(exp));
				res = evt;
			}

		} catch (Exception e) {
			LOG.error("General overall exception on "
					+ "buildInternalEventFromProperties(MapEventBean body): " 
					+ e.getMessage(), e);
		}

		LOG.info("Printing buildInternalEventFromProperties(MapEventBean body) res: " + res);
		return res;
	}

	/**
	 * @param body
	 * @return
	 */
	protected Event buildEventFromProperties(MapEventBean body) {

		Event res = null;

		try {

			Object e = null;
			Object datasource = null;
			Object exp = null;

			try {
				e = body.get("e");
			} catch (PropertyAccessException ex) {
				LOG.error("Exception in buildEventFromProperties(MapEventBean body) at datasource = body.get(\"datasource\"): " + ex.getMessage(), e);
			}

			try {
				datasource = body.get("datasource");
			} catch (PropertyAccessException ex) {
				LOG.error("Exception in buildEventFromProperties(MapEventBean body) at datasource = body.get(\"datasource\"): " + ex.getMessage(), datasource);
			}

			try {
				exp = body.get("exp");

				//DEBUG: delete log print
				LogHelper.debug(LOG, "got \"exp\" just fine: " + exp);
			} catch (PropertyAccessException ex) {
				LOG.error("Exception in buildEventFromProperties(MapEventBean body) at exp = body.get(\"exp\"): " + ex.getMessage(), exp);
			}


			if (e != null && e instanceof Event) {
				res = (Event) e;
			} else if (datasource != null) {

				// process other aggregations
				String val = null;
				String desc = null;
				Map<String, Object> props = body.getProperties();

				EventType eventType = EventType.DATA;

				for (Entry<String, Object> entry : props.entrySet()) {
					if (entry.getKey().contains("avg(")) {
						eventType = EventType.DATA_AVG;
					} else if (entry.getKey().contains("count(")) {
						eventType = EventType.DATA_COUNT;
					} else if (entry.getKey().contains("min(")) {
						eventType = EventType.DATA_MIN;
					} else if (entry.getKey().contains("max(")) {
						eventType = EventType.DATA_MAX;
					} else if (entry.getKey().contains("sum(")) {
						eventType = EventType.DATA_SUM;
					}
					//
					if (eventType != EventType.DATA) {
						val = String.valueOf(entry.getValue());
						desc = String.valueOf(entry.getKey());
						break;
					}
				}

				if (val != null && desc != null) {
					String dataSourceName = String.valueOf(datasource);
					DataSource ds = DataSource.findDataSourcesByNameEquals(
							dataSourceName).getSingleResult();
					if (ds != null) {
						Date d = new Date();
						Event newEvt = new Event();
						newEvt.setCreationDate(d);
						newEvt.setDataSource(ds);
						try {
							newEvt.setFloatValue(Float.valueOf(val));
						} catch (Exception ex) {
							LOG.error(ex.getMessage(), e);
						}
						newEvt.setUpdateDate(d);
						newEvt.setEventType(eventType);
						newEvt.setValue(val);
						newEvt.setValueRaw(desc);
						res = newEvt;
					}

				}
			}

		} catch (Exception e) {
			log.error("General overall exception on "
					+ "buildEventFromProperties(MapEventBean body): " + e.getMessage(), e);
		}

		LogHelper.debug(LOG, "Printing buildEventFromProperties(MapEventBean body) res: " + res);
		return res;
	}

	/**
	 * @param event
	 * @return
	 */
	protected Event buildEvent(CamelInternalEvent event) {
		Event evt = new Event();
		try {
			evt.setCreationDate(event.getCreationDate());
			evt.setCreationUser((AuthUser) event.getCreationUser());
			evt.setDate(event.getDate());
			evt.setFloatValue(event.getFloatValue());

			Session session = CacheDataService.getSession(getSessionId());
			evt.setSession(session);

			if (event.getDataSourceId() != null) {
				DataSource ds = CacheDataService.getDataSource(event
						.getDataSourceId());
				evt.setDataSource(ds);
			}

			evt.setValue(event.getValue());
			evt.setValueRaw(event.getValueRaw());
			EventType evtType = EventType
					.valueOf(event.getType() != null ? event.getType().name()
							: EventType.DATA.name());
			evt.setEventType(evtType);
			evt.setUpdateDate(event.getUpdateDate());
			evt.setUpdateUser((AuthUser) event.getUpdateUser());
		} catch (Exception e) {
			evt = null;
			LOG.error(e.getMessage(), e);
		}
		return evt;
	}

}
