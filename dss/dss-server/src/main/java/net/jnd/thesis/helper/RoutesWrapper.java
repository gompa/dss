package net.jnd.thesis.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import net.jnd.thesis.camel.route.EsperEventConsumer;
import net.jnd.thesis.camel.route.EsperEventConsumerFactory;
import net.jnd.thesis.camel.route.EsperEventConsumerUser;
import net.jnd.thesis.camel.route.EsperEventProducer;
import net.jnd.thesis.camel.route.EsperEventProducerFactory;
import net.jnd.thesis.camel.route.GenericRouteBuilder;
import net.jnd.thesis.camel.route.ReplayEventProducer;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.EventType;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocket;
import net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocketHelper;

import org.apache.camel.CamelContext;
import org.apache.camel.component.jpa.JpaComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public class RoutesWrapper {

	private static final Logger LOG = LoggerFactory
			.getLogger(RoutesWrapper.class);

	private CamelContext ctx;

	/**
	 * dataSourceId -> EsperEventProducer
	 */
	private Map<Long, EsperEventProducer> producers = Collections
			.synchronizedMap(new HashMap<Long, EsperEventProducer>());

	/**
	 * 
	 */
	private EsperEventConsumer internalConsumer;

	/**
	 * userId -> EsperEventConsumer
	 */
	private Map<Long, EsperEventConsumer> userConsumers = Collections
			.synchronizedMap(new HashMap<Long, EsperEventConsumer>());

	/**
	 * 
	 */
	private Long sessionId;

	public List<UserSessionConnection> getUserConnections() {
		List<UserSessionConnection> conns = new ArrayList<UserSessionConnection>();
		for (EsperEventConsumer consumer : userConsumers.values()) {
			Long id = ((EsperEventConsumerUser) consumer)
					.getUserSessionConnectionId();
			UserSessionConnection conn = CacheDataService.getConnection(id);
			if (conn != null) {
				conns.add(conn);
			}
		}
		return conns;
	}

	/**
	 * @param ctx
	 * @param session
	 * @param consumer
	 * @param producers
	 * @param test
	 */
	RoutesWrapper(Long sessionId) {
		this.ctx = new DefaultCamelContext();
		// this.ctx = new SpringCamelContext();
		JpaComponent jpaComponent = (JpaComponent) SpringApplicationContext
				.getBean("jpa");
		this.ctx.addComponent("jpa", jpaComponent);
		this.sessionId = sessionId;
	}

	/**
	 * @param connection
	 * @param userId
	 * @param replace
	 * @return
	 */
	boolean setUserConsumer(DSSUserSessionConnection connection, boolean replace) {
		LOG.debug(String.format("setUserConsumer(%s, %s)", connection.getId(),
				replace));
		boolean res = false;
		Long userId = connection.getConnectionUser().getId();
		EsperEventConsumer currentConsumer = this.userConsumers.get(userId);
		// if replace, stop existing user consumer if any
		if (currentConsumer != null && replace) {
			stopUserConsumer(userId, true);
			try {
				boolean ok = getCtx().removeRoute(currentConsumer.getId());
				System.out.println(ok);
			} catch (Exception e) {
				//DEBUG delete log.error
				LOG.error(String.format("setUserConsumer(%s, %s)", connection.getId(),
						replace));
				LogHelper.error(LOG, e);
			}
		}
		// set and start the new user consumer
		if (currentConsumer == null || replace) {
			EsperEventConsumer userConsumer = EsperEventConsumerFactory
					.createUserConsumer(ctx, connection, false);
			userConsumers.put(userId, userConsumer);
			//
			try {
				getCtx().addRoutes(userConsumer);
			} catch (Exception e) {
				//DEBUG delete log.error
				LOG.error(String.format("setUserConsumer(%s, %s)", connection.getId(),
						replace));
				LogHelper.error(LOG, e);
			}
			res = startUserConsumer(userId, false);
		}
		return res;
	}

	/**
	 * @param dataSourceId
	 * @param replace
	 * @return
	 */
	boolean setProducer(Long dataSourceId, boolean replace) {
		LOG.debug(String.format("setProducer(%s, %s)", dataSourceId, replace));
		boolean res = false;
		EsperEventProducer currentProducer = this.producers.get(dataSourceId);
		// if replace, stop existing producer if any
		if (currentProducer != null && replace) {
			stopProducer(dataSourceId, true);
			try {
				boolean ok = getCtx().removeRoute(currentProducer.getId());
				System.out.println(ok);
			} catch (Exception e) {
				//DEBUG delete log.error
				LOG.error(String.format("setProducer(%s, %s)", dataSourceId, replace));
				LogHelper.error(LOG, e);
			}
		}
		// set and start the new user producer
		if (currentProducer == null || replace) {
			EsperEventProducer newProducer = null;
			Session session = CacheDataService.getSession(getSessionId());
			if (dataSourceId == 0 && session.isReplaying()) {
				Long delay = session.getReplayDelay();
				if (delay == null) {
					delay = 10000L;
				}
				newProducer = new ReplayEventProducer(ctx, sessionId, delay,
						false);
			} else {
				newProducer = EsperEventProducerFactory.create(getCtx(),
						dataSourceId, sessionId, false);
			}

			producers.put(dataSourceId, newProducer);
			// start route
			try {
				getCtx().addRoutes(newProducer);
			} catch (Exception e) {
				//DEBUG delete log.error
				LOG.error(String.format("setProducer(%s, %s)", dataSourceId, replace));
				LogHelper.error(LOG, e);
			}
			res = startProducer(dataSourceId, false);
		}
		return res;
	}

	/**
	 * @return
	 */
	boolean setInternalConsumer(boolean replace) {
		LOG.debug(String.format("setInternalConsumer(%s)", replace));
		boolean res = false;
		// if replace, stop existing internal consumer if any
		if (internalConsumer != null && replace) {
			stopInternalConsumer(true);
			try {
				boolean ok = getCtx().removeRoute(internalConsumer.getId());
				System.out.println(ok);
			} catch (Exception e) {
				//DEBUG delete log.error
				LOG.error(String.format("setInternalConsumer(%s)", replace));
				LogHelper.error(LOG, e);
			}
		}
		// set and start the new user internal consumer
		if (internalConsumer == null || replace) {
			EsperEventConsumer newInternalConsumer = EsperEventConsumerFactory
					.createInternal(ctx, sessionId, false);
			this.internalConsumer = newInternalConsumer;
			// start route
			try {
				getCtx().addRoutes(newInternalConsumer);
			} catch (Exception e) {
				//DEBUG delete log.error
				LOG.error(String.format("setInternalConsumer(%s)", replace));
				LogHelper.error(LOG, e);
			}
			res = startInternalConsumer(false);
		}
		return res;
	}

	/**
	 * 
	 */
	boolean startContext(boolean resume) {
		LOG.debug("startContext(" + resume + ")");
		boolean res = false;
		CamelContext context = getCtx();
		try {
			
			LOG.debug("Starting startInternalConsumer");
			// internal consumer
			startInternalConsumer(resume);
			LOG.debug("Finished startInternalConsumer");
			
			// producers
			startAllProducers(resume);
			LOG.debug("Finished startAllProducers");

			// consumers
			startAllUserConsumers(resume);
			LOG.debug("Finished startAllUserConsumers");
			
			// context
			context.start();
			LOG.debug("Finished context.start()");
			
			res = true;
		} catch (Exception e) {
			//DEBUG delete log.error
			LOG.error("Error on startContext("+  resume + ")");
			LogHelper.error(LOG, e);
		}
		return res;
	}

	/**
	 * 
	 */
	boolean stopContext(boolean end) {
		LOG.debug(String.format("stopContext(%s)", end));
		boolean res = false;
		CamelContext context = getCtx();
		try {
			// producers
			stopAllProducers(end);
			// consumers
			stopAllUserConsumers(end);
			// internal consumer
			stopInternalConsumer(end);
			// context
			context.getShutdownStrategy().setTimeUnit(TimeUnit.SECONDS);
			context.getShutdownStrategy().setTimeout(5);
			context.stop();
			this.ctx = new DefaultCamelContext();
			res = true;
		} catch (Exception e) {
			//DEBUG delete log.error
			LOG.error(String.format("stopContext(%s)", end));
			LogHelper.error(LOG, e);
		}
		return res;
	}

	/**
	 * 
	 */
	void startAllProducers(boolean resume) {
		for (EsperEventProducer producer : this.producers.values()) {
			startProducer(producer.getDataSource().getId(), resume);
		}
	}

	/**
	 * 
	 */
	void stopAllProducers(boolean end) {
		for (EsperEventProducer producer : this.producers.values()) {
			stopProducer(producer.getDataSource().getId(), end);
		}
	}

	/**
	 * 
	 */
	void startAllUserConsumers(boolean resume) {
		for (Long userConsumer : this.userConsumers.keySet()) {
			startUserConsumer(userConsumer, resume);
		}
	}

	/**
	 * 
	 */
	void stopAllUserConsumers(boolean end) {
		for (Long userConsumer : this.userConsumers.keySet()) {
			stopUserConsumer(userConsumer, end);
		}
	}

	/**
	 * @param userId
	 * @return
	 */
	boolean startUserConsumer(Long userId, boolean resume) {
		LOG.debug(String.format("startUserConsumer(%s, %s)", userId, resume));
		return start(this.userConsumers.get(userId), resume);
	}

	/**
	 * @param userId
	 * @return
	 */
	boolean stopUserConsumer(Long userId, boolean end) {
		LOG.debug(String.format("stopUserConsumer(%s, %s)", userId, end));
		return stop(this.userConsumers.get(userId), end);
	}

	/**
	 * @return
	 */
	boolean stopInternalConsumer(boolean end) {
		LOG.debug(String.format("stopInternalConsumer(%s)", end));
		return stop(this.internalConsumer, end);
	}

	/**
	 * @return
	 */
	boolean startInternalConsumer(boolean resume) {
		LOG.debug(String.format("startInternalConsumer(%s)", resume));
		return start(this.internalConsumer, resume);
	}

	/**
	 * @param dataSourceId
	 * @return
	 */
	boolean stopProducer(Long dataSourceId, boolean end) {
		LOG.debug(String.format("stopProducer(%s),%s", dataSourceId, end));
		return stop(this.producers.get(dataSourceId), end);
	}

	/**
	 * @param dataSourceId
	 * @return
	 */
	boolean startProducer(Long dataSourceId, boolean resume) {
		LOG.debug(String.format("startProducer(%s,%s)", dataSourceId, resume));
		return start(this.producers.get(dataSourceId), resume);
	}

	/**
	 * @param routeBuilder
	 * @return
	 */
	private boolean start(GenericRouteBuilder routeBuilder, boolean resume) {
		boolean res = false;
		if (routeBuilder != null) {
			res = routeBuilder.start(resume);
		}

		return res;
	}

	/**
	 * @param routeBuilder
	 * @return
	 */
	private boolean stop(GenericRouteBuilder routeBuilder, boolean end) {
		boolean res = false;
		if (routeBuilder != null) {
			res = routeBuilder.stop(end);
		}
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CONSUMER: ").append(this.internalConsumer);
		for (EsperEventProducer producer : producers.values()) {
			sb.append("\n").append("PRODUCER: ").append(producer);
		}
		return sb.toString();
	}

	/**
	 * @return
	 */
	public List<String> dumpRoutes() {
		List<String> routesList = new ArrayList<String>();

		for (EsperEventProducer producer : producers.values()) {
			routesList.add(String.valueOf(producer));
			LOG.debug(String.format("dumpRoutes(%s)", producer));
		}

		routesList.add(String.valueOf(this.internalConsumer));
		LOG.debug(String.format("dumpRoutes(%s)", internalConsumer));

		for (EsperEventConsumer consumer : userConsumers.values()) {
			routesList.add(String.valueOf(consumer));
			LOG.debug(String.format("dumpRoutes(%s)", consumer));
		}
		return routesList;
	}

	/**
	 * @return the ctx
	 */
	CamelContext getCtx() {
		return ctx;
	}

	/**
	 * Adds new data sources or removes datasources that no longer belong to the
	 * session
	 * 
	 * @param session
	 */
	public void updateDataSourceRoutes(DSSSession session) {
		LOG.debug(String.format("updateDataSourceRoutes(%s)", session.getId()));
		if (!((Session) session).isReplaying()) {
			// update session data sources
			Set<Long> newDataSourceIds = new HashSet<Long>();
			for (DSSDataSource ds : session.getDataSources()) {
				newDataSourceIds.add(ds.getId());
			}
			// find data sources to remove
			Set<Long> dataSourceIdsToRemove = new HashSet<Long>();
			for (Long dataSourceId : producers.keySet()) {
				if (!newDataSourceIds.contains(dataSourceId)) {
					dataSourceIdsToRemove.add(dataSourceId);
				}
			}
			// stop removed data sources
			for (Long dataSourceId : dataSourceIdsToRemove) {
				stopProducer(dataSourceId, true);
				producers.remove(dataSourceId);
			}
			// start new data sources
			for (Long dataSourceId : newDataSourceIds) {
				if (!producers.containsKey(dataSourceId)) {
					setProducer(dataSourceId, false);
				}
			}
		}
	}

	/**
	 * Only removes user routes, if they were removed from the session
	 * 
	 * @param session
	 */
	public void updateUserRoutes(DSSSession session) {
		LOG.debug(String.format("updateUserRoutes(%s)", session.getId()));
		// build new users list
		Set<Long> newUserIds = new HashSet<Long>();
		for (DSSUser user : session.getUsers()) {
			newUserIds.add(user.getId());
		}
		// remove users
		Set<Long> userIdsToRemove = new HashSet<Long>();
		for (Long currentUserId : userConsumers.keySet()) {
			if (!newUserIds.contains(currentUserId)) {
				userIdsToRemove.add(currentUserId);
			}
		}
		// stop user routes
		for (Long userId : userIdsToRemove) {
			try {
				// setup message
				AuthUser usr = CacheDataService.getUser(userId);
				String dt = DateFormatUtils.format(new Date(),
						"yyyy-MM-dd HH:mm:ss.S");
				String msg = String.format("(%s) [%s] %s", dt,
						EventType.USER_DISCONNECT, usr.getUsername());
				System.out.println("-------------> pushing to \""
						+ usr.getUsername() + "\" event: " + msg);
				ServerSideWebSocket ws = ServerSideWebSocketHelper
						.getConnection(userId);

				Event e = new Event();
				e.setDate(new Date());
				e.setValueRaw(EventType.USER_DISCONNECT.name());
				e.setEventType(EventType.USER_DISCONNECT);
				e.setValue(usr.getUsername());

				ws.sendMessageDirect(e);
			} catch (Exception e) {
				//DEBUG delete log.error
				LOG.error(String.format("updateUserRoutes(%s)", session.getId()));
				e.printStackTrace();
			}
			// stop route
			stopUserConsumer(userId, true);
			userConsumers.remove(userId);
		}
	}

	/**
	 * restarts the internal session, for example, if the esper expression
	 * changed
	 * 
	 * @param session
	 */
	public void updateInternalRoute(DSSSession session) {
		LOG.debug(String.format("updateInternalRoute(%s)", session.getId()));
		setInternalConsumer(true);
	}

	/**
	 * @param session
	 */
	public void updateSessionRoutes(DSSSession session, boolean dataSources,
			boolean users, boolean internal) {
		LOG.debug(String.format("updateSessionRoutes(%s,%s,%s,%s)",
				session.getId(), dataSources, users, internal));
		//
		if (dataSources) {
			updateDataSourceRoutes(session);
		}
		//
		if (users) {
			updateUserRoutes(session);
		}
		//
		if (internal) {
			updateInternalRoute(session);
		}
	}

	/**
	 * @param connection
	 */
	public void updateConnectionRoutes(DSSUserSessionConnection connection) {
		LOG.debug(String.format("updateConnectionRoutes(%s)",
				connection.getId()));
		setUserConsumer(connection, true);
	}

	/**
	 * @return the sessionId
	 */
	public Long getSessionId() {
		return sessionId;
	}

}
