package net.jnd.thesis.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.service.CacheDataService;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author João Domingos
 * @since Jun 26, 2012
 */
@RooJavaBean
@Table(name = "DSS_USER")
@RooJpaActiveRecord(finders = { "findAuthUsersByUsernameEquals" })
public class AuthUser extends DomainEntity implements UserDetails, DSSUser {

	private static final long serialVersionUID = -1427689145075239131L;

	@NotNull
	@Column(unique = true)
	@Size(max = 20)
	private String username;

	@NotNull
	@Size(max = 20)
	private String password;

	@NotNull
	@Column(unique = true)
	@Size(max = 40)
	private String name;

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<AuthRole> authorities = new HashSet<AuthRole>();

	private Boolean enabled = Boolean.TRUE;

	private Boolean credentialsNonExpired = Boolean.TRUE;

	private Boolean accountNonExpired = Boolean.TRUE;

	private Boolean accountNonLocked = Boolean.TRUE;

	@Override
	public boolean isEnabled() {
		return Boolean.TRUE.equals(getEnabled());
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return Boolean.TRUE.equals(getCredentialsNonExpired());
	}

	@Override
	public boolean isAccountNonExpired() {
		return Boolean.TRUE.equals(getAccountNonExpired());
	}

	@Override
	public boolean isAccountNonLocked() {
		return Boolean.TRUE.equals(getAccountNonLocked());
	}

	/**
	 * @return
	 */
	public boolean isAdministrator() {
		boolean res = false;
		for (AuthRole authRole : getAuthorities()) {
			if (res = "ROLE_ADMIN".equals(authRole.getAuthority())) {
				break;
			}
		}
		return res;
	}

	@Override
	public String toString() {
		// return String.format("%s (%s)", getName(), getUsername());
		return String.format("%s", getName(), getUsername());
	}

	@PostPersist
	@PostUpdate
	protected void updateCache() {
		CacheDataService.update((DSSUser) this);
	}

	@Override
	public boolean isAdmin() {
		return authorities.contains("ROLE_ADMIN");
	}

}
