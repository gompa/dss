/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;

/**
 * @author João Domingos
 * @since Dec 7, 2012
 */
public class ServerSideWebSocketServlet extends WebSocketServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4549529078521578228L;

	@Override
	public WebSocket doWebSocketConnect(HttpServletRequest request,
			String params) {
		return new ServerSideWebSocket(request, params);
	}
}
