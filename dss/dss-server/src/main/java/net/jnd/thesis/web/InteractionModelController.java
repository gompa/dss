package net.jnd.thesis.web;

import net.jnd.thesis.domain.InteractionModel;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/interactionmodels")
@Controller
@RooWebScaffold(path = "interactionmodels", formBackingObject = InteractionModel.class)
public class InteractionModelController {
}
