/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import net.jnd.thesis.common.bean.AuthenticationBean;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.DomainEntity;
import net.jnd.thesis.helper.RequestHelper;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author João Domingos
 * @since Jan 27, 2013
 */
public abstract class ServerSideCoreServices implements WebSocketServices {

	@SuppressWarnings("unused")
	private static final Log LOG = LogFactory
			.getLog(ServerSideCoreServices.class);

	protected AuthUser user;

	@Override
	public void authenticate(AuthenticationBean auth) throws WSException {
		LOG.debug(String.format("authenticate(%s)", auth.getUsername()));
		AuthUser usr = CacheDataService.getUserByUsername(auth.getUsername());
		if (usr != null && usr.getPassword().equals(auth.getPassword())) {
			this.user = usr;
		}
	}

	@Override
	public boolean isAuthenticated() {
		if (this.user == null) {
			this.user = RequestHelper.getCurrentUser(false);
		}
		return this.user != null;
	}

	/**
	 * @param userId
	 */
	public void setAuthenticated(Long userId) {
		this.user = CacheDataService.getUser(userId);
	}

	@Override
	public String echo(String msg) throws WSException {
		return msg;
	}

	/**
	 * @param entity
	 */
	protected void buildCreateAudit(DomainEntity entity) {
		if (isAuthenticated()) {
			entity.setCreationUser(user);
			entity.setUpdateUser(user);
		}
	}

	/**
	 * @param entity
	 */
	protected void buildUpdateAudit(DomainEntity entity) {
		if (isAuthenticated()) {
			entity.setUpdateUser(user);
		}
	}

}
