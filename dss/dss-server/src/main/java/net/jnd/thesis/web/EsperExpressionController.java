package net.jnd.thesis.web;

import net.jnd.thesis.domain.EsperExpression;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/esperexpressions")
@Controller
@RooWebScaffold(path = "esperexpressions", formBackingObject = EsperExpression.class)
public class EsperExpressionController {
}
