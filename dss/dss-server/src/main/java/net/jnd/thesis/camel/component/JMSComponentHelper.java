package net.jnd.thesis.camel.component;

import javax.jms.ConnectionFactory;
import javax.naming.OperationNotSupportedException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultComponent;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public class JMSComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "test-jms";
	private static final String URI_TO = COMPONENT_NAME + ":queue:%s";
	private static final String URI_FROM = URI_TO;

	private String queueName = "test.queue";

	public JMSComponentHelper(CamelContext ctx, String queueName) {
		super(ctx);
		this.queueName = queueName;
	}

	// /**
	// * @param ctx
	// */
	// public JMSComponentHelper(CamelContext ctx) {
	// super(ctx);
	// }
	//
	// /**
	// * @param ctx
	// * @param queueName
	// */
	// public JMSComponentHelper(CamelContext ctx, String queueName) {
	// super(ctx);
	// this.queueName = queueName;
	// }

	@Override
	protected DefaultComponent buildComponent(CamelContext ctx) {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				"vm://localhost?broker.persistent=false");
		ctx.addComponent(COMPONENT_NAME,
				JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
		JmsComponent component = ctx.getComponent(COMPONENT_NAME,
				JmsComponent.class);
		return component;
	}

	@Override
	protected String getFromUri() throws OperationNotSupportedException {
		return String.format(URI_FROM, this.queueName);
	}

	@Override
	protected String getToUri() throws OperationNotSupportedException {
		return String.format(URI_TO, this.queueName);
	}

}
