package net.jnd.thesis.camel.test;

import javax.persistence.EntityManagerFactory;

import net.jnd.thesis.camel.component.JPAComponentHelper;
import net.jnd.thesis.domain.Event;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jpa.JpaComponent;
import org.apache.camel.component.websocket.WebsocketComponent;
import org.apache.camel.main.Main;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

public class JPAWebSocketRoute extends RouteBuilder {
	
	public static final int WEB_SOCKET_PORT = 9090;
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private PlatformTransactionManager transactionManager;

	/*
	 * Misc
	 */
	@Override
	public void configure() throws Exception {

		// setup Camel web-socket component on the port we have defined
		WebsocketComponent wc = getContext().getComponent("websocket",
				WebsocketComponent.class);
		wc.setPort(WEB_SOCKET_PORT);
		
		// we can serve static resources from the classpath: or file: system
		wc.setStaticResources("classpath:.");

		JpaComponent jpaComponent = getContext().getComponent("jpa",
				JpaComponent.class);
		jpaComponent.setEntityManagerFactory(entityManagerFactory);
		jpaComponent.setTransactionManager(transactionManager);
		// getContext().addComponent("jpa", jpaComponent);
		// camelContext.addComponent("jpa", jpaComponent);

		JPAComponentHelper jpaComponentHelper = new JPAComponentHelper(
				getContext(), Event.class);
		jpaComponentHelper.setConsumeDelete(Boolean.FALSE);
		jpaComponentHelper
				.setQuery("select e from Event e order by e.creationDate desc 1");

		from(jpaComponentHelper.from()).to("websocket:camel-ws?sendToAll=true");

	}

	/**
	 * @param transactionManager2
	 * @param entityManager
	 * 
	 */
	public static void start(EntityManagerFactory entityManagerFactory,
			PlatformTransactionManager transactionManager) {
		System.out.println("\n\n\n\n");
		System.out.println("===============================================");
		System.out.println("Open your web browser on http://localhost:"+WEB_SOCKET_PORT);
		System.out.println("Press ctrl+c to stop this example");
		System.out.println("===============================================");
		System.out.println("\n\n\n\n");

		// create a new Camel Main so we can easily start Camel
		final Main main = new Main();

		// enable hangup support which mean we detect when the JVM terminates,
		// and stop Camel graceful
		main.enableHangupSupport();

		JPAWebSocketRoute j = new JPAWebSocketRoute();

		j.setEntityManagerFactory(entityManagerFactory);
		j.setTransactionManager(transactionManager);

		// add our routes to Camel
		main.addRouteBuilder(j);

		// and run, which keeps blocking until we terminate the JVM (or stop
		// CamelContext)

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					main.run();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}).start();

	}
	
	/*
	 * Getters and Setters
	 */
	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory anEntity) {
		entityManagerFactory = anEntity;
	}

	public PlatformTransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(
			PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}
}
