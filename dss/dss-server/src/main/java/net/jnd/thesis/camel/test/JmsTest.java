package net.jnd.thesis.camel.test;

import net.jnd.thesis.camel.component.FileComponentHelper;
import net.jnd.thesis.camel.component.JMSComponentHelper;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;

/**
 *
 */
public class JmsTest extends GenericTest {

	private String jmsUri;

	@Override
	public void configure() throws Exception {
		JMSComponentHelper jms = new JMSComponentHelper(getContext(),
				"test.queue");
		FileComponentHelper file = new FileComponentHelper(getContext(), "test");
		from(jmsUri = jms.from()).to(file.to());
		// run extra tests in method "runCustomTests"
		setRunTests(Boolean.TRUE);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		GenericTest.start(JmsTest.class);
	}

	@Override
	protected void runCustomTests(CamelContext ctx) {
		ProducerTemplate template = ctx.createProducerTemplate();
		for (int i = 0; i < 10; i++) {
			template.sendBody(jmsUri, "Test Message: " + i);
		}
	}

}
