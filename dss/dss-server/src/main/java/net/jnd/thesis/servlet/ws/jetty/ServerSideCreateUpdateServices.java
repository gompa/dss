/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import net.jnd.thesis.common.bean.DataSourceBean;
import net.jnd.thesis.common.bean.DynamicReconfigurationBean;
import net.jnd.thesis.common.bean.EsperExpressionBean;
import net.jnd.thesis.common.bean.InteractionModelBean;
import net.jnd.thesis.common.bean.SessionBean;
import net.jnd.thesis.common.bean.SessionConditionBean;
import net.jnd.thesis.common.bean.UserBean;
import net.jnd.thesis.common.bean.UserSessionConnectionBean;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.domain.DomainEntity;
import net.jnd.thesis.ws.services.exception.UserNotAuthenticatedException;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author João Domingos
 * @since Jan 27, 2013
 */
public abstract class ServerSideCreateUpdateServices extends
		ServerSideListGetServices {

	@SuppressWarnings("unused")
	private static final Log LOG = LogFactory
			.getLog(ServerSideCreateUpdateServices.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServicesUpdating#createUser(net.jnd
	 * .thesis.common.interfaces.DSSUser)
	 */
	@Override
	public Long createUser(DSSUser user) throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildUser((UserBean) user);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServicesUpdating#updateUser(net.jnd
	 * .thesis.common.interfaces.DSSUser)
	 */
	@Override
	public Boolean updateUser(DSSUser user) throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper.buildUser(
					(UserBean) user, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#createDataSource(net.jnd
	 * .thesis.common.interfaces.DSSDataSource)
	 */
	@Override
	public Long createDataSource(DSSDataSource bean) throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildDataSource((DataSourceBean) bean);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#updateDataSource(net.jnd
	 * .thesis.common.interfaces.DSSDataSource)
	 */
	@Override
	public Boolean updateDataSource(DSSDataSource bean) throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper.buildDataSource(
					(DataSourceBean) bean, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#createDynamicReconfiguration
	 * (net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration)
	 */
	@Override
	public Long createDynamicReconfiguration(DSSDynamicReconfiguration bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildDynamicReconfiguration((DynamicReconfigurationBean) bean);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#updateDynamicReconfiguration
	 * (net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration)
	 */
	@Override
	public Boolean updateDynamicReconfiguration(DSSDynamicReconfiguration bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildDynamicReconfiguration(
							(DynamicReconfigurationBean) bean, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#createEsperExpression(net
	 * .jnd.thesis.common.interfaces.DSSEsperExpression)
	 */
	@Override
	public Long createEsperExpression(DSSEsperExpression bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildEsperExpression((EsperExpressionBean) bean);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#updateEsperExpression(net
	 * .jnd.thesis.common.interfaces.DSSEsperExpression)
	 */
	@Override
	public Boolean updateEsperExpression(DSSEsperExpression bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper.buildEsperExpression(
					(EsperExpressionBean) bean, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#createInteractionModel(net
	 * .jnd.thesis.common.interfaces.DSSInteractionModel)
	 */
	@Override
	public Long createInteractionModel(DSSInteractionModel bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildInteractionModel((InteractionModelBean) bean);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#updateInteractionModel(net
	 * .jnd.thesis.common.interfaces.DSSInteractionModel)
	 */
	@Override
	public Boolean updateInteractionModel(DSSInteractionModel bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper.buildInteractionModel(
					(InteractionModelBean) bean, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#createSessionCondition(net
	 * .jnd.thesis.common.interfaces.DSSSessionCondition)
	 */
	@Override
	public Long createSessionCondition(DSSSessionCondition bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildSessionCondition((SessionConditionBean) bean);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#updateSessionCondition(net
	 * .jnd.thesis.common.interfaces.DSSSessionCondition)
	 */
	@Override
	public Boolean updateSessionCondition(DSSSessionCondition bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper.buildSessionCondition(
					(SessionConditionBean) bean, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#createUserSessionConnection
	 * (net.jnd.thesis.common.interfaces.DSSUserSessionConnection)
	 */
	@Override
	public Long createUserSessionConnection(DSSUserSessionConnection bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildUserSessionConnection((UserSessionConnectionBean) bean);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#updateUserSessionConnection
	 * (net.jnd.thesis.common.interfaces.DSSSession)
	 */
	@Override
	public Boolean updateUserSessionConnection(DSSSession bean)
			throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildUserSessionConnection(
							(UserSessionConnectionBean) bean, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#createSession(net.jnd.thesis
	 * .common.interfaces.DSSSession)
	 */
	@Override
	public Long createSession(DSSSession bean) throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper
					.buildSession((SessionBean) bean);
			buildCreateAudit(e);
			e.persist();
			return e.getId();
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.ws.services.WebSocketServices#updateSession(net.jnd.thesis
	 * .common.interfaces.DSSSession)
	 */
	@Override
	public Boolean updateSession(DSSSession bean) throws WSException {
		if (isAuthenticated()) {
			DomainEntity e = ServerSideServicesHelper.buildSession(
					(SessionBean) bean, true);
			buildUpdateAudit(e);
			e.merge();
			return true;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}
}
