package net.jnd.thesis.domain;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.service.CacheDataService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooJson
@Table(name = "DSS_DATASOURCE")
@RooJpaActiveRecord(finders = { "findDataSourcesByCreationUser", "findDataSourcesByNameEquals" })
public abstract class DataSource extends DomainEntity implements DSSDataSource {

    @NotNull
    @Column
    @Size(max = 40)
    private String name;

    @Size(max = 60)
    private String regex = ".*";

    @NotNull
    @Enumerated
    private DataSourceValueType valueType = DataSourceValueType.STRING;

    @NotNull
    private Boolean enabled = Boolean.TRUE;

    public abstract DataSourceType getDataSourceType();

    @PrePersist
    public void prePersist() {
        setName(StringUtils.abbreviate(getName(), 40));
    }

    @Override
    public DSSUser getOwner() {
        return getCreationUser();
    }

    @Override
    public String toString() {
        return String.format("%s", getName());
    }

    @PostPersist
    @PostUpdate
    protected void updateCache() {
        CacheDataService.update((DSSDataSource) this);
    }

    public boolean isEnabled() {
        return Boolean.TRUE.toString().equals(getEnabled());
    }

    public net.jnd.thesis.common.enums.DataSourceValueType getDataSourceValueType() {
        net.jnd.thesis.common.enums.DataSourceValueType res = null;
        if (getValueType() != null) {
            res = net.jnd.thesis.common.enums.DataSourceValueType.valueOf(getValueType().name());
        }
        return res;
    }
}
