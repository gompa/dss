package net.jnd.thesis.web;

import net.jnd.thesis.domain.SessionCondition;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/sessionconditions")
@Controller
@RooWebScaffold(path = "sessionconditions", formBackingObject = SessionCondition.class)
public class SessionConditionController {
}
