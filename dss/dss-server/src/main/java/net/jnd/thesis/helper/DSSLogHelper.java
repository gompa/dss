/**
 * 
 */
package net.jnd.thesis.helper;

import net.jnd.thesis.common.enums.LogType;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.domain.LogEntry;
import net.jnd.thesis.service.CacheDataService;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
public final class DSSLogHelper {

	/**
	 * @param type
	 * @param user
	 * @return
	 */
	private static LogEntry buildLogEntry(LogType type, DSSUser user) {
		LogEntry logEntry = new LogEntry();
		logEntry.setType(type);
		logEntry.setUsr(format(user));
		logEntry.merge();
		return logEntry;
	}

	/**
	 * @param type
	 * @param session
	 * @return
	 */
	public static LogEntry buildLogEntry(LogType type, DSSSession session) {
		LogEntry logEntry = new LogEntry();
		logEntry.setType(type);
		logEntry.setSession(format(session));
		logEntry.merge();
		return logEntry;
	}

	/**
	 * @param type
	 * @param user
	 * @param session
	 * @return
	 */
	public static LogEntry buildLogEntry(LogType type, DSSUser user,
			DSSSession session) {
		LogEntry logEntry = new LogEntry();
		logEntry.setType(type);
		logEntry.setUsr(format(user));
		logEntry.setSession(format(session));
		logEntry.merge();
		return logEntry;
	}

	/**
	 * @param user
	 * @return
	 */
	private static String format(DSSUser user) {
		String res = null;
		if (user != null) {
			res = String.format("%s (%s)", user.getName(), user.getId());
		}
		return res;
	}

	/**
	 * @param session
	 * @return
	 */
	private static String format(DSSSession session) {
		String res = null;
		if (session != null) {
			res = String.format("%s (%s)", session.getName(), session.getId());
		}
		return res;
	}

	/**
	 * @param user
	 */
	public static void userConnect(DSSUser user) {
		buildLogEntry(LogType.USER_CONNECT, user);
	}

	/**
	 * @param user
	 */
	public static void userDisconnect(DSSUser user) {
		buildLogEntry(LogType.USER_DISCONNECT, user);
	}

	/**
	 * @param session
	 */
	public static void testStart(DSSSession session) {
		buildLogEntry(LogType.TEST_START,
				session);
	}

	/**
	 * @param session
	 */
	public static void testEnd(DSSSession session) {
		buildLogEntry(LogType.TEST_END, session);
	}

	/**
	 * @param session
	 */
	public static void sessionStart(DSSSession session) {
		buildLogEntry(LogType.SESSION_START, session);
	}

	/**
	 * @param session
	 */
	public static void sessionEnd(DSSSession session) {
		Long sessionId = session.getId();
		buildLogEntry(LogType.SESSION_END, session);
	}

}
