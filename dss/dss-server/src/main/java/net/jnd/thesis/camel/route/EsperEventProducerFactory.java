/**
 * 
 */
package net.jnd.thesis.camel.route;

import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.domain.HttpDataSource;
import net.jnd.thesis.domain.RssDataSource;
import net.jnd.thesis.domain.TwitterDataSource;
import net.jnd.thesis.domain.XmppDataSource;
import net.jnd.thesis.service.CacheDataService;

import org.apache.camel.CamelContext;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public final class EsperEventProducerFactory {

	/**
	 * @param ctx
	 * @param ds
	 * @param sessionId
	 * @param test
	 * @return
	 */
	public static EsperEventProducer create(CamelContext ctx,
			Long dataSourceId, Long sessionId, boolean test) {
		DSSDataSource ds = CacheDataService.getDataSource(dataSourceId);
		switch (ds.getType()) {
		case HTTP:
			return new HttpEventProducer(ctx, (HttpDataSource) ds, sessionId,
					test);
		case RSS:
			return new RssEventProducer(ctx, (RssDataSource) ds, sessionId,
					test);
		case TWITTER:
			return new TwitterEventProducer(ctx, (TwitterDataSource) ds,
					sessionId, test);
		case XMPP:
			return new XmppEventProducer(ctx, (XmppDataSource) ds, sessionId,
					test);
		default:
			throw new IllegalArgumentException(
					"data source type not supported...");
		}
	}

}
