package net.jnd.thesis.web;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import net.jnd.thesis.common.bean.ReplaySessionBean;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.helper.RequestHelper;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.servlet.ws.jetty.ServerSideServices;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

/**
 * @author João Domingos
 * @since Nov 3, 2012
 */
@RequestMapping("/customaction/**")
@Controller
public class CustomActionController {

	@RequestMapping(value = "websocket/{id}", produces = "text/html")
	public String openWebsocket(@PathVariable("id") Long connectionId,
			HttpServletRequest request, Model uiModel) {
		uiModel.addAttribute("connectionId", connectionId);
		try {
			UserSessionConnection conn = CacheDataService
					.getConnection(connectionId);
			String sessionName = conn.getSession().getName();
			uiModel.addAttribute("SESSION_NAME", sessionName);
		} catch (Exception e) {
			RequestHelper.saveUserMessage(uiModel, e.getMessage());
		}

		return "customaction/websocket";
	}

	@RequestMapping(value = "startSession", produces = "text/html")
	public String startSession(HttpServletRequest request, Model uiModel) {
		Long sessionId = RequestHelper.getSessionId(request);
		String res = forwardToSessionDetail(request, sessionId);
		String errors = "";
		ServerSideServices services = new ServerSideServices();
		Boolean ok = false;
		try {
			ok = services.startSession(sessionId);
			errors = ok ? "" : "unable to start session...";
		} catch (WSException e) {
			errors = e.getMessage();
		}
		//
		if (StringUtils.isNotBlank(errors)) {
			RequestHelper.saveUserMessage(uiModel, errors);
		}
		return res;
	}

	@RequestMapping(value = "replaySession", produces = "text/html")
	public String replaySession(HttpServletRequest request, Model uiModel) {
		Long sessionId = RequestHelper.getSessionId(request);
		String res = forwardToSessionDetail(request, sessionId);
		String errors = "";
		ServerSideServices services = new ServerSideServices();
		Boolean ok = false;
		try {
			Session session = CacheDataService.getSession(sessionId);
			ReplaySessionBean replayBean = new ReplaySessionBean();
			replayBean.setReplayDelay(session.getReplayDelay());
			replayBean.setSessionId(session.getId());
			ok = services.replaySession(replayBean);
			errors = ok ? "" : "unable to start session...";
		} catch (WSException e) {
			errors = e.getMessage();
		}
		//
		if (StringUtils.isNotBlank(errors)) {
			RequestHelper.saveUserMessage(uiModel, errors);
		}
		return res;
	}

	@RequestMapping(value = "pauseSession")
	public String pauseSession(HttpServletRequest request, Model uiModel) {
		Long sessionId = RequestHelper.getSessionId(request);
		String res = forwardToSessionDetail(request, sessionId);

		// String errors = RouteHelper.stopSession(sessionId);

		String errors = "";
		ServerSideServices services = new ServerSideServices();
		Boolean ok = false;
		try {
			ok = services.stopSession(sessionId);
			errors = ok ? "" : "unable to pause session...";
		} catch (WSException e) {
			errors = e.getMessage();
		}

		if (StringUtils.isNotBlank(errors)) {
			RequestHelper.saveUserMessage(uiModel, errors);
		}
		return res;
	}

	@RequestMapping(value = "endSession")
	public String endSession(HttpServletRequest request, Model uiModel) {
		Long sessionId = RequestHelper.getSessionId(request);
		String res = forwardToSessionDetail(request, sessionId);

		// String errors = RouteHelper.stopSession(sessionId);

		String errors = "";
		ServerSideServices services = new ServerSideServices();
		Boolean ok = false;
		try {
			ok = services.stopSession(sessionId);
			errors = ok ? "" : "unable to end session...";
		} catch (WSException e) {
			errors = e.getMessage();
		}

		if (StringUtils.isNotBlank(errors)) {
			RequestHelper.saveUserMessage(uiModel, errors);
		}
		return res;
	}

	/**
	 * @param request
	 * @param sessionId
	 * @return
	 */
	private String forwardToSessionDetail(HttpServletRequest request,
			Long sessionId) {
		return "forward:/sessions/"
				+ encodeUrlPathSegment(String.valueOf(sessionId), request);
	}

	@RequestMapping(value = "openUserSessionConnection", produces = "text/html")
	public String openUserSessionConnection(HttpServletRequest request,
			Model uiModel) {
		Long connectionId = RequestHelper.getUserSessionConnectionId(request);
		String res = forwardToUserSessionConnectionDetail(request, connectionId);

		// String errors = RouteHelper.openUserSessionConnection(connectionId);

		String errors = "";
		ServerSideServices services = new ServerSideServices();
		Boolean ok = false;
		try {
			UserSessionConnection userSessionConnection = CacheDataService
					.getConnection(connectionId);
			ok = services.connectToSession(userSessionConnection);
			errors = ok ? "" : "unable to open connection...";
		} catch (WSException e) {
			errors = e.getMessage();
		}

		if (StringUtils.isBlank(errors)) {
			res = "redirect:/customaction/websocket/"
					+ encodeUrlPathSegment(String.valueOf(connectionId),
							request);
		} else {
			RequestHelper.saveUserMessage(uiModel, errors);
		}
		return res;
	}

	@RequestMapping(value = "closeUserSessionConnection")
	public String closeUserSessionConnection(HttpServletRequest request,
			Model uiModel) {
		Long connectionId = RequestHelper.getUserSessionConnectionId(request);
		String res = forwardToUserSessionConnectionDetail(request, connectionId);

		// String errors = RouteHelper.closeUserSessionConnection(connectionId);

		String errors = "";
		ServerSideServices services = new ServerSideServices();
		Boolean ok = false;
		try {
			ok = services.disconnectFromSession(connectionId);
			errors = ok ? "" : "unable to close connection...";
		} catch (WSException e) {
			errors = e.getMessage();
		}

		if (StringUtils.isNotBlank(errors)) {
			RequestHelper.saveUserMessage(uiModel, errors);
		}
		return res;
	}

	/**
	 * @param request
	 * @param connectionId
	 * @return
	 */
	private String forwardToUserSessionConnectionDetail(
			HttpServletRequest request, Long connectionId) {
		return "forward:/usersessionconnections/"
				+ encodeUrlPathSegment(String.valueOf(connectionId), request);
	}

	/**
	 * @param pathSegment
	 * @param httpServletRequest
	 * @return
	 */
	private String encodeUrlPathSegment(String pathSegment,
			HttpServletRequest httpServletRequest) {
		String enc = httpServletRequest.getCharacterEncoding();
		if (enc == null) {
			enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
		}
		try {
			pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
		} catch (UnsupportedEncodingException uee) {
		}
		return pathSegment;
	}

}
