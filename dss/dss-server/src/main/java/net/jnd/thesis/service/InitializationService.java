package net.jnd.thesis.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.jnd.thesis.common.exception.ServiceException;
import net.jnd.thesis.domain.AuthRole;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.ConnectionStatus;
import net.jnd.thesis.domain.EsperExpression;
import net.jnd.thesis.domain.HttpDataSource;
import net.jnd.thesis.domain.RssDataSource;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionStatus;
import net.jnd.thesis.domain.TwitterDataSource;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.domain.XmppDataSource;
import net.jnd.thesis.service.loadBalancer.LoadBalancerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author João Domingos
 * @since Jun 30, 2012
 */
public class InitializationService implements Service {

	private static final Logger LOG = LoggerFactory
			.getLogger(InitializationService.class);

	@PersistenceContext
	protected EntityManager em;

	@Autowired
	protected CacheDataService dataService;

	@Autowired
	protected DSSDataService dssDataService;

	@Autowired
	protected LoadBalancerService loadBalancerService;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.Service#start()
	 */
	@Override
	@PostConstruct
	@Transactional
	public void start() throws ServiceException {
		LOG.info("InitializationService - start()");
		@SuppressWarnings("unused")
		AuthUser admin = initSpringSecurity();
		// initDataSources(admin);
		// initSessionData(admin);

		loadBalancerService.start();
		dataService.start();
		dssDataService.start();
		
		// after services have started
		setConnectionsClosed();
		setSessionsStopped();
	}

	/**
	 * 
	 */
	private void setConnectionsClosed() {
		// set all connections to closed
		for (UserSessionConnection connection : UserSessionConnection
				.findAllUserSessionConnections()) {
			connection.setStatus(ConnectionStatus.CLOSED);
			connection.merge();
		}
	}

	/**
	 * 
	 */
	private void setSessionsStopped() {
		// set all connections to closed
		for (Session session : Session.findAllSessions()) {
			session.setSessionStatus(SessionStatus.PAUSED);
			session.merge();
		}
	}

	@Override
	public void stop() throws ServiceException {
		// do nothing
	}

	/**
	 * @param admin
	 * 
	 */
	@SuppressWarnings("unused")
	private void initSessionData(AuthUser admin) {
		// initialize esper expressions
		if (EsperExpression.findAllEsperExpressions().size() < 1) {
			EsperExpression exp = new EsperExpression();
			exp.setUpdateUser(admin);
			exp.merge();
		}
	}

	/**
	 * @param admin
	 * 
	 */
	@SuppressWarnings("unused")
	private void initDataSources(AuthUser admin) {
		// initialize http datasource
		if (HttpDataSource.findAllHttpDataSources().size() < 1) {
			HttpDataSource ds = new HttpDataSource();
			ds.setCreationUser(admin);
			ds.setUpdateUser(admin);
			ds.merge();
		}
		// initialize rss datasource
		if (RssDataSource.findAllRssDataSources().size() < 1) {
			RssDataSource ds = new RssDataSource();
			ds.setCreationUser(admin);
			ds.setUpdateUser(admin);
			ds.merge();
		}
		// initialize twitter datasource
		if (TwitterDataSource.findAllTwitterDataSources().size() < 1) {
			TwitterDataSource ds = new TwitterDataSource();
			ds.setCreationUser(admin);
			ds.setUpdateUser(admin);
			ds.merge();
		}
		// initialize http datasource
		if (XmppDataSource.findAllXmppDataSources().size() < 1) {
			XmppDataSource ds = new XmppDataSource();
			ds.setCreationUser(admin);
			ds.setUpdateUser(admin);
			ds.merge();
		}
	}

	/**
	 * 
	 */
	private AuthUser initSpringSecurity() {
		Set<AuthRole> rolesAdmin = new HashSet<AuthRole>();
		Set<AuthRole> rolesUser = new HashSet<AuthRole>();
		// build roles if necessary
		List<AuthRole> roles = AuthRole.findAllAuthRoles();
		if (roles.isEmpty()) {
			// build role admin
			AuthRole roleAdmin = new AuthRole();
			roleAdmin.setAuthority("ROLE_ADMIN");
			roleAdmin.merge();
			// build role user
			AuthRole roleUser = new AuthRole();
			roleUser.setAuthority("ROLE_USER");
			roleUser.merge();
		}
		// build role sets
		roles = AuthRole.findAllAuthRoles();
		for (AuthRole authRole : roles) {
			if ("ROLE_ADMIN".equals(authRole.getAuthority())) {
				rolesAdmin.add(authRole);
			} else if ("ROLE_USER".equals(authRole.getAuthority())) {
				rolesAdmin.add(authRole);
				rolesUser.add(authRole);
			}
		}
		// build users
		AuthUser userAdmin = new AuthUser();
		List<AuthUser> users = AuthUser.findAllAuthUsers();
		if (users.isEmpty()) {
			// build admin users
			if (!rolesAdmin.isEmpty()) {
				userAdmin.setUsername("admin");
				userAdmin.setPassword("admin_pwd");
				userAdmin.setEnabled(Boolean.TRUE);
				userAdmin.setName("Administrator");
				userAdmin.setAuthorities(rolesAdmin);
				userAdmin.merge();
			}

			userAdmin = AuthUser.findAllAuthUsers().get(0);

			// build standard users
			/*
			if (!rolesUser.isEmpty()) {
				// user A
				AuthUser userA = new AuthUser();
				userA.setUsername("usera");
				userA.setPassword("usera_pwd");
				userA.setEnabled(Boolean.TRUE);
				userA.setName("User A");
				userA.setAuthorities(rolesUser);
				userA.merge();
				// user B
				AuthUser userB = new AuthUser();
				userB.setUsername("userb");
				userB.setPassword("userb_pwd");
				userB.setEnabled(Boolean.TRUE);
				userB.setName("User B");
				userB.setAuthorities(rolesUser);
				userB.merge();
				// user C
				AuthUser userC = new AuthUser();
				userC.setUsername("userc");
				userC.setPassword("userc");
				userC.setEnabled(Boolean.TRUE);
				userC.setName("User C");
				userC.setAuthorities(rolesUser);
				userC.merge();
			}
			*/

		}

		return userAdmin;
	}

}
