package net.jnd.thesis.camel.route;

import java.util.Date;

import net.jnd.thesis.camel.bean.CamelInternalEvent;
import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.camel.component.JPAComponentHelper;
import net.jnd.thesis.common.enums.DataSourceType;
import net.jnd.thesis.common.enums.DataSourceValueType;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.ReplaySession;

import org.apache.camel.Body;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;
import org.apache.commons.lang3.StringUtils;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
public class ReplayEventProducer extends EsperEventProducer {

	/*
	 * 
	 */

	private String query;

	private long delay;

	/**
	 * @param ctx
	 * @param sessionId
	 * @param test
	 */
	public ReplayEventProducer(CamelContext ctx, long sessionId, long delay,
			boolean test) {
		super(ctx, sessionId, test);
		this.delay = delay;
		this.query = String
				.format("SELECT e FROM %s e, %s r WHERE r.sessionId = %s and e.session.id = r.sessionId and e.id > r.eventId order by e.id",
						Event.class.getName(), ReplaySession.class.getName(),
						getSessionId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getFrom()
	 */
	@Override
	protected ComponentHelper getFrom() {
		JPAComponentHelper jpa = new JPAComponentHelper(getContext(),
				Event.class);
		jpa.setConsumeDelete(false);
		jpa.setInitialDelay(10L * 1000);
		jpa.setDelay(delay);
		jpa.setMaximumResults(1);
		jpa.setQuery(query);
		return jpa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.route.GenericRouteBuilder#getPreFilterClass()
	 */
	@Override
	protected Class<?> getPreFilterClass() {
		return XmppPreFilter.class;
	}

	/**
	 * 
	 */
	public static class XmppPreFilter {

		/**
		 * @param feed
		 * @return
		 */
		public boolean filter(@Body String msg) {
			return StringUtils.isNotBlank(msg);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getProcessor()
	 */
	@Override
	protected Processor getProcessor() {

		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				DefaultMessage msg = (DefaultMessage) exchange.getIn();
				Event evt = (Event) msg.getBody();
				CamelInternalEvent internalEvt = new CamelInternalEvent();
				internalEvt.setCreationDate(evt.getCreationDate());
				internalEvt.setCreationUser(evt.getCreationUser());
				internalEvt.setUpdateDate(evt.getUpdateDate());
				internalEvt.setUpdateUser(evt.getUpdateUser());
				internalEvt.setDataSourceId(evt.getDataSourceId());
				internalEvt.setDate(evt.getDate());
				internalEvt.setFloatValue(evt.getFloatValue());
				internalEvt.setId(evt.getId());
				internalEvt.setName(evt.getName());
				internalEvt.setSessionId(evt.getSessionId());
				internalEvt.setSid(0); // for session internal consumer
				internalEvt.setType(evt.getType());
				internalEvt.setValue(evt.getValue());
				internalEvt.setValueRaw(evt.getValueRaw());
				msg.setBody(internalEvt, internalEvt.getClass());
				//
				ReplaySession replay = ReplaySession
						.findReplaySessionsBySessionIdEquals(getSessionId())
						.getSingleResult();
				replay.setEventId(evt.getId());
				replay.merge();
			}

		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.route.EsperEventProducer#getDataSource()
	 */
	@Override
	public DSSDataSource getDataSource() {
		return new DSSDataSource() {

			@Override
			public DSSUser getUpdateUser() {
				return null;
			}

			@Override
			public Date getUpdateDate() {
				return null;
			}

			@Override
			public Long getId() {
				return 0L;
			}

			@Override
			public DSSUser getCreationUser() {
				return null;
			}

			@Override
			public Date getCreationDate() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isEnabled() {
				return true;
			}

			@Override
			public DataSourceType getType() {
				return null;
			}

			@Override
			public String getRegex() {
				return null;
			}

			@Override
			public DSSUser getOwner() {
				return null;
			}

			@Override
			public String getName() {
				return ReplayEventProducer.class.getName();
			}

			@Override
			public DataSourceValueType getDataSourceValueType() {
				return DataSourceValueType.STRING;
			}
		};
	}

}
