// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.domain;

import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.DataSourceValueType;

privileged aspect DataSource_Roo_JavaBean {
    
    public String DataSource.getName() {
        return this.name;
    }
    
    public void DataSource.setName(String name) {
        this.name = name;
    }
    
    public String DataSource.getRegex() {
        return this.regex;
    }
    
    public void DataSource.setRegex(String regex) {
        this.regex = regex;
    }
    
    public DataSourceValueType DataSource.getValueType() {
        return this.valueType;
    }
    
    public void DataSource.setValueType(DataSourceValueType valueType) {
        this.valueType = valueType;
    }
    
    public Boolean DataSource.getEnabled() {
        return this.enabled;
    }
    
    public void DataSource.setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
}
