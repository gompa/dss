package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import net.jnd.thesis.common.enums.EsperExpressionType;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultComponent;
import org.apachextras.camel.component.esper.EsperComponent;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
public class EsperComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "esper";
	private static final String URI_FROM = COMPONENT_NAME + "://%s?%s";
	private static final String URI_TO = COMPONENT_NAME + "://%s";

	private String nameFrom = "test";
	private String nameTo = "test";
	private String expression;

	/**
	 * @param ctx
	 * @param nameFrom
	 * @param nameTo
	 */
	public EsperComponentHelper(CamelContext ctx, String nameFrom, String nameTo) {
		super(ctx);
		this.nameFrom = nameFrom;
		this.nameTo = nameTo;
	}

	/**
	 * @param ctx
	 * @param nameFrom
	 * @param nameTo
	 * @param type
	 * @param expression
	 */
	public EsperComponentHelper(CamelContext ctx, String nameFrom,
			String nameTo, EsperExpressionType type, String expression) {
		this(ctx, nameFrom, nameTo);
		this.expression = type.name().toLowerCase() + "=" + expression;
	}

	// /**
	// * @param ctx
	// * @param name
	// */
	// public EsperComponentHelper(CamelContext ctx, String name) {
	// super(ctx);
	// this.name = name;
	// }
	//
	// /**
	// * @param ctx
	// * @param name
	// * @param type
	// * @param expression
	// */
	// public EsperComponentHelper(CamelContext ctx, String name,
	// EsperExpressionType type, String expression) {
	// this(ctx, name);
	// this.expression = type.name() + "=" + expression;
	// }

	@Override
	protected DefaultComponent buildComponent(CamelContext ctx) {
		EsperComponent component = ctx.getComponent(COMPONENT_NAME,
				EsperComponent.class);
		return component;
	}

	@Override
	protected String getFromUri() throws OperationNotSupportedException {
		return String.format(URI_FROM, this.nameFrom, this.expression);
	}

	@Override
	protected String getToUri() throws OperationNotSupportedException {
		return String.format(URI_TO, this.nameTo);
	}

	/**
	 * @param sessionId
	 * @param isTest
	 * @return
	 */
	public static String getEsperNameInternal(Long sessionId, Boolean isTest) {
		String format = isTest ? "test_%s" : "session_%s";
		return String.format(format, sessionId);
	}

	/**
	 * @param sessionId
	 * @param user
	 * @param isTest
	 * @return
	 */
	public static String getEsperNameUser(Long sessionId, Long userId,
			Boolean isTest) {
		String esperNameInternal = getEsperNameInternal(sessionId, isTest);
		String format = "%s_%s";
		return String.format(format, esperNameInternal, userId);
	}

	/**
	 * @param sessionId
	 * @param userId
	 * @param isTest
	 * @return
	 */
	public static String getEsperNameUserComplete(Long sessionId, Long userId,
			Boolean isTest) {
		String esperNameUser = getEsperNameUser(sessionId, userId, isTest);
		return String.format(URI_TO, esperNameUser);
	}

	/**
	 * @param sessionId
	 * @param isTest
	 * @return
	 */
	public static String getEsperNameInternalComplete(Long sessionId,
			Boolean isTest) {
		String esperNameUser = getEsperNameInternal(sessionId, isTest);
		return String.format(URI_TO, esperNameUser);
	}
	
	@Override
	public String toString(){
		return "nameFrom: " + nameFrom + "\n" + "nameTo: " + nameTo + "\n" + 
	"expression: " + expression;
	}

}
