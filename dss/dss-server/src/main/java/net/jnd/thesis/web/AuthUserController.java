package net.jnd.thesis.web;

import net.jnd.thesis.domain.AuthUser;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/authusers")
@Controller
@RooWebScaffold(path = "authusers", formBackingObject = AuthUser.class)
public class AuthUserController {
}
