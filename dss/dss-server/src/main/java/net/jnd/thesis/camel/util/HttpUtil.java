package net.jnd.thesis.camel.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.cyberneko.html.parsers.DOMParser;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.DOMReader;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 */
public class HttpUtil {

	/**
	 * 
	 */
	private static final Logger LOG = Logger
			.getLogger(HttpUtil.class.getName());

	/**
	 * @param url
	 * @return
	 */
	public static String getHttpResponseString(String url) {
		StringBuffer sb = new StringBuffer();
		BufferedReader rd = null;
		try {
			URLConnection connection = new URL(url).openConnection();
			rd = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			for (String l; (l = rd.readLine()) != null; sb.append(l), LOG
					.finest(l))
				;
		} catch (IOException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			if (rd != null) {
				try {
					rd.close();
				} catch (IOException e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		}
		return sb.toString();
	}

	/**
	 * @param input
	 * @param cssQuery
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static String applyCssQuery(InputStream input, String cssQuery,
			String url) throws IOException {
		input.reset();
		Elements elements = Jsoup.parse(input, null, url).select(cssQuery);
		return elements.isEmpty() ? StringUtils.EMPTY : elements.get(0).text();
	}

	/**
	 * @param input
	 * @param xpathQuery
	 * @return
	 * @throws IOException
	 * @throws XPathExpressionException
	 * @throws SAXException
	 */
	public static String applyXpathQuery(InputStream input, String xpathQuery,
			String url) {
		String res = "";
		/*
		 * input.reset(); String preparedStr =
		 * cleanupDocString(Jsoup.parse(input, null, url) .toString());
		 * XPathExpression xpathExp = XPathFactory.newInstance().newXPath()
		 * .compile(xpathQuery); return xpathExp .evaluate(new InputSource(new
		 * StringReader(preparedStr)));
		 */
		try {
			DOMParser parser = new DOMParser();
			parser.parse(new InputSource(input));
			org.w3c.dom.Document document = parser.getDocument();
			DOMReader reader = new DOMReader();
			Document doc = reader.read(document);
			Node node = doc.selectSingleNode(xpathQuery);
			res = node.getText();
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
		return res;
	}

	/**
	 * @param originalDoc
	 * @return
	 */
	public static String cleanupDocString(String originalDoc) {
		String preparedStr = originalDoc.replaceAll(
				"(?s).*<html.*?>(.*?)</html>.*", "<html>$1</html>");
		preparedStr = preparedStr.replaceAll("(?s)<head.*?</head>", "");
		preparedStr = preparedStr.replaceAll("(?s)<script.*?</script>", "");
		preparedStr = preparedStr.replaceAll("(?s)<!--.*?-->", "");
		preparedStr = preparedStr.replaceAll("[\n\r\t]", "");
		preparedStr = preparedStr.replaceAll("  ", "");
		preparedStr = preparedStr.replaceAll("> <", "><");
		preparedStr = StringEscapeUtils.unescapeHtml4(preparedStr);
		preparedStr = preparedStr.replaceAll("&", "&amp;");
		preparedStr = preparedStr
				.replaceAll(
						"<g:plusone size=\"standard\" source=\"google:FINANCE\"></g:plusone>",
						"");
		return preparedStr;
	}

}
