package net.jnd.thesis.domain;

import java.util.List;

import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.jnd.thesis.common.enums.Pattern;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooJson
@Table(name = "DSS_INTERACT_MODEL")
@RooJpaActiveRecord(finders = { "findInteractionModelsByCreationUser" })
public class InteractionModel extends DomainEntity implements
		DSSInteractionModel {

	private String description;
	
	@Enumerated
	@NotNull
	private PatternTemplate patternTemplate;

	private Long eventDelay = 0L;

	@ManyToOne(fetch = FetchType.EAGER)
	private SessionCondition topic;

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder(String.valueOf(getPattern()));
		switch (getPattern()) {
		case PRODUCER_CONSUMER:
			res.append(String.format(" (delay: %s)", getEventDelay()));
			break;
		case PUBLISHER_SUBSCRIBER:
			res.append(String.format(" (topic: %s)", getTopic()));
			break;
		default:
			break;
		}
		return res.toString();
	}

	@Override
	public DSSSessionCondition getDssSessionCondition() {
		return getTopic();
	}

	public String getToString() {
		return toString();
	}

	@Override
	public Pattern getPattern() {
		return getPatternTemplate() != null ? Pattern
				.valueOf(getPatternTemplate().name()) : null;
	}

	public static long countInteractionModels() {
		return count(entityManager(), InteractionModel.class);
	}

	public static List<net.jnd.thesis.domain.InteractionModel> findAllInteractionModels() {
		return findAll(entityManager(), InteractionModel.class);
	}

	public static net.jnd.thesis.domain.InteractionModel findInteractionModel(
			Long id) {
		return find(entityManager(), InteractionModel.class, id);
	}
	
	public static net.jnd.thesis.domain.EsperExpression findEsperExpression(
			Long id) {
		return find(entityManager(), EsperExpression.class, id);
	}

	public static List<net.jnd.thesis.domain.InteractionModel> findInteractionModelEntries(
			int firstResult, int maxResults) {
		return findEntries(entityManager(), InteractionModel.class,
				firstResult, maxResults);
	}

}
