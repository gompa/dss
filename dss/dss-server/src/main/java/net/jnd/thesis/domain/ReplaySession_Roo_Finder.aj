// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.domain;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import net.jnd.thesis.domain.ReplaySession;

privileged aspect ReplaySession_Roo_Finder {
    
    public static TypedQuery<ReplaySession> ReplaySession.findReplaySessionsBySessionIdEquals(Long sessionId) {
        if (sessionId == null) throw new IllegalArgumentException("The sessionId argument is required");
        EntityManager em = ReplaySession.entityManager();
        TypedQuery<ReplaySession> q = em.createQuery("SELECT o FROM ReplaySession AS o WHERE o.sessionId = :sessionId", ReplaySession.class);
        q.setParameter("sessionId", sessionId);
        return q;
    }
    
}
