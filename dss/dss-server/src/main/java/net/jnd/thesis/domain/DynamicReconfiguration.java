package net.jnd.thesis.domain;

import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooJson
@Table(name = "DSS_DYNA_CONF")
@RooJpaActiveRecord(finders = { "findDynamicReconfigurationsByCreationUser" })
public class DynamicReconfiguration extends DomainEntity implements
		DSSDynamicReconfiguration {

	@NotNull
	private String description;

	private String eventName;

	private Boolean eventNameEnabled = Boolean.FALSE;

	@ManyToOne(fetch = FetchType.EAGER)
	private SessionCondition sessionCondition;

	private Boolean sessionConditionEnabled = Boolean.FALSE;

	@ManyToOne(fetch = FetchType.EAGER)
	private InteractionModel interactionModel;

	private Boolean interactionModelEnabled = Boolean.FALSE;

	@ManyToOne(fetch = FetchType.EAGER)
	private EsperExpression esperExpression;

	private Boolean esperExpressionEnabled = Boolean.FALSE;

	@Override
	public String toString() {

		Object condition = null;
		Object result = null;

		if (eventNameEnabled) {
			condition = String.format("event(%s)", eventName);
		} else if (sessionConditionEnabled) {
			condition = sessionCondition;
		}

		if (esperExpressionEnabled) {
			result = esperExpression;
		} else if (interactionModelEnabled) {
			result = interactionModel;
		}

		String res = String.format("if (%s) then apply %s", condition, result);
		return res;
	}

	// @Override
	public DSSSessionCondition getDssSessionCondition() {
		return getSessionCondition();
	}

	// @Override
	public DSSInteractionModel getDssInteractionModel() {
		return getInteractionModel();
	}

	public static long countDynamicReconfigurations() {
		return count(entityManager(), DynamicReconfiguration.class);
	}

	public static List<net.jnd.thesis.domain.DynamicReconfiguration> findAllDynamicReconfigurations() {
		return findAll(entityManager(), DynamicReconfiguration.class);
	}

	public static net.jnd.thesis.domain.DynamicReconfiguration findDynamicReconfiguration(
			Long id) {
		return find(entityManager(), DynamicReconfiguration.class, id);
	}

	public static List<net.jnd.thesis.domain.DynamicReconfiguration> findDynamicReconfigurationEntries(
			int firstResult, int maxResults) {
		return findEntries(entityManager(), DynamicReconfiguration.class,
				firstResult, maxResults);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration#
	 * getDssEsperExpression()
	 */
	@Override
	public DSSEsperExpression getDssEsperExpression() {
		return getEsperExpression();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration#isEventNameEnabled
	 * ()
	 */
	@Override
	public boolean isEventNameEnabled() {
		return getEventNameEnabled();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration#
	 * isInteractionModelEnabled()
	 */
	@Override
	public boolean isInteractionModelEnabled() {
		return getInteractionModelEnabled();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration#
	 * isEsperExpressionEnabled()
	 */
	@Override
	public boolean isEsperExpressionEnabled() {
		return getEsperExpressionEnabled();
	}
	
	/* (non-Javadoc)
	 * @see net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration#isSessionConditionEnabled()
	 */
	@Override
	public boolean isSessionConditionEnabled() {
		return getSessionConditionEnabled();
	}
	
}
