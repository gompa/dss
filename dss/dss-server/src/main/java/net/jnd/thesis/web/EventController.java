package net.jnd.thesis.web;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import net.jnd.thesis.camel.test.JPAWebSocketRoute;
import net.jnd.thesis.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/events")
@Controller
@RooWebScaffold(path = "events", formBackingObject = Event.class)
public class EventController {

    @PersistenceContext
    transient EntityManager entityManager;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @RequestMapping(value = "cleanEvents", produces = "text/html")
    public String cleanEvents() {
        cleanEventsTable();
        JPAWebSocketRoute.start(entityManagerFactory, transactionManager);
        return "events/list";
    }

    @Transactional
    private void cleanEventsTable() {
        Query q = entityManager.createQuery("delete from Event");
        q.executeUpdate();
    }
}
