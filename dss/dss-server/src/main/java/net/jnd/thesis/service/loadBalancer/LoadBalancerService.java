package net.jnd.thesis.service.loadBalancer;

import java.util.Timer;

import net.jnd.thesis.common.exception.ServiceException;
import net.jnd.thesis.service.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pedro Martins
 * 
 * This class implements the {@link Service Service} interface and it is 
 * responsible for launching and initializing all the pieces of the load 
 * balancing system. Currently, this involves the launch of the
 * {@link CpuMonitor CpuMonitor}. 
 * 
 * @see		CpuMonitor
 * @see		Service
 * @since	1.3.1
 * */
public class LoadBalancerService implements Service{

	private static final Logger LOG = LoggerFactory
			.getLogger(LoadBalancerService.class);

	/*
	 * Sigar Initialization
	 * */
	private final static long RATE = 30000;			//task repeat rate, in ms
	private final static long INNITIAL_DELAY = 0; 	//initial start delay, in ms

	private Timer cpuCheckTimer;
	private CpuMonitor cpuChecker;

	@Override
	public void start() throws ServiceException {
		
		//setting up CpuMonitor
		LOG.info(LoadBalancerService.class.getName() + ", sigarStart()");

		cpuChecker = new CpuMonitor();
		cpuCheckTimer = new Timer();
		cpuCheckTimer.schedule(cpuChecker, INNITIAL_DELAY, RATE);
	}

	@Override
	public void stop() throws ServiceException {
		LOG.info(LoadBalancerService.class.getName() + ", sigarStop()");

		if(cpuChecker != null && cpuCheckTimer != null){
			cpuCheckTimer.cancel();
			cpuChecker.cancel();
		}
	}
}
