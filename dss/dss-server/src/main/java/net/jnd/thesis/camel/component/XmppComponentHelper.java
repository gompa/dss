package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import org.apache.camel.CamelContext;
import org.apache.camel.component.xmpp.XmppComponent;
import org.apache.camel.impl.DefaultComponent;
import org.apache.commons.lang3.StringUtils;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * Xmpp connector that allows the middleware to be configured and to received
 * information from Xmpp data sources. 
 * 
 * @see		{@link ComponentHelper}
 * @since	1.2
 */
public class XmppComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "xmpp";
	
	/**
	 * URL of the Xmpp-server that the middleware will use to retrieve messages 
	 * from. It can be linked to Google's service by using the following URI:
	 * COMPONENT_NAME + "://talk.google.com:5222/%s?serviceName=gmail.com&user=%s&password=%s" + "&nickname=%s";  
	 * */
	private static final String URI_FROM = COMPONENT_NAME
			+ "://localhost:5222/%s?user=%s&password=%s"
			+ "&nickname=%s";

//	private static final String URI_FROM = COMPONENT_NAME
//			+ "://talk.google.com:5222/%s?serviceName=gmail.com&user=%s&password=%s"
//			+ "&nickname=%s";
	
	private static final String URI_TO = URI_FROM;
	
	/**
	 * The user to whom the account that the middleware will consume data from
	 * belongs to. It usually is the email. 
	 * */
	private String user = StringUtils.EMPTY;
	
	/**
	 * Password of the {@link #user}.
	 * */
	private String pass = StringUtils.EMPTY;
	
	/**
	 * The user to which the middleware will send messages to.
	 * */
	private String userTarget = StringUtils.EMPTY;
	
	/**
	 * Nickname of the #user.
	 * */
	private String nickName = StringUtils.EMPTY;

	/*
	 * Constructors
	 */

	public XmppComponentHelper(CamelContext ctx, String user, String pass) {
		super(ctx);
		this.user = user;
		this.pass = pass;
	}

	public XmppComponentHelper(CamelContext ctx, String user, String pass,
			String userTarget) {
		this(ctx, user, pass);
		if (StringUtils.isNotBlank(userTarget)) {
			this.userTarget = userTarget;
		}
	}

	public XmppComponentHelper(CamelContext ctx, String user, String pass,
			String userTarget, String nickName) {
		this(ctx, user, pass, userTarget);
		if (StringUtils.isNotBlank(nickName)) {
			this.nickName = nickName;
		}
	}

	/*
	 * Override Methods
	 */
	
	@Override
	public DefaultComponent buildComponent(CamelContext ctx) {
		XmppComponent component = ctx.getComponent(COMPONENT_NAME,
				XmppComponent.class);
		return component;
	}

	@Override
	public String getFromUri() throws OperationNotSupportedException {
		return String.format(URI_FROM, this.userTarget, this.user, this.pass,
				this.nickName);
	}

	@Override
	public String getToUri() throws OperationNotSupportedException {
		/*
		 * if a xmpp producer is started and a "receive all" xmpp consumer is
		 * present then the consumer will be disabled after the first message is
		 * produced by the producer (the producer will start to receive the
		 * messages, replacing the consumer!).
		 * 
		 * to prevent this behavior, if no target user is defined this operation
		 * is not allowed
		 */
		if (StringUtils.isNotBlank(this.userTarget)) {
			return String.format(URI_TO, this.userTarget, this.user, this.pass);
		} else {
			throw new OperationNotSupportedException();
		}
	}

}
