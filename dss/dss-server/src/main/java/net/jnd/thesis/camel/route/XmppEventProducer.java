package net.jnd.thesis.camel.route;

import java.util.Date;

import javax.naming.OperationNotSupportedException;

import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.camel.component.XmppComponentHelper;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.XmppDataSource;

import org.apache.camel.Body;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.xmpp.XmppMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
class XmppEventProducer extends EsperEventProducer {


	private static final Logger LOG = LoggerFactory
			.getLogger(XmppEventProducer.class);

	private XmppDataSource ds;

	/**
	 * @param ctx
	 * @param ds
	 * @param session
	 * @param test
	 */
	public XmppEventProducer(CamelContext ctx, XmppDataSource ds,
			Long sessionId, boolean test) {
		super(ctx, sessionId, test);
		this.ds = ds;
	}

	@Override
	protected ComponentHelper getFrom() {
		return new XmppComponentHelper(getContext(), ds.getUsername(),
				ds.getPassword(), ds.getFromUser(), ds.getName());
	}

	@Override
	protected Class<?> getPreFilterClass() {
		return XmppPreFilter.class;
	}

	@Override
	protected Processor getProcessor() {

		final DSSDataSource dataSource = this.ds;

		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				XmppMessage msg = (XmppMessage) exchange.getIn();
				String body = (String) msg.getBody();
				setMessageBodyEvent(msg, body, body, dataSource.getName());

				LOG.debug("TIME_CHECK_P1_START " + body + " " + System.currentTimeMillis() + " " + getSessionId());
				
				//DEBUG printing XmppEventProducer info
				try {
					LOG.debug("DEBUG_PATH XmppEventProducer, process(), from route: " + getFrom().from());
					LOG.debug("DEBUG_PATH XmppEventProducer, process(),  to route: " + getTo().to());
					LOG.debug("DEBUG_PATH XmppEventProducer,  process(), threadId: " + Thread.currentThread().getId());
					LOG.debug("DEBUG_PATH XmppEventProducer,  process(), messageId: " + msg.getMessageId());
				} catch (OperationNotSupportedException e) {
					e.printStackTrace();
				}

			}

		};
	}

	@Override
	public DataSource getDataSource() {
		return ds;
	}


	/**
	 * 
	 */
	public static class XmppPreFilter {

		/**
		 * @param feed
		 * @return
		 */
		public boolean filter(@Body String msg) {
			LOG.debug("DEBUG_PATH XmppPreFilter, filter(String msg), received message: " + msg + " at time: " + new Date());
			return StringUtils.isNotBlank(msg);
		}

	}
}
