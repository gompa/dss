package net.jnd.thesis.domain;

import java.util.List;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

@RooJavaBean
@Table(name = "DSS_ESPER_EXPRESSION")
@RooJpaActiveRecord(finders = { "findEsperExpressionsByCreationUser" })
public class EsperExpression extends DomainEntity implements DSSEsperExpression {

	@NotNull
    private String description;

    @NotNull
    @Enumerated
    private EsperExpressionType expressionType = EsperExpressionType.EQL;

    @NotNull
    @Size(max = 400)
    private String expressionQuery = "select * from pattern [every e=" + Event.class.getName() + "]";

    @Override
    public String toString() {
        //return String.format("[%s] %s;", expressionType, expressionQuery);
    	return String.format("%s;", description);
    }

    public static long countEsperExpressions() {
        return count(entityManager(), EsperExpression.class);
    }

    public static List<net.jnd.thesis.domain.EsperExpression> findAllEsperExpressions() {
        return findAll(entityManager(), EsperExpression.class);
    }

    public static net.jnd.thesis.domain.EsperExpression findEsperExpression(Long id) {
        return find(entityManager(), EsperExpression.class, id);
    }

    public static List<net.jnd.thesis.domain.EsperExpression> findEsperExpressionEntries(int firstResult, int maxResults) {
        return findEntries(entityManager(), EsperExpression.class, firstResult, maxResults);
    }

    @Override
    public net.jnd.thesis.common.enums.EsperExpressionType getEsperExpressionType() {
        return net.jnd.thesis.common.enums.EsperExpressionType.valueOf(getExpressionType().name());
    }
}
