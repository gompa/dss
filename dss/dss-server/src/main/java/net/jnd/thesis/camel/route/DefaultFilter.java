/**
 * 
 */
package net.jnd.thesis.camel.route;

/**
 * @author João Domingos
 * @since Jun 8, 2012
 */
public class DefaultFilter {

	/**
	 * @param feed
	 * @return
	 */
	public boolean filter(Object obj) {
		return Boolean.TRUE;
	}

	/**
	 * @param feed
	 * @return
	 */
	public boolean preFilter(Object obj) {
		return Boolean.TRUE;
	}

	/**
	 * @param feed
	 * @return
	 */
	public boolean posFilter(Object obj) {
		return Boolean.TRUE;
	}

}
