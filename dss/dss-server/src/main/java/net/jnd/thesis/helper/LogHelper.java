/**
 * 
 */
package net.jnd.thesis.helper;

import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.servlet.ws.DSSWebSocket;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public class LogHelper {

	/**
	 * @param logger
	 * @param t
	 */
	public static void error(Logger logger, Throwable t) {
		logger.error(t.getMessage(), t);
	}

	/**
	 * @param logger
	 * @param t
	 */
	public static void error(Logger logger, String msg) {
		logger.error(msg);
	}

	/**
	 * @param logger
	 * @param msg
	 * @param params
	 */
	public static void errorFormat(Logger logger, String msg, Object... params) {
		logger.error(String.format(msg, params));
	}

	/**
	 * @param logger
	 * @param msg
	 * @param params
	 */
	public static void debugFormat(Logger logger, String msg, Object... params) {
		logger.debug(String.format(msg, params));
	}

	/**
	 * @param logger
	 * @param msg
	 */
	public static void info(Logger logger, String msg) {
		logger.info(msg);
	}

	/**
	 * @param logger
	 * @param msg
	 */
	public static void debug(Logger logger, String msg) {
		logger.debug(msg);
	}

	/**
	 * @param logger
	 * @param user
	 * @param msg
	 */
	public static void debug(Logger logger, DSSUser user, String msg) {
		logger.debug(String.format("%s %s", msg, format(user)));
	}

	/**
	 * @param logger
	 * @param ws
	 * @param msg
	 */
	public static void debug(Logger logger, DSSWebSocket ws, String msg) {
		logger.debug(String.format("%s %s", msg, format(ws)));
	}

	/*
	 * 
	 */

	private static String clean(Object obj) {
		return (obj != null) ? String.valueOf(obj) : StringUtils.EMPTY;
	}

	@SuppressWarnings("unused")
	private static String build(Object obj) {
		return String.format("[{OBJECT} obj: %s]", clean(obj));
	}

	private static String format(DSSUser user) {
		String idStr = (user != null) ? clean(user.getId()) : StringUtils.EMPTY;
		String nameStr = (user != null) ? clean(user.getName())
				: StringUtils.EMPTY;
		return String.format("[{USER} id: %s; name: %s]", idStr, nameStr);
	}

	@SuppressWarnings("unused")
	private static String format(DSSSession session) {
		String idStr = (session != null) ? clean(session.getId())
				: StringUtils.EMPTY;
		String nameStr = (session != null) ? clean(session.getName())
				: StringUtils.EMPTY;
		return String.format("[{SESSION} id: %s; name: %s]", idStr, nameStr);
	}

	/**
	 * @param ws
	 * @return
	 */
	private static String format(DSSWebSocket ws) {
		// DSSUserSessionConnection connection =
		// CacheDataService.getConnection(ws
		// .getUserSessionConnectionId());
		// String userStr = clean(connection.getConnectionUser());
		// String sessionStr = clean(connection.getConnectionSession());
		// return String.format("[{WEBSOCKET} user: %s; session: %s]", userStr,
		// sessionStr);
		return String.valueOf(ws);
	}

}
