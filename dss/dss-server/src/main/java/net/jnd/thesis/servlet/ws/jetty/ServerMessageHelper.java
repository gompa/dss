/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;

/**
 * @author João Domingos
 * @since Dec 7, 2012
 */
public class ServerMessageHelper {

	/**
	 * @param msg
	 */
	static void displayMessage(ServerSideWebSocket ws, String msg) {
		System.out.println(String.format("[DSS-SERVER][%s] %s", Calendar
				.getInstance().getTime(), msg));
	}

	/**
	 * @param msg
	 * @param params
	 */
	static void displayMessage(ServerSideWebSocket ws, String msg,
			String... params) {
		System.out.println(String.format("[DSS-SERVER][%s] %s", Calendar
				.getInstance().getTime(), StringUtils.join(params, ", ")));
	}

}
