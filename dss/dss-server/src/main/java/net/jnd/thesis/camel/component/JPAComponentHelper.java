package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import org.apache.camel.CamelContext;
import org.apache.camel.component.jpa.JpaComponent;
import org.apache.camel.impl.DefaultComponent;
import org.apache.commons.lang3.StringUtils;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public class JPAComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "jpa";

	private static final String URI_FROM = new StringBuilder(COMPONENT_NAME)
			.append("://%s").append("?persistenceUnit=%s")
			.append("&consumeDelete=%s").append("&maximumResults=%s")
			.append("&consumer.delay=%s").append("&consumer.initialDelay=%s")
			.toString();

	private static final String URI_TO = new StringBuilder(COMPONENT_NAME)
			.append("://%s").append("?persistenceUnit=%s").toString();

	private String entityClass;
	private Boolean consumeDelete = Boolean.TRUE;
	private Integer maximumResults = -1;
	private Long delay = 500L;
	private Long initialDelay = 1000L;
	private String persistenceUnit = "persistenceUnit";

	private String query;

	/*
	 * 
	 */

	public JPAComponentHelper(CamelContext ctx, Class<?> entityClass) {
		super(ctx);
		this.entityClass = entityClass.getName();
	}

	// /**
	// * @param ctx
	// */
	// public JPAComponentHelper(CamelContext ctx, Class<?> entityClass) {
	// super(ctx);
	// this.entityClass = entityClass.getName();
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.jnd.thesis.camel.component.ComponentHelper#buildComponent(org.apache
	 * .camel.CamelContext)
	 */
	@Override
	protected DefaultComponent buildComponent(CamelContext ctx) {
		JpaComponent component = ctx.getComponent(COMPONENT_NAME,
				JpaComponent.class);
		return component;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.component.ComponentHelper#getFromUri()
	 */
	@Override
	protected String getFromUri() throws OperationNotSupportedException {
		String from = String.format(URI_FROM, this.entityClass,
				this.persistenceUnit, this.consumeDelete, this.maximumResults,
				this.delay, this.initialDelay);
		if (StringUtils.isNotBlank(this.query)) {
			from += "&consumer.query=" + query;
		}
		return from;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.component.ComponentHelper#getToUri()
	 */
	@Override
	protected String getToUri() throws OperationNotSupportedException {
		return String.format(URI_TO, this.entityClass, this.persistenceUnit);
	}

	/**
	 * @param entityClass the entityClass to set
	 */
	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * @param consumeDelete the consumeDelete to set
	 */
	public void setConsumeDelete(Boolean consumeDelete) {
		this.consumeDelete = consumeDelete;
	}

	/**
	 * @param maximumResults the maximumResults to set
	 */
	public void setMaximumResults(Integer maximumResults) {
		this.maximumResults = maximumResults;
	}

	/**
	 * @param delay the delay to set
	 */
	public void setDelay(Long delay) {
		this.delay = delay;
	}

	/**
	 * @param initialDelay the initialDelay to set
	 */
	public void setInitialDelay(Long initialDelay) {
		this.initialDelay = initialDelay;
	}

	/**
	 * @param persistenceUnit the persistenceUnit to set
	 */
	public void setPersistenceUnit(String persistenceUnit) {
		this.persistenceUnit = persistenceUnit;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

}
