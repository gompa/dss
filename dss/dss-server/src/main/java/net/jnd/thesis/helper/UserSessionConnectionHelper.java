package net.jnd.thesis.helper;

import java.util.Map;
import java.util.Map.Entry;

import net.jnd.thesis.common.interfaces.DSSEvent;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocketHelper;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Nov 4, 2012
 */
public class UserSessionConnectionHelper {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(UserSessionConnectionHelper.class);

	/**
	 * @param connectionId
	 * @param event
	 */
	public static void broadcast(UserSessionConnection connection, Event event) {
		if (event != null) {
			Long cid = connection.getId();
			Long uid = connection.getConnectionUser().getId();
			Long sid = connection.getConnectionSession().getId();
			ServerSideWebSocketHelper.broadcastEvent(uid, event);
			dumpProperties(sid, uid, event);
		} else {
			LOG.error("trying to send null event...");
		}
	}

	/**
	 * @param eventProperties
	 */
	private static void dumpProperties(Long sessionId, Long userId,
			DSSEvent event) {
		LogHelper.info(LOG,
				String.format("[SID: %s][UID: %s]; ", sessionId, userId));
		try {
			@SuppressWarnings("unchecked")
			Map<String, Object> bla = (Map<String, Object>) BeanUtils
					.describe(event);
			for (Entry<String, Object> entry : bla.entrySet()) {
				LogHelper.info(
						LOG,
						String.format("%s = %s; ", entry.getKey(),
								entry.getValue()));
			}
		} catch (Exception e) {
			LogHelper.error(LOG, e);
		}
	}

}
