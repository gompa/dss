// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.domain;

import net.jnd.thesis.domain.LogEntry;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect LogEntry_Roo_Configurable {
    
    declare @type: LogEntry: @Configurable;
    
}
