package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import org.apache.camel.CamelContext;
import org.apache.camel.component.http.HttpComponent;
import org.apache.camel.impl.DefaultComponent;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public class HttpComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "http";
	private static final String URI_FROM = COMPONENT_NAME + "://%s";

	private String url;

	public HttpComponentHelper(CamelContext ctx, String url) {
		super(ctx);
		this.url = String.valueOf(url).replaceFirst("http://", "");
	}

	//
	// /**
	// * @param ctx
	// * @param userTarget
	// */
	// public HttpComponentHelper(CamelContext ctx, String url) {
	// super(ctx);
	// this.url = String.valueOf(url).replaceFirst("http://", "");
	// }

	@Override
	public DefaultComponent buildComponent(CamelContext ctx) {
		HttpComponent component = ctx.getComponent(COMPONENT_NAME,
				HttpComponent.class);
		return component;
	}

	@Override
	public String getFromUri() throws OperationNotSupportedException {
		return String.format(URI_FROM, this.url);
	}

	@Override
	public String getToUri() throws OperationNotSupportedException {
		throw new OperationNotSupportedException();
	}

}
