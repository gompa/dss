package net.jnd.thesis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jnd.thesis.domain.Event;

/**
 * @author João Domingos
 * @deprecated
 */
public class DataGeneratorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -439844920817257431L;

	private static long sleepTime = 1000;

	private Thread generatorThread;

	private static boolean active = Boolean.TRUE;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		out.println(getClass().getSimpleName());
		out.close();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		out.println(getClass().getSimpleName());
		out.close();
	}

	@Override
	public void init() throws ServletException {
		this.generatorThread = new Thread(getGenerator(), "DATA_GENERATOR");
		this.generatorThread.start();
	}

	@Override
	public void destroy() {
		setActive(Boolean.FALSE);
		setSleepTime(Integer.MAX_VALUE);
		if (this.generatorThread != null) {
			this.generatorThread.interrupt();
		}
	}

	/**
	 * @return
	 */
	public static long getSleepTime() {
		return sleepTime;
	}

	/**
	 * @param sleepTime
	 */
	public static void setSleepTime(long sleepTime) {
		DataGeneratorServlet.sleepTime = sleepTime;
	}

	/**
	 * @return
	 */
	public static boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 */
	public static void setActive(boolean active) {
		DataGeneratorServlet.active = active;
	}

	/**
	 * @return
	 */
	private Runnable getGenerator() {
		return new Runnable() {
			@Override
			public void run() {
				while (isActive()) {
					generateEvent().persist();
					try {
						Thread.sleep(getSleepTime());
					} catch (InterruptedException e) {
						// e.printStackTrace();
					}
				}
			}
		};
	}

	/**
	 * @return
	 */
	private Event generateEvent() {
//		Random randomGen = new Random();
//		int val = randomGen.nextInt(40);
//		@SuppressWarnings("unused")
//		DataSource[] dataSources = DataSource.values();
//		// DataSource dataSource = dataSources[randomGen
//		// .nextInt(dataSources.length)];
//		Event event = new Event();
//		event.setValStr(String.valueOf(val));
//		// event.setDataSource(dataSource);
//		event.setDate(new Date());
//		return event;
		return null;
	}

}
