/**
 * 
 */
package net.jnd.thesis.domain;

/**
 * @author João Domingos
 * @since Nov 3, 2012
 */
public enum SessionStatus {
	NEW, STARTING, STARTED, STOPPING, PAUSED, ENDED;
}
