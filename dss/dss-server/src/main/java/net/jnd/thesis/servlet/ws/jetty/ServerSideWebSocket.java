/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import net.jnd.thesis.common.bean.BaseBean;
import net.jnd.thesis.common.bean.StringBean;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.helper.LogHelper;
import net.jnd.thesis.helper.RequestHelper;
import net.jnd.thesis.helper.RouteHelper;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.ws.services.WebSocketMessage;
import net.jnd.thesis.ws.services.WebSocketMessageHelper;
import net.jnd.thesis.ws.services.WebSocketServices;
import net.jnd.thesis.ws.services.exception.WSException;

import org.eclipse.jetty.websocket.WebSocket.OnTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Dec 4, 2012
 */
public class ServerSideWebSocket implements OnTextMessage {

	private static final Logger LOG = LoggerFactory
			.getLogger(ServerSideWebSocket.class);

	@SuppressWarnings("unused")
	private String params;

	private Connection connection;

	// private static final WebSocketServices services = ServiceLoader
	// .load(WebSocketServices.class).iterator().next();

	private final WebSocketServices services = new ServerSideServices();

	private static final Map<String, Method> serviceNames = new HashMap<String, Method>();

	private Long userSessionConnectionId;

	/**
	 * @param request
	 * @param params
	 */
	public ServerSideWebSocket(HttpServletRequest request, String params) {
		this.params = params;
		this.userSessionConnectionId = RequestHelper
				.getUserSessionConnectionId(request);
		for (Method method : services.getClass().getMethods()) {
			serviceNames.put(method.getName(), method);
		}
	}

	/**
	 * @return
	 */
	public boolean isDirectConnection() {
		return this.userSessionConnectionId != null;
	}

	@Override
	public final void onMessage(String data) {
		LOG.debug(String.format("onMessage(%s)", data));
		Long requestId = null;
		String serviceName = null;
		try {
			WebSocketMessage wsMessage = WebSocketMessageHelper
					.deserializeMessage(data);
			requestId = wsMessage.getRequestId();
			serviceName = String.valueOf(wsMessage.getServiceName());
			Method serviceMethod = serviceNames.get(serviceName);
			if (serviceMethod != null) {
				Collection<BaseBean> payload = invokeService(wsMessage,
						serviceMethod);
				WebSocketMessage response = new WebSocketMessage(requestId,
						serviceName, payload);
				sendMessage(response);
				if ("authenticate".equals(serviceMethod.getName())
						&& services.isAuthenticated()) {
					ServerSideServices srvcs = (ServerSideServices) services;
					ServerSideWebSocketHelper.put(srvcs.user.getId(), this);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			WebSocketMessage response = new WebSocketMessage(requestId,
					serviceName, e);
			sendMessage(response);
		}
	}

	/**
	 * @param wsMessage
	 * @param serviceMethod
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@SuppressWarnings("unchecked")
	private Collection<BaseBean> invokeService(WebSocketMessage wsMessage,
			Method serviceMethod) throws WSException, IllegalAccessException,
			IllegalArgumentException {
		Collection<BaseBean> payload = new ArrayList<BaseBean>();
		// build parameters
		Collection<Object> params = new ArrayList<Object>();
		for (BaseBean b : wsMessage.getPayload()) {
			if (b instanceof BaseBean) {
				params.add(((BaseBean) b).asParam());
			} else {
				params.add(b);
			}
		}
		// invoke service
		try {
			Object payloadObj = serviceMethod
					.invoke(services, params.toArray());
			if (payloadObj instanceof Collection<?>) {
				payload = (Collection<BaseBean>) payloadObj;
			} else if (payloadObj instanceof BaseBean) {
				payload.add((BaseBean) payloadObj);
			} else {
				payload.add(new StringBean(String.valueOf(payloadObj)));
			}
		} catch (InvocationTargetException e) {
			LOG.debug(e.getMessage(), e);
			throw new WSException(e.getCause());
		}
		return payload;
	}

	@Override
	public final void onOpen(Connection connection) {
		ServerMessageHelper.displayMessage(this, "onOpen(%s)",
				connection.toString());
		this.connection = connection;

		if (userSessionConnectionId != null) {
			UserSessionConnection conn = CacheDataService
					.getConnection(userSessionConnectionId);
			ServerSideCoreServices srvcs = ((ServerSideCoreServices) services);
			srvcs.setAuthenticated(conn.getConnectionUser().getId());
			ServerSideWebSocketHelper.put(srvcs.user.getId(), this);
		}

	}

	@Override
	public final void onClose(int closeCode, String message) {
		ServerMessageHelper.displayMessage(this, "onClose(%s, %s)",
				String.valueOf(closeCode), message);
		if (services.isAuthenticated()) {
			ServerSideServices srvcs = (ServerSideServices) services;
			ServerSideWebSocketHelper.remove(srvcs.user.getId());
			for (Long connId : srvcs.userConnections) {
				try {
					RouteHelper.closeUserSessionConnection(connId);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			}

		}
	}

	@Override
	public String toString() {
		return String.format("[%s][OPEN? %s]", getClass().getSimpleName(),
				this.connection != null ? this.connection.isOpen() : null);
	}

	/**
	 * @param string
	 */
	private void sendMessage(String msg) {
		sendMessage(new WebSocketMessage(null, null, new StringBean(msg)));
	}

	/**
	 * @param string
	 */
	public void sendMessageDirect(Event evt) {
		try {


			//DEBUG printing EsperEventConsumerInternal info
			LOG.debug("DEBUG_PATH ServerSideWebsocket, sendMessageDirect(Event evt), threadId: " + Thread.currentThread().getId());
			LOG.debug("DEBUG_PATH ServerSideWebsocket, sendMessageDirect(Event evt), eventId: " + evt.getId());

			LOG.debug("ServerSideWebsocket, sendMessageDirect: time in Middleware: " + getDateDiff(evt.getCreationDate(), new Date(), TimeUnit.MILLISECONDS));
			connection.sendMessage(String.valueOf(evt));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	/**
     * Calculates the difference between two dates and returns the result in the
     * specified time unit.
     * 
     * @param   startTime   the start time in milliseconds.
     * @param   endTime     the end time in milliseconds.  
     * @param   timeUnit    the TimeUnit in which to return the difference 
     *                      between the two times. 
     * @return              the difference in the specified timeUnit. This 
     *                      result suffers from the limitations of the 
     *                      {@link java.util.concurrent.TimeUnit#convert(long, java.util.concurrent.TimeUnit) }
     *                      method and so it is advised to check its 
     *                      documentation.
     * @see                 TimeUnit#convert(long, java.util.concurrent.TimeUnit) 
     * */
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}

	/**
	 * @param string
	 */
	public void sendMessageDirectJSON(Event evt) {
		try {
			WebSocketMessage msg = new WebSocketMessage(null, "broadcastEvent",
					ServerSideServicesHelper.buildEventBean(evt));
			String data = WebSocketMessageHelper.serializeMessage(msg);

			connection.sendMessage(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void close() {
		try {
			if (this.connection != null && this.connection.isOpen()) {
				sendMessage("closing connection...");
				this.connection.close();
			}
		} catch (Exception e) {
			LogHelper.error(LOG, e);
		}
	}

	/**
	 * @param msg
	 */
	void sendMessage(WebSocketMessage msg) {
		String data = WebSocketMessageHelper.serializeMessage(msg);
		try {
			if (this.connection != null && this.connection.isOpen()) {
				this.connection.sendMessage(data);
			} else {
				ServerMessageHelper
				.displayMessage(this, "connection closed...");
			}
		} catch (IOException e) {
			LogHelper.error(LOG, e);
		}
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection
	 *            the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @return the userSessionConnectionId
	 */
	public Long getUserSessionConnectionId() {
		return userSessionConnectionId;
	}
}
