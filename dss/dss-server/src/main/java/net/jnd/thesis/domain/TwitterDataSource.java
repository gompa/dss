package net.jnd.thesis.domain;

import java.util.List;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.interfaces.DSSDataSourceEventTwitter;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
@RooJavaBean
@RooJpaActiveRecord
@RooJson
@Table(name = "DSS_DATASOURCE_TWITTER")
public class TwitterDataSource extends EventDrivenDataSource implements
		DSSDataSourceEventTwitter {

	@NotNull
	@Size(max = 400)
	private String screenNames = "thesisdimfccs; TSFRadio; Visao_pt; tvi24_iol; SICNoticias; SolOnline";

	public DataSourceType getDataSourceType() {
		return DataSourceType.TWITTER;
	}

	public net.jnd.thesis.common.enums.DataSourceType getType() {
		return net.jnd.thesis.common.enums.DataSourceType
				.valueOf(getDataSourceType().name());
	}
	
//	/**
//	 * @return
//	 */
//	public static long countTwitterDataSources() {
//		return count(entityManager(), TwitterDataSource.class);
//	}
//
//	/**
//	 * @return
//	 */
//	public static List<TwitterDataSource> findAllTwitterDataSources() {
//		return findAll(entityManager(), TwitterDataSource.class);
//	}
//
//	/**
//	 * @param id
//	 * @return
//	 */
//	public static TwitterDataSource findTwitterDataSource(Long id) {
//		return find(entityManager(), TwitterDataSource.class, id);
//	}
//
//	/**
//	 * @param firstResult
//	 * @param maxResults
//	 * @return
//	 */
//	public static List<TwitterDataSource> findTwitterDataSourceEntries(
//			int firstResult, int maxResults) {
//		return findEntries(entityManager(), TwitterDataSource.class,
//				firstResult, maxResults);
//	}

}
