package net.jnd.thesis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author João Domingos
 * 
 */
public class CustomInitServlet extends HttpServlet {

	private static final long serialVersionUID = 1517381732212856858L;

	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CustomInitServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		out.println(getClass().getSimpleName());
		out.close();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		out.println(getClass().getSimpleName());
		out.close();
	}

	@Override
	public void init() throws ServletException {
		// try {
		// MQService.INSTANCE.start();
		// } catch (ServiceException e) {
		// log.error(e.getMessage(), e);
		// throw new ServletException(e);
		// }
	}

	@Override
	public void destroy() {
		// TODO
	}

}
