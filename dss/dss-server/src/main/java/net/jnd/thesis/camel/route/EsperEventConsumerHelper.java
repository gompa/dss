/**
 * 
 */
package net.jnd.thesis.camel.route;

import net.jnd.thesis.camel.component.EsperComponentHelper;
import net.jnd.thesis.common.enums.ConditionOperator;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSEvent;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;

/**
 * @author João Domingos
 * @since Jan 6, 2013
 */
class EsperEventConsumerHelper {

	/**
	 * @param condition
	 * @param event
	 * @return
	 */
	static boolean checkCondition(DSSSessionCondition condition, DSSEvent event) {
		boolean ok = false;
		ConditionOperator op = condition.getConditionOperator();
		DSSDataSource ds = condition.getDssDataSource();
		if (event.getDataSourceId() == ds.getId()) {
			String conditionValue = String.valueOf(condition
					.getConditionValue());
			Float conditionValueFloat = buildFloat(conditionValue);
			String eventValue = String.valueOf(event.getValue());
			Float eventValueFloat = buildFloat(eventValue);
			boolean isNumeric = conditionValueFloat != null
					&& eventValueFloat != null;
			switch (op) {
			case EQUALS:
				if (isNumeric) {
					ok = eventValueFloat.equals(conditionValueFloat);
				}
				break;
			case GREATER:
				if (isNumeric) {
					ok = eventValueFloat.compareTo(conditionValueFloat) > 0;
				}
				break;
			case SMALLER:
				if (isNumeric) {
					ok = eventValueFloat.compareTo(conditionValueFloat) < 0;
				}
				break;
			default:
				break;
			}
		}
		return ok;
	}

	/**
	 * @param conditionValue
	 * @return
	 */
	static Float buildFloat(String conditionValue) {
		Float res = null;
		try {
			res = new Float(conditionValue);
		} catch (Exception e) {
			// ignore
		}
		return res;
	}

	/**
	 * @return
	 */
	static String buildRecipientList(final DSSSession session) {
		/*
		 * Set<String> recipients = new HashSet<String>(); Collection<DSSUser>
		 * users = session.getUsers(); for (DSSUser user : users) { if
		 * (user.isEnabled()) {
		 * recipients.add(EsperComponentHelper.getEsperNameUserComplete(
		 * session.getId(), user.getId(), Boolean.FALSE)); } } return
		 * StringUtils.collectionToDelimitedString(recipients,
		 * EsperEventConsumer.DELIMITER_RECIPIENT_LIST);
		 */
		return EsperComponentHelper.getEsperNameInternalComplete(
				session.getId(), false);
	}

}
