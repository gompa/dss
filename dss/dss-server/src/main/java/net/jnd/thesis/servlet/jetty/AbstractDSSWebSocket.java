/**
 * 
 */
package net.jnd.thesis.servlet.jetty;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import net.jnd.thesis.helper.LogHelper;
import net.jnd.thesis.helper.RequestHelper;
import net.jnd.thesis.servlet.ws.DSSWebSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Jun 4, 2012
 */
abstract class AbstractDSSWebSocket implements CustomJettyWebSocket {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractDSSWebSocket.class);

	private UUID id;

	private DSSWebSocket.Type type;

	private Connection connection;

	private Long userSessionConnectionId;

	/**
	 * @param request
	 */
	AbstractDSSWebSocket(DSSWebSocket.Type type, HttpServletRequest request) {
		this.id = UUID.randomUUID();
		this.type = type;
		this.userSessionConnectionId = RequestHelper
				.getUserSessionConnectionId(request);
		LogHelper.debug(LOG, this, "new DataSourceWebSocket()");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jetty.websocket.WebSocket.OnTextMessage#onMessage(java.lang
	 * .String)
	 */
	@Override
	public final void onMessage(String data) {
		LogHelper.debug(LOG, this, String.format("onMessage(%s)", data));
		onMessageSpecific(data);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jetty.websocket.WebSocket#onOpen(org.eclipse.jetty.websocket
	 * .WebSocket.Connection)
	 */
	@Override
	public final void onOpen(Connection connection) {
		String formattedStr = String.format("onOpen(%s)", connection);
		LogHelper.debug(LOG, this, formattedStr);
		this.connection = connection;
		onOpenSpecific(connection);
		sendMessage("WebSocket opened...");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jetty.websocket.WebSocket#onClose(int, java.lang.String)
	 */
	@Override
	public final void onClose(int closeCode, String message) {
		String formattedStr = String.format("onClose(%s, %s)", closeCode,
				message);
		LogHelper.debug(LOG, this, formattedStr);
		onCloseSpecific(closeCode, message);
		// sendMessage("WebSocket closed...");
	}

	/**
	 * @param msg
	 */
	public void sendMessage(String msg) {
		LogHelper.debug(LOG, this, String.format("sendMessage(%s)", msg));
		try {
			if (this.connection.isOpen()) {
				this.connection.sendMessage(msg);
			}
		} catch (IOException e) {
			LogHelper.error(LOG, e);
		}
	}

	/**
	 * 
	 */
	public void close() {
		LogHelper.debug(LOG, this, "close()");
		try {
			if (this.connection.isOpen()) {
				this.connection.close();
			}
		} catch (Exception e) {
			LogHelper.error(LOG, e);
		}
	}

	/**
	 * @param connection
	 */
	public abstract void onOpenSpecific(Connection connection);

	/**
	 * @param closeCode
	 * @param message
	 */
	public abstract void onCloseSpecific(int closeCode, String message);

	/**
	 * @param data
	 */
	protected abstract void onMessageSpecific(String data);

	@Override
	public Long getUserSessionConnectionId() {
		return userSessionConnectionId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.servlet.ws.DSSWebSocket#getType()
	 */
	public Type getType() {
		return type;
	}

	public boolean isOpen() {
		return this.connection != null && this.connection.isOpen();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	// @Override
	// public String toString() {
	// DSSUserSessionConnection userConnection = CacheDataService
	// .getConnection(getUserSessionConnectionId());
	// return String.format(
	// "-- [WEBSOCKET][TYPE: %s][UID: %s][SID: %s][OPEN? %s]",
	// getType(), userConnection.getConnectionUser().getId(),
	// userConnection.getConnectionSession().getId(), isOpen());
	// }

	public UUID getId() {
		return id;
	}

}
