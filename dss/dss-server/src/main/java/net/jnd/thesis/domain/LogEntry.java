package net.jnd.thesis.domain;

import java.util.List;

import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.enums.LogType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
@RooJavaBean
@RooJpaActiveRecord
@Table(name = "DSS_LOG")
public class LogEntry extends DomainEntity {

	@NotNull
	@Enumerated
	private LogType type;

	@Size(max = 400)
	private String usr;

	@Size(max = 400)
	private String session;

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder(String.valueOf(getType()));
		// build user
		if (StringUtils.isNotBlank(getUsr())) {
			res.append(String.format(" {user: }", getUsr()));
		}
		// build session
		if (StringUtils.isNotBlank(getSession())) {
			res.append(String.format(" {session: }", getSession()));
		}
		return res.toString();
	}

	/**
	 * @return
	 */
	public static long countLogEntrys() {
		return count(entityManager(), LogEntry.class);
	}

	/**
	 * @return
	 */
	public static List<LogEntry> findAllLogEntrys() {
		return findAll(entityManager(), LogEntry.class);
	}

	/**
	 * @param id
	 * @return
	 */
	public static LogEntry findLogEntry(Long id) {
		return find(entityManager(), LogEntry.class, id);
	}

	/**
	 * @param firstResult
	 * @param maxResults
	 * @return
	 */
	public static List<LogEntry> findLogEntryEntries(int firstResult,
			int maxResults) {
		return findEntries(entityManager(), LogEntry.class, firstResult,
				maxResults);
	}

}
