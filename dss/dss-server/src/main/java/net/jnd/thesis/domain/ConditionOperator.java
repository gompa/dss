/**
 * 
 */
package net.jnd.thesis.domain;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
public enum ConditionOperator {
	GREATER {
		@Override
		public String toString() {
			return ">";
		}
	},
	SMALLER {
		@Override
		public String toString() {
			return "<";
		}
	},
	EQUALS {
		@Override
		public String toString() {
			return "<";
		}
	};

}
