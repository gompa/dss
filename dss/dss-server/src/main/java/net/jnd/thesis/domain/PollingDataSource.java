package net.jnd.thesis.domain;

import javax.validation.constraints.NotNull;

import net.jnd.thesis.common.interfaces.DSSDataSourcePolling;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
@RooJavaBean
@RooJpaActiveRecord(mappedSuperclass = true)
public abstract class PollingDataSource extends DataSource implements DSSDataSourcePolling {

	@NotNull
	private Long pollingInterval = 10000L;

	/* (non-Javadoc)
	 * @see net.jnd.thesis.common.interfaces.DSSDataSourcePolling#getInterval()
	 */
	public Long getInterval() {
		return getPollingInterval();
	}
	
}
