package net.jnd.thesis.web;

import net.jnd.thesis.domain.UserSessionConnection;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/usersessionconnections")
@Controller
@RooWebScaffold(path = "usersessionconnections", formBackingObject = UserSessionConnection.class)
public class UserSessionConnectionController {
}
