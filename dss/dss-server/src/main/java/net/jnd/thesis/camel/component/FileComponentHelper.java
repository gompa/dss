package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import org.apache.camel.CamelContext;
import org.apache.camel.component.file.FileComponent;
import org.apache.camel.impl.DefaultComponent;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public class FileComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "file";
	private static final String URI_TO = COMPONENT_NAME + "://%s";
	private static final String BASE_PATH = "C:\\home\\dev\\tmp\\camel\\";

	private String folder = "default";

	/*
	 * 
	 */

	public FileComponentHelper(CamelContext ctx, String folder) {
		super(ctx);
		this.folder = folder;
	}

	// /**
	// * @param ctx
	// */
	// public FileComponentHelper(CamelContext ctx) {
	// super(ctx);
	// }
	//
	// /**
	// * @param ctx
	// * @param folder
	// */
	// public FileComponentHelper(CamelContext ctx, String folder) {
	// this(ctx);
	// this.folder = folder;
	// }

	@Override
	protected DefaultComponent buildComponent(CamelContext ctx) {
		FileComponent component = ctx.getComponent(COMPONENT_NAME,
				FileComponent.class);
		return component;
	}

	@Override
	protected String getFromUri() throws OperationNotSupportedException {
		throw new OperationNotSupportedException();
	}

	@Override
	protected String getToUri() throws OperationNotSupportedException {
		return String.format(URI_TO, BASE_PATH + this.folder);
	}

}
