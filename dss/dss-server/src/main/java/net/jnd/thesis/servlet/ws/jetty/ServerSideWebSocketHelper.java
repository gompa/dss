/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.ws.services.WebSocketMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Jan 6, 2013
 */
public class ServerSideWebSocketHelper {

	private static final Logger LOG = LoggerFactory
			.getLogger(ServerSideWebSocketHelper.class);

	/**
	 * 
	 */
	private static Map<Long, ServerSideWebSocket> connections = Collections
			.synchronizedMap(new HashMap<Long, ServerSideWebSocket>());

	public static ServerSideWebSocket getConnection(Long userId) {
		return connections.get(userId);
	}
	
	/**
	 * @param connectionId
	 * @param evt
	 */
	//INFO: Here we send the messages to the clients !
	public static void broadcastEvent(Long userId, Event evt) {
		if (userId == null) {
			LOG.error("null  user id...");
		} else {
			ServerSideWebSocket connection = connections.get(userId);
			if (connection == null) {
				LOG.error("connection to " + userId + " is null");
			} else if (connection.isDirectConnection()) {
				// direct web connection
				connection.sendMessageDirect(evt);
			} else {
				EventBean bean = ServerSideServicesHelper.buildEventBean(evt);
				WebSocketMessage msg = new WebSocketMessage(null,
						"broadcastEvent", bean);
				connection.sendMessage(msg);
			}
		}
	}

	/**
	 * @param connectionId
	 * @param ws
	 */
	static void put(Long userId, ServerSideWebSocket ws) {
		if (userId != null && ws != null) {
			connections.put(userId, ws);
		}
	}

	/**
	 * @param connectionId
	 * @return
	 */
	static ServerSideWebSocket remove(Long userId) {
		ServerSideWebSocket res = null;
		if (userId != null) {
			res = connections.remove(userId);
		}
		return res;
	}

}
