/**
 * 
 */
package net.jnd.thesis.service;

import java.util.List;

import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.helper.DSSLogHelper;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author João Domingos
 * @since Jun 30, 2012
 */
public class CustomUserDetailsService extends HibernateDaoSupport implements
		UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		List<AuthUser> results = AuthUser.findAuthUsersByUsernameEquals(
				username).getResultList();
		if (results.size() < 1) {
			throw new UsernameNotFoundException(username);
		}
		AuthUser user = results.get(0);
		DSSLogHelper.userConnect(user);
		return user;
	}

}
