// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.domain;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.jnd.thesis.domain.DataSource;

privileged aspect DataSource_Roo_Json {
    
    public String DataSource.toJson() {
        return new JSONSerializer().exclude("*.class").serialize(this);
    }
    
    public static DataSource DataSource.fromJsonToDataSource(String json) {
        return new JSONDeserializer<DataSource>().use(null, DataSource.class).deserialize(json);
    }
    
    public static String DataSource.toJsonArray(Collection<DataSource> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
    
    public static Collection<DataSource> DataSource.fromJsonArrayToDataSources(String json) {
        return new JSONDeserializer<List<DataSource>>().use(null, ArrayList.class).use("values", DataSource.class).deserialize(json);
    }
    
}
