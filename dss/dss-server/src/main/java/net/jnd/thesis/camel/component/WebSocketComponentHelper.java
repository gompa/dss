package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import org.apache.camel.CamelContext;
import org.apache.camel.component.websocket.WebsocketComponent;
import org.apache.camel.impl.DefaultComponent;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public class WebSocketComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "websocket";
	private static final String URI_TO = COMPONENT_NAME + ":%s?sendToAll=true";
	private static final String URI_FROM = COMPONENT_NAME + ":%s";

	private String wsName = "websocket";
	private int wsPort = 9090;

	/*
	 * 
	 */

	public WebSocketComponentHelper(CamelContext ctx, String wsName, int wsPort) {
		super(ctx);
		this.wsName = wsName;
		this.wsPort = wsPort;
	}

	// /**
	// * @param ctx
	// */
	// public WebSocketComponentHelper(CamelContext ctx) {
	// super(ctx);
	// }
	//
	// /**
	// * @param ctx
	// * @param wsName
	// * @param wsPort
	// */
	// public WebSocketComponentHelper(CamelContext ctx, String wsName, int
	// wsPort) {
	// this(ctx);
	// this.wsName = wsName;
	// this.wsPort = wsPort;
	// }

	@Override
	public DefaultComponent buildComponent(CamelContext ctx) {
		WebsocketComponent component = ctx.getComponent(COMPONENT_NAME,
				WebsocketComponent.class);
		component.setPort(this.wsPort);
		component.setStaticResources("classpath:.");
		return component;
	}

	@Override
	protected String getFromUri() throws OperationNotSupportedException {
		return String.format(URI_FROM, this.wsName);
	}

	@Override
	protected String getToUri() throws OperationNotSupportedException {
		return String.format(URI_TO, this.wsName);
	}

}
