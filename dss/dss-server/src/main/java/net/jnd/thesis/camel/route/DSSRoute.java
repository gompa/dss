package net.jnd.thesis.camel.route;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * 
 * 
 * @since 1.2
 */
public interface DSSRoute {
	
	/**
	 * Starts the route. 
	 *
	 * @param 	resume
	 * @return	
	 */
	public boolean start(boolean resume);

	/**
	 * 
	 * */
	public boolean stop(boolean end);

}
