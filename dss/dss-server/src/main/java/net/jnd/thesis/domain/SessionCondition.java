package net.jnd.thesis.domain;

import java.util.List;

import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

@RooJavaBean
@Table(name = "DSS_CONDITION")
@RooJpaActiveRecord(finders = { "findSessionConditionsByCreationUser" })
public class SessionCondition extends DomainEntity implements
		DSSSessionCondition {

	@NotNull
	private String description;

	@ManyToOne(fetch = FetchType.EAGER)
	private DataSource dataSource;

	@NotNull
	@Enumerated
	private ConditionOperator operator = ConditionOperator.GREATER;

	@NotNull
	@Size(max = 40)
	private String value;

	@Override
	public String toString() {
		return String.format("%s", description);
	}

	public static long countSessionConditions() {
		return count(entityManager(), SessionCondition.class);
	}

	public static List<net.jnd.thesis.domain.SessionCondition> findAllSessionConditions() {
		return findAll(entityManager(), SessionCondition.class);
	}

	public static net.jnd.thesis.domain.SessionCondition findSessionCondition(
			Long id) {
		return find(entityManager(), SessionCondition.class, id);
	}

	public static List<net.jnd.thesis.domain.SessionCondition> findSessionConditionEntries(
			int firstResult, int maxResults) {
		return findEntries(entityManager(), SessionCondition.class,
				firstResult, maxResults);
	}

	@Override
	public DSSDataSource getDssDataSource() {
		return getDataSource();
	}

	@Override
	public net.jnd.thesis.common.enums.ConditionOperator getConditionOperator() {
		net.jnd.thesis.common.enums.ConditionOperator res = null;
		if (getOperator() != null) {
			res = net.jnd.thesis.common.enums.ConditionOperator
					.valueOf(getOperator().name());
		}
		return res;
	}

	@Override
	public String getConditionValue() {
		return getValue();
	}
	
}
