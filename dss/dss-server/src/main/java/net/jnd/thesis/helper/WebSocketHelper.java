/**
 * 
 */
package net.jnd.thesis.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.service.DSSDataService;
import net.jnd.thesis.servlet.ws.DSSWebSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Jun 4, 2012
 */
public final class WebSocketHelper {

	private static final Logger LOG = LoggerFactory
			.getLogger(WebSocketHelper.class);

	/**
	 * @return
	 */
	private static Map<Long, Map<Long, DSSWebSocket>> getSessionConnections() {
		return DSSDataService.getSessionWebSockets();
	}

	/**
	 * @return
	 */
	public static String dumpWebSockets() {
		StringBuilder sb = new StringBuilder();
		sb.append("-- WEBSOCKETS DUMP START --").append("\n");
		for (Map<Long, DSSWebSocket> sessionWebSockets : getSessionConnections()
				.values()) {
			for (DSSWebSocket ws : sessionWebSockets.values()) {
				sb.append(ws);
				sb.append("\n").append("--").append("\n");
			}
		}
		sb.append("-- WEBSOCKETS DUMP END --");
		return sb.toString();
	}

	/**
	 * @param ws
	 * @return
	 */
	private static boolean validate(DSSWebSocket ws) {
		boolean res = Boolean.TRUE;
		if (ws == null || ws.getUserSessionConnectionId() == null) {
			LogHelper.debug(LOG, ws, "invalid data");
			res = Boolean.FALSE;
		}
		return res;
	}

	/**
	 * @param sessionConnections
	 * @param connection
	 * @return
	 */
	private static Map<Long, DSSWebSocket> clearUserSessionWebsocket(
			Map<Long, Map<Long, DSSWebSocket>> sessionConnections,
			DSSUserSessionConnection connection) {
		Long uid = connection.getConnectionUser().getId();
		Long sid = connection.getConnectionSession().getId();
		Map<Long, DSSWebSocket> webSockets = sessionConnections.get(sid);
		// if the session is empty, create new websockets map
		if (webSockets == null) {
			webSockets = Collections
					.synchronizedMap(new HashMap<Long, DSSWebSocket>());
			sessionConnections.put(sid, webSockets);
		} else {
			DSSWebSocket currentWs = webSockets.remove(uid);
			if (currentWs != null) {
				currentWs.close();
			}
		}
		return webSockets;
	}

	/**
	 * @param ws
	 */
	public static void clear(DSSWebSocket ws) {
		if (validate(ws)) {
			Map<Long, Map<Long, DSSWebSocket>> sessionConnections = getSessionConnections();
			DSSUserSessionConnection connection = CacheDataService
					.getConnection(ws.getUserSessionConnectionId());
			clearUserSessionWebsocket(sessionConnections, connection);
		}
	}

	/**
	 * @param ws
	 */
	public static void set(DSSWebSocket ws) {
		if (validate(ws)) {
			Map<Long, Map<Long, DSSWebSocket>> sidConnections = getSessionConnections();
			DSSUserSessionConnection connection = CacheDataService
					.getConnection(ws.getUserSessionConnectionId());
			Map<Long, DSSWebSocket> webSockets = clearUserSessionWebsocket(
					sidConnections, connection);
			webSockets.put(connection.getConnectionUser().getId(), ws);
		}
	}

	/**
	 * @param connection
	 * @return
	 */
	private static DSSWebSocket getWebSocket(DSSUserSessionConnection connection) {
		DSSWebSocket res = null;
		Long uid = connection.getConnectionUser().getId();
		Long sid = connection.getConnectionSession().getId();
		Map<Long, DSSWebSocket> sessionWebSockets = getSessionConnections()
				.get(sid);
		if (sessionWebSockets != null) {
			res = sessionWebSockets.get(uid);
		}
		return res;
	}

	/**
	 * @param userSessionConnectionId
	 * @param msg
	 */
	public static void sendMessage(Long userSessionConnectionId, String msg) {
		DSSUserSessionConnection connection = CacheDataService
				.getConnection(userSessionConnectionId);
		DSSWebSocket webSocket = getWebSocket(connection);
		if (webSocket != null) {
			webSocket.sendMessage(msg);
		}
	}

}
