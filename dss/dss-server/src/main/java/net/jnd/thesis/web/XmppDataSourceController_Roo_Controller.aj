// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.web;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.DataSourceValueType;
import net.jnd.thesis.domain.XmppDataSource;
import net.jnd.thesis.web.XmppDataSourceController;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect XmppDataSourceController_Roo_Controller {
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String XmppDataSourceController.create(@Valid XmppDataSource xmppDataSource, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, xmppDataSource);
            return "xmppdatasources/create";
        }
        uiModel.asMap().clear();
        xmppDataSource.persist();
        return "redirect:/xmppdatasources/" + encodeUrlPathSegment(xmppDataSource.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String XmppDataSourceController.createForm(Model uiModel) {
        populateEditForm(uiModel, new XmppDataSource());
        return "xmppdatasources/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String XmppDataSourceController.show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("xmppdatasource", XmppDataSource.findXmppDataSource(id));
        uiModel.addAttribute("itemId", id);
        return "xmppdatasources/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String XmppDataSourceController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("xmppdatasources", XmppDataSource.findXmppDataSourceEntries(firstResult, sizeNo));
            float nrOfPages = (float) XmppDataSource.countXmppDataSources() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("xmppdatasources", XmppDataSource.findAllXmppDataSources());
        }
        addDateTimeFormatPatterns(uiModel);
        return "xmppdatasources/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String XmppDataSourceController.update(@Valid XmppDataSource xmppDataSource, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, xmppDataSource);
            return "xmppdatasources/update";
        }
        uiModel.asMap().clear();
        xmppDataSource.merge();
        return "redirect:/xmppdatasources/" + encodeUrlPathSegment(xmppDataSource.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String XmppDataSourceController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, XmppDataSource.findXmppDataSource(id));
        return "xmppdatasources/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String XmppDataSourceController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        XmppDataSource xmppDataSource = XmppDataSource.findXmppDataSource(id);
        xmppDataSource.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/xmppdatasources";
    }
    
    void XmppDataSourceController.addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("xmppDataSource_creationdate_date_format", "yyyy-MM-dd HH:mm:ss");
        uiModel.addAttribute("xmppDataSource_updatedate_date_format", "yyyy-MM-dd HH:mm:ss");
    }
    
    void XmppDataSourceController.populateEditForm(Model uiModel, XmppDataSource xmppDataSource) {
        uiModel.addAttribute("xmppDataSource", xmppDataSource);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("authusers", AuthUser.findAllAuthUsers());
        uiModel.addAttribute("datasourcevaluetypes", Arrays.asList(DataSourceValueType.values()));
    }
    
    String XmppDataSourceController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
