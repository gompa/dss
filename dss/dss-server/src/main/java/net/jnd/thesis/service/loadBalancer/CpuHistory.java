package net.jnd.thesis.service.loadBalancer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pedro Martins
 * 
 * Singleton that works as history of the last X usage levels of the CPU, where
 * X is determined by the {@link #maxResultsSaved maxResultsSaved} variable and 
 * can be set by the {@link #setResultsCheckNum(int) setResultsCheckNum(int)} 
 * method.
 * <p>
 * This singleton <b>is not thread safe</b>, meaning that if multiple threads 
 * try to use the {@link #getInstance() getInstance()} method, multiple 
 * instances may be created. Consistency of the other public methods is also not 
 * guaranteed in such a scenario. To change this you can use the 
 * <code>synchronized</code> java keyword, or implement a more sophisticated 
 * solution. For more information feel free to read 
 * <a href="http://www.javaworld.com/article/2073352/core-java/simply-singleton.html">Simply Singleton</a>.
 * <p>
 * However, not being thread safe is not considered to be a problem here. 
 * The #getInstance() method will be first called by a single thread that will 
 * be responsible for monitoring this machine's CPU, and the 
 * {@link #addCpuCharge(double)  addCpuCharge(double)} method will only be 
 * called every Y seconds by that same thread. This way, even if multiple 
 * threads access the #getInstance() and the
 * {@link #getResultsAvg() getResultsAvg()} methods, there will not be a problem
 * because the instance will be already instantiated by then (and so both 
 * threads will still have a reference to the same object), and because the 
 * #getResultsAvg() method is atomic.
 * <p>
 * The {@link #setResultsCheckNum(int) setResultsCheckNum(int)} method should 
 * only be called by the same single thread that initializes the singleton and
 * checks for this VMs CPU usage, as it is provided as a "in case it is needed"
 * method.
 * <p>
 * Currently, the thread checking the CPU usage levels and updating this 
 * Singleton is define by the {@link CpuMonitor CpuMonitor} class.
 * 
 * @see 	<a href="http://www.javaworld.com/article/2073352/core-java/simply-singleton.html">Simply Singleton</a>
 * @see		CpuMonitor
 * @since	1.0.2
 * */
public class CpuHistory {

	/**
	 * Initial size of the list that stores the cpu usage results.
	 * */
	public static final int MAX_RESULTS_SAVED_INIT = 10;
	
	private static CpuHistory instance = null;
	private int maxResultsSaved;
	private List<Double> results;
	private double avg, sum;
	
	/**
	 * Private constructor that initializes the Singleton in case it was not
	 * initialized before. It is private to prevent further instantiation of 
	 * this class, even from classes that are inside the same package and even
	 * for classes that may extend it.
	 * <p>
	 * It also initializes the list that saves the cpu usage results to a size
	 * of {@link #MAX_RESULTS_SAVED_INIT MAX_RESULTS_SAVED_INIT}, which is 10.
	 * */
	private CpuHistory(){
		maxResultsSaved = MAX_RESULTS_SAVED_INIT;
		results = new ArrayList<Double>(maxResultsSaved);
		avg = -1; 
		sum = 0;
	}

	/**
	 * Returns the CpuHistory Singleton that already exists, and it initializes 
	 * one in case it is called by the first time. It is not thread-safe. 
	 * 
	 * @return	the CpuHistory singleton
	 * */
	public static CpuHistory getInstance(){
		if(instance == null)
			instance = new CpuHistory();

		return instance;
	}

	/**
	 * Adds a cpu usage value to the history of cpu usage values being kept and
	 * immediately calculates the average of all the values being kept after. If
	 * the value being added exceeds the number of values the history is allowed 
	 * to save, then we replace the oldest cpu usage value being kept by the new
	 * one.
	 * 
	 * @param	currentCharge	the current level of cpu usage that is going to 
	 * 							be add to the history list.
	 * */
	public void addCpuCharge(double currentCharge){
		if(results.size() < maxResultsSaved){
			results.add(currentCharge);
			updateAvg();
		}else{
			results.add(results.size() % maxResultsSaved, currentCharge);
			updateAvg();
		}
	}

	/**
	 * Auxiliary method that calculates the average of all the cpu usage levels
	 * currently story. For internal use only.
	 * */
	private void updateAvg(){
		for(double number : results)
			sum += number;
		avg = sum / results.size();
		sum = 0;
	}

	/**
	 * Returns the average of all the results stored in the list.
	 * 
	 * @return	the average of all the results stored.
	 * */
	public double getResultsAvg(){
		return avg;
	}

	/**
	 * Changes the number of results saved in the list. If the number of results
	 * increases, than we simply keep adding them until we fill the list, if the
	 * number of results to be saved decreases, then we clear the list and start
	 * from zero.
	 * 
	 * @param	newNum	the new size of the list that stores the cpu average 
	 * 					results	
	 * */
	public void setResultsCheckNum(int newNum){
		if(newNum < maxResultsSaved)
			results.clear();

		maxResultsSaved = newNum;
	}
}
