package net.jnd.thesis.service.loadBalancer;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Pedro Martins
 * 
 * Interface that exposes the methods offered by the RMI server deployed in Dad.
 * <p>
 * This interface is implemented by the class 
 * {@link RMICommunicator RMICommunicator}.
 * <p>
 * If this EC2 instance is Dad, then the implementation will simply call the 
 * {@link VMManager VMManager} singleton and ask it to fulfill the request to 
 * the AWS cloud. If this EC2 instance is a client, then the implementation 
 * sends a request to Dad's VMManager for it to fulfill the request.
 * 
 * @see		RMICommunicator
 * @see		VMManager
 * @since	1.0.0
 */
public interface ICommunicator extends Remote{
	
	/**
	 * Ask for a new VM.
	 * 
	 * @return					<code>true</code> if the request was successful, 
	 * 							or <code>false</code> if it failed or was denied 
	 * @throws 	RemoteException	if this EC2 instance is a son and there was a 
	 * 							problem communicating with Dad
	 */
	public boolean askNewVM() throws RemoteException;
	
	/**
	 * Kill target VM.
	 * 
	 * @param	anInstanceId	the id of the EC2 instance we wish to terminate
	 * @return					<code>true</code> if the request was successful,
	 * 							 or <code>false</code> if it failed or was 
	 * 							denied 
	 * @throws 	RemoteException	if this EC2 instance is a son and there was a 
	 * 							problem communicating with Dad
	 */
	public boolean killVM(String anInstanceId) throws RemoteException;
}
