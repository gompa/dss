package net.jnd.thesis.camel.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class TwitterUtil {

	/**
	 * 
	 */
	private static final Logger LOG = Logger.getLogger(TwitterUtil.class
			.getName());

	/**
	 * 
	 */
	private static final String USER_URL = "https://api.twitter.com/1/users/show.xml?screen_name=%s";

	/**
	 * @param username
	 * @return
	 * @throws MalformedURLException
	 */
	public static String getTwitterUserFullXml(String screenName)
			throws MalformedURLException {
		URL url = getTwitterUserFullXmlUrl(screenName);
		return HttpUtil.getHttpResponseString(url.getPath());
	}

	/**
	 * @param screenName
	 * @return
	 * @throws MalformedURLException
	 */
	public static URL getTwitterUserFullXmlUrl(String screenName)
			throws MalformedURLException {
		return new URL(String.format(USER_URL, screenName));
	}

	/**
	 * @param screenName
	 * @return
	 */
	public static Long getUserIdFromScreenName(String screenName) {
		Long res = null;
		try {
			URL url = getTwitterUserFullXmlUrl(screenName);
			JAXBContext jaxbContext = JAXBContext
					.newInstance(TwitterUser.class);
			TwitterUser user = (TwitterUser) jaxbContext.createUnmarshaller()
					.unmarshal(url);
			LOG.fine(String.format("screenName = %s -> userId = %s",
					screenName, user.getId()));
			if (user != null && StringUtils.isNumeric(user.getId())) {
				res = Long.valueOf(user.getId());
			}
		} catch (MalformedURLException e) {
			LOG.log(Level.FINE, e.getMessage(), e);
		} catch (JAXBException e) {
			LOG.log(Level.FINE, e.getMessage(), e);
		}
		return res;
	}

	/**
	 *
	 */
	@XmlRootElement(name = "user")
	private static class TwitterUser {

		private String id;

		public String getId() {
			return id;
		}

		@SuppressWarnings("unused")
		@XmlElement
		public void setId(String id) {
			this.id = id;
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String screenName = "thesisdimfccs";
		String userId = String.valueOf(getUserIdFromScreenName(screenName));
		System.out.println(String.format("screenName = %s\r\nuserId = %s",
				screenName, userId));
	}
}
