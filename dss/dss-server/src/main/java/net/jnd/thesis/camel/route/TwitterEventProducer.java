package net.jnd.thesis.camel.route;

import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.camel.component.TwitterComponentHelper;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.TwitterDataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;

import twitter4j.Status;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
class TwitterEventProducer extends EsperEventProducer {

	private String consumerKey = "k5daNvn0cpzNL7hbD3Q9UQ";
	private String consumerSecret = "4Hk2E89uwgE63WAc5noPsM03gCkHZiUYgZ0eNNkA";
	private String accessToken = "568373904-knwoLVTXvrKgkgYJfQSkbOP9xV44IMZmNE1QvhwY";
	private String accessTokenSecret = "EkznKl8OiEz2zCwExCjxiAoBo7n98SqCqoQ00i60";
	// private String screenNames = "thesisdimfccs";

	private TwitterDataSource ds;

	/*
	 * 
	 */

	/**
	 * @param ctx
	 * @param ds
	 * @param session
	 * @param test
	 */
	public TwitterEventProducer(CamelContext ctx, TwitterDataSource ds,
			Long sessionId, boolean test) {
		super(ctx, sessionId, test);
		this.ds = ds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getProcessor()
	 */
	@Override
	protected Processor getProcessor() {

		final DSSDataSource dataSource = this.ds;
		
		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				DefaultMessage msg = (DefaultMessage) exchange.getIn();
				Status body = (Status) msg.getBody();
				setMessageBodyEvent(msg, body.getText(), body.getText(), dataSource.getName());
			}

		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getFrom()
	 */
	@Override
	protected ComponentHelper getFrom() {
		return new TwitterComponentHelper(getContext(), consumerKey,
				consumerSecret, accessToken, accessTokenSecret,
				ds.getScreenNames());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getDataSource()
	 */
	@Override
	public DataSource getDataSource() {
		return ds;
	}

}
