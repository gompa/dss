package net.jnd.thesis.service;

import java.rmi.RemoteException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jnd.thesis.common.exception.ServiceException;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.ConnectionStatus;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.EsperExpression;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.EventType;
import net.jnd.thesis.domain.HttpDataSource;
import net.jnd.thesis.domain.InteractionModel;
import net.jnd.thesis.domain.RssDataSource;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionStatus;
import net.jnd.thesis.domain.TwitterDataSource;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.domain.XmppDataSource;
import net.jnd.thesis.helper.RouteHelper;
import net.jnd.thesis.helper.UserSessionConnectionHelper;
import net.jnd.thesis.service.loadBalancer.VMManager;
import net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocketHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Nov 1, 2012
 */
public class CacheDataService implements Service {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(CacheDataService.class);

	/**
	 * 
	 */
	private static boolean initialized = false;

	/**
	 * 
	 */
	private static final Map<Long, DataSource> dataSources = Collections
			.synchronizedMap(new HashMap<Long, DataSource>());

	/**
	 * 
	 */
	private static final Map<Long, AuthUser> users = Collections
			.synchronizedMap(new HashMap<Long, AuthUser>());

	/**
	 * 
	 */
	private static final Map<Long, Session> sessions = Collections
			.synchronizedMap(new HashMap<Long, Session>());

	/**
	 * 
	 */
	private static final Map<Long, UserSessionConnection> connections = Collections
			.synchronizedMap(new HashMap<Long, UserSessionConnection>());

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.Service#start()
	 */
	@Override
	public void start() throws ServiceException {
		if (!initialized) {
			reset();
			// build users
			for (AuthUser authUser : AuthUser.findAllAuthUsers()) {
				users.put(authUser.getId(), authUser);
			}
			// build dataSources
			for (DataSource dataSource : HttpDataSource
					.findAllHttpDataSources()) {
				dataSources.put(dataSource.getId(), dataSource);
			}
			for (DataSource dataSource : TwitterDataSource
					.findAllTwitterDataSources()) {
				dataSources.put(dataSource.getId(), dataSource);
			}
			for (DataSource dataSource : RssDataSource.findAllRssDataSources()) {
				dataSources.put(dataSource.getId(), dataSource);
			}
			for (DataSource dataSource : XmppDataSource
					.findAllXmppDataSources()) {
				dataSources.put(dataSource.getId(), dataSource);
			}
			// build sessions
			List<Session> dbSessions = Session.findAllSessions();
			for (Session session : dbSessions) {
				sessions.put(session.getId(), session);
			}
			// build connections
			List<UserSessionConnection> dbConnections = UserSessionConnection
					.findAllUserSessionConnections();
			for (UserSessionConnection connection : dbConnections) {
				connections.put(connection.getId(), connection);
			}
			// set init flag
			initialized = true;
			// set all connections to closed
			for (UserSessionConnection connection : dbConnections) {
				connection.setStatus(ConnectionStatus.CLOSED);
				connection.merge();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.Service#end()
	 */
	@Override
	public void stop() throws ServiceException {
		if (initialized) {
			reset();
			initialized = false;
		}
	}

	/**
	 * 
	 */
	private void reset() {
		users.clear();
		dataSources.clear();
		sessions.clear();
	}

	/*
	 * 
	 */

	/**
	 * @param sessionId
	 * @return
	 */
	public static Session getSession(Long sessionId) {
		Session res = null;
		if (!initialized) {
			throw new IllegalStateException("Service not initialized");
		}
		//
		try {
			res = Session.findSession(sessionId);
		} catch (Exception e) {
			LOG.debug(e.getMessage(), e);
		}
		//
		if (res == null) {
			res = sessions.get(sessionId);
		}
		//
		return res;
	}

	/**
	 * @param username
	 * @return
	 */
	public static AuthUser getUserByUsername(String username) {
		if (!initialized) {
			throw new IllegalStateException("Service not initialized");
		}
		return AuthUser.findAuthUsersByUsernameEquals(username)
				.getSingleResult();
	}

	/**
	 * @param userId
	 * @return
	 */
	public static AuthUser getUser(Long userId) {
		if (!initialized) {
			throw new IllegalStateException("Service not initialized");
		}
		// return users.get(userId);
		return AuthUser.findAuthUser(userId);
	}

	/**
	 * @param dataSourceId
	 */
	public static DataSource getDataSource(Long dataSourceId) {
		if (!initialized) {
			throw new IllegalStateException("Service not initialized");
		}
		return DataSource.findDataSource(dataSourceId);
	}

	/**
	 * @param id
	 * @return
	 */
	public static UserSessionConnection getConnection(Long id) {
		if (!initialized) {
			throw new IllegalStateException("Service not initialized");
		}
		return UserSessionConnection.findUserSessionConnection(id);
	}

	/**
	 * @param obj
	 */
	public static void update(Object obj) {
		LOG.debug("dummy update(" + obj + ") -> doing nothing");
		// do nothing
	}

	/**
	 * @param user
	 */
	public static void update(AuthUser user) {
		LOG.debug(String.format("update(AuthUser: %s)", user.getId()));
		users.put(user.getId(), user);
	}

	/**
	 * @param ds
	 */
	public static void update(DataSource ds) {
		LOG.debug(String.format("update(DataSource: %s)", ds.getId()));
		dataSources.put(ds.getId(), ds);
	}

	/**
	 * @param session
	 */
	public static void update(final Session session) {
		LOG.debug(CacheDataService.class.getName() + " -> " + 
				"void update(final Session session)" +  " -> session: " + session.getId());

		try {
			VMManager manager = VMManager.getInstance();
			
			LOG.debug("Checking if new VM is needed");
			if(manager.isNewVMNeeded())
				if(manager.launchNewVM()) 
					LOG.warn("Launched new VM successfuly");
				else 
					LOG.warn("Request for new VM was denied");
		} catch (RemoteException e) {
			LOG.error("Could not communicate with dad or AWS service when "
					+ "asking for a new VM: " + e);
		}

		sessions.put(session.getId(), session);
		new Thread(new Runnable() {

			@Override
			public void run() {
				RouteHelper.updateSessionRoutes(session, true, true, true);
			}

		}).start();

	}

	/**
	 * @param userSessionConnection
	 */
	public static void update(final UserSessionConnection connection) {
		LOG.debug(String.format("update(UserSessionConnection: %s)", connection.getId()));
		connections.put(connection.getId(), connection);
		new Thread(new Runnable() {

			@Override
			public void run() {
				RouteHelper.updateConnectionRoutes(connection);
			}

		}).start();
	}

	/**
	 * @param sessionId
	 * @param status
	 */
	public static void updateSessionStatus(final Session session,
			final SessionStatus status) {
		LOG.debug(String.format("updateSessionStatus(%s,%s)", session.getId(), status));
		//		new Thread(new Runnable() {
		//
		//			@Override
		//			public void run() {
		Session s = getSession(session.getId());
		s.setSessionStatus(status);
		s.merge();
		// generate event
		Event e = new Event();
		e.setSession(session);
		e.setEventType(EventType.SESSION_STATUS);
		e.setValueRaw(String.valueOf(status));
		e.merge();
		//			}

		//		}).start();
	}

	/**
	 * @param connectionId
	 * @param status
	 */
	public static void updateConnectionStatus(
			final UserSessionConnection connection,
			final ConnectionStatus status) {
		LOG.debug(String.format("updateConnectionStatus(%s,%s)", connection.getId(), status));
		//		new Thread(new Runnable() {
		//
		//			@Override
		//			public void run() {
		UserSessionConnection c = getConnection(connection.getId());
		c.setStatus(status);
		c.merge();
		// generate event
		Event e = new Event();
		e.setSession(connection.getSession());
		e.setEventType(EventType.CONNECTION_STATUS);
		e.setValueRaw(String.format("[%s] %s", connection.getUsr(),
				String.valueOf(status)));
		e.merge();

		//			}
		//
		//		}).start();
	}

	/**
	 * @param sessionId
	 * @param interactionModelId
	 */
	public static void updateSessionInteractionModel(final Session session,
			final Long newInteractionModelId, final Long newEsperExpressionId) {
		LOG.debug(String.format("updateSessionInteractionModel(%s, %s,%s)", session.getId(), newInteractionModelId, newEsperExpressionId));
		//		new Thread(new Runnable() {
		//
		//			@Override
		//			public void run() {

		Event e = null;
		Session s = getSession(session.getId());
		if (newInteractionModelId != null) {
			InteractionModel newInteractionModel = InteractionModel
					.findInteractionModel(newInteractionModelId);
			s.setModifiedInteractionModel(session.getInteractionModel());
			s.setInteractionModel(newInteractionModel);
			s.merge();
			// generate event
			e = new Event();
			e.setSession(session);
			e.setEventType(EventType.SESSION_INTERACTION_MODEL);
			e.setValue(String.valueOf(newInteractionModel
					.getDescription()));
			e.setValueRaw(String.valueOf(newInteractionModelId));
			e.merge();
		} else if (newEsperExpressionId != null) {
			EsperExpression newEsperExpression = EsperExpression
					.findEsperExpression(newEsperExpressionId);
			s.setEsperExpression(newEsperExpression);
			s.merge();
			// generate event
			e = new Event();
			e.setSession(session);
			e.setEventType(EventType.SESSION_ESPER_EXPRESSION);
			e.setValue(String.valueOf(newEsperExpression
					.getDescription()));
			e.setValueRaw(String.valueOf(newEsperExpressionId));
			e.merge();
		}

		// broadcast event
		if (e != null) {
			for (DSSUser dssUser : s.getUsers()) {
				ServerSideWebSocketHelper.broadcastEvent(
						dssUser.getId(), e);
			}
		}

		//			}
		//
		//		}).start();

	}

	/**
	 * @param connectionId
	 * @param interactionModelId
	 */
	public static void updateConnectionInteractionModel(
			UserSessionConnection conn, Long newInteractionModelId,
			Long newEsperExpressionId) {
		LOG.debug(String.format("updateConnectionInteractionModel(%s, %s,%s)",
				conn.getId(), newInteractionModelId, newEsperExpressionId));
		Event e = null;
		if (newInteractionModelId != null) {
			InteractionModel newInteractionModel = InteractionModel
					.findInteractionModel(newInteractionModelId);
			conn.setModifiedInteractionModel(conn.getInteractionModel());
			conn.setInteractionModel(newInteractionModel);
			conn.merge();
			// generate event
			e = new Event();
			e.setSession(conn.getSession());
			e.setEventType(EventType.CONNECTION_INTERACTION_MODEL);
			e.setValue(String.valueOf(newInteractionModel.getDescription()));
			e.setValueRaw(String.valueOf(newInteractionModelId));
			e.merge();
		} else if (newEsperExpressionId != null) {
			EsperExpression newEsperExpression = EsperExpression
					.findEsperExpression(newEsperExpressionId);
			conn.setEsperExpression(newEsperExpression);
			conn.merge();
			// generate event
			e = new Event();
			e.setSession(conn.getSession());
			e.setEventType(EventType.SESSION_ESPER_EXPRESSION);
			e.setValue(String.valueOf(newEsperExpression.getDescription()));
			e.setValueRaw(String.valueOf(newEsperExpressionId));
			e.merge();
		}

		// broadcast event
		if (e != null) {
			UserSessionConnectionHelper.broadcast(conn, e);
		}

	}

}
