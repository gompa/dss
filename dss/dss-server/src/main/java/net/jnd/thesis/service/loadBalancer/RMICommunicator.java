package net.jnd.thesis.service.loadBalancer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


/**
 * @author Pedro Martins
 * 
 * The Class RMICommunicator, implementation of the 
 * {@link ICommunicator ICommunicator} interface. 
 * 
 * @see ICommunicator
 * @since 1.0.0
 */
public class RMICommunicator extends UnicastRemoteObject implements ICommunicator{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Instantiates a new RMI communicator, it is only called by Dad. The sons
	 * do not create their own communicators, instead they try to use the Dad's 
	 * one so they can communicate with him.
	 *
	 * @throws RemoteException	dummy exception required for compile reasons
	 */
	public RMICommunicator() throws RemoteException {
		super();
	}

	/* (non-Javadoc)
	 * @see net.jnd.thesis.service.loadBalancer.ICommunicator#askNewVM()
	 */
	@Override
	public boolean askNewVM()  throws RemoteException{
		return VMManager.getInstance().launchNewVM();
	}

	/* (non-Javadoc)
	 * @see net.jnd.thesis.service.loadBalancer.ICommunicator#killVM(java.lang.String)
	 */
	@Override
	public boolean killVM(String anInstanceId) throws RemoteException{
		return VMManager.getInstance().killVM(anInstanceId);
	}

}
