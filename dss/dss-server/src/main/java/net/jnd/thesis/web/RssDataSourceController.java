package net.jnd.thesis.web;

import net.jnd.thesis.domain.RssDataSource;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/rssdatasources")
@Controller
@RooWebScaffold(path = "rssdatasources", formBackingObject = RssDataSource.class)
public class RssDataSourceController {
}
