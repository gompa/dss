package net.jnd.thesis.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.helper.RequestHelper;
import net.jnd.thesis.service.CacheDataService;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooJson
@Table(name = "DSS_USER_SESSION_CONNECTION")
@RooJpaActiveRecord(finders = { "findUserSessionConnectionsByCreationUser" })
public class UserSessionConnection extends DomainEntity implements
		DSSUserSessionConnection {

	@ManyToOne(fetch = FetchType.EAGER)
	private AuthUser usr;

	private String description;

	@NotNull
	@ManyToOne(fetch = FetchType.EAGER)
	private Session session;

	@NotNull
	@Enumerated
	private ConnectionStatus status = ConnectionStatus.CLOSED;

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<DynamicReconfiguration> dynamicReconfigurations = new HashSet<DynamicReconfiguration>();

	@ManyToOne(fetch = FetchType.EAGER)
	private InteractionModel interactionModel;

	@ManyToOne(fetch = FetchType.EAGER)
	private InteractionModel modifiedInteractionModel;

	@ManyToOne(fetch = FetchType.EAGER)
	private EsperExpression esperExpression;

	@SuppressWarnings("unused")
	@PrePersist
	@PreUpdate
	private void buildUser() {
		setUsr(RequestHelper.getCurrentUser(true));
	}

	@Override
	public String toString() {
		return String.format("{%s}{%s}{%s}", getSession(), getUsr(),
				getStatus());
	}

	public static long countUserSessionConnections() {
		return count(entityManager(), UserSessionConnection.class);
	}

	public static List<net.jnd.thesis.domain.UserSessionConnection> findAllUserSessionConnections() {
		return findAll(entityManager(), UserSessionConnection.class);
	}

	public static net.jnd.thesis.domain.UserSessionConnection findUserSessionConnection(
			Long id) {
		return find(entityManager(), UserSessionConnection.class, id);
	}

	public static List<net.jnd.thesis.domain.UserSessionConnection> findUserSessionConnectionEntries(
			int firstResult, int maxResults) {
		return findEntries(entityManager(), UserSessionConnection.class,
				firstResult, maxResults);
	}

	@PostPersist
	@PostUpdate
	protected void updateCache() {
		CacheDataService.update((DSSUserSessionConnection) this);
	}

	@Override
	public DSSSession getConnectionSession() {
		return getSession();
	}

	@Override
	public DSSUser getConnectionUser() {
		return getCreationUser();
	}

	@Override
	public net.jnd.thesis.common.enums.ConnectionStatus getConnectionStatus() {
		net.jnd.thesis.common.enums.ConnectionStatus res = null;
		if (getStatus() != null) {
			res = net.jnd.thesis.common.enums.ConnectionStatus
					.valueOf(getStatus().name());
		}
		return res;
	}

	@Override
	public void setConnectionStatus(
			net.jnd.thesis.common.enums.ConnectionStatus status) {
		if (status != null) {
			setStatus(ConnectionStatus.valueOf(status.name()));
		}
	}

	@Override
	public DSSInteractionModel getConnectionInteractionModel() {
		return getModifiedInteractionModel() != null ? getModifiedInteractionModel()
				: getInteractionModel();
	}

	@Override
	public Collection<net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration> getDynamicReconfs() {
		Collection<DSSDynamicReconfiguration> c = new ArrayList<DSSDynamicReconfiguration>();
		for (DynamicReconfiguration dr : getDynamicReconfigurations()) {
			c.add(dr);
		}
		return c;
	}

	@Override
	public DSSEsperExpression getConnectionEsperExpression() {
		return esperExpression;
	}

}
