package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import org.apache.camel.CamelContext;
import org.apache.camel.component.rss.RssComponent;
import org.apache.camel.impl.DefaultComponent;

/**
 * @author João Domingos
 * @since Jun 6, 2012
 */
public class RssComponentHelper extends ComponentHelper {

	private static final String COMPONENT_NAME = "rss";
	private static final String URI_FROM = COMPONENT_NAME
			+ ":%s?splitEntries=true&consumer.initialDelay=%s&consumer.delay=%s";

	private String rssUri = "<NULL rssUri>";
	private long consumerInitialDelay = 10 * 1000;
	private long consumerDelay = 30 * 1000;

	public RssComponentHelper(CamelContext ctx, String rssUri,
			long consumerInitialDelay, long consumerDelay) {
		super(ctx);
		this.rssUri = rssUri;
		this.consumerDelay = consumerDelay;
		this.consumerInitialDelay = consumerInitialDelay;
	}

	// /**
	// * @param ctx
	// * @param folder
	// */
	// public RssComponentHelper(CamelContext ctx, String rssUri,
	// long consumerInitialDelay, long consumerDelay) {
	// super(ctx);
	// this.rssUri = rssUri;
	// this.consumerDelay = consumerDelay;
	// this.consumerInitialDelay = consumerInitialDelay;
	// }

	@Override
	protected DefaultComponent buildComponent(CamelContext ctx) {
		RssComponent component = ctx.getComponent(COMPONENT_NAME,
				RssComponent.class);
		return component;
	}

	@Override
	protected String getFromUri() throws OperationNotSupportedException {
		return String.format(URI_FROM, this.rssUri, this.consumerInitialDelay,
				this.consumerDelay);
	}

	@Override
	protected String getToUri() throws OperationNotSupportedException {
		throw new OperationNotSupportedException();
	}

}
