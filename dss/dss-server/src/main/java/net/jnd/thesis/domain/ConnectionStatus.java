/**
 * 
 */
package net.jnd.thesis.domain;

/**
 * @author João Domingos
 * @since Nov 3, 2012
 */
public enum ConnectionStatus {
	OPEN, CLOSED;
}
