package net.jnd.thesis.camel.component;

import javax.naming.OperationNotSupportedException;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultComponent;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * Connector layer build on top of Camel to allow and facilitate the integration
 * of heterogeneous datasources via creation of customized Camel endpoints. 
 * <p>
 * This abstract class and its concrete implementations only create the Camel 
 * endpoints through the {@link #buildComponent(CamelContext)} method and alllow
 * access to their static configuration, through the {@link #getFromUri()} and 
 * {@link #getToUri()} methods. In the specific case of the last two methods, 
 * if a concrete implementation should not support it, it throws the java
 * {@link OperationNotSupportedException}.
 * <p>
 * This class is officially implemented by the {@link HttpComponentHelper}, 
 * {@link RssComponentHelper}, {@link TwitterComponentHelper} and the 
 * {@link XmppComponentHelper} classes. However, other implementations not 
 * described in the thesis are also available. These are the 
 * {@link FileComponentHelper}, {@link JMSComponentHelper}, 
 * {@link JPAComponentHelper} and the {@link WebSocketComponentHelper}.
 * 
 * @see		#buildComponent(CamelContext)
 * @see		#getFromUri()
 * @see		#getToUri()
 * @see		HttpComponentHelper
 * @see 	RssComponentHelper
 * @see		TwitterComponentHelper
 * @see		XmppComponentHelper
 * @see 	FileComponentHelper
 * @see		JMSComponentHelper
 * @see		JPAComponentHelper
 * @see		WebSocketComponentHelper
 * @see 	OperationNotSupportedException
 * @since	1.2
 */
public abstract class ComponentHelper {


//	@SuppressWarnings("unused")
//	private DefaultComponent component;

	private CamelContext ctx;
	private boolean initialized;

	/**
	 * Constructor of this abstract class. Must be called by all implementations
	 * by using the java super() method. 
	 * 
	 * @param ctx	the {@link CamelContext} going to be used by the concrete 
	 * 				components.
	 */
	public ComponentHelper(CamelContext ctx) {
//		super();
		this.ctx = ctx;
	}

	/**
	 * Initializes this component by calling the 
	 * {@link #buildComponent(CamelContext)} method and setting the #initialized
	 * variable to <code>true</code>.
	 * 
	 * @see	#buildComponent(CamelContext)
	 */
	private void initialize() {
//		this.component = buildComponent(this.ctx);
		buildComponent(this.ctx);
		initialized = true;
	}

	/**
	 * Does the same that the #getFromUri() method does, but it makes sure that
	 * the component has been initialized first.
	 * 
	 * @return									the path of the route from which
	 * 											this component is consuming 
	 * 											events.
	 * @throws 	OperationNotSupportedException	if this component is not 
	 * 											consuming data from any 
	 * 											endpoint.
	 * @see										#getFromUri()
	 */
	public String from() throws OperationNotSupportedException {
		if (!initialized) {
			initialize();
		}
		return getFromUri();
	}

	/**
	 * Does the same that the #getToUri() method does, but it makes sure that
	 * the component has been initialized first.
	 * 
	 * @return									the path of the route to which
	 * 											this component is sending events
	 * 											to.
	 * @throws OperationNotSupportedException	if this component is not sending
	 * 											events to any endpoint.
	 * @see										#getToUri()
	 */
	public String to() throws OperationNotSupportedException {
		if (!initialized) {
			initialize();
		}
		return getToUri();
	}

	/**
	 * Performs the connector's specific initializations, depending on the 
	 * specific datasource type.
	 * 
	 * @param	ctx	the {@link CamelContext} used to build the specific 
	 * 				component.
	 * @return		the specific initialized component that was built through
	 * 				the given context and depending on the connector's type. 
	 */
	protected abstract DefaultComponent buildComponent(CamelContext ctx);

	/**
	 * Returns the path of the route from which this component is consuming, or 
	 * throws OperationNotSupportedException if the component cannot be used as 
	 * an input mechanism.
	 * 
	 * @return									the path of the route from which
	 * 											this component is consuming 
	 * 											events.
	 * @throws 	OperationNotSupportedException	if this component is not 
	 * 											consuming data from any 
	 * 											endpoint.
	 */
	protected abstract String getFromUri()
			throws OperationNotSupportedException;

	/**
	 * Returns the path of the route to which this component will output 
	 * information to, or throws OperationNotSupportedException if this 
	 * component cannot be used as an output endpoint.
	 * 
	 * @return									the path of the route to which
	 * 											this component is sending events
	 * 											to.
	 * @throws OperationNotSupportedException	if this component is not sending
	 * 											events to any endpoint.
	 */
	protected abstract String getToUri() throws OperationNotSupportedException;

}
