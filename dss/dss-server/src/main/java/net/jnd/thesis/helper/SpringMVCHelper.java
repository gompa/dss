/**
 * 
 */
package net.jnd.thesis.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.DomainEntity;

/**
 * @author João Domingos
 * @since Nov 11, 2012
 */
public class SpringMVCHelper {

	/**
	 * @param em
	 * @param type
	 * @return
	 */
	public static <T extends DomainEntity> long count(EntityManager em,
			Class<T> type) {
		Long res = null;
		AuthUser currentUser = RequestHelper.getCurrentUser(false);
		if (currentUser == null || currentUser.isAdministrator()) {
			res = em.createQuery(
					String.format("SELECT COUNT(o) FROM %s o",
							type.getSimpleName()), Long.class)
					.getSingleResult();
		} else {
			TypedQuery<Long> q = em
					.createQuery(
							String.format(
									"SELECT COUNT(o) FROM %s o WHERE o.creationUser = :creationUser",
									type.getSimpleName()), Long.class);
			q.setParameter("creationUser", currentUser);
			res = q.getSingleResult();
		}
		return res;
	}

	/**
	 * @param em
	 * @param type
	 * @return
	 */
	public static <T extends DomainEntity> List<T> findAll(EntityManager em,
			Class<T> type) {
		List<T> res = null;
		AuthUser currentUser = RequestHelper.getCurrentUser(false);
		if (currentUser == null || currentUser.isAdministrator()) {
			res = em.createQuery(
					String.format("SELECT o FROM %s o", type.getSimpleName()),
					type).getResultList();
		} else {
			TypedQuery<T> q = em.createQuery(String.format(
					"SELECT o FROM %s o WHERE o.creationUser = :creationUser",
					type.getSimpleName()), type);
			q.setParameter("creationUser", currentUser);
			res = q.getResultList();
		}
		return res;
	}

	/**
	 * @param em
	 * @param type
	 * @param id
	 * @return
	 */
	public static <T extends DomainEntity> T find(EntityManager em,
			Class<T> type, Long id) {
		T res = null;
		if (id == null) {
			return null;
		} else {
			res = em.find(type, id);
			AuthUser currentUser = RequestHelper.getCurrentUser(false);
			if (currentUser == null
					|| (!currentUser.isAdministrator() && !res
							.getCreationUser().equals(currentUser))) {
				res = null;
			}
		}
		return res;
	}

	/**
	 * @param em
	 * @param type
	 * @param firstResult
	 * @param maxResults
	 * @return
	 */
	public static <T extends DomainEntity> List<T> findEntries(
			EntityManager em, Class<T> type, int firstResult, int maxResults) {
		List<T> res = null;
		AuthUser currentUser = RequestHelper.getCurrentUser(false);
		if (currentUser == null || currentUser.isAdministrator()) {
			res = em.createQuery(
					String.format("SELECT o FROM %s o", type.getSimpleName()),
					type).setFirstResult(firstResult).setMaxResults(maxResults)
					.getResultList();
		} else {
			TypedQuery<T> q = em
					.createQuery(
							String.format(
									"SELECT o FROM %s o WHERE o.creationUser = :creationUser",
									type.getSimpleName()), type)
					.setFirstResult(firstResult).setMaxResults(maxResults);
			q.setParameter("creationUser", currentUser);
			res = q.getResultList();
		}
		return res;

	}

}
