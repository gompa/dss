package net.jnd.thesis.service.loadBalancer;

import java.rmi.RemoteException;
import java.util.Timer;
import java.util.TimerTask;

import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Pedro Martins
 * 
 * Small auxiliary class that extends a {@link java.util.TimerTask TimerTask} in
 * order to encapsulate a personalized task that checks for CPU usage using the
 * <a href="https://support.hyperic.com/display/SIGAR/Home">SIGAR API</a>. 
 * <p>
 * SIGAR requires special libraries and dll files to run depending on the base
 * operative system (OS) where it is running. This Service has support to run on 
 * Linux, Windows, Solaris, MacOS and more. You can check all the OSs
 * supported by visiting the folder "dss-server/src/main/webapp/WEB-INF/lib". 
 * All the files inside that folder are SIGAR dependencies that will allow it 
 * to run on certain OS. 
 * <p>
 * This task will check for CPU usage every time it is called by the 
 * Timer and it will execute the operations specified in the 
 * {@link #run() run()} method. The Timer that defines the execution of this
 * class is defined in the {@link LoadBalancerService LoadBalancerService} 
 * class.
 * <p>
 * Every time it checks for CPU usage it stores the CPU usage values into the 
 * {@link CpuHistory CpuHistory} singleton, that works as an history of the last
 * X saved CPU usage values. It also asks the {@link VMManager VMManager} 
 * singleton if it is needed or not, and if it is not needed, it asks Dad to 
 * kill it. This class is therefore responsible for the initialization of both
 * those singletons.
 * <p>
 * For more examples on how to run timed and repeated tasks using a timer, 
 * check <a href="http://enos.itcollege.ee/~jpoial/docs/tutorial/essential/threads/timer.html">Using the Timer and TimerTask Classes</a> tutorials.
 * 
 * @see		<a href="https://support.hyperic.com/display/SIGAR/Home">SIGAR API</a>
 * @see		LoadBalancerService
 * @see		TimerTask
 * @see		Timer
 * @see		#run()
 * @see		CpuHistory
 * @see		VMManager
 * @since	1.5.0
 * */
public class CpuMonitor extends TimerTask{

	private static final Logger LOG = LoggerFactory
			.getLogger(CpuMonitor.class);

	private Sigar cpuinfo;
	private CpuHistory history;
	private double usage;
	private VMManager suicideManager;
	
	/**
	 * Creates a new {@link CpuMonitor} instance. Because this will be the first
	 * class to call the {@link CpuHistory.getInstance() getInstance()} method
	 * from CpuHistory, it will also initialize the singleton, for future use.
	 * */
	public CpuMonitor() {
		cpuinfo = new Sigar();
		history = CpuHistory.getInstance();
		suicideManager = VMManager.getInstance();
		usage = -1;
	}
	
	/**
	 * Checks for the current CPU usage of this machine and adds it to the 
	 * {@link #history CpuHistory} singleton.
	 * */
	@Override
	public void run() {
		try {

			usage = 1 - cpuinfo.getCpuPerc().getIdle();
			LOG.info(CpuMonitor.class.getCanonicalName() + ", cpuCheck: " + usage + "% being used.");
			history.addCpuCharge(usage);
			
			LOG.info("avg CPU load is: " + history.getResultsAvg());
			
			
			if(!suicideManager.isThisVMNeeded())
				if(suicideManager.commitSuicide())
					LOG.warn("Request for suicide approved!");
				else
					LOG.warn("Request for suicide denied!");

		} catch (SigarException e) {
			LOG.error("Cannot get CPU usage information: " + e);
		} catch (RemoteException e) {
			LOG.error("Could not communicate with dad or AWS service when "
					+ "trying to commit suicide: " + e);
		}
	}
}
