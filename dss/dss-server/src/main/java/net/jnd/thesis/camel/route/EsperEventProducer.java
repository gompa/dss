package net.jnd.thesis.camel.route;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.naming.OperationNotSupportedException;

import net.jnd.thesis.camel.bean.CamelInternalEvent;
import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.camel.component.EsperComponentHelper;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.helper.LogHelper;
import net.jnd.thesis.service.CacheDataService;

import org.apache.camel.Body;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.model.ExpressionNode;
import org.apache.camel.model.FilterDefinition;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.model.ThreadsDefinition;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * Helps define routes that will receive events from datasources and send them 
 * to Esper queues. Classes that extend this abstract class will receive data 
 * from classes that implement the 
 * {@link net.jnd.thesis.common.interfaces.DSSDataSource} interface and channel 
 * it to specific session queues that are actually Esper queues. 
 * <p> 
 * This class forces an association between a datasource and a route, therefore
 * classes that extend it work as input endpoints.
 * <p> 
 * The configuration of the routes is done in the {@link #configure()} method.
 * <p>
 * This class is extended by {@link HttpEventProducer}, {@link RssEventProducer}
 * , {@link TwitterEventProducer} and {@link XmppEventProducer}. Furthermore, it 
 * is also used by the {@link EsperEventProducerFactory} to create each producer
 * correctly depending on the type of datasource. 
 * 
 * @see		#configure()
 * @see 	HttpEventProducer
 * @see		RssEventProducer
 * @see		TwitterEventProducer
 * @see		XmppEventProducer
 * @see		EsperEventProducerFactory
 * @since	1.2.1
 */
public abstract class EsperEventProducer extends GenericRouteBuilder {

	private static final Logger LOG = LoggerFactory
			.getLogger(EsperEventProducer.class);


	private Pattern patternCustom;
	private static final Pattern PATTERN_NUMBER = Pattern
			.compile("[-+]?[0-9]*[\\.,]?[0-9]+");

	/**
	 * @param ctx
	 * @param sessionId
	 * @param test
	 */
	public EsperEventProducer(CamelContext ctx, Long sessionId, boolean test) {
		super(ctx, sessionId, test);
	}

	/**
	 * Compiles the Datasource's pattern expression (regex) to see if it is a 
	 * valid one. If the regex expression from the datasource is not valid, a 
	 * simple error message is print to the log, but the program keeps running
	 * and no notification is given to the user that the regex is incorrect. 
	 * As a consequence, this issues a potential feedback problem between user
	 * and application.
	 */
	private void buildPatternCustom() {
		if (getDataSource() != null
				&& StringUtils.isNotBlank(getDataSource().getRegex())) {
			try {
				this.patternCustom = Pattern
						.compile(getDataSource().getRegex());
			} catch (PatternSyntaxException e) {
				LOG.error(e.getMessage());
			}
		}
	}

	/**
	 * Configuration of the camel route that receives information from the
	 * datasource and sends it to the Esper session.
	 * 
	 * @throws OperationNotSupportedException	if this configuration does not
	 * 											have a "from" endpoint or a "to"
	 * 											endpoint defined. 
	 * */
	@Override
	public final void configure() throws OperationNotSupportedException{
//		normalConfig();
//		executorConfig();
//		threadPoolsConfig();
		sedaConfig();
	}

	/**
	 * Original route configuration. A simple and single threaded version that 
	 * processes messages one by one and sends them to esper queues.
	 * 
	 * @throws OperationNotSupportedException	if this configuration does not
	 * 											have a "from" endpoint or a "to"
	 * 											endpoint defined. 
	 * */
	private void normalConfig() throws OperationNotSupportedException{
		// build custom regex pattern if necessary
		buildPatternCustom();

		// start route definition
		RouteDefinition route = from(getFrom().from());

		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(), from route: " + getFrom().from());

		// apply pre filter
		FilterDefinition filterDefinition = route
				.filter()
				.method(getPreFilterClass() != null ? getPreFilterClass()
						: EsperEventProducerPreFilter.class, FILTER_METHOD_NAME);
		// apply main processor
		ExpressionNode expressionNode = filterDefinition
				.process(getProcessor());

		// apply internal processor
		expressionNode = expressionNode.process(getInternalProcessor());

		// apply post filter
		filterDefinition = expressionNode.filter().method(
				EsperEventProducerPosFilter.class, FILTER_METHOD_NAME);

		// if there is a custom pos filter to apply...
		if (getPosFilterClass() != null) {
			filterDefinition = filterDefinition.filter().method(
					getPosFilterClass(), FILTER_METHOD_NAME);
		}

		// set destination
		expressionNode = filterDefinition.to(getTo().to());
		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(),  to route: " + getTo().to());
		LOG.debug("DEBUG_PATH EsperEventProducer,  method configure(), threadId: " + Thread.currentThread().getId());

		// append specific route rules if necessary
		appendToRoute(expressionNode);
	}

	/**
	 * A more complex configuration that uses the SEDA component to message 
	 * asynchronously to the esper component. Here the processing of each 
	 * message is still single threaded however, but once processed, multiple 
	 * threads will send the data to multiple sessions a lot faster.
	 * 
	 * @throws OperationNotSupportedException	if this configuration does not
	 * 											have a "from" endpoint or a "to"
	 * 											endpoint defined. 
	 * */
	private void sedaConfig() throws OperationNotSupportedException{
		// build custom regex pattern if necessary
		buildPatternCustom();

		// start route definition
		RouteDefinition route = from(getFrom().from());

		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(), from route: " + getFrom().from());

		// apply pre filter
		FilterDefinition filterDefinition = route
				.filter()
				.method(getPreFilterClass() != null ? getPreFilterClass()
						: EsperEventProducerPreFilter.class, FILTER_METHOD_NAME);

		// apply main processor
		ExpressionNode expressionNode = filterDefinition
				.process(getProcessor());

		// apply internal processor
		expressionNode = expressionNode.process(getInternalProcessor());

		// apply post filter
		filterDefinition = expressionNode.filter().method(
				EsperEventProducerPosFilter.class, FILTER_METHOD_NAME);

		// if there is a custom pos filter to apply...
		if (getPosFilterClass() != null) {
			filterDefinition = filterDefinition.filter().method(
					getPosFilterClass(), FILTER_METHOD_NAME);
		}

		// set destination
		//http://camel.apache.org/seda.html
		expressionNode = filterDefinition.to("seda:sub");
		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(),  to route: seda:sub");

		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(), from route: seda:sub");

		from("seda:sub?concurrentConsumers=10").to(getTo().to());

		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(),  to route: " + getTo().to());
		LOG.debug("DEBUG_PATH EsperEventProducer,  method configure(), threadId: " + Thread.currentThread().getId());				

		// append specific route rules if necessary
		appendToRoute(expressionNode);
	}

	/**
	 * A configuration that attempts to have the processing being done in a 
	 * parallel way by multiple threads. The perfect solution would be to 
	 * combine this configuration with the SEDA configuration, however this one 
	 * does not work yet and it has not been tested yet.
	 * 
	 * @throws OperationNotSupportedException	if this configuration does not
	 * 											have a "from" endpoint or a "to"
	 * 											endpoint defined. 
	 * */
	private void executorConfig() throws OperationNotSupportedException{
		// build custom regex pattern if necessary
		buildPatternCustom();
		// start route definition
		RouteDefinition route = from(getFrom().from());

		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(), from route: " + getFrom().from());

		// apply pre filter
		FilterDefinition filterDefinition = route
				.filter()
				.method(getPreFilterClass() != null ? getPreFilterClass()
						: EsperEventProducerPreFilter.class, FILTER_METHOD_NAME);
		// apply main processor
		ThreadsDefinition expressionNode = filterDefinition
				.process(getProcessor()).threads()
		        .executorService(Executors.newFixedThreadPool(10));

		// apply internal processor
		expressionNode = expressionNode.process(getInternalProcessor()).threads()
		        .executorService(Executors.newFixedThreadPool(10));
		
		// apply post filter
		filterDefinition = expressionNode.filter().method(
				EsperEventProducerPosFilter.class, FILTER_METHOD_NAME);
		
		// if there is a custom pos filter to apply...
		if (getPosFilterClass() != null) {
			filterDefinition = filterDefinition.filter().method(
					getPosFilterClass(), FILTER_METHOD_NAME);
		}

		// set destination
		expressionNode.to(getTo().to());
		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(),  to route: " + getTo().to());
		LOG.debug("DEBUG_PATH EsperEventProducer,  method configure(), threadId: " + Thread.currentThread().getId());

		// append specific route rules if necessary
//		appendToRoute(expressionNode);
	}

	/**
	 * Minor improvement that will allow the middleware to have multiple threads
	 * collecting data from the sources. This would be exceptionally good in a 
	 * situation where the middleware is being drowned by millions of requests.
	 * However, since we only tested up to 1 source sending 4K messages per 
	 * minute the benefits of this solution are neglectable at best. Further 
	 * testing is required to see if this configuration brings any benefit at 
	 * all. 
	 * 
	 * @throws OperationNotSupportedException	if this configuration does not
	 * 											have a "from" endpoint or a "to"
	 * 											endpoint defined. 
	 * */
	private void threadPoolsConfig() throws OperationNotSupportedException{
		// build custom regex pattern if necessary
		buildPatternCustom();
		// start route definition
		ThreadsDefinition route = from(getFrom().from()).threads(10);

		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(), from route: " + getFrom().from());

		// apply pre filter
		FilterDefinition filterDefinition = route
				.filter()
				.method(getPreFilterClass() != null ? getPreFilterClass()
						: EsperEventProducerPreFilter.class, FILTER_METHOD_NAME);
		// apply main processor
		ExpressionNode expressionNode = filterDefinition
				.process(getProcessor());

		// apply internal processor
		expressionNode = expressionNode.process(getInternalProcessor());
		// apply post filter
		filterDefinition = expressionNode.filter().method(
				EsperEventProducerPosFilter.class, FILTER_METHOD_NAME);
		// if there is a custom pos filter to apply...
		if (getPosFilterClass() != null) {
			filterDefinition = filterDefinition.filter().method(
					getPosFilterClass(), FILTER_METHOD_NAME);
		}

		// set destination
		expressionNode = filterDefinition.to(getTo().to());
		LOG.debug("DEBUG_PATH EsperEventProducer, method configure(),  to route: " + getTo().to());
		LOG.debug("DEBUG_PATH EsperEventProducer,  method configure(), threadId: " + Thread.currentThread().getId());

		// append specific route rules if necessary
		appendToRoute(expressionNode);
	}

	/**
	 * @return
	 */
	private Processor getInternalProcessor() {
		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				if (getDataSource() != null) {
					Message msg = exchange.getIn();
					CamelInternalEvent event = (CamelInternalEvent) msg
							.getBody();

					event.setCreationDate(new Date());

					// only apply processing if the event raw value is not blank
					String rawValue = event.getValueRaw();
					if (StringUtils.isNotBlank(rawValue)) {
						String filteredValue = rawValue;

						// apply custom regex if present
						if (patternCustom != null) {

							LOG.info("EsperEventProducer, process(), filteredValue b4 regex= " + filteredValue);
							LOG.info("EsperEventProducer, process(), patternCustom used= " + patternCustom.toString());

							Matcher matcher = patternCustom.matcher(rawValue);
							if (matcher.find()) {
								filteredValue = matcher.group();
							} else {
								filteredValue = "";
							}

							LOG.info("EsperEventProducer, process(), filteredValue after regex= " + filteredValue);
						}

						event.setValue(filteredValue);

						// apply value type conversion if necessary
						switch (getDataSource().getDataSourceValueType()) {
						case NUMERIC:

							LOG.info("NUMERIC " + rawValue);

							Matcher matcher = PATTERN_NUMBER
									.matcher(filteredValue.replaceAll(",", "."));

							LOG.info("filteredValue after NUMERIC rep= " + filteredValue);

							if (matcher.find()) {
								Float floatValue = new Float(matcher.group());
								event.setFloatValue(floatValue);
								event.setValue(String.valueOf(floatValue));
								LOG.info("filteredValue after NUMERIC match= " + floatValue);
							}

							break;
						default:
							break;
						}
					}

					//DEBUG printing EsperEventProducer info
					try {
						LOG.debug("DEBUG_PATH EsperEventProducer, process(), from route: " + getFrom().from());
						LOG.debug("DEBUG_PATH EsperEventProducer, process(),  to route: " + getTo().to());
						LOG.debug("DEBUG_PATH EsperEventProducer, process(), threadId: " + Thread.currentThread().getId());
						LOG.debug("DEBUG_PATH EsperEventProducer, process(), messageId: " + msg.getMessageId());
						LOG.debug("DEBUG_PATH EsperEventProducer, process(), eventId: " + event.getId());
						LOG.debug("DEBUG_PATH EsperEventProducer, process(), eventCreationDate: " + event.getCreationDate());
					} catch (OperationNotSupportedException e) {
						e.printStackTrace();
					}
					LOG.debug("TIME_CHECK_P1_END " + event.getValue() + " " + System.currentTimeMillis() + " " + event.getSessionId());		

				}
			}

		};
	}

	/**
	 * 
	 */
	public static class EsperEventProducerPreFilter {

		/**
		 * @param feed
		 * @return
		 */
		public boolean filter(Object event) {
			return true;
		}
	}

	/**
	 * Retrieve the datasource associated with this route.
	 * 
	 * @return	the datasource associated with this route.
	 */
	public abstract DSSDataSource getDataSource();

	/**
	 * @return
	 */
	protected final ComponentHelper getTo() {
		String esperNameTo = EsperComponentHelper.getEsperNameInternal(
				getSessionId(), isTest());
		return new EsperComponentHelper(getContext(), esperNameTo, esperNameTo);
	}

	/**
	 * @param msg
	 * @param value
	 */
	protected final void setMessageBodyEvent(DefaultMessage msg,
			String rawValue, String processedValue, String name) {

		//INFO: Here we create, process and print the body of the message
		Session session = CacheDataService.getSession(getSessionId());
		CamelInternalEvent event = new CamelInternalEvent();

		event.setCreationUser(session.getCreationUser());
		event.setUpdateUser(session.getCreationUser());
		Date dt = new Date();
		event.setDate(dt);
		event.setCreationDate(dt);
		event.setUpdateDate(dt);
		event.setValueRaw(rawValue);
		event.setValue(processedValue);
		event.setDataSourceId(getDataSource().getId());
		event.setSessionId(getSessionId());
		event.setName(name);
		msg.setBody(event, event.getClass());

		LogHelper.debug(LOG, "event produced: " + event);
	}

	/**
	 * NOT BEING USED
	 * 
	 * @param msg
	 * @param rawValue
	 * @param processedValue
	 */
	protected final void setMessageBodyEvent(DefaultMessage msg,
			String rawValue, String processedValue) {
		setMessageBodyEvent(msg, rawValue, processedValue, null);
	}

	/**
	 * 
	 */
	public static class EsperEventProducerPosFilter {

		/**
		 * @param feed
		 * @return
		 */
		public boolean filter(@Body CamelInternalEvent event) {

			LOG.debug("DEBUG_PATH EsperEventProducerPosFilter, filter(CamelInternalEvent event), checking event created at " + event.getCreationDate());
			boolean exclude = StringUtils.isBlank(event.getValue());
			if (exclude) {
				LOG.debug("event excluded: " + event);
			}
			return !exclude;
		}
	}

}
