/**
 * 
 */
package net.jnd.thesis.servlet.ws.jetty;

import java.util.HashSet;
import java.util.Set;

import net.jnd.thesis.common.interfaces.DSSEvent;
import net.jnd.thesis.common.interfaces.DSSReplaySession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.EventType;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionStatus;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.helper.RouteHelper;
import net.jnd.thesis.service.CacheDataService;
import net.jnd.thesis.ws.services.exception.UserNotAuthenticatedException;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Dec 30, 2012
 */
public class ServerSideServices extends ServerSideCreateUpdateServices {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(ServerSideServices.class);

	Set<Long> userConnections = new HashSet<Long>();

	@Override
	public DSSUserSessionConnection startTestDataSourceSession(
			Long dataSourceId, Long interactionModelId) throws WSException {
		LOG.debug(String.format("startTestDataSourceSession(%s,%s)",
				dataSourceId, interactionModelId));
		if (isAuthenticated()) {
			UserSessionConnection conn = null;
			Session session = ServerSideServicesHelper
					.buildTestDataSourceSession(dataSourceId,
							interactionModelId, user);
			session.persist();
			String errorMsg = RouteHelper.startSession(session.getId());
			if (StringUtils.isNotBlank(errorMsg)) {
				throw new WSException(errorMsg);
			} else {
				conn = ServerSideServicesHelper.buildTestUserSessionConnection(
						user, session);
				conn.persist();
				Boolean connectionOk = connectToSession(conn);
				if (!connectionOk) {
					stopSession(session.getId());
					throw new WSException("connection problem...");
				}
			}
			return ServerSideServicesHelper
					.buildUserSessionConnectionBean(conn);
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Boolean startSession(Long sessionId) throws WSException {
		LOG.debug(String.format("startSession(%s)", sessionId));
		if (isAuthenticated()) {

			Session session = CacheDataService.getSession(sessionId);
			if (SessionStatus.PAUSED.name().equals(
					session.getSessionStatus().name())) {
				session.setReplaying(false);
				session.merge();
			}

			String errorMsg = RouteHelper.startSession(sessionId);
			if (StringUtils.isNotBlank(errorMsg)) {
				throw new WSException(errorMsg);
			}
			return Boolean.TRUE;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Boolean stopSession(Long sessionId) throws WSException {
		LOG.debug(String.format("stopSession(%s)", sessionId));
		if (isAuthenticated()) {
			String errorMsg = RouteHelper.stopSession(sessionId);
			if (StringUtils.isNotBlank(errorMsg)) {
				throw new WSException(errorMsg);
			}
			return Boolean.TRUE;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public void broadcastEvent(DSSEvent evt) throws WSException {
		// on server side, this is performed directly in websocket layer
	}

	@Override
	public Boolean connectToSession(DSSUserSessionConnection userConnection)
			throws WSException {
		LOG.debug(String.format("connectToSession(%s)", userConnection.getId()));
		if (isAuthenticated()) {
			Long connId = userConnection.getId();
			String errorMsg = RouteHelper.openUserSessionConnection(connId);
			if (StringUtils.isNotBlank(errorMsg)) {
				throw new WSException(errorMsg);
			} else {
				Session session = CacheDataService.getSession(userConnection
						.getConnectionSession().getId());

				Event evt = new Event();
				evt.setEventType(EventType.USER_CONNECT);
				evt.setValue(this.user.getName());
				evt.setValueRaw(this.user.getName());
				evt.setFloatValue(Float.valueOf(this.user.getId()));
				evt.setSession(session);
				evt.persist();

				// broadcast event
				for (DSSUser dssUser : session.getUsers()) {
					ServerSideWebSocketHelper.broadcastEvent(dssUser.getId(),
							evt);
				}

				userConnections.add(connId);
			}
			return Boolean.TRUE;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Boolean disconnectFromSession(Long connectionId) throws WSException {
		LOG.debug(String.format("disconnectFromSession(%s)", connectionId));
		if (isAuthenticated()) {
			String errorMsg = RouteHelper
					.closeUserSessionConnection(connectionId);
			if (StringUtils.isNotBlank(errorMsg)) {
				throw new WSException(errorMsg);
			} else {

				UserSessionConnection userConnection = CacheDataService
						.getConnection(connectionId);
				Session session = CacheDataService.getSession(userConnection
						.getConnectionSession().getId());

				Event evt = new Event();
				evt.setEventType(EventType.USER_DISCONNECT);
				evt.setValue(this.user.getName());
				evt.setValueRaw(this.user.getName());
				evt.setFloatValue(Float.valueOf(this.user.getId()));
				evt.setSession(session);
				evt.persist();

				// broadcast event
				for (DSSUser dssUser : session.getUsers()) {
					ServerSideWebSocketHelper.broadcastEvent(dssUser.getId(),
							evt);
				}

				userConnections.remove(connectionId);
			}
			return Boolean.TRUE;
		} else {
			throw new UserNotAuthenticatedException();
		}
	}

	@Override
	public Boolean replaySession(DSSReplaySession replayBean)
			throws WSException {
		LOG.debug(String.format("replaySession(%s)", replayBean.getId()));
		if (isAuthenticated()) {
			Session session = CacheDataService.getSession(replayBean
					.getSessionId());
			if (SessionStatus.PAUSED.name().equals(
					session.getSessionStatus().name())) {
				session.setReplaying(true);
				session.setReplayDelay(replayBean.getReplayDelay());
				session.merge();
			}

			String errorMsg = RouteHelper.startSession(replayBean.getSessionId());
			if (StringUtils.isNotBlank(errorMsg)) {
				throw new WSException(errorMsg);
			}
			return Boolean.TRUE;
		} else {
			throw new UserNotAuthenticatedException();
		}

	}

}
