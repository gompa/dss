package net.jnd.thesis.web;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.ConnectionStatus;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionStatus;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.helper.RequestHelper;
import net.jnd.thesis.helper.RouteHelper;
import net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocket;
import net.jnd.thesis.servlet.ws.jetty.ServerSideWebSocketHelper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/admin/**")
@Controller
public class AdminController {

	@RequestMapping(method = RequestMethod.POST, value = "{id}")
	public void post(@PathVariable Long id, ModelMap modelMap,
			HttpServletRequest request, HttpServletResponse response) {
	}

	@RequestMapping
	public String index() {
		return "admin/index";
	}

	@RequestMapping(value = "pushEvent", produces = "text/html")
	public String pushEvent(HttpServletRequest request, Model uiModel) {

		String extraUrl = "";

		String userName = request.getParameter("usr");
		String dsName = request.getParameter("ds");
		String val = request.getParameter("val");

		uiModel.addAttribute("usr", userName);
		uiModel.addAttribute("ds", dsName);
		uiModel.addAttribute("val", val);

		if (StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(dsName)
				&& StringUtils.isNotBlank(val) /* && StringUtils.isNumeric(val) */) {
			try {

				Event e = new Event();
				e.setDate(new Date());
				e.setValueRaw(dsName);
				e.setValue(val);

				AuthUser usr = AuthUser.findAuthUsersByUsernameEquals(userName)
						.getSingleResult();
				System.out.println("-------------> pushing to \"" + userName
						+ "\" event: " + e);
				ServerSideWebSocket ws = ServerSideWebSocketHelper
						.getConnection(usr.getId());

				// check type of push
				if (ws.getUserSessionConnectionId() != null) {
					// direct web connection
					ws.sendMessageDirect(e);
				} else {
					// api connection
					ws.sendMessageDirectJSON(e);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("-------------> BULLOCKS!!!");
		}

		return "admin/index" + extraUrl;
	}

	@RequestMapping(value = "dumpRoutes", produces = "text/html")
	public String dumpRoutes(HttpServletRequest request, Model uiModel) {
		List<String> dump = RouteHelper.dumpRoutes();
		for (String str : dump) {
			System.out.println(str);
			RequestHelper.saveUserMessage(uiModel, str);
		}
		return "admin/index";
	}

	@RequestMapping(value = "killAllSessions", produces = "text/html")
	public String killAllSessions(HttpServletRequest request, Model uiModel) {
		//
		for (UserSessionConnection userSessionConnection : UserSessionConnection
				.findAllUserSessionConnections()) {
			if (ConnectionStatus.OPEN.name().equals(
					userSessionConnection.getStatus().name())) {
				try {
					RequestHelper.saveUserMessage(uiModel, RouteHelper
							.closeUserSessionConnection(userSessionConnection
									.getId()));
				} catch (Exception e) {
					RequestHelper.saveUserMessage(uiModel, e.getMessage());
				}
				//
				userSessionConnection.setStatus(ConnectionStatus.CLOSED);
				userSessionConnection.merge();
			}
		}
		//
		for (Session session : Session.findAllSessions()) {
			//
			if (SessionStatus.STARTED.name().equals(
					session.getSessionStatus().name())) {
				try {
					RequestHelper.saveUserMessage(uiModel,
							RouteHelper.stopSession(session.getId()));
				} catch (Exception e) {
					RequestHelper.saveUserMessage(uiModel, e.getMessage());
				}
				//
				session.setSessionStatus(SessionStatus.PAUSED);
				session.merge();
			}
		}
		//
		return "admin/index";
	}

}
