package net.jnd.thesis.web;

import net.jnd.thesis.domain.DynamicReconfiguration;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/dynamicreconfigurations")
@Controller
@RooWebScaffold(path = "dynamicreconfigurations", formBackingObject = DynamicReconfiguration.class)
public class DynamicReconfigurationController {
}
