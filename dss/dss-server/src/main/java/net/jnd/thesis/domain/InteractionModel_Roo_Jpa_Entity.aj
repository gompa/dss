// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.domain;

import javax.persistence.Entity;
import net.jnd.thesis.domain.InteractionModel;

privileged aspect InteractionModel_Roo_Jpa_Entity {
    
    declare @type: InteractionModel: @Entity;
    
}
