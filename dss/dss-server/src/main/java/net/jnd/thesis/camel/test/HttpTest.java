package net.jnd.thesis.camel.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.jnd.thesis.camel.component.FileComponentHelper;
import net.jnd.thesis.camel.component.HttpComponentHelper;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.xml.XPathBuilder;
import org.apache.camel.impl.DefaultMessage;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

/**
 *
 */
public class HttpTest extends GenericTest {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory.getLogger(HttpTest.class);

	private static final String URL = "http://www.jornaldenegocios.pt/home.php?template=FICHA_INDICE_V2&indice=psi20&isin=PTING0200002&plaza=51&nombre=PSI20&pestanaActual=li_interactivo";
	private static final String CSS_PATH = "html body div#principal div#contenido div#CENTER div#com_ficha_indice_v2_12551.component div.width100x100 div.width100x100 div.floatLeft div.floatLeft div div div.floatLeft span.bigPrice";
	private static final String XPATH = "/html/body/div/div[2]/div[3]/div[3]/div/div/div/div/div[3]/div/div[2]/span";
	private static final String VALUE_LABEL = "PSI20: ";

	@Override
	public void configure() throws Exception {
		HttpComponentHelper http = new HttpComponentHelper(getContext(), URL);
		// WebSocketHelper ws = new WebSocketHelper(getContext());
		FileComponentHelper file = new FileComponentHelper(getContext(), getClass()
				.getSimpleName());
		from(http.from()).filter()
				.method(CustomFilter.class.getName(), "filter")
				.process(new CustomProcessor())
				// .to(ws.to());
				.to(file.to()).delay(10000);
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		GenericTest.start(HttpTest.class);
	}

	/**
	 *
	 */
	public static class CustomProcessor implements Processor {
		@Override
		public void process(Exchange exchange) {
			DefaultMessage msg = (DefaultMessage) exchange.getIn();
			InputStream body = (InputStream) msg.getBody();
			String time = GenericTest.getFormattedTime();
			StringBuilder newBody = new StringBuilder(time);
			newBody.append(VALUE_LABEL);
			LOG.debug("starting...");
			String css = null;
			try {
				css = applyCssQuery(body, CSS_PATH, URL);
				LOG.debug("css returned: " + css);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			LOG.debug("css done!");
			String xpath = null;
			try {
				xpath = applyXpathQuery(body, XPATH);
				LOG.debug("xpath returned: " + xpath);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			LOG.debug("xpath done!");
			String xpathCamel = null;
			try {
				xpathCamel = applyXpathQueryCamel(exchange, body, XPATH);
				LOG.debug("xpathCamel returned: " + xpathCamel);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			LOG.debug("xpathCamel done!");
			newBody.append(String.format(
					"css = %s ; xpath = %s ; xpathCamel = %s", css, xpath,
					xpathCamel));
			msg.setBody(newBody.toString(), String.class);
		}
	}

	/**
	 *
	 */
	public static class CustomFilter {
		public boolean filter(Object feed) {
			return Boolean.TRUE;
		}
	}

	/**
	 * @param input
	 * @param cssQuery
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static String applyCssQuery(InputStream input, String cssQuery,
			String url) throws IOException {
		input.reset();
		Elements elements = Jsoup.parse(input, null, url).select(cssQuery);
		return elements.isEmpty() ? StringUtils.EMPTY : elements.get(0).text();
	}

	/**
	 * @param input
	 * @param xpathQuery
	 * @return
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public static String applyXpathQuery(InputStream input, String xpathQuery)
			throws IOException, XPathExpressionException {
		input.reset();
		// String preparedStr = cleanupDocString(IOUtils.toString(input));
		String preparedStr = cleanupDocString(Jsoup.parse(input, null, URL)
				.toString());
		XPathExpression xpathExp = XPathFactory.newInstance().newXPath()
				.compile(xpathQuery);
		return xpathExp
				.evaluate(new InputSource(new StringReader(preparedStr)));
	}

	/**
	 * @param exchange
	 * @param input
	 * @param xpathQuery
	 * @return
	 * @throws IOException
	 */
	public static String applyXpathQueryCamel(Exchange exchange,
			InputStream input, String xpathQuery) throws IOException {
		input.reset();
		// String preparedStr = cleanupDocString(IOUtils.toString(input));
		String preparedStr = cleanupDocString(Jsoup.parse(input, null, URL)
				.toString());
		return XPathBuilder.xpath(xpathQuery).evaluate(exchange.getContext(),
				preparedStr);
	}

	/**
	 * @param originalDoc
	 * @return
	 */
	public static String cleanupDocString(String originalDoc) {
		String preparedStr = originalDoc.replaceAll(
				"(?s).*<html.*?>(.*?)</html>.*", "<html>$1</html>");
		preparedStr = preparedStr.replaceAll("(?s)<head.*?</head>", "");
		preparedStr = preparedStr.replaceAll("(?s)<script.*?</script>", "");
		preparedStr = preparedStr.replaceAll("(?s)<!--.*?-->", "");
		preparedStr = preparedStr.replaceAll("[\n\r\t]", "");
		preparedStr = preparedStr.replaceAll("  ", "");
		preparedStr = preparedStr.replaceAll("> <", "><");
		preparedStr = StringEscapeUtils.unescapeHtml4(preparedStr);
		preparedStr = preparedStr.replaceAll("&", "&amp;");
		return preparedStr;
	}

}
