package net.jnd.thesis.domain;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

@RooJavaBean
@Table(name = "DSS_REPLAY_SESSION")
@RooJpaActiveRecord(finders = { "findReplaySessionsBySessionIdEquals" })
public class ReplaySession extends DomainEntity {

    @NotNull
    @Column(unique = true)
    private Long sessionId;

    private Long eventId = 0L;
}
