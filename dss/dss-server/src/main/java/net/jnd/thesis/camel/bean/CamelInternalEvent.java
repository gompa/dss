package net.jnd.thesis.camel.bean;

import java.util.Date;

import net.jnd.thesis.common.enums.EventType;
import net.jnd.thesis.common.interfaces.DSSEvent;
import net.jnd.thesis.common.interfaces.DSSUser;

/**
 * @author João Domingos
 * @author Pedro Martins
 * 
 * @see	DSSEvent
 * @since 1.1.0
 */
public class CamelInternalEvent implements DSSEvent {

	private String valueRaw;

	private String value;

	private Float floatValue;

	private Long dataSourceId;

	private Long sessionId;

	private Date date;

	private Long id;

	private DSSUser creationUser;

	private Date creationDate;

	private DSSUser updateUser;

	private Date updateDate;

	private EventType type;

	private String name;

	private long sid = 0;

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the eventDate to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the floatValue
	 */
	public Float getFloatValue() {
		return floatValue;
	}

	/**
	 * @param floatValue the floatValue to set
	 */
	public void setFloatValue(Float floatValue) {
		this.floatValue = floatValue;
	}

	/**
	 * @return the dataSourceId
	 */
	public Long getDataSourceId() {
		return dataSourceId;
	}

	/**
	 * @param dataSourceId
	 *            the dataSourceId to set
	 */
	public void setDataSourceId(Long dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	/**
	 * Returns the id of the session to which this event belongs to. 
	 * 
	 * @return	the id of the session to which this event belongs to.
	 */
	public Long getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the session id to which this event belongs to.
	 * 
	 * @param	sessionId	the sessionId to which this event belongs to.
	 */
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the valueRaw
	 */
	public final String getValueRaw() {
		return valueRaw;
	}

	/**
	 * @param valueRaw
	 *            the valueRaw to set
	 */
	public final void setValueRaw(String valueRaw) {
		this.valueRaw = valueRaw;
	}

	/**
	 * @return the value
	 */
	public final String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public final void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the creationUser
	 */
	public DSSUser getCreationUser() {
		return creationUser;
	}

	/**
	 * @param creationUser
	 *            the creationUser to set
	 */
	public void setCreationUser(DSSUser creationUser) {
		this.creationUser = creationUser;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the updateUser
	 */
	public DSSUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser
	 *            the updateUser to set
	 */
	public void setUpdateUser(DSSUser updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate
	 *            the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the type
	 */
	public EventType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(EventType type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the sid
	 */
	public long getSid() {
		return sid;
	}

	/**
	 * @param sid the sid to set
	 */
	public void setSid(long sid) {
		this.sid = sid;
	}

	@Override
	public String toString() {
		return String
				.format("%s[sid: %s; did: %s; val: %s]", getClass()
						.getSimpleName(), getSessionId(), getDataSourceId(),
						getValue());
	}
}
