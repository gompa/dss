package net.jnd.thesis.web;

import net.jnd.thesis.domain.XmppDataSource;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/xmppdatasources")
@Controller
@RooWebScaffold(path = "xmppdatasources", formBackingObject = XmppDataSource.class)
public class XmppDataSourceController {
}
