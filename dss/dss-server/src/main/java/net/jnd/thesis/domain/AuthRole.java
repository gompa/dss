/**
 * 
 */
package net.jnd.thesis.domain;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author João Domingos
 * @since Jun 26, 2012
 */
@RooJavaBean
@RooJpaActiveRecord
@Table(name = "DSS_USER_ROLE")
public class AuthRole extends DomainEntity implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 114404475697304562L;

	@NotNull
	@Column(unique = true)
	@Size(max = 20)
	private String authority;

	@Override
	public String toString() {
		return String.valueOf(authority);
	}

}
