/**
 * 
 */
package net.jnd.thesis.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.jnd.thesis.common.exception.ServiceException;
import net.jnd.thesis.helper.LogHelper;
import net.jnd.thesis.helper.RouteHelper;
import net.jnd.thesis.helper.RoutesWrapper;
import net.jnd.thesis.servlet.ws.DSSWebSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Nov 1, 2012
 */
public class DSSDataService implements Service {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(DSSDataService.class);

	/**
	 * 
	 */
	private static Boolean initialized = Boolean.FALSE;

	/**
	 * Session id -> RoutesWrapper
	 */
	private static final Map<Long, RoutesWrapper> sessionRoutes = Collections
			.synchronizedMap(new HashMap<Long, RoutesWrapper>());

	/**
	 * * Session id -> [ User id -> WebSocket ]
	 */
	private static final Map<Long, Map<Long, DSSWebSocket>> sessionWebSockets = Collections
			.synchronizedMap(new HashMap<Long, Map<Long, DSSWebSocket>>());

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.Service#start()
	 */
	@Override
	public void start() throws ServiceException {
		if (!initialized) {
			reset();
			initialized = Boolean.TRUE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.interfaces.Service#end()
	 */
	@Override
	public void stop() throws ServiceException {
		if (initialized) {
			reset();
			initialized = Boolean.FALSE;
		}
	}

	/**
	 * 
	 */
	private void reset() {
		// clear session routes
		for (Entry<Long, RoutesWrapper> entry : sessionRoutes.entrySet()) {
			try {
				RouteHelper.stopSession(entry.getKey());
			} catch (Exception e) {
				LogHelper.error(LOG, e);
			}
		}
		sessionRoutes.clear();
		// clear session connections
		for (Entry<Long, Map<Long, DSSWebSocket>> entry : sessionWebSockets
				.entrySet()) {
			for (Entry<Long, DSSWebSocket> entry2 : entry.getValue().entrySet()) {
				try {
					entry2.getValue().close();
				} catch (Exception e) {
					LogHelper.error(LOG, e);
				}
			}
			entry.getValue().clear();
		}
		sessionWebSockets.clear();
	}

	/*
	 * 
	 */

	/**
	 * @return
	 */
	public static Map<Long, RoutesWrapper> getSessionRoutes() {
		if (!initialized) {
			throw new IllegalStateException("Service not initialized");
		}
		return sessionRoutes;
	}

	/**
	 * @return
	 */
	public static Map<Long, Map<Long, DSSWebSocket>> getSessionWebSockets() {
		if (!initialized) {
			throw new IllegalStateException("Service not initialized");
		}
		return sessionWebSockets;
	}

}
