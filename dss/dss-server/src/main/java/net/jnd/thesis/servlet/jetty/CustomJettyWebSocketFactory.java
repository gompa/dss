/**
 * 
 */
package net.jnd.thesis.servlet.jetty;

import javax.servlet.http.HttpServletRequest;

import net.jnd.thesis.helper.WebSocketHelper;
import net.jnd.thesis.servlet.ws.DSSWebSocket;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public class CustomJettyWebSocketFactory {

	/**
	 * @param type
	 * @param request
	 * @return
	 */
	static CustomJettyWebSocket create(DSSWebSocket.Type type,
			HttpServletRequest request) {
		switch (type) {
		case TEST:
			return new AbstractDSSWebSocket(type, request) {

				@Override
				public void onOpenSpecific(Connection connection) {
					WebSocketHelper.set(this);
				}

				@Override
				public void onCloseSpecific(int closeCode, String message) {
					WebSocketHelper.clear(this);
				}

				@Override
				protected void onMessageSpecific(String data) {
					// TODO Auto-generated method stub
				}
			};
		case SESSION:
			return new AbstractDSSWebSocket(type, request) {

				@Override
				public void onOpenSpecific(Connection connection) {
					WebSocketHelper.set(this);
				}

				@Override
				public void onCloseSpecific(int closeCode, String message) {
					WebSocketHelper.clear(this);
				}

				@Override
				protected void onMessageSpecific(String data) {
					// TODO Auto-generated method stub
				}
			};
		case MONITOR:
			return new AbstractDSSWebSocket(type, request) {

				@Override
				public void onOpenSpecific(Connection connection) {
					WebSocketHelper.set(this);
				}

				@Override
				public void onCloseSpecific(int closeCode, String message) {
					WebSocketHelper.clear(this);
				}

				@Override
				protected void onMessageSpecific(String data) {
					// TODO Auto-generated method stub
				}
			};
		default:
			throw new IllegalArgumentException("type not supported...");
		}
	}

}
