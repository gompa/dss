package net.jnd.thesis.camel.service;

import javax.jms.ConnectionFactory;

import net.jnd.thesis.common.exception.ServiceException;
import net.jnd.thesis.service.Service;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public enum MQService implements Service {

	INSTANCE;

	/**
	 *
	 */
	private static final Log log = LogFactory.getLog(MQService.class);

	private boolean started = Boolean.FALSE;

	private CamelContext context;

	private ProducerTemplate producer;

	@SuppressWarnings("unused")
	private ConsumerTemplate consumer;

	private static final String TEST_QUEUE_NAME = "test-jms:queue:test.queue";

	@Override
	public void start() throws ServiceException {
		if (!this.started) {
			try {
				this.context = new DefaultCamelContext();
				ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
						"vm://localhost?broker.persistent=false");
				this.context.addComponent("test-jms", JmsComponent
						.jmsComponentAutoAcknowledge(connectionFactory));
				this.producer = this.context.createProducerTemplate();
				this.consumer = this.context.createConsumerTemplate();
				context.start();
				this.started = Boolean.TRUE;
			} catch (Exception e) {
				throw new ServiceException(e.getMessage(), e);
			}
			// test the mq to ensure that everything is working
			//testMQ();
		}
	}

	@Override
	public void stop() throws ServiceException {
		try {
			this.context.stop();
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private void testMQ() {
		
		final CamelContext ctx = this.context;
		final ProducerTemplate prod = this.producer;
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				log.info("mq test started.");
				
				RouteBuilder route = new RouteBuilder() {
					public void configure() {
						from(TEST_QUEUE_NAME).routeId("testMQ").to(
								"log:net.jnd.thesis.service?level=INFO");
					}
				};
				
				try {
					ctx.addRoutes(route);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
				
				for (int i = 0; i < 10; i++) {
					prod.sendBody(TEST_QUEUE_NAME, "Test Message: " + i);
				}
				
				try {
					ctx.stopRoute("testMQ");
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
				
				sleep(5);
				log.info("mq test ended.");
			}
		}, "MQ_TEST_PRODUCER").start();
		
	}

	/**
	 * @param seconds
	 */
	private void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			log.debug(e.getMessage());
		}
	}

}
