/**
 * 
 */
package net.jnd.thesis.camel.route;

import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;

import org.apache.camel.CamelContext;

/**
 * @author João Domingos
 * @since Jun 13, 2012
 */
public final class EsperEventConsumerFactory {

	/**
	 * @param ctx
	 * @param sessionId
	 * @param test
	 * @return
	 */
	public static EsperEventConsumer createInternal(CamelContext ctx,
			Long sessionId, boolean test) {
		return new EsperEventConsumerInternal(ctx, sessionId, test);
	}

	/**
	 * @param ctx
	 * @param connection
	 * @param test
	 * @return
	 */
	public static EsperEventConsumer createUserConsumer(CamelContext ctx,
			DSSUserSessionConnection connection, boolean test) {
		return new EsperEventConsumerUser(ctx, connection, test);
	}

}
