/**
 * 
 */
package net.jnd.thesis.domain;

/**
 * @author João Domingos
 * @since Nov 12, 2012
 */
public enum PatternTemplate {
	STREAMING, PUBLISHER_SUBSCRIBER, PRODUCER_CONSUMER/*, CLIENT_SERVER*/;

}
