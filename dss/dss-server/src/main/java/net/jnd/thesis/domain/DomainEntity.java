/**
 * 
 */
package net.jnd.thesis.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import net.jnd.thesis.helper.RequestHelper;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
@RooJavaBean
@RooJpaActiveRecord(mappedSuperclass = true)
public abstract class DomainEntity {

	@ManyToOne(fetch = FetchType.EAGER)
	private AuthUser creationUser;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date creationDate;

	@ManyToOne(fetch = FetchType.EAGER)
	private AuthUser updateUser;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateDate;

	@Override
	public int hashCode() {
		Long id = this.getId();
		return (id != null) ? id.hashCode() : -1;
	}

	@Override
	public boolean equals(Object obj) {
		Long id = this.getId();
		return (id != null) && (obj != null)
				&& (getClass().getName().equals(obj.getClass().getName()))
				&& (id == ((DomainEntity) obj).getId());
	}

	@Override
	public String toString() {
		return String.format("%s (%s)", getClass().getSimpleName(), getId());
	}

	/**
	 * @return
	 */
	protected final boolean isCreated() {
		return getId() == null;
	}

	@PrePersist
	@PreUpdate
	protected final void buildAuthData() {
		// build user fields
		AuthUser user = RequestHelper.getCurrentUser(true);
		if (user != null) {
			setUpdateUser(user);
			if (isCreated()) {
				setCreationUser(user);
			}
		}
		// build date fields
		Date now = new Date();
		setUpdateDate(now);
		if (isCreated() && getCreationDate() == null) {
			setCreationDate(now);
		}
	}

	/**
	 * @param em
	 * @param type
	 * @return
	 */
	protected final static <T extends DomainEntity> long count(
			EntityManager em, Class<T> type) {
		Long res = null;
		AuthUser currentUser = RequestHelper.getCurrentUser(false);
		if (currentUser == null || currentUser.isAdministrator()) {
			res = em.createQuery(
					String.format("SELECT COUNT(o) FROM %s o",
							type.getSimpleName()), Long.class)
					.getSingleResult();
		} else {
			TypedQuery<Long> q = em
					.createQuery(
							String.format(
									"SELECT COUNT(o) FROM %s o WHERE o.creationUser = :creationUser",
									type.getSimpleName()), Long.class);
			q.setParameter("creationUser", currentUser);
			res = q.getSingleResult();
		}
		return res;
	}

	/**
	 * @param em
	 * @param type
	 * @return
	 */
	protected final static <T extends DomainEntity> List<T> findAll(
			EntityManager em, Class<T> type) {
		List<T> res = null;
		AuthUser currentUser = RequestHelper.getCurrentUser(false);
		if (currentUser == null || currentUser.isAdministrator()) {
			res = em.createQuery(
					String.format("SELECT o FROM %s o", type.getSimpleName()),
					type).getResultList();
		} else {
			TypedQuery<T> q = em.createQuery(String.format(
					"SELECT o FROM %s o WHERE o.creationUser = :creationUser",
					type.getSimpleName()), type);
			q.setParameter("creationUser", currentUser);
			res = q.getResultList();
		}
		return res;
	}

	/**
	 * @param em
	 * @param type
	 * @param id
	 * @return
	 */
	protected final static <T extends DomainEntity> T find(EntityManager em,
			Class<T> type, Long id) {
		T res = null;
		if (id == null) {
			return null;
		} else {
			res = em.find(type, id);
			AuthUser currentUser = RequestHelper.getCurrentUser(false);
			if (currentUser != null) {
				if (!currentUser.isAdministrator()
						&& res.getCreationUser() != null
						&& !currentUser.equals(res.getCreationUser())) {
					res = null;
				}
			}
		}
		return res;
	}

	/**
	 * @param em
	 * @param type
	 * @param firstResult
	 * @param maxResults
	 * @return
	 */
	protected final static <T extends DomainEntity> List<T> findEntries(
			EntityManager em, Class<T> type, int firstResult, int maxResults) {
		List<T> res = null;
		AuthUser currentUser = RequestHelper.getCurrentUser(false);
		if (currentUser == null || currentUser.isAdministrator()) {
			res = em.createQuery(
					String.format("SELECT o FROM %s o", type.getSimpleName()),
					type).setFirstResult(firstResult).setMaxResults(maxResults)
					.getResultList();
		} else {
			TypedQuery<T> q = em
					.createQuery(
							String.format(
									"SELECT o FROM %s o WHERE o.creationUser = :creationUser",
									type.getSimpleName()), type)
					.setFirstResult(firstResult).setMaxResults(maxResults);
			q.setParameter("creationUser", currentUser);
			res = q.getResultList();
		}
		return res;
	}

	@PreRemove
	public void preRemoveAudit() {
		System.out.println("----------------");
		System.out.println("removing entity:  + this");
		StackTraceElement[] StackTrace = Thread.currentThread().getStackTrace();
		for (StackTraceElement stackTraceElement : StackTrace) {
			System.out.println(stackTraceElement);
		}
	}
	
}
