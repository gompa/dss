package net.jnd.thesis.camel.route;

import java.io.InputStream;

import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.camel.component.HttpComponentHelper;
import net.jnd.thesis.camel.util.HttpUtil;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDataSourcePollingHttp;
import net.jnd.thesis.domain.HttpDataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.model.ExpressionNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
class HttpEventProducer extends EsperEventProducer {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(HttpEventProducer.class);

	private DSSDataSourcePollingHttp ds;

	/*
	 * 
	 */

	/**
	 * @param ctx
	 * @param ds
	 * @param sessionId
	 * @param test
	 */
	public HttpEventProducer(CamelContext ctx, DSSDataSourcePollingHttp ds,
			Long sessionId, boolean test) {
		super(ctx, sessionId, test);
		this.ds = ds;
	}

	@Override
	public void appendToRouteSpecific(ExpressionNode route) {
		// add the delay defined by the polling interval
		route.delay(ds.getInterval());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getProcessor()
	 */
	@Override
	protected Processor getProcessor() {
		
		final DSSDataSource dataSource = this.ds;
		
		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				DefaultMessage msg = (DefaultMessage) exchange.getIn();
				InputStream body = (InputStream) msg.getBody();
				HttpDataSource ds = (HttpDataSource) getDataSource();
				String value = StringUtils.EMPTY;
				try {
					switch (ds.getQueryType()) {
					case CSS:
						value = HttpUtil.applyCssQuery(body, ds.getQuery(),
								ds.getUrl());
						break;
					case XPATH:
						value = HttpUtil.applyXpathQuery(body, ds.getQuery(),
								ds.getUrl());
						break;
					}
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
				setMessageBodyEvent(msg, value, value, dataSource.getName());
			}

		};

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getFrom()
	 */
	@Override
	protected ComponentHelper getFrom() {
		return new HttpComponentHelper(getContext(), ds.getUrl());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.camel.producer.EventProducerRoute#getDataSource()
	 */
	@Override
	public DSSDataSource getDataSource() {
		return ds;
	}

}
