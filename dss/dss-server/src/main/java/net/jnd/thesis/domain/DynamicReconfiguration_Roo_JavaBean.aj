// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.domain;

import net.jnd.thesis.domain.DynamicReconfiguration;
import net.jnd.thesis.domain.EsperExpression;
import net.jnd.thesis.domain.InteractionModel;
import net.jnd.thesis.domain.SessionCondition;

privileged aspect DynamicReconfiguration_Roo_JavaBean {
    
    public String DynamicReconfiguration.getDescription() {
        return this.description;
    }
    
    public void DynamicReconfiguration.setDescription(String description) {
        this.description = description;
    }
    
    public String DynamicReconfiguration.getEventName() {
        return this.eventName;
    }
    
    public void DynamicReconfiguration.setEventName(String eventName) {
        this.eventName = eventName;
    }
    
    public Boolean DynamicReconfiguration.getEventNameEnabled() {
        return this.eventNameEnabled;
    }
    
    public void DynamicReconfiguration.setEventNameEnabled(Boolean eventNameEnabled) {
        this.eventNameEnabled = eventNameEnabled;
    }
    
    public SessionCondition DynamicReconfiguration.getSessionCondition() {
        return this.sessionCondition;
    }
    
    public void DynamicReconfiguration.setSessionCondition(SessionCondition sessionCondition) {
        this.sessionCondition = sessionCondition;
    }
    
    public Boolean DynamicReconfiguration.getSessionConditionEnabled() {
        return this.sessionConditionEnabled;
    }
    
    public void DynamicReconfiguration.setSessionConditionEnabled(Boolean sessionConditionEnabled) {
        this.sessionConditionEnabled = sessionConditionEnabled;
    }
    
    public InteractionModel DynamicReconfiguration.getInteractionModel() {
        return this.interactionModel;
    }
    
    public void DynamicReconfiguration.setInteractionModel(InteractionModel interactionModel) {
        this.interactionModel = interactionModel;
    }
    
    public Boolean DynamicReconfiguration.getInteractionModelEnabled() {
        return this.interactionModelEnabled;
    }
    
    public void DynamicReconfiguration.setInteractionModelEnabled(Boolean interactionModelEnabled) {
        this.interactionModelEnabled = interactionModelEnabled;
    }
    
    public EsperExpression DynamicReconfiguration.getEsperExpression() {
        return this.esperExpression;
    }
    
    public void DynamicReconfiguration.setEsperExpression(EsperExpression esperExpression) {
        this.esperExpression = esperExpression;
    }
    
    public Boolean DynamicReconfiguration.getEsperExpressionEnabled() {
        return this.esperExpressionEnabled;
    }
    
    public void DynamicReconfiguration.setEsperExpressionEnabled(Boolean esperExpressionEnabled) {
        this.esperExpressionEnabled = esperExpressionEnabled;
    }
    
}
