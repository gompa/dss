package net.jnd.thesis.camel.route;

import javax.naming.OperationNotSupportedException;

import net.jnd.thesis.camel.component.ComponentHelper;
import net.jnd.thesis.camel.component.RssComponentHelper;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.RssDataSource;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;

/**
 * @author João Domingos
 * @since Jun 5, 2012
 */
class RssEventProducer extends EsperEventProducer {

	private static final Logger LOG = LoggerFactory
			.getLogger(RssEventProducer.class);
	
	private long consumerInitialDelay = 10 * 1000;

	private RssDataSource ds;

	/*
	 * 
	 */

	/**
	 * @param ctx
	 * @param ds
	 * @param session
	 * @param test
	 */
	public RssEventProducer(CamelContext ctx, RssDataSource ds, Long sessionId,
			boolean test) {
		super(ctx, sessionId, test);
		this.ds = ds;
	}

	@Override
	protected Processor getProcessor() {

		final DSSDataSource dataSource = this.ds;
		
		return new Processor() {

			@Override
			public void process(Exchange exchange) {
				DefaultMessage msg = (DefaultMessage) exchange.getIn();
				SyndFeed body = (SyndFeed) msg.getBody();
				SyndEntry firstEntry = (SyndEntry) body.getEntries().get(0);
				String firstEntryTitle = firstEntry.getTitle() + ". ";
				String firstEntryContent = firstEntry.getDescription()
						.getValue();
				StringBuilder newBody = new StringBuilder();
				newBody.append(firstEntryTitle).append(firstEntryContent);
				String msgStr = newBody.toString();
				setMessageBodyEvent(msg, msgStr, msgStr, dataSource.getName());
				
				//DEBUG printing RssEventProducer info
				try {
					LOG.debug("DEBUG_PATH RssEventProducer, process(), from route: " + getFrom().from());
					LOG.debug("DEBUG_PATH RssEventProducer, process(),  to route: " + getTo().to());
					LOG.debug("DEBUG_PATH RssEventProducer,  process(), threadId: " + Thread.currentThread().getId());
					LOG.debug("DEBUG_PATH RssEventProducer,  process(), messageId: " + msg.getMessageId());
				} catch (OperationNotSupportedException e) {
					e.printStackTrace();
				}
			}

		};

	}

	@Override
	protected ComponentHelper getFrom() {
		return new RssComponentHelper(getContext(), ds.getUrl(),
				consumerInitialDelay, ds.getInterval());
	}

	@Override
	public DataSource getDataSource() {
		return ds;
	}

}
