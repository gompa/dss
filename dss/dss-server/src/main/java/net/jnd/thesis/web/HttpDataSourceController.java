package net.jnd.thesis.web;

import net.jnd.thesis.domain.HttpDataSource;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/httpdatasources")
@Controller
@RooWebScaffold(path = "httpdatasources", formBackingObject = HttpDataSource.class)
public class HttpDataSourceController {
}
