package net.jnd.thesis.web;

import net.jnd.thesis.domain.AuthRole;
import net.jnd.thesis.domain.AuthUser;
import net.jnd.thesis.domain.DataSource;
import net.jnd.thesis.domain.DynamicReconfiguration;
import net.jnd.thesis.domain.EsperExpression;
import net.jnd.thesis.domain.Event;
import net.jnd.thesis.domain.HttpDataSource;
import net.jnd.thesis.domain.InteractionModel;
import net.jnd.thesis.domain.LogEntry;
import net.jnd.thesis.domain.RssDataSource;
import net.jnd.thesis.domain.Session;
import net.jnd.thesis.domain.SessionCondition;
import net.jnd.thesis.domain.TwitterDataSource;
import net.jnd.thesis.domain.UserSessionConnection;
import net.jnd.thesis.domain.XmppDataSource;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.roo.addon.web.mvc.controller.converter.RooConversionService;

/**
 * A central place to register application converters and formatters.
 */
@RooConversionService
public class ApplicationConversionServiceFactoryBean extends
		FormattingConversionServiceFactoryBean {

	@SuppressWarnings("deprecation")
	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
		registry.addConverter(getAuthRoleToStringConverter());
		registry.addConverter(getAuthUserToStringConverter());
		registry.addConverter(getDynamicReconfigurationToStringConverter());
		registry.addConverter(getEsperExpressionToStringConverter());
		registry.addConverter(getEventToStringConverter());
		registry.addConverter(getHttpDataSourceToStringConverter());
		registry.addConverter(getInteractionModelToStringConverter());
		registry.addConverter(getLogEntryToStringConverter());
		registry.addConverter(getRssDataSourceToStringConverter());
		registry.addConverter(getSessionToStringConverter());
		registry.addConverter(getTwitterDataSourceToStringConverter());
		registry.addConverter(getUserSessionConnectionToStringConverter());
		registry.addConverter(getUserSessionConnectionToStringConverter());
		registry.addConverter(getXmppDataSourceToStringConverter());
		registry.addConverter(getDataSourceToStringConverter());
	}
	
	public Converter<DataSource, String> getDataSourceToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.DataSource, java.lang.String>() {
			public String convert(DataSource ds) {
				return ds.toString();
			}
		};
	}

	public Converter<AuthRole, String> getAuthRoleToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.AuthRole, java.lang.String>() {
			public String convert(AuthRole authRole) {
				// return new StringBuilder().append(authRole.getCreationDate())
				// .append(' ').append(authRole.getUpdateDate())
				// .append(' ').append(authRole.getAuthority()).toString();
				return authRole.toString();
			}
		};
	}

	public Converter<AuthUser, String> getAuthUserToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.AuthUser, java.lang.String>() {
			public String convert(AuthUser authUser) {
				// return new StringBuilder().append(authUser.getCreationDate())
				// .append(' ').append(authUser.getUpdateDate())
				// .append(' ').append(authUser.getUsername()).append(' ')
				// .append(authUser.getPassword()).toString();
				return authUser.toString();
			}
		};
	}

	public Converter<DynamicReconfiguration, String> getDynamicReconfigurationToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.DynamicReconfiguration, java.lang.String>() {
			public String convert(DynamicReconfiguration dynamicReconfiguration) {
				// return "(no displayable fields)";
				return dynamicReconfiguration.toString();
			}
		};
	}

	public Converter<EsperExpression, String> getEsperExpressionToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.EsperExpression, java.lang.String>() {
			public String convert(EsperExpression esperExpression) {
				// return new StringBuilder()
				// .append(esperExpression.getCreationDate()).append(' ')
				// .append(esperExpression.getUpdateDate()).append(' ')
				// .append(esperExpression.getExpressionQuery())
				// .toString();
				return esperExpression.toString();
			}
		};
	}

	public Converter<Event, String> getEventToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.Event, java.lang.String>() {
			public String convert(Event event) {
				// return new StringBuilder().append(event.getCreationDate())
				// .append(' ').append(event.getUpdateDate()).append(' ')
				// .append(event.getValueRaw()).append(' ')
				// .append(event.getValue()).toString();
				return event.toString();
			}
		};
	}

	public Converter<HttpDataSource, String> getHttpDataSourceToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.HttpDataSource, java.lang.String>() {
			public String convert(HttpDataSource httpDataSource) {
				// return new StringBuilder()
				// .append(httpDataSource.getCreationDate()).append(' ')
				// .append(httpDataSource.getUpdateDate()).append(' ')
				// .append(httpDataSource.getName()).append(' ')
				// .append(httpDataSource.getRegex()).toString();
				return httpDataSource.toString();
			}
		};
	}

	public Converter<InteractionModel, String> getInteractionModelToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.InteractionModel, java.lang.String>() {
			public String convert(InteractionModel interactionModel) {
				// return new StringBuilder().append(
				// interactionModel.getEventDelay()).toString();
				return interactionModel.toString();
			}
		};
	}

	public Converter<LogEntry, String> getLogEntryToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.LogEntry, java.lang.String>() {
			public String convert(LogEntry logEntry) {
				// return new StringBuilder().append(logEntry.getCreationDate())
				// .append(' ').append(logEntry.getUpdateDate())
				// .append(' ').append(logEntry.getUsr()).append(' ')
				// .append(logEntry.getSession()).toString();
				return logEntry.toString();
			}
		};
	}

	public Converter<RssDataSource, String> getRssDataSourceToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.RssDataSource, java.lang.String>() {
			public String convert(RssDataSource rssDataSource) {
				// return new StringBuilder()
				// .append(rssDataSource.getCreationDate()).append(' ')
				// .append(rssDataSource.getUpdateDate()).append(' ')
				// .append(rssDataSource.getName()).append(' ')
				// .append(rssDataSource.getRegex()).toString();
				return rssDataSource.toString();
			}
		};
	}

	public Converter<Session, String> getSessionToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.Session, java.lang.String>() {
			public String convert(Session session) {
				// return new StringBuilder().append(session.getCreationDate())
				// .append(' ').append(session.getUpdateDate())
				// .append(' ').append(session.getName()).toString();
				return session.toString();
			}
		};
	}

	public Converter<SessionCondition, String> getSessionConditionToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.SessionCondition, java.lang.String>() {
			public String convert(SessionCondition sessionCondition) {
				// return new StringBuilder()
				// .append(sessionCondition.getCreationDate()).append(' ')
				// .append(sessionCondition.getUpdateDate()).append(' ')
				// .append(sessionCondition.getValue()).toString();
				return sessionCondition.toString();
			}
		};
	}

	public Converter<TwitterDataSource, String> getTwitterDataSourceToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.TwitterDataSource, java.lang.String>() {
			public String convert(TwitterDataSource twitterDataSource) {
				// return new StringBuilder()
				// .append(twitterDataSource.getCreationDate())
				// .append(' ').append(twitterDataSource.getUpdateDate())
				// .append(' ').append(twitterDataSource.getName())
				// .append(' ').append(twitterDataSource.getRegex())
				// .toString();
				return twitterDataSource.toString();
			}
		};
	}

	public Converter<UserSessionConnection, String> getUserSessionConnectionToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.UserSessionConnection, java.lang.String>() {
			public String convert(UserSessionConnection userSessionConnection) {
				// return new StringBuilder()
				// .append(userSessionConnection.getCreationDate())
				// .append(' ')
				// .append(userSessionConnection.getUpdateDate())
				// .toString();
				return userSessionConnection.toString();
			}
		};
	}

	public Converter<XmppDataSource, String> getXmppDataSourceToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<net.jnd.thesis.domain.XmppDataSource, java.lang.String>() {
			public String convert(XmppDataSource xmppDataSource) {
				// return new StringBuilder()
				// .append(xmppDataSource.getCreationDate()).append(' ')
				// .append(xmppDataSource.getUpdateDate()).append(' ')
				// .append(xmppDataSource.getName()).append(' ')
				// .append(xmppDataSource.getRegex()).toString();
				return xmppDataSource.toString();
			}
		};
	}
}
