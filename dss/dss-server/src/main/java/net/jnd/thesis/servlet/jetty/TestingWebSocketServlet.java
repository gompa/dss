package net.jnd.thesis.servlet.jetty;

import javax.servlet.http.HttpServletRequest;

import net.jnd.thesis.servlet.ws.DSSWebSocket.Type;

import org.eclipse.jetty.websocket.WebSocket;

/**
 * @author João Domingos
 * @since Jun 4, 2012
 */
public class TestingWebSocketServlet extends DataSourceWebSocketServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3694786178278768277L;

	@Override
	public WebSocket doWebSocketConnect(HttpServletRequest request,
			String params) {
		return CustomJettyWebSocketFactory.create(Type.TEST, request);
	}

}
