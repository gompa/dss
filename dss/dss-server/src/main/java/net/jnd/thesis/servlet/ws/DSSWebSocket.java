/**
 * 
 */
package net.jnd.thesis.servlet.ws;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public interface DSSWebSocket {

	/**
	 * 
	 */
	public enum Type {
		SESSION, TEST, MONITOR;
	}

	/**
	 * @param msg
	 */
	void sendMessage(String msg);

	/**
	 * 
	 */
	void close();

	/**
	 * @return
	 */
	DSSWebSocket.Type getType();

	// /**
	// * @return
	// */
	// DSSSession getSession();
	//
	// /**
	// * @return
	// */
	// DSSUser getUser();

	/**
	 * @return
	 */
	Long getUserSessionConnectionId();

	/**
	 * @return
	 */
	boolean isOpen();

}
