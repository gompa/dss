// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.jnd.thesis.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import net.jnd.thesis.domain.DomainEntity;

privileged aspect DomainEntity_Roo_Jpa_Entity {
    
    declare @type: DomainEntity: @MappedSuperclass;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long DomainEntity.id;
    
    @Version
    @Column(name = "version")
    private Integer DomainEntity.version;
    
    public Long DomainEntity.getId() {
        return this.id;
    }
    
    public void DomainEntity.setId(Long id) {
        this.id = id;
    }
    
    public Integer DomainEntity.getVersion() {
        return this.version;
    }
    
    public void DomainEntity.setVersion(Integer version) {
        this.version = version;
    }
    
}
