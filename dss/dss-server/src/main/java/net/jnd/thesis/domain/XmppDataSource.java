package net.jnd.thesis.domain;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import net.jnd.thesis.common.interfaces.DSSDataSourceEventXmpp;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;

/**
 * @author João Domingos
 * @since Nov 13, 2012
 */
@RooJavaBean
@RooJpaActiveRecord
@RooJson
@Table(name = "DSS_DATASOURCE_XMPP")
public class XmppDataSource extends EventDrivenDataSource implements
		DSSDataSourceEventXmpp {

	//INFO: configure the name of the default xmpp account being used by the server
	//TESTING: changed
	@NotNull
	@Size(max = 40)
//	private String username = "thesis.dimfccs@gmail.com";
	private String username = "thesis.dimfccs";
	
	@NotNull
	@Size(max = 40)
	private String password;
	
	@Size(max = 40)
	private String fromUser;

	public DataSourceType getDataSourceType() {
		return DataSourceType.XMPP;
	}

	public net.jnd.thesis.common.enums.DataSourceType getType() {
		return net.jnd.thesis.common.enums.DataSourceType
				.valueOf(getDataSourceType().name());
	}
	
//	/**
//	 * @return
//	 */
//	public static long countXmppDataSources() {
//		return count(entityManager(), XmppDataSource.class);
//	}
//
//	/**
//	 * @return
//	 */
//	public static List<XmppDataSource> findAllXmppDataSources() {
//		return findAll(entityManager(), XmppDataSource.class);
//	}
//
//	/**
//	 * @param id
//	 * @return
//	 */
//	public static XmppDataSource findXmppDataSource(Long id) {
//		return find(entityManager(), XmppDataSource.class, id);
//	}
//
//	/**
//	 * @param firstResult
//	 * @param maxResults
//	 * @return
//	 */
//	public static List<XmppDataSource> findXmppDataSourceEntries(
//			int firstResult, int maxResults) {
//		return findEntries(entityManager(), XmppDataSource.class, firstResult,
//				maxResults);
//	}

}
