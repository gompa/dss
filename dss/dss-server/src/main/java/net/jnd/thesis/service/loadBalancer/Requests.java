package net.jnd.thesis.service.loadBalancer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.CancelSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.LaunchSpecification;
import com.amazonaws.services.ec2.model.RequestSpotInstancesRequest;
import com.amazonaws.services.ec2.model.RequestSpotInstancesResult;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.SpotInstanceRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.Instance;

/**
 * @author Pedro Martins
 * 
 * This class manages all the requests done to AWS EC2 for new VMs and it also 
 * keeps track of all the VMs currently running as well. It is based on AWS SDK
 * Java samples. For more information check:
 * {@link http://aws.amazon.com/developers/getting-started/java/}
 * 
 * @since 1.4.0
 * */
public class Requests {

	/* INFO:
	 * Path inside the .war file is "/WEB-INF/classes/". For more information see the 
	 * stackOverflow question:
	 * http://stackoverflow.com/questions/22131718/access-aws-credentials-file-in-war?noredirect=1#comment33580244_22131718
	 * 
	 * To make a file accessible in the "/" relative path, put it inside the 
	 * "/dss-server/src/main/resources/" directory.
	 * */
	private static final String AWS_CREDENTIALS_PATH = "/AwsCredentials.properties";

	private static final Logger LOG = LoggerFactory
			.getLogger(Requests.class);

	public final static String AMI_ID = "ami-5fe02428";
	public final static String INSTANCE_TYPE = "m1.small";
	public final static String SPOT_INSTANCE_PRICE = "0.03";

	public final static String INSTANCE_METADATA_URL = "http://169.254.169.254/latest/meta-data/";
	public final static String INSTANCE_ID_URL = INSTANCE_METADATA_URL + "instance-id";
	public final static String INSTANCE_PUBLIC_IPV4_URL = INSTANCE_METADATA_URL + "public-ipv4";
	public final static String URL_ENCODING = "UTF-8";

	private AmazonEC2		ec2;
	private List<String> 	instanceIds;			//active instances running
	private List<String> 	spotInstanceRequestIds;	//instances waiting for activation
	private List<String>	securityGroups;			//security groups used
	private String instance_id, myPublicIp;

	private static Requests requests;

	/**
	 * The only information needed to create a client are security credentials
	 * consisting of the AWS Access Key ID and Secret Access Key. All other
	 * configuration, such as the service endpoints, are performed
	 * automatically. Client parameters, such as proxies, can be specified in an
	 * optional ClientConfiguration object when constructing a client.
	 * 
	 * @param	awsCredentialsFile	inputStream to the 
	 * 								"AwsCredentials.properties" file that is 
	 * 								storing the credentials for this account.
	 * 
	 * @throws IOException			if it fails to read the 
	 * 								"AwsCredentials.properties" file containing 
	 * 								the access and secret keys.
	 *
	 * @see com.amazonaws.auth.BasicAWSCredentials
	 * @see com.amazonaws.auth.PropertiesCredentials
	 * @see com.amazonaws.ClientConfiguration
	 */
	private Requests () {

		BufferedReader reader = null;
		try{
			InputStream inputStream = 
					getClass().getClassLoader().getResourceAsStream(AWS_CREDENTIALS_PATH);


			AWSCredentials credentials = new PropertiesCredentials(inputStream);

			ec2 = new AmazonEC2Client(credentials);
			Region euWest1 = Region.getRegion(Regions.EU_WEST_1);
			ec2.setRegion(euWest1);

			// Initialize variables.
			instanceIds = new LinkedList<String>();

			// Setup a list to collect all of the request ids we want to watch hit the running state.
			spotInstanceRequestIds = new LinkedList<String>();

			//setup security groups
			securityGroups = new LinkedList<String>();
			securityGroups.add("GettingStartedGroup");
			
			//getting instance information
			URL url = new URL(INSTANCE_ID_URL);
			reader = new BufferedReader(new InputStreamReader(url.openStream(), URL_ENCODING));
			instance_id = reader.readLine();

			url = new URL(INSTANCE_PUBLIC_IPV4_URL);
			reader = new BufferedReader(new InputStreamReader(url.openStream(), URL_ENCODING));
			myPublicIp = reader.readLine();

		}catch(MalformedURLException e){
			LOG.error("Unable to access AWS Polling services to retrieve "
					+ "instance data because the URL being used has changed or "
					+ "is incorrect: " + e);
		} catch (UnsupportedEncodingException e) {
			LOG.error("Character Encoding from retrieveing instance data using "
					+ "AWS Polling service has changed or is incorrect, and "
					+ "thus data received cannot be read: " + e);
		} catch (IOException e) {
			LOG.error("IO error ocurred while reading AWS credentials file or "
					+ "while trying to establish a connection: " + e);
		}finally{
			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					LOG.error("Unable to close reader after using it to read "
							+ "content from AWS Polling Services: " + e);
				}
		}



	}

	public static Requests getInstance(){
		if(requests == null)
			requests = new Requests();

		return requests;
	}

	/**
	 * The submit method will create 1 x one-time t1.micro request with a 
	 * maximum bid price of SPOT_INSTANCE_PRICE dollars using the dss-server AMI 
	 * identified by the AMI id.
	 */
	public void submitRequests() {
		//==========================================================================//
		//================= Submit a Spot Instance Request =====================//
		//==========================================================================//

		// Initializes a Spot Instance Request
		RequestSpotInstancesRequest requestRequest = new RequestSpotInstancesRequest();

		// Request 1 x t1.micro instance with a bid price of $0.03.
		requestRequest.setSpotPrice(SPOT_INSTANCE_PRICE);
		requestRequest.setInstanceCount(1);

		// Setup the specifications of the launch. This includes the instance type (e.g. t1.micro)
		// and the latest dss-server AMI id available. 
		LaunchSpecification launchSpecification = new LaunchSpecification();
		launchSpecification.setImageId(AMI_ID);
		launchSpecification.setInstanceType(INSTANCE_TYPE);

		// Add the security group to the request.
		launchSpecification.setSecurityGroups(securityGroups);

		// Add the launch specifications to the request.
		requestRequest.setLaunchSpecification(launchSpecification);

		// Call the RequestSpotInstance API.
		RequestSpotInstancesResult requestResult = ec2.requestSpotInstances(requestRequest);
		List<SpotInstanceRequest> requestResponses = requestResult.getSpotInstanceRequests();

		/*
		 * Add all of the request ids to the list, so we can determine when they 
		 * hit the active state.
		 */
		for (SpotInstanceRequest requestResponse : requestResponses) {
			LOG.info("Created Spot Request: " + requestResponse.getSpotInstanceRequestId());
			spotInstanceRequestIds.add(requestResponse.getSpotInstanceRequestId());
		}
	}

	/**
	 * This method travels through all spotInstanceRequests and determines if 
	 * they are active or not. If they are active, we add them to our list of 
	 * active instances and remove them from out spotInstanceRequests list, 
	 * otherwise we just wait until they become active. This method updates the
	 * lists of active and open instances and it should be called before 
	 * checking them.
	 * 
	 * This method has complexity of O(n) where n is the number of 
	 * spotInstanceRequests currently open.
	 * 
	 * @see	#getActiveInstances()
	 * @see	#getOpenRequests()
	 * @see	#areAnyActive()
	 * @see	#areAnyOpen()
	 */
	public boolean updateLists(){
		//====================================================================//
		//============== Describe Spot Instance Requests to determine ========//
		//====================================================================//

		//if there are no requests, there is nothing to update!
		if(!areAnyOpen())
			return true;

		// Create the describeRequest with all of the spot instance requests
		DescribeSpotInstanceRequestsRequest describeRequest = new DescribeSpotInstanceRequestsRequest();
		describeRequest.setSpotInstanceRequestIds(spotInstanceRequestIds);

		LOG.info("Checking to determine if Spot Bids have reached the active "
				+ "state...");

		try
		{
			// Retrieve all of the requests we want to monitor.
			DescribeSpotInstanceRequestsResult describeResult = ec2.describeSpotInstanceRequests(describeRequest);
			List<SpotInstanceRequest> describeResponses = describeResult.getSpotInstanceRequests();
			String instanceId = null;

			// Look through each request and determine if they are all in the active state.
			for (SpotInstanceRequest describeResponse : describeResponses) {
				instanceId = describeResponse.getInstanceId();

				LOG.info(describeResponse.getSpotInstanceRequestId() +
						" is in the " +describeResponse.getState() + " state.");

				/*
				 * If the state is not open and we have not processed this 
				 * instance yet, then we add the instance to the list of active 
				 * instances and remove it from the list of spot requests. 
				 * Otherwise we just wait.
				 */
				if (!describeResponse.getState().equals("open") && !instanceIds.contains(instanceId)) {
					spotInstanceRequestIds.remove(instanceId);
					instanceIds.add(instanceId);
				}
			}
			return true;
		} catch (AmazonServiceException e) {
			// Print out the error.
			LOG.error("Error when calling describeSpotInstances");
			LOG.error("Caught Exception: " + e);
			LOG.error("Reponse Status Code: " + e.getStatusCode());
			LOG.error("Error Code: " + e.getErrorCode());
			LOG.error("Request ID: " + e.getRequestId());
			return false;
		}
	}

	/**
	 * Cancels any open requests and terminates any running instances that were 
	 * created.
	 */
	public boolean cleanup () {
		return cleanupRequests() && cleanupActiveInstances();
	}

	public boolean cleanupRequests(){
		try {
			// Cancel requests if we have any
			if(spotInstanceRequestIds.size() > 0){
				LOG.info("Cancelling requests.");
				CancelSpotInstanceRequestsRequest cancelRequest = new CancelSpotInstanceRequestsRequest(spotInstanceRequestIds);
				ec2.cancelSpotInstanceRequests(cancelRequest);

				// Delete all requests that we have canceled.
				spotInstanceRequestIds.clear();
			}else{
				LOG.info("There are no requests to cancel.");
			}
			
		} catch (AmazonServiceException e) {
			// Write out any exceptions that may have occurred.
			LOG.error("Error cancelling requests");
			LOG.error("Caught Exception: " + e.getMessage());
			LOG.error("Reponse Status Code: " + e.getStatusCode());
			LOG.error("Error Code: " + e.getErrorCode());
			LOG.error("Request ID: " + e.getRequestId());
			return false;
		}
		return true;
	}
	
	public boolean cleanupActiveInstances(){
		try {
			// Terminate instances if we have any
			if(instanceIds.size() > 0){
				LOG.info("Terminate instances");
				TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest(instanceIds);
				ec2.terminateInstances(terminateRequest);

				// Delete all instances that we have terminated.
				instanceIds.clear();
			}else{
				LOG.info("There are no instances to terminate.");
			}

		} catch (AmazonServiceException e) {
			// Write out any exceptions that may have occurred.
			LOG.error("Error terminating instances");
			LOG.error("Caught Exception: " + e.getMessage());
			LOG.error("Reponse Status Code: " + e.getStatusCode());
			LOG.error("Error Code: " + e.getErrorCode());
			LOG.error("Request ID: " + e.getRequestId());
			return false;
		}
		return true;
	}
	
	public boolean killVM(String instanceId){
		try{
			List<String> tmpInstanceList = new LinkedList<String>();
			tmpInstanceList.add(instanceId);
			TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest(tmpInstanceList);
			ec2.terminateInstances(terminateRequest);
			instanceIds.remove(instanceId);
		} catch (AmazonServiceException e) {
			// Write out any exceptions that may have occurred.
			LOG.error("Error terminating instances");
			LOG.error("Caught Exception: " + e.getMessage());
			LOG.error("Reponse Status Code: " + e.getStatusCode());
			LOG.error("Error Code: " + e.getErrorCode());
			LOG.error("Request ID: " + e.getRequestId());
			return false;
		}
		return true;
	}
	
	/**
	 * Returns a list with the ids of open spotInstanceRequests since the last 
	 * time we called the {@link #updateLists()} method. 
	 * 
	 * @return	a list with the ids of all the instances that are still in the
	 * 			"open" state waiting for activation since the last time we 
	 * 			called the #updateLists() method.
	 * @see		#updateLists()
	 * */
	public List<String> getOpenRequests(){
		return spotInstanceRequestIds;
	}

	/**
	 * Returns the list with the ids of the active instances since the last time
	 * we called the {@link #updateLists()} method.
	 * 
	 * @return	a list with the ids of all the active instances since last time 
	 * 			we called the #updateLists() method.
	 * @see		#updateLists()
	 * */
	public List<String> getActiveInstances(){
		return instanceIds;
	}

	/**
	 * Returns true if any instances are active since the last time we called 
	 * the {@link #updateLists()} method, false otherwise.
	 * 
	 * @return	<code>true</code> if any instances are active since the last 
	 * 			time we updated the lists or <code>false</code> otherwise.
	 * @see		#updateLists()
	 * */
	public boolean areAnyActive(){
		return  instanceIds.size() > 0;
	}

	/**
	 * Returns true if we have any open requests since the last time we called 
	 * the {@link #updateLists()} method, false otherwise.
	 * 
	 * @return	<code>false</code> if all the requests made in the past are now 
	 *			active, cancelled or terminated, or <code>true</code> otherwise,
	 *			since the last time the list was updated.
	 * @see		#updateLists()
	 * */
	public boolean areAnyOpen() {
		return  spotInstanceRequestIds.size() > 0;
	}

	/**
	 * Returns a list with the public IPs of all the active instances, which are
	 * returned by the {@link #getActiveInstances()} method.
	 * 
	 * @return	a list with the public IPs of all the active instances.
	 * @see		#getActiveInstances()
	 * */
	public List<String> getPublicIPs(){
		List<String> publicIpsList = new LinkedList<String>();

		//if there are no active instances, we return immediately to avoid extra 
		//computations.
		if(!areAnyActive())
			return publicIpsList;

		DescribeInstancesRequest request =  new DescribeInstancesRequest();
		request.setInstanceIds(instanceIds);

		DescribeInstancesResult result = ec2.describeInstances(request);
		List<Reservation> reservations = result.getReservations();

		List<Instance> instances;
		for(Reservation res : reservations){
			instances = res.getInstances();
			for(Instance ins : instances){
				LOG.info("PublicIP from " + ins.getImageId() + " is " + ins.getPublicIpAddress());
				publicIpsList.add(ins.getPublicIpAddress());
			}
		}

		return publicIpsList;
	}

	public String getMyPublicIP(){
		return myPublicIp;
	}

	public String getMyInstanceId(){
		return instance_id;
	}
}

