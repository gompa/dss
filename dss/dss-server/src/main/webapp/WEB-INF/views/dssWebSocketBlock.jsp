<%@page import="net.jnd.thesis.common.enums.RequestParams"%>

<div xmlns:util="urn:jsptagdir:/WEB-INF/tags/util">

  <util:panel id="title" title="Test">
  
    <style type="text/css">
    
        #console-container {
            width: 97%;
        }
    
        #console {
            border: 1px solid #CCCCCC;
            border-right-color: #999999;
            border-bottom-color: #999999;
            height: 170px;
            overflow-y: scroll;
            padding: 5px;
            width: 100%;
            height: 600px;
        }
    
        #console p {
            padding: 0;
            margin: 0;
        }
        
    </style>
    
    <script type="text/javascript">
    
        var DSWebSocket = {};
    
        DSWebSocket.socket = null;
        DSWebSocket.active = false;
    
        DSWebSocket.connect = (function(host) {
            if ('WebSocket' in window) {
            	DSWebSocket.socket = new WebSocket(host);
            } else if ('MozWebSocket' in window) {
            	DSWebSocket.socket = new MozWebSocket(host);
            } else {
            	DSWebSocketConsole.log('Error: WebSocket is not supported by this browser.');
                return;
            }
    
            DSWebSocket.socket.onopen = function () {
                DSWebSocketConsole.logLocal('Info: WebSocket connection opened.');
            	DSWebSocket.active = true;
            };
    
            DSWebSocket.socket.onclose = function () {
                DSWebSocketConsole.logLocal('Info: WebSocket closed.');
            	DSWebSocket.active = false;
            };
    
            DSWebSocket.socket.onmessage = function (message) {
            	DSWebSocketConsole.logInbound(message.data);
            };
            
        });
    
        DSWebSocket.initialize = function() {
        	DSWebSocketConsole.log('-------------');
        	DSWebSocketConsole.logLocal('initializing websocket...');
        	DSWebSocket.connect('ws://' + window.location.host + '/dataSourceSessions/' + '<%= request.getParameter(RequestParams.WEBSOCKET_LOCATION.name()) %>');
        };
    
        DSWebSocket.sendMessage = (function(origin) {
            /*var message = document.getElementById(origin).value;
            if (message != '') {
            	DSWebSocket.socket.send(origin + ':' + message);
            }*/
        });
        
        var DSWebSocketConsole = {};
    
        DSWebSocketConsole.log = (function(message) {
            var console = document.getElementById('console');
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.innerHTML = message;
            console.appendChild(p);
            /* while (console.childNodes.length > 25) {
                console.removeChild(console.firstChild);
            } */
            console.scrollTop = console.scrollHeight;
        });
    
        DSWebSocketConsole.logInbound = (function(message) {
        	DSWebSocketConsole.log('<b>[inbound]</b> ' + message);
        });
    
        DSWebSocketConsole.logLocal = (function(message) {
        	DSWebSocketConsole.log('<b>[local]</b> ' + message);
        });
    
        DSWebSocketConsole.clear = (function() {
        	document.getElementById('console').innerHTML = '';
        });
    
        function openWebSocket() {
        	DSWebSocketConsole.log('-------------');
        	DSWebSocketConsole.logLocal('initializing test...');
       		dojo.xhrPost({
       			  form: dojo.query('form')[0],
       		      url: "http://" + window.location.host + "/dataSourceSessions/" + '<%= request.getParameter(RequestParams.START_CONSOLE_ACTION.name()) %>',
       		      handleAs: "json",
       		      load: function(response, ioArgs) {
       		    	   DSWebSocketConsole.logLocal("ajax call success!");
       		    	   if(!response) {
       		    		   DSWebSocketConsole.logInbound("reponse is empty...");
       		    	   } else if(response.errors) {
            			   DSWebSocketConsole.logInbound("please validate the form: <br/>");
            			   DSWebSocketConsole.logInbound(response.errors);
       		    	   } else {
       		    		   console.log(dojo.toJson(response));
                           DSWebSocket.initialize();
       		    	   }
       		      },
       		      error: function(error, ioArgs) {
       		    	  DSWebSocketConsole.logLocal("ajax call failed...");
       		    	  console.log(error);
       		      }
       		});
    
       	}
        
        function clearAction() {
        	DSWebSocketConsole.clear();
        }
        
        openWebSocket();
        
    </script>
    
    <noscript>
      <h2 style="color: #ff0000">
        Seems your browser doesn't support Javascript! Websockets rely on Javascript being enabled. Please enable Javascript and reload this page!
      </h2>
    </noscript>
    
    <p>
      <script type="text/javascript">Spring.addDecoration(new Spring.ValidateAllDecoration({elementId:'clearConsole', event:'onclick'}));</script>
      <div class="submit" xmlns:fn="http://java.sun.com/jsp/jstl/functions" xmlns:spring="http://www.springframework.org/tags" >
          <spring:message code="websocket_block_button_label_clear" var="clear_button_label" htmlEscape="false" />
    	  <input type="submit" onclick="clearAction(); return false;" value="Clear console" id="clearConsole" />
      </div>
    </p>
    
    <div id="console-container">
      <div id="console"></div>
    </div>
  
  </util:panel>

</div>