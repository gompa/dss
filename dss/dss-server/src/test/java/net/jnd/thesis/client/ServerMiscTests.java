/**
 * 
 */
package net.jnd.thesis.client;

import java.io.IOException;
import java.net.URL;

import net.jnd.thesis.common.bean.StringBean;
import net.jnd.thesis.ws.services.WebSocketMessage;
import net.jnd.thesis.ws.services.WebSocketMessageHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xpath.XPathAPI;
import org.cyberneko.html.parsers.DOMParser;
import org.dom4j.io.DOMReader;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.w3c.dom.Node;

/**
 * @author João Domingos
 * @since Dec 6, 2012
 */
public class ServerMiscTests {

	private static final Log LOG = LogFactory.getLog(ServerMiscTests.class);

	private static final String CLIENT_WEBSOCKET_URL = "ws://localhost:8080/dss-server/ws/client";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// jsonTesting();
		htmlXpathTest();
	}

	/**
	 * 
	 */
	private static void htmlXpathTest() {
		// jsoupTest();
		// htmlCleanerTest();
		nekoHtmlTest();
	}

	/**
	 * 
	 */
	private static void nekoHtmlTest() {
		DOMParser parser = new DOMParser();
		try {
			parser.parse("http://www.bolsapt.com/cotacoes/PSI/");
			org.w3c.dom.Document document = parser.getDocument();
			DOMReader reader = new DOMReader();
			org.dom4j.Document doc = reader.read(document);
			org.dom4j.Node node = doc.selectSingleNode("//SPAN[@id='dados-do-activo-dedf-ALTR-LS']");
			node = doc.selectSingleNode("//SPAN[@id='dados-do-activo-dedf-ALTR-LS']/parent::*/following-sibling::*/text()");
			System.out.println(node.getText());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private static void htmlCleanerTest() {
		final HtmlCleaner cleaner = new HtmlCleaner();
		try {
			TagNode node = cleaner.clean(new URL(
					"http://www.bolsapt.com/cotacoes/PSI/"));
			Object[] res = node
					.evaluateXPath("//span[@id=\"dados-do-activo-dedf-ALTR-LS\"]");
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private static void jsoupTest() {
		Document doc;
		try {
			doc = Jsoup.connect("http://www.bolsapt.com/cotacoes/PSI/").get();
			Elements elems = doc
					.select("//span[@id=\"dados-do-activo-dedf-ALTR-LS\"]");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private static void jsonTesting() {
		// String json =
		// "{\"class\":\"net.jnd.thesis.ws.services.WebSocketMessage\",\"errors\":[],\"payload\":[{\"class\":\"net.jnd.thesis.common.bean.StringBean\",\"creationDate\":null,\"creationUser\":null,\"id\":null,\"message\":\"Hello world!\",\"updateDate\":null,\"updateUser\":null}],\"requestId\":1,\"serviceName\":\"echo\"}";
		// WebSocketMessage csd =
		// WebSocketMessageHelper.deserializeMessage(json);
		// System.out.println(csd);

		try {
			// Collection<DSSBaseBean> cas = new ArrayList<DSSBaseBean>();
			// cas.add(new AuthenticationBean("usr", "pwd"));
			WebSocketMessage msg = new WebSocketMessage(1L, "echo",
					new StringBean("csdcsdcsdcscscsdc"));
			String json = WebSocketMessageHelper.serializeMessage(msg);
			WebSocketMessage caca = WebSocketMessageHelper
					.deserializeMessage(json);
			System.out.println(caca);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
