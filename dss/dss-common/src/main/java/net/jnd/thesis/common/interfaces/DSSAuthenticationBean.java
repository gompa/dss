/**
 * 
 */
package net.jnd.thesis.common.interfaces;

/**
 * @author João Domingos
 * @since Jan 12, 2013
 */
public interface DSSAuthenticationBean extends DSSBaseBean {

	String getUsername();

	String getPassword();

}
