/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Nov 3, 2012
 */
public enum SessionStatus {
	NEW, STARTING, STARTED, STOPPING, PAUSED, ENDED;
}
