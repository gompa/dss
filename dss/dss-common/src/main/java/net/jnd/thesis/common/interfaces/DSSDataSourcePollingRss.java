package net.jnd.thesis.common.interfaces;

/**
 * @author João Domingos
 * @since Nov 1, 2012
 */
public interface DSSDataSourcePollingRss extends DSSDataSourcePolling {

	String getUrl();

}
