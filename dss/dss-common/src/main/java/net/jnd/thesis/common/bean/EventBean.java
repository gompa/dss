/**
 * 
 */
package net.jnd.thesis.common.bean;

import java.util.Date;

import net.jnd.thesis.common.ToStringHelper;
import net.jnd.thesis.common.enums.EventType;
import net.jnd.thesis.common.interfaces.DSSEvent;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class EventBean extends BaseBean implements DSSEvent {

	private Date date;

	private Long dataSourceId;

	private Long sessionId;

	private Float floatValue;

	private String value;

	private String valueRaw;

	private EventType eventType;
	
	private String name;

	@Override
	public String toString() {
		return ToStringHelper.build(this);
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the sessionId
	 */
	public Long getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId
	 *            the sessionId to set
	 */
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the valueRaw
	 */
	public String getValueRaw() {
		return valueRaw;
	}

	/**
	 * @param valueRaw
	 *            the valueRaw to set
	 */
	public void setValueRaw(String valueRaw) {
		this.valueRaw = valueRaw;
	}

	/**
	 * @return the dataSourceId
	 */
	public Long getDataSourceId() {
		return dataSourceId;
	}

	/**
	 * @param dataSourceId
	 *            the dataSourceId to set
	 */
	public void setDataSourceId(Long dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	/**
	 * @return the floatValue
	 */
	public Float getFloatValue() {
		return floatValue;
	}

	/**
	 * @param floatValue
	 *            the floatValue to set
	 */
	public void setFloatValue(Float floatValue) {
		this.floatValue = floatValue;
	}

	/**
	 * @return the eventType
	 */
	public EventType getType() {
		return eventType;
	}

	/**
	 * @param eventType
	 *            the eventType to set
	 */
	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the eventType
	 */
	public EventType getEventType() {
		return eventType;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	
}
