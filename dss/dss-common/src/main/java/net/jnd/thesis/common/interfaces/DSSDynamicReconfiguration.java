/**
 * 
 */
package net.jnd.thesis.common.interfaces;

/**
 * @author João Domingos
 * @since Nov 18, 2012
 */
public interface DSSDynamicReconfiguration extends DSSBaseBean {

	String getDescription();

	String getEventName();

	DSSSessionCondition getDssSessionCondition();

	DSSInteractionModel getDssInteractionModel();

	DSSEsperExpression getDssEsperExpression();

	boolean isSessionConditionEnabled();

	boolean isEventNameEnabled();

	boolean isInteractionModelEnabled();

	boolean isEsperExpressionEnabled();

}
