/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.interfaces.DSSDataSourceEvent;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class DataSourceBeanEvent extends DataSourceBean implements
		DSSDataSourceEvent {

}
