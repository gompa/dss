/**
 * 
 */
package net.jnd.thesis.common.bean;

/**
 * @author João Domingos
 * @since Dec 11, 2012
 */
public class WSConnectionBean extends BaseBean {

	public String url;

	public Long timeout;

	public Integer maxBinaryMessageSize;

	public Integer maxIdleTime;

	public Integer maxTextMessageSize;

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the maxBinaryMessageSize
	 */
	public Integer getMaxBinaryMessageSize() {
		return maxBinaryMessageSize;
	}

	/**
	 * @param maxBinaryMessageSize
	 *            the maxBinaryMessageSize to set
	 */
	public void setMaxBinaryMessageSize(Integer maxBinaryMessageSize) {
		this.maxBinaryMessageSize = maxBinaryMessageSize;
	}

	/**
	 * @return the maxIdleTime
	 */
	public Integer getMaxIdleTime() {
		return maxIdleTime;
	}

	/**
	 * @param maxIdleTime
	 *            the maxIdleTime to set
	 */
	public void setMaxIdleTime(Integer maxIdleTime) {
		this.maxIdleTime = maxIdleTime;
	}

	/**
	 * @return the maxTextMessageSize
	 */
	public Integer getMaxTextMessageSize() {
		return maxTextMessageSize;
	}

	/**
	 * @param maxTextMessageSize
	 *            the maxTextMessageSize to set
	 */
	public void setMaxTextMessageSize(Integer maxTextMessageSize) {
		this.maxTextMessageSize = maxTextMessageSize;
	}

	/**
	 * @return the timeout
	 */
	public Long getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout
	 *            the timeout to set
	 */
	public void setTimeout(Long timeout) {
		this.timeout = timeout;
	}

}
