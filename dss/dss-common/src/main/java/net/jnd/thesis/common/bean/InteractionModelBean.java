/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.enums.Pattern;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class InteractionModelBean extends BaseBean implements
		DSSInteractionModel {

	private Long id;

	private DSSSessionCondition dssSessionCondition;

	private Pattern pattern;

	private Long eventDelay;

	private String description;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the dssSessionCondition
	 */
	public DSSSessionCondition getDssSessionCondition() {
		return dssSessionCondition;
	}

	/**
	 * @param dssSessionCondition
	 *            the dssSessionCondition to set
	 */
	public void setDssSessionCondition(DSSSessionCondition dssSessionCondition) {
		this.dssSessionCondition = dssSessionCondition;
	}

	/**
	 * @return the eventDelay
	 */
	public Long getEventDelay() {
		return eventDelay;
	}

	/**
	 * @param eventDelay
	 *            the eventDelay to set
	 */
	public void setEventDelay(Long eventDelay) {
		this.eventDelay = eventDelay;
	}

	/**
	 * @return the pattern
	 */
	public Pattern getPattern() {
		return pattern;
	}

	/**
	 * @param pattern
	 *            the pattern to set
	 */
	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
