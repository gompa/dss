/**
 * 
 */
package net.jnd.thesis.common.bean;

/**
 * @author João Domingos
 * @since Dec 9, 2012
 */
public class IntegerBean extends BaseBean {

	private Integer value;

	/**
	 * 
	 */
	public IntegerBean() {

	}

	/**
	 * @param value
	 */
	public IntegerBean(Integer value) {
		super();
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@Override
	public Object asParam() {
		return value;
	}

	/**
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Integer value) {
		this.value = value;
	}

}
