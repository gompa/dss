/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import java.util.Date;

import net.jnd.thesis.common.enums.EventType;

/**
 * @author João Domingos
 * @since Nov 5, 2012
 */
public interface DSSEvent extends DSSBaseBean {

	String getValueRaw();

	String getValue();

	Float getFloatValue();

	Long getDataSourceId();

	Long getSessionId();

	Date getDate();

	EventType getType();
	
	String getName();

}
