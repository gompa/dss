/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class DynamicReconfigurationBean extends BaseBean implements
		DSSDynamicReconfiguration {

	private String description;

	private String eventName;

	private DSSSessionCondition dssSessionCondition;

	private DSSInteractionModel dssInteractionModel;

	private DSSEsperExpression dssEsperExpression;

	private boolean eventNameEnabled = false;

	private boolean interactionModelEnabled = false;

	private boolean esperExpressionEnabled = false;

	private boolean sessionConditionEnabled = false;

	/**
	 * @return the dssSessionCondition
	 */
	public DSSSessionCondition getDssSessionCondition() {
		return dssSessionCondition;
	}

	/**
	 * @param dssSessionCondition
	 *            the dssSessionCondition to set
	 */
	public void setDssSessionCondition(DSSSessionCondition dssSessionCondition) {
		this.dssSessionCondition = dssSessionCondition;
	}

	/**
	 * @return the dssInteractionModel
	 */
	public DSSInteractionModel getDssInteractionModel() {
		return dssInteractionModel;
	}

	/**
	 * @param dssInteractionModel
	 *            the dssInteractionModel to set
	 */
	public void setDssInteractionModel(DSSInteractionModel dssInteractionModel) {
		this.dssInteractionModel = dssInteractionModel;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName
	 *            the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the dssEsperExpression
	 */
	public DSSEsperExpression getDssEsperExpression() {
		return dssEsperExpression;
	}

	/**
	 * @param dssEsperExpression
	 *            the dssEsperExpression to set
	 */
	public void setDssEsperExpression(DSSEsperExpression dssEsperExpression) {
		this.dssEsperExpression = dssEsperExpression;
	}

	/**
	 * @return the eventNameEnabled
	 */
	public boolean isEventNameEnabled() {
		return eventNameEnabled;
	}

	/**
	 * @param eventNameEnabled
	 *            the eventNameEnabled to set
	 */
	public void setEventNameEnabled(boolean eventNameEnabled) {
		this.eventNameEnabled = eventNameEnabled;
	}

	/**
	 * @return the interactionModelEnabled
	 */
	public boolean isInteractionModelEnabled() {
		return interactionModelEnabled;
	}

	/**
	 * @return the esperExpressionEnabled
	 */
	public boolean isEsperExpressionEnabled() {
		return esperExpressionEnabled;
	}

	/**
	 * @param esperExpressionEnabled
	 *            the esperExpressionEnabled to set
	 */
	public void setEsperExpressionEnabled(boolean esperExpressionEnabled) {
		this.esperExpressionEnabled = esperExpressionEnabled;
	}

	/**
	 * @return the eventNameEnabled
	 */
	public boolean getEventNameEnabled() {
		return eventNameEnabled;
	}

	/**
	 * @return the interactionModelEnabled
	 */
	public boolean getInteractionModelEnabled() {
		return interactionModelEnabled;
	}

	/**
	 * @param interactionModelEnabled
	 *            the interactionModelEnabled to set
	 */
	public void setInteractionModelEnabled(boolean interactionModelEnabled) {
		this.interactionModelEnabled = interactionModelEnabled;
	}

	/**
	 * @return the esperExpressionEnabled
	 */
	public boolean getEsperExpressionEnabled() {
		return esperExpressionEnabled;
	}

	/**
	 * @return the sessionConditionEnabled
	 */
	public boolean isSessionConditionEnabled() {
		return sessionConditionEnabled;
	}

	/**
	 * @param sessionConditionEnabled
	 *            the sessionConditionEnabled to set
	 */
	public void setSessionConditionEnabled(boolean sessionConditionEnabled) {
		this.sessionConditionEnabled = sessionConditionEnabled;
	}

}
