/**
 * 
 */
package net.jnd.thesis.common.bean;

/**
 * @author João Domingos
 * @since Dec 9, 2012
 */
public class LongBean extends BaseBean {

	private Long value;

	/**
	 * 
	 */
	public LongBean() {

	}

	/**
	 * @param value
	 */
	public LongBean(Long value) {
		super();
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@Override
	public Object asParam() {
		return value;
	}

	/**
	 * @return the value
	 */
	public Long getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Long value) {
		this.value = value;
	}

}
