/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.enums.EsperExpressionType;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;

/**
 * @author João Domingos
 * @since Jan 20, 2013
 */
public class EsperExpressionBean extends BaseBean implements DSSEsperExpression {

	private EsperExpressionType esperExpressionType;

	private String expressionQuery;

	private String description;

	/**
	 * @return the esperExpressionType
	 */
	public EsperExpressionType getEsperExpressionType() {
		return esperExpressionType;
	}

	/**
	 * @param esperExpressionType
	 *            the esperExpressionType to set
	 */
	public void setEsperExpressionType(EsperExpressionType esperExpressionType) {
		this.esperExpressionType = esperExpressionType;
	}

	/**
	 * @return the expressionQuery
	 */
	public String getExpressionQuery() {
		return expressionQuery;
	}

	/**
	 * @param expressionQuery
	 *            the expressionQuery to set
	 */
	public void setExpressionQuery(String expressionQuery) {
		this.expressionQuery = expressionQuery;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
