/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.enums.ConditionOperator;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class SessionConditionBean extends BaseBean implements
		DSSSessionCondition {

	private DSSDataSource dssDataSource;

	private ConditionOperator conditionOperator;

	private String conditionValue;

	private String description;

	/**
	 * @return the dssDataSource
	 */
	public DSSDataSource getDssDataSource() {
		return dssDataSource;
	}

	/**
	 * @param dssDataSource
	 *            the dssDataSource to set
	 */
	public void setDssDataSource(DSSDataSource dssDataSource) {
		this.dssDataSource = dssDataSource;
	}

	/**
	 * @return the conditionOperator
	 */
	public ConditionOperator getConditionOperator() {
		return conditionOperator;
	}

	/**
	 * @param conditionOperator
	 *            the conditionOperator to set
	 */
	public void setConditionOperator(ConditionOperator conditionOperator) {
		this.conditionOperator = conditionOperator;
	}

	/**
	 * @return the conditionValue
	 */
	public String getConditionValue() {
		return conditionValue;
	}

	/**
	 * @param conditionValue
	 *            the conditionValue to set
	 */
	public void setConditionValue(String conditionValue) {
		this.conditionValue = conditionValue;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
