/**
 * 
 */
package net.jnd.thesis.common.bean;

import java.util.Collection;

import net.jnd.thesis.common.enums.SessionStatus;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;

/**
 * @author João Domingos
 * @since Dec 4, 2012
 */
public class SessionBean extends BaseBean implements DSSSession {

	private String name;

	private Collection<DSSDataSource> dataSources;

	private Collection<DSSUser> users;

	private DSSInteractionModel sessionInteractionModel;

	private DSSEsperExpression sessionEsperExpression;

	private Collection<DSSDynamicReconfiguration> dynamicReconfs;

	private SessionStatus status;

	private Boolean clientModeAllowed = true;

	private Boolean repositoryEnabled = true;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the users
	 */
	public Collection<DSSUser> getUsers() {
		return users;
	}

	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(Collection<DSSUser> users) {
		this.users = users;
	}

	/**
	 * @return the sessionInteractionModel
	 */
	public DSSInteractionModel getSessionInteractionModel() {
		return sessionInteractionModel;
	}

	/**
	 * @param sessionInteractionModel
	 *            the sessionInteractionModel to set
	 */
	public void setSessionInteractionModel(
			DSSInteractionModel sessionInteractionModel) {
		this.sessionInteractionModel = sessionInteractionModel;
	}

	/**
	 * @return the dynamicReconfs
	 */
	public Collection<DSSDynamicReconfiguration> getDynamicReconfs() {
		return dynamicReconfs;
	}

	/**
	 * @param dynamicReconfs
	 *            the dynamicReconfs to set
	 */
	public void setDynamicReconfs(
			Collection<DSSDynamicReconfiguration> dynamicReconfs) {
		this.dynamicReconfs = dynamicReconfs;
	}

	/**
	 * @return the owner
	 */
	public DSSUser getOwner() {
		return getCreationUser();
	}

	/**
	 * @return the dataSources
	 */
	public Collection<DSSDataSource> getDataSources() {
		return dataSources;
	}

	/**
	 * @param dataSources
	 *            the dataSources to set
	 */
	public void setDataSources(Collection<DSSDataSource> dataSources) {
		this.dataSources = dataSources;
	}

	@Override
	public String toString() {
		return String.format("[%s] %s", getClass().getSimpleName(), getName());
	}

	/**
	 * @return the clientModeAllowed
	 */
	public boolean isClientModeAllowed() {
		return clientModeAllowed;
	}

	/**
	 * @param clientModeAllowed
	 *            the clientModeAllowed to set
	 */
	public void setClientModeAllowed(boolean clientModeAllowed) {
		this.clientModeAllowed = clientModeAllowed;
	}

	/**
	 * @return the repositoryEnabled
	 */
	public boolean isRepositoryEnabled() {
		return repositoryEnabled;
	}

	/**
	 * @param repositoryEnabled
	 *            the repositoryEnabled to set
	 */
	public void setRepositoryEnabled(boolean repositoryEnabled) {
		this.repositoryEnabled = repositoryEnabled;
	}

	/**
	 * @return the status
	 */
	public SessionStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(SessionStatus status) {
		this.status = status;
	}

	/**
	 * @return the sessionEsperExpression
	 */
	public DSSEsperExpression getSessionEsperExpression() {
		return sessionEsperExpression;
	}

	/**
	 * @param sessionEsperExpression
	 *            the sessionEsperExpression to set
	 */
	public void setSessionEsperExpression(
			DSSEsperExpression sessionEsperExpression) {
		this.sessionEsperExpression = sessionEsperExpression;
	}

}
