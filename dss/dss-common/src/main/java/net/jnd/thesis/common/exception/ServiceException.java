package net.jnd.thesis.common.exception;

public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6785326370362862161L;

	/**
	 * @param message
	 * @param e
	 */
	public ServiceException(String message, Throwable e) {
		super(message, e);
	}

}
