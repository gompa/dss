/**
 * 
 */
package net.jnd.thesis.common.bean;

/**
 * @author João Domingos
 * @since Dec 9, 2012
 */
public class StringBean extends BaseBean {

	private String message;

	/**
	 * 
	 */
	public StringBean() {

	}

	/**
	 * 
	 */
	public StringBean(String message) {
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return String.valueOf(message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.jnd.thesis.common.bean.BaseBean#asParam()
	 */
	@Override
	public Object asParam() {
		return getMessage();
	}

}
