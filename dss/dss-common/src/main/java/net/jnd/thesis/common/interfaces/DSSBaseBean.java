/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import java.util.Date;

/**
 * @author João Domingos
 * @since Jan 12, 2013
 */
public interface DSSBaseBean {

	Long getId();

	DSSUser getCreationUser();

	Date getCreationDate();

	DSSUser getUpdateUser();

	Date getUpdateDate();

}
