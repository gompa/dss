/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
public enum RequestParams {
	REQUEST_DATA_SOURCE, WEBSOCKET_LOCATION, START_CONSOLE_ACTION, STOP_CONSOLE_ACTION, SESSION_ID, USER_SESSION_CONNECTION_ID, USER_MESSAGE;
}
