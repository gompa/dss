/**
 * 
 */
package net.jnd.thesis.common.bean;

import java.util.Collection;

import net.jnd.thesis.common.enums.ConnectionStatus;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class UserSessionConnectionBean extends BaseBean implements
		DSSUserSessionConnection {

	private String description;

	private DSSSession connectionSession;

	private DSSUser connectionUser;

	private ConnectionStatus connectionStatus;

	private DSSInteractionModel connectionInteractionModel;

	private DSSEsperExpression connectionEsperExpression;

	private Collection<DSSDynamicReconfiguration> dynamicReconfs;

	/**
	 * @return the connectionSession
	 */
	public DSSSession getConnectionSession() {
		return connectionSession;
	}

	/**
	 * @param connectionSession
	 *            the connectionSession to set
	 */
	public void setConnectionSession(DSSSession connectionSession) {
		this.connectionSession = connectionSession;
	}

	/**
	 * @return the connectionUser
	 */
	public DSSUser getConnectionUser() {
		return connectionUser;
	}

	/**
	 * @param connectionUser
	 *            the connectionUser to set
	 */
	public void setConnectionUser(DSSUser connectionUser) {
		this.connectionUser = connectionUser;
	}

	/**
	 * @return the connectionStatus
	 */
	public ConnectionStatus getConnectionStatus() {
		return connectionStatus;
	}

	/**
	 * @param connectionStatus
	 *            the connectionStatus to set
	 */
	public void setConnectionStatus(ConnectionStatus connectionStatus) {
		this.connectionStatus = connectionStatus;
	}

	/**
	 * @return the connectionInteractionModel
	 */
	public DSSInteractionModel getConnectionInteractionModel() {
		return connectionInteractionModel;
	}

	/**
	 * @param connectionInteractionModel
	 *            the connectionInteractionModel to set
	 */
	public void setConnectionInteractionModel(
			DSSInteractionModel connectionInteractionModel) {
		this.connectionInteractionModel = connectionInteractionModel;
	}

	/**
	 * @return the dynamicReconfs
	 */
	public Collection<DSSDynamicReconfiguration> getDynamicReconfs() {
		return dynamicReconfs;
	}

	/**
	 * @param dynamicReconfs
	 *            the dynamicReconfs to set
	 */
	public void setDynamicReconfs(
			Collection<DSSDynamicReconfiguration> dynamicReconfs) {
		this.dynamicReconfs = dynamicReconfs;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the connectionEsperExpression
	 */
	public DSSEsperExpression getConnectionEsperExpression() {
		return connectionEsperExpression;
	}

	/**
	 * @param connectionEsperExpression
	 *            the connectionEsperExpression to set
	 */
	public void setConnectionEsperExpression(
			DSSEsperExpression connectionEsperExpression) {
		this.connectionEsperExpression = connectionEsperExpression;
	}

}
