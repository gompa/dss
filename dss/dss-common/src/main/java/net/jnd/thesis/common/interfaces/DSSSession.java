/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import java.util.Collection;

import net.jnd.thesis.common.enums.SessionStatus;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public interface DSSSession extends DSSBaseBean {

	/**
	 * @return
	 */
	String getName();

	/**
	 * @return
	 */
	Collection<DSSDataSource> getDataSources();

	/**
	 * @return
	 */
	Collection<DSSUser> getUsers();

	/**
	 * @return
	 */
	DSSInteractionModel getSessionInteractionModel();

	/**
	 * @return
	 */
	DSSEsperExpression getSessionEsperExpression();
	
	/**
	 * @return
	 */
	Collection<DSSDynamicReconfiguration> getDynamicReconfs();

	/**
	 * @return
	 */
	DSSUser getOwner();

	/**
	 * @return
	 */
	SessionStatus getStatus();

	/**
	 * @return
	 */
	boolean isClientModeAllowed();

	/**
	 * @return
	 */
	boolean isRepositoryEnabled();

}
