/**
 * 
 */
package net.jnd.thesis.common.interfaces;

/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public interface DSSUser extends DSSBaseBean {

	/**
	 * @return
	 */
	String getUsername();

	/**
	 * @return
	 */
	String getName();

	/**
	 * @return
	 */
	boolean isEnabled();

	/**
	 * @return
	 */
	String getPassword();

	boolean isAdmin();
	
}
