/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import net.jnd.thesis.common.enums.Pattern;

/**
 * @author João Domingos
 * @since Nov 18, 2012 
 */
public interface DSSInteractionModel extends DSSBaseBean {

	Pattern getPattern();

	Long getEventDelay();

	DSSSessionCondition getDssSessionCondition();

	String getDescription();
	
}
