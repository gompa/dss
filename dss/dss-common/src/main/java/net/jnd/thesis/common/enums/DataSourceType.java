/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
public enum DataSourceType {
	HTTP, RSS, TWITTER, XMPP;
}
