/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Nov 3, 2012
 */
public enum ConnectionStatus {
	OPEN, CLOSED;
}
