/**
 * 
 */
package net.jnd.thesis.common.interfaces;


/**
 * @author João Domingos
 * @since Jun 12, 2012
 */
public interface DSSReplaySession extends DSSBaseBean {

	Long getSessionId();
	
	Long getReplayDelay();
	
}
