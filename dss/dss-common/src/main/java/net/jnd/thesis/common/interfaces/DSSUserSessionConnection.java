/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import java.util.Collection;

import net.jnd.thesis.common.enums.ConnectionStatus;

/**
 * @author João Domingos
 * @since Nov 3, 2012
 */
public interface DSSUserSessionConnection extends DSSBaseBean {
	
	String getDescription();

	DSSSession getConnectionSession();

	DSSUser getConnectionUser();

	ConnectionStatus getConnectionStatus();

	void setConnectionStatus(ConnectionStatus status);

	DSSInteractionModel getConnectionInteractionModel();

	Collection<DSSDynamicReconfiguration> getDynamicReconfs();

	DSSEsperExpression getConnectionEsperExpression();

}
