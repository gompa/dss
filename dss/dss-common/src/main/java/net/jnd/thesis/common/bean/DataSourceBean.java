/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.enums.DataSourceType;
import net.jnd.thesis.common.enums.DataSourceValueType;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSUser;

/**
 * @author João Domingos
 * @since Dec 4, 2012
 */
public class DataSourceBean extends BaseBean implements DSSDataSource {

	private Boolean enabled = true;

	private String name;

	private String regex;

	private DataSourceType type;

	private DataSourceValueType dataSourceValueType;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the regex
	 */
	public String getRegex() {
		return regex;
	}

	/**
	 * @param regex
	 *            the regex to set
	 */
	public void setRegex(String regex) {
		this.regex = regex;
	}

	/**
	 * @return the owner
	 */
	public DSSUser getOwner() {
		return getCreationUser();
	}

	@Override
	public String toString() {
		return String.format("[%s] %s", getType(), getName());
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the type
	 */
	public DataSourceType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(DataSourceType type) {
		this.type = type;
	}

	/**
	 * @return the dataSourceValueType
	 */
	public DataSourceValueType getDataSourceValueType() {
		return dataSourceValueType;
	}

	/**
	 * @param dataSourceValueType
	 *            the dataSourceValueType to set
	 */
	public void setDataSourceValueType(DataSourceValueType dataSourceValueType) {
		this.dataSourceValueType = dataSourceValueType;
	}

}
