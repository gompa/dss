package net.jnd.thesis.common.interfaces;

import net.jnd.thesis.common.enums.HttpQueryType;

/**
 * @author João Domingos
 * @since Nov 1, 2012
 */
public interface DSSDataSourcePollingHttp extends DSSDataSourcePolling {

	String getUrl();

	String getQuery();

	HttpQueryType getHttpQueryType();

}
