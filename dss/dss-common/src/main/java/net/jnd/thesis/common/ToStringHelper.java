/**
 * 
 */
package net.jnd.thesis.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.jnd.thesis.common.interfaces.DSSEvent;

/**
 * @author João Domingos
 * @since Apr 13, 2013
 */
public class ToStringHelper {

	/**
	 * @param evt
	 * @return
	 */
	public static String build(DSSEvent evt) {
		StringBuilder sb = new StringBuilder();
		Date dt = evt.getDate() != null ? evt.getDate() : new Date();
		String dtStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dt);
		sb.append("[").append(dtStr).append("]");
		//
		if (evt.getName() != null) {
			sb.append("[").append(evt.getName()).append("]");
		}
		//
		if (evt.getValueRaw() != null) {
			sb.append("[").append(evt.getValueRaw()).append("]");
		}
		sb.append(" ").append(evt.getValue());
		return sb.toString();
	}

}
