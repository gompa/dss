/**
 * 
 */
package net.jnd.thesis.common.bean;

/**
 * @author João Domingos
 * @since Dec 9, 2012
 */
public class BooleanBean extends BaseBean {

	private Boolean response;

	/**
	 * 
	 */
	public BooleanBean() {

	}

	/**
	 * @param response
	 */
	public BooleanBean(boolean response) {
		this.response = response;
	}

	/**
	 * @return the response
	 */
	public Boolean getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the response to set
	 */
	public void setResponse(Boolean response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return String.valueOf(response);
	}

	@Override
	public Object asParam() {
		return getResponse();
	}

}
