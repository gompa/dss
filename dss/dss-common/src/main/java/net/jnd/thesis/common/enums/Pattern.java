/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Nov 12, 2012
 */
public enum Pattern {
	STREAMING, PUBLISHER_SUBSCRIBER, PRODUCER_CONSUMER, CLIENT_SERVER;

}
