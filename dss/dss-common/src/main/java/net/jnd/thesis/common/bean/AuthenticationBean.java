/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.interfaces.DSSAuthenticationBean;

/**
 * @author João Domingos
 * @since Dec 4, 2012
 */
public class AuthenticationBean extends BaseBean implements
		DSSAuthenticationBean {

	private String username;

	private String password;

	/**
	 * 
	 */
	public AuthenticationBean() {

	}

	/**
	 * @param username
	 * @param password
	 */
	public AuthenticationBean(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
