package net.jnd.thesis.common.interfaces;

/**
 * @author João Domingos
 * @since Nov 1, 2012
 */
public interface DSSDataSourceEventXmpp extends DSSDataSource {

	String getUsername();

	String getPassword();

	String getFromUser();

}
