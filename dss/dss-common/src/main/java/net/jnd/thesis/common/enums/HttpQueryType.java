/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
public enum HttpQueryType {
	CSS, XPATH;
}
