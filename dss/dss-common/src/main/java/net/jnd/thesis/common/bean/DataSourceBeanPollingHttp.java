/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.enums.HttpQueryType;
import net.jnd.thesis.common.interfaces.DSSDataSourcePollingHttp;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class DataSourceBeanPollingHttp extends DataSourceBeanPolling implements
		DSSDataSourcePollingHttp {

	private String url;

	private String query;
	
	private HttpQueryType httpQueryType;
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * @return the httpQueryType
	 */
	public HttpQueryType getHttpQueryType() {
		return httpQueryType;
	}

	/**
	 * @param httpQueryType the httpQueryType to set
	 */
	public void setHttpQueryType(HttpQueryType httpQueryType) {
		this.httpQueryType = httpQueryType;
	}
	
	
	
}
