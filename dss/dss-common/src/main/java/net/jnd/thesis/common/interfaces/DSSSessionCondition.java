/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import net.jnd.thesis.common.enums.ConditionOperator;

/**
 * @author João Domingos
 * @since Nov 18, 2012
 */
public interface DSSSessionCondition extends DSSBaseBean {

	DSSDataSource getDssDataSource();

	ConditionOperator getConditionOperator();

	String getConditionValue();
	
	String getDescription();

}
