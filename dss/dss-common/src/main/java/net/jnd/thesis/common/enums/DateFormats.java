/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Jul 1, 2012
 */
public enum DateFormats {
	DATE_TIME {
		@Override
		public String toString() {
			return "yyyy-MM-dd HH:mm:ss";
		}
	};
	
}
