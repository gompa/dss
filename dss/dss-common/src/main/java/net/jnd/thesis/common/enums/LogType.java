/**
 * 
 */
package net.jnd.thesis.common.enums;

/**
 * @author João Domingos
 * @since Jun 23, 2012
 */
public enum LogType {
	USER_CONNECT, USER_DISCONNECT, TEST_START, TEST_END, SESSION_START, SESSION_END;
}
