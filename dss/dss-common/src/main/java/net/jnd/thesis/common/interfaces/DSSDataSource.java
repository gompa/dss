/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import net.jnd.thesis.common.enums.DataSourceType;
import net.jnd.thesis.common.enums.DataSourceValueType;

/**
 * @author João Domingos
 * @since Jun 30, 2012
 */
public interface DSSDataSource extends DSSBaseBean {

	/**
	 * @return
	 */
	String getName();

	/**
	 * @return
	 */
	DataSourceType getType();

	/**
	 * @return
	 */
	DSSUser getOwner();

	/**
	 * @return
	 */
	String getRegex();

	/**
	 * @return
	 */
	DataSourceValueType getDataSourceValueType();

	/**
	 * @return
	 */
	boolean isEnabled();

}
