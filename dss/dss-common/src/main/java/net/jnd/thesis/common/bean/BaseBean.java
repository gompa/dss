/**
 * 
 */
package net.jnd.thesis.common.bean;

import java.util.Date;

import net.jnd.thesis.common.interfaces.DSSBaseBean;
import net.jnd.thesis.common.interfaces.DSSUser;

/**
 * @author João Domingos
 * @since Dec 9, 2012
 */
public class BaseBean implements DSSBaseBean {

	private Long id;

	private DSSUser creationUser;

	private Date creationDate;

	private DSSUser updateUser;

	private Date updateDate;

	/**
	 * @return the creationUser
	 */
	public DSSUser getCreationUser() {
		return creationUser;
	}

	/**
	 * @param creationUser
	 *            the creationUser to set
	 */
	public void setCreationUser(DSSUser creationUser) {
		this.creationUser = creationUser;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the updateUser
	 */
	public DSSUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser
	 *            the updateUser to set
	 */
	public void setUpdateUser(DSSUser updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate
	 *            the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("[%s] %s", getClass().getSimpleName(), getId());
	}

	public Object asParam() {
		return this;
	}

}
