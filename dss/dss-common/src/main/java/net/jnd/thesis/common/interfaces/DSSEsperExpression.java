/**
 * 
 */
package net.jnd.thesis.common.interfaces;

import net.jnd.thesis.common.enums.EsperExpressionType;

/**
 * @author João Domingos
 * @since Nov 18, 2012
 */
public interface DSSEsperExpression extends DSSBaseBean {

	EsperExpressionType getEsperExpressionType();

	String getExpressionQuery();

	String getDescription();

}
