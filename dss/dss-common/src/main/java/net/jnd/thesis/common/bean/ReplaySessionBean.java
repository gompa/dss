/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.interfaces.DSSReplaySession;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class ReplaySessionBean extends BaseBean implements DSSReplaySession {

	private Long sessionId;

	private Long replayDelay;

	/**
	 * @return the sessionId
	 */
	public Long getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId
	 *            the sessionId to set
	 */
	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the replayDelay
	 */
	public Long getReplayDelay() {
		return replayDelay;
	}

	/**
	 * @param replayDelay
	 *            the replayDelay to set
	 */
	public void setReplayDelay(Long replayDelay) {
		this.replayDelay = replayDelay;
	}

}
