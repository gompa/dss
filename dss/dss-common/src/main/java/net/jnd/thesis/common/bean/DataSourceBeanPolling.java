/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.interfaces.DSSDataSourcePolling;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class DataSourceBeanPolling extends DataSourceBean implements DSSDataSourcePolling {

	private Long interval;

	/**
	 * @return the interval
	 */
	public Long getInterval() {
		return interval;
	}

	/**
	 * @param interval the interval to set
	 */
	public void setInterval(Long interval) {
		this.interval = interval;
	}

}
