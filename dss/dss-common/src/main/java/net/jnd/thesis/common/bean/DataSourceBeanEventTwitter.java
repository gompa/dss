/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.interfaces.DSSDataSourceEventTwitter;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class DataSourceBeanEventTwitter extends DataSourceBeanEvent implements
		DSSDataSourceEventTwitter {

	private String screenNames;

	/**
	 * @return the screenNames
	 */
	public String getScreenNames() {
		return screenNames;
	}

	/**
	 * @param screenNames
	 *            the screenNames to set
	 */
	public void setScreenNames(String screenNames) {
		this.screenNames = screenNames;
	}

}
