/**
 * 
 */
package net.jnd.thesis.common.bean;

import net.jnd.thesis.common.interfaces.DSSDataSourcePollingRss;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public class DataSourceBeanPollingRss extends DataSourceBeanPolling implements
		DSSDataSourcePollingRss {

	private String url;

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
