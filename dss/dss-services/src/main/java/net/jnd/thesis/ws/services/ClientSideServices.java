/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.util.Collection;

import net.jnd.thesis.common.bean.AuthenticationBean;
import net.jnd.thesis.common.bean.BaseBean;
import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.bean.LongBean;
import net.jnd.thesis.common.bean.StringBean;
import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSEvent;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSReplaySession;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author João Domingos
 * @since Dec 11, 2012
 */
public class ClientSideServices implements WebSocketServices {

	@SuppressWarnings("unused")
	private static final Log LOG = LogFactory.getLog(ClientSideServices.class);

	private boolean authenticated = false;

	private SessionEventHandler eventHandler = new DefaultSessionEventHandler();

	/**
	 * @param eventHandler
	 */
	void setEventHandler(SessionEventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}

	@Override
	public void authenticate(AuthenticationBean auth) throws WSException {
		ClientWebSocketContext.invokeService("authenticate", false, auth);
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"isAuthenticated", false);
		authenticated = WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public String echo(String msg) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService("echo",
				false, new StringBean(msg));
		return WebSocketMessageHelper.getPayloadString(res);
	}

	@Override
	public Collection<DSSUser> listUsers() throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listUsers", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Collection<DSSDataSource> listDataSources() throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listDataSources", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Collection<DSSSession> listSessions() throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listSessions", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Collection<DSSInteractionModel> listInteractionModels()
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listInteractionModels", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Collection<DSSDynamicReconfiguration> listDynamicReconfigurations()
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listDynamicReconfigurations", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Collection<DSSSessionCondition> listSessionConditions()
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listSessionConditions", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Collection<DSSUserSessionConnection> listUserSessionConnections()
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listUserSessionConnections", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Boolean stopSession(Long sessionId) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"stopSession", true, new LongBean(sessionId));
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Boolean startSession(Long sessionId) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"startSession", true, new LongBean(sessionId));
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Collection<DSSSession> listOwnedSessions() throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listOwnedSessions", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public DSSSession getSession(Long sessionId) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"getSession", true, new LongBean(sessionId));
		return WebSocketMessageHelper.getSinglePayloadBean(res);
	}

	@Override
	public Collection<DSSEsperExpression> listEsperExpressions()
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"listEsperExpressions", true);
		return WebSocketMessageHelper.getPayloadCollection(res);
	}

	@Override
	public Long createUser(DSSUser bean) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createUser", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateUser(DSSUser bean) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateUser", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Long createDataSource(DSSDataSource bean) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createDataSource", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateDataSource(DSSDataSource bean) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateDataSource", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Long createDynamicReconfiguration(DSSDynamicReconfiguration bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createDynamicReconfiguration", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateDynamicReconfiguration(DSSDynamicReconfiguration bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateDynamicReconfiguration", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Long createEsperExpression(DSSEsperExpression bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createEsperExpression", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateEsperExpression(DSSEsperExpression bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateEsperExpression", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Long createInteractionModel(DSSInteractionModel bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createInteractionModel", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateInteractionModel(DSSInteractionModel bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateInteractionModel", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Long createSessionCondition(DSSSessionCondition bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createSessionCondition", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateSessionCondition(DSSSessionCondition bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateSessionCondition", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Long createUserSessionConnection(DSSUserSessionConnection bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createUserSessionConnection", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateUserSessionConnection(DSSSession bean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateUserSessionConnection", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Long createSession(DSSSession bean) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"createSession", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadLong(res);
	}

	@Override
	public Boolean updateSession(DSSSession bean) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"updateSession", true, (BaseBean) bean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public DSSUserSessionConnection startTestDataSourceSession(
			Long dataSourceId, Long interactionModelId) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"startTestDataSourceSession", true, new LongBean(dataSourceId),
				new LongBean(interactionModelId));
		return WebSocketMessageHelper.getSinglePayloadBean(res);
	}

	@Override
	public void broadcastEvent(DSSEvent evt) throws WSException {
		eventHandler.handleEvent((EventBean) evt);
	}

	@Override
	public Boolean connectToSession(DSSUserSessionConnection userConnection)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"connectToSession", true, (BaseBean) userConnection);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Boolean disconnectFromSession(Long connectionId) throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"disconnectFromSession", true, new LongBean(connectionId));
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public Boolean replaySession(DSSReplaySession replayBean)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"replaySession", true, (BaseBean) replayBean);
		return WebSocketMessageHelper.getPayloadBoolean(res);
	}

	@Override
	public DSSUserSessionConnection getConnection(Long connectionId)
			throws WSException {
		WebSocketMessage res = ClientWebSocketContext.invokeService(
				"getConnection", true, new LongBean(connectionId));
		return WebSocketMessageHelper.getSinglePayloadBean(res);
	}

}
