/**
 * 
 */
package net.jnd.thesis.ws.services.exception;

/**
 * @author João Domingos
 * @since Jan 12, 2013
 */
public class InvalidPayloadException extends WSException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8035858094696089439L;

}
