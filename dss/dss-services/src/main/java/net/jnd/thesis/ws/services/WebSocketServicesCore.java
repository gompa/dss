/**
 * 
 */
package net.jnd.thesis.ws.services;

import net.jnd.thesis.common.bean.AuthenticationBean;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public interface WebSocketServicesCore {

	/**
	 * @param auth
	 * @return
	 */
	void authenticate(AuthenticationBean auth) throws WSException;

	/**
	 * @return
	 */
	boolean isAuthenticated();

	/**
	 * @param msg
	 * @return
	 */
	String echo(String msg) throws WSException;

}
