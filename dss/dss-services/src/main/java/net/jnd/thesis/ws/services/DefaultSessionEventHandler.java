/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.util.Date;

import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.enums.EventType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author João Domingos
 * @since Jan 27, 2013
 */
public class DefaultSessionEventHandler implements SessionEventHandler {

	private static final Log LOG = LogFactory
			.getLog(DefaultSessionEventHandler.class);

	@Override
	public void handleEvent(EventBean evt) {
		LOG.info(buildEventString(evt));
	}

	/**
	 * @param evt
	 */
	protected String buildEventString(EventBean evt) {
		Date dt = evt.getDate() != null ? evt.getDate() : evt.getCreationDate();
		Long dataSourceId = evt.getDataSourceId();
		String val = evt.getFloatValue() != null ? String.valueOf(evt
				.getFloatValue()) : evt.getValue();
		Long sessionId = evt.getSessionId();
		EventType eventType = evt.getType();
		String mask = "[EVENT][%s][%s][%s][%s] %s";
		return String.format(mask, dt, eventType, sessionId, dataSourceId, val);
	}

}
