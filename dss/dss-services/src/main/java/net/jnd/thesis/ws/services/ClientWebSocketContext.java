/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.lang.reflect.Method;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import net.jnd.thesis.common.bean.BaseBean;
import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.bean.WSConnectionBean;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocket.Connection;
import org.eclipse.jetty.websocket.WebSocketClient;
import org.eclipse.jetty.websocket.WebSocketClientFactory;

/**
 * @author João Domingos
 * @since Dec 3, 2012
 */
public enum ClientWebSocketContext {

	INSTANCE;

	private static final Log LOG = LogFactory
			.getLog(ClientWebSocketContext.class);

	private static final long START_CONNECTION_TIMEOUT = 300;

	private static final int MAX_TEXT_MESSAGE_SIZE = Integer.MAX_VALUE;

	private static final int MAX_BIN_MESSAGE_SIZE = Integer.MAX_VALUE;

	public static final String WS_ADDRESS = "ws://localhost:8080/dss-server/ws/client";

	private AtomicBoolean initialized = new AtomicBoolean(false);

	private ClientSideWebSocket clientWebSocket;

	private WebSocket.Connection connection;

	private ClientSideServices services;

	private Set<String> serviceNames = new HashSet<String>();

	/**
	 * @param eventHandler
	 */
	public static void setEventHandler(SessionEventHandler eventHandler) {
		INSTANCE.services.setEventHandler(eventHandler);
	}

	/**
	 * @return
	 */
	public static WebSocketServices getServices() {
		return INSTANCE.services;
	}

	/**
	 * @return
	 */
	public static Set<String> getServiceNames() {
		return INSTANCE.serviceNames;
	}

	/**
	 * 
	 */
	public static void disconnect() {
		if (isInitialized()) {
			if(INSTANCE.connection.isOpen()) {
				try {
					INSTANCE.connection.close();
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			}
			INSTANCE.initialized.set(false);
		}
	}

	/**
	 * 
	 */
	private static void setupLoggers() {
		Logger rootLogger = Logger.getRootLogger();
		rootLogger.removeAllAppenders();
		ConsoleAppender console = new ConsoleAppender(); // create appender
		// configure the appender
		String PATTERN = "%d [%p|%c|%C{1}] %m%n";
		console.setLayout(new PatternLayout(PATTERN));
		console.setThreshold(Level.DEBUG);
		console.activateOptions();
		// add appender to any Logger (here is root)
		rootLogger.addAppender(console);
	}

	/**
	 * @param auth
	 * @param wsConn
	 * @return
	 */
	public static boolean connect(WSConnectionBean wsConn) {
		if (!isInitialized()) {
			setupLoggers();
			// build url
			String url = wsConn.getUrl();
			if (StringUtils.isBlank(url)) {
				url = WS_ADDRESS;
			}
			// build timeout
			Long timeout = wsConn.getTimeout();
			if (timeout == null) {
				timeout = START_CONNECTION_TIMEOUT;
			}
			// setup connection
			try {
				WebSocketClientFactory factory = new WebSocketClientFactory();
				factory.start();
				WebSocketClient client = factory.newWebSocketClient();
				URI uri = new URI(url);
				
				INSTANCE.clientWebSocket= new ClientSideWebSocket();

				Future<Connection> future = client.open(uri, INSTANCE.clientWebSocket);
				Connection conn = future.get(timeout, TimeUnit.SECONDS);
				
				
				// set max binary message size
				if (wsConn.getMaxBinaryMessageSize() != null) {
					conn.setMaxBinaryMessageSize(wsConn
							.getMaxBinaryMessageSize());
				} else {
					conn.setMaxBinaryMessageSize(MAX_BIN_MESSAGE_SIZE);
				}
				// set max text message size
				if (wsConn.getMaxTextMessageSize() != null) {
					conn.setMaxTextMessageSize(wsConn.getMaxTextMessageSize());
				} else {
					conn.setMaxTextMessageSize(MAX_TEXT_MESSAGE_SIZE);
				}
				// set max idle time
				if (wsConn.getMaxIdleTime() != null) {
					conn.setMaxIdleTime(wsConn.getMaxIdleTime());
				}
				INSTANCE.connection = conn;
				// initialize services
				// INSTANCE.services =
				// ServiceLoader.load(WebSocketServices.class)
				// .iterator().next();
				INSTANCE.services = new ClientSideServices();
				for (Method method : WebSocketServices.class
						.getMethods()) {
					INSTANCE.serviceNames.add(method.getName());
				}
				// set initialized flag
				INSTANCE.initialized.set(true);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return isInitialized();
	}

	/**
	 * @return
	 */
	static boolean isInitialized() {
		return INSTANCE.initialized.get();
	}

	/**
	 * @param serviceName
	 * @return
	 * @throws WSException
	 */
	static WebSocketMessage invokeService(String serviceName, boolean secure)
			throws WSException {
		return INSTANCE.clientWebSocket.invokeService(serviceName, secure,
				(BaseBean) null);
	}

	/**
	 * @param serviceName
	 * @param payload
	 * @return
	 * @throws WSException
	 */
	static WebSocketMessage invokeService(String serviceName, boolean secure,
			BaseBean... payload) throws WSException {
		return INSTANCE.clientWebSocket.invokeService(serviceName, secure,
				payload);
	}

	/**
	 * @param evt
	 * @throws WSException
	 */
	public static void broadcastEvent(EventBean evt) throws WSException {
		if (getServices() != null) {
			getServices().broadcastEvent(evt);
		}
	}
}
