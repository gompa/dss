/**
 * 
 */
package net.jnd.thesis.ws.services;

import net.jnd.thesis.common.interfaces.DSSEvent;
import net.jnd.thesis.common.interfaces.DSSReplaySession;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public interface WebSocketServicesSession extends WebSocketServicesUpdating {

	/**
	 * @param dataSourceId
	 * @param interactionModelId
	 * @return
	 * @throws WSException
	 */
	DSSUserSessionConnection startTestDataSourceSession(Long dataSourceId,
			Long interactionModelId) throws WSException;

	/**
	 * @param sessionId
	 * @return
	 * @throws WSException
	 */
	Boolean startSession(Long sessionId) throws WSException;

	/**
	 * @param sessionId
	 * @return
	 * @throws WSException
	 */
	Boolean stopSession(Long sessionId) throws WSException;

	/**
	 * @param evt
	 * @throws WSException
	 */
	void broadcastEvent(DSSEvent evt) throws WSException;

	/**
	 * @param userConnection
	 * @return
	 */
	Boolean connectToSession(DSSUserSessionConnection userConnection)
			throws WSException;

	/**
	 * @param connectionId
	 * @return
	 */
	Boolean disconnectFromSession(Long connectionId) throws WSException;

	/**
	 * @param replayBean
	 * @return
	 */
	Boolean replaySession(DSSReplaySession replayBean) throws WSException;

}
