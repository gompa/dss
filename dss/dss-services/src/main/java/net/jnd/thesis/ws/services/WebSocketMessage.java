/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import net.jnd.thesis.common.bean.BaseBean;
import net.jnd.thesis.common.bean.StringBean;

/**
 * @author João Domingos
 * @since Dec 4, 2012
 */
public class WebSocketMessage {

	private Long requestId;

	private Collection<BaseBean> payload = new ArrayList<BaseBean>();

	private String serviceName;

	private Collection<StringBean> errors = new ArrayList<StringBean>();

	/**
	 * 
	 */
	public WebSocketMessage() {

	}

	/**
	 * @param requestId
	 * @param serviceName
	 * @param payload
	 */
	public WebSocketMessage(Long requestId, String serviceName,
			BaseBean... payload) {
		Collection<BaseBean> payloadCollection = new ArrayList<BaseBean>();
		for (BaseBean b : payload) {
			if (b != null) {
				payloadCollection.add(b);
			}
		}
		this.requestId = requestId;
		this.serviceName = serviceName;
		setPayload(payloadCollection);
	}

	/**
	 * @param requestId
	 * @param serviceName
	 * @param payload
	 */
	public WebSocketMessage(Long requestId, String serviceName,
			Collection<BaseBean> payload) {
		this.requestId = requestId;
		this.serviceName = serviceName;
		setPayload(payload);
	}

	/**
	 * @param requestId
	 * @param serviceName
	 * @param payload
	 */
	public WebSocketMessage(Long requestId, String serviceName, Throwable e) {
		this.requestId = requestId;
		this.serviceName = serviceName;
		errors.add(new StringBean(String.valueOf(e)));
	}

	/**
	 * @return the requestId
	 */
	public Long getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId
	 *            the requestId to set
	 */
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	/**
	 * @return the payload
	 */
	public Collection<BaseBean> getPayload() {
		return payload;
	}

	/**
	 * @param payload
	 *            the payload to set
	 */
	public void setPayload(Collection<BaseBean> payload) {
		if (payload != null) {
			this.payload = payload;
		} else {
			this.payload.clear();
		}
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName
	 *            the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the errors
	 */
	public Collection<StringBean> getErrors() {
		return errors;
	}

	/**
	 * @param errors
	 *            the errors to set
	 */
	public void setErrors(Collection<StringBean> errors) {
		if (errors != null) {
			this.errors = errors;
		} else {
			this.errors.clear();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (payload != null) {
			Iterator<BaseBean> itr = payload.iterator();
			while (itr.hasNext())
				sb.append(itr.next());
			if (itr.hasNext()) {
				sb.append(";");
			}
		}
		return sb.toString();
	}

}
