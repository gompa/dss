/**
 * 
 */
package net.jnd.thesis.ws.services;

/**
 * @author João Domingos
 * @since Dec 4, 2012
 */
public enum WebSocketMessageType {

	AUTH, INFO, ECHO, LIST_DATASOURCES, LIST_SESSIONS, LIST_INTERACTION_MODELS, LIST_DYNAMIC_RECONFIGURATIONS, LIST_SESSION_CONDITIONS, USER_SESSION_CONNECTION, REPLAY_SESSION;

}
