/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.jnd.thesis.common.bean.BaseBean;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author João Domingos
 * @since Dec 6, 2012
 */
class MessageHelper {

	/**
	 * @param s
	 */
	private static void display(String s) {
		Date dt = Calendar.getInstance().getTime();
		String.format("[%s] %s", dt, s);
		System.out.println(s);
	}

	/**
	 * @param clazz
	 * @param params
	 */
	static void displayMessage(Class<?> clazz, String... params) {
		display(StringUtils.join(params, ", "));
	}

	/**
	 * @param msg
	 * @param params
	 */
	static void displayMessage(ClientSideWebSocket ws, String... params) {
		display(StringUtils.join(params, ", "));
	}

	/**
	 * @param bean
	 */
	static void dump(BaseBean bean) {
		List<String> s = new ArrayList<String>();
		s.add(bean.getClass().getName());
		try {
			@SuppressWarnings("unchecked")
			Map<Object, Object> properties = BeanUtils.describe(bean);
			for (Entry<Object, Object> entry : properties.entrySet()) {
				String attr = String.valueOf(entry.getKey());
				String val = String.valueOf(entry.getValue());
				s.add(String.format("%s = \"%s\"", attr, val));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
