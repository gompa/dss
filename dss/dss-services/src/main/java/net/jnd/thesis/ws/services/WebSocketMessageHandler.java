/**
 * 
 */
package net.jnd.thesis.ws.services;

import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Jan 12, 2013
 */
public interface WebSocketMessageHandler<T> {

	/**
	 * @param msg
	 * @return
	 * @throws WSException
	 */
	T handle(WebSocketMessage msg) throws WSException;

}
