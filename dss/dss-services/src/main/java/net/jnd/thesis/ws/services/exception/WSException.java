/**
 * 
 */
package net.jnd.thesis.ws.services.exception;

/**
 * @author João Domingos
 * @since Jan 12, 2013
 */
public class WSException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8838764244976255346L;

	/**
	 * 
	 */
	public WSException() {
		super();
	}

	/**
	 * 
	 */
	public WSException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	public WSException(Throwable cause) {
		super(cause);
	}

}
