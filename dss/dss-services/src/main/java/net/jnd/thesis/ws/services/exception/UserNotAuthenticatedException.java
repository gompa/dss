/**
 * 
 */
package net.jnd.thesis.ws.services.exception;

/**
 * @author João Domingos
 * @since Jan 12, 2013
 */
public class UserNotAuthenticatedException extends WSException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8917867590040906015L;

}
