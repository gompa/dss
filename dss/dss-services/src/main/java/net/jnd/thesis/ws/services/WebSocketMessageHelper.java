/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.util.ArrayList;
import java.util.Collection;

import net.jnd.thesis.common.interfaces.DSSBaseBean;
import net.jnd.thesis.ws.services.exception.InvalidPayloadException;
import net.jnd.thesis.ws.services.exception.WSException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

/**
 * @author João Domingos
 * @since Dec 6, 2012
 */
public class WebSocketMessageHelper {

	private static final Log LOG = LogFactory
			.getLog(WebSocketMessageHelper.class);

	/**
	 * @param msg
	 * @return
	 */
	public static String serializeMessage(WebSocketMessage msg) {
		return new JSONSerializer().deepSerialize(msg);
	}

	/**
	 * @param json
	 * @return
	 */
	public static WebSocketMessage deserializeMessage(String json) {
		WebSocketMessage res = null;
		try {
			res = new JSONDeserializer<WebSocketMessage>()
					.use(null, WebSocketMessage.class)
					.use("payload", ArrayList.class).deserialize(json);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return res;
	}

	/**
	 * @param msg
	 * @return
	 * @throws WSException
	 */
	@SuppressWarnings("unchecked")
	static <T extends DSSBaseBean> Collection<T> getPayloadCollection(
			WebSocketMessage msg) throws WSException {
		try {
			return msg != null ? new ArrayList<T>(
					(Collection<T>) msg.getPayload()) : null;
		} catch (Exception e) {
			throw new InvalidPayloadException();
		}
	}

	/**
	 * @param msg
	 * @return
	 */
	static boolean isSingleResult(WebSocketMessage msg) {
		return msg != null && msg.getPayload() != null
				&& msg.getPayload().size() == 1;
	}

	/**
	 * @param msg
	 * @return
	 */
	static String getPayloadString(WebSocketMessage msg) throws WSException {
		if (isSingleResult(msg)) {
			return String.valueOf(msg.getPayload().iterator().next());
		} else {
			throw new InvalidPayloadException();
		}
	}

	/**
	 * @param msg
	 * @return
	 */
	static boolean getPayloadBoolean(WebSocketMessage msg) throws WSException {
		return Boolean.TRUE.toString().equals(getPayloadString(msg));
	}

	/**
	 * @param msg
	 * @return
	 * @throws WSException
	 */
	static Long getPayloadLong(WebSocketMessage msg) throws WSException {
		String payloadStr = getPayloadString(msg);
		if (StringUtils.isNumeric(payloadStr)) {
			return new Long(payloadStr);
		} else {
			throw new InvalidPayloadException();
		}
	}

	/**
	 * @param msg
	 * @return
	 * @throws WSException
	 */
	@SuppressWarnings("unchecked")
	static <T extends DSSBaseBean> T getSinglePayloadBean(WebSocketMessage msg)
			throws WSException {
		if (isSingleResult(msg)) {
			try {
				return (T) msg.getPayload().iterator().next();
			} catch (Exception e) {
				throw new InvalidPayloadException();
			}
		} else {
			throw new InvalidPayloadException();
		}
	}

}
