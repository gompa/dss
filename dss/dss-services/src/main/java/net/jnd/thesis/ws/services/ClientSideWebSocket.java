/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import net.jnd.thesis.common.bean.BaseBean;
import net.jnd.thesis.common.bean.EventBean;
import net.jnd.thesis.common.bean.StringBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jetty.websocket.WebSocket;

/**
 * @author João Domingos
 * @since Dec 6, 2012
 */
class ClientSideWebSocket implements WebSocket.OnTextMessage {

	private static final Log LOG = LogFactory.getLog(ClientSideWebSocket.class);

	private Connection connection;

	private AtomicLong syncRequestCount = new AtomicLong(0);

	private AtomicBoolean syncRequestPending = new AtomicBoolean(false);

	private long syncRequestPollingInterval = 500L;

	private long syncRequestDefaultTimeout = 300000L;

	private Map<Long, WebSocketMessage> syncResponseMap = new HashMap<Long, WebSocketMessage>();

	/**
	 * @param msg
	 */
	synchronized WebSocketMessage invokeService(final String serviceName,
			boolean secure, final BaseBean... payload) {
		WebSocketMessage res = new WebSocketMessage();
		if (connection == null || !connection.isOpen()) {
			LOG.error("connection closed...");
		} else if (secure
				&& !ClientWebSocketContext.getServices().isAuthenticated()) {
			MessageHelper.displayMessage(this, "client not authenticated...");
		} else if (!ClientWebSocketContext.getServiceNames().contains(
				serviceName)) {
			MessageHelper.displayMessage(this, "invalid service...");
		} else {
			this.syncRequestPending.set(true);
			final Connection conn = this.connection;
			long dt = Calendar.getInstance().getTimeInMillis();
			final long requestId = dt + syncRequestCount.incrementAndGet();
			syncResponseMap.remove(requestId);
			ThreadGroup tg = new ThreadGroup("SYNC_REQUEST");
			Thread timeout = buildTimeoutThread(tg, requestId);
			Thread req = buildRequestThread(tg, conn, requestId, serviceName,
					payload);
			req.start();
			timeout.start();
			while (!syncResponseMap.containsKey(requestId)) {
				try {
					Thread.sleep(syncRequestPollingInterval);
				} catch (InterruptedException e) {
					LOG.debug(e.getMessage(), e);
				}
			}
			// cancel timeout
			try {
				tg.interrupt();
			} catch (Exception e) {
				LOG.debug(e.getMessage(), e);
			}
			// cleanup request
			res = syncResponseMap.remove(requestId);
			// manage errors if necessary
			Collection<StringBean> cds = res.getErrors();
			for (StringBean stringBean : cds) {
				MessageHelper.displayMessage(this, String.valueOf(stringBean));
			}
			this.syncRequestPending.set(false);
		}
		return res;
	}

	/**
	 * @param tg
	 * @param conn
	 * @param requestId
	 * @param serviceName
	 * @param payload
	 * @return
	 */
	private Thread buildRequestThread(final ThreadGroup tg,
			final Connection conn, final long requestId,
			final String serviceName, final BaseBean... payload) {
		Thread req = new Thread(tg, "REQUEST") {
			@Override
			public void run() {
				WebSocketMessage msg = new WebSocketMessage(requestId,
						serviceName, payload);
				String data = WebSocketMessageHelper.serializeMessage(msg);
				try {
					conn.sendMessage(data);
				} catch (IOException e) {
					LOG.error("an error occurred while sending message", e);
				}
			};
		};
		return req;
	}

	/**
	 * @param tg
	 * @param requestId
	 * @return
	 */
	private Thread buildTimeoutThread(final ThreadGroup tg, final long requestId) {
		Thread timeout = new Thread(tg, "TIMEOUT") {
			@Override
			public void run() {
				try {
					Thread.sleep(syncRequestDefaultTimeout);
					syncResponseMap.put(requestId, null);
					LOG.debug(String.format("request [%s] timedout...",
							requestId));
				} catch (Exception e) {
					LOG.debug("timeout cancelled...");
				}
			}
		};
		return timeout;
	}

	@Override
	public void onOpen(final Connection connection) {
		this.connection = connection;
	}

	@Override
	public void onClose(final int closeCode, final String message) {
		MessageHelper.displayMessage(
				this,
				String.format("connection closed [%s]: %s",
						String.valueOf(closeCode), message));
	}

	@Override
	public void onMessage(final String data) {
		MessageHelper
				.displayMessage(this, String.format("onMessage(%s)", data));
		try {
			WebSocketMessage wsMessage = WebSocketMessageHelper
					.deserializeMessage(data);
			if (wsMessage.getRequestId() != null) {
				syncResponseMap.put(wsMessage.getRequestId(), wsMessage);
			} else if ("broadcastEvent".equals(wsMessage.getServiceName())) {
				EventBean evt = WebSocketMessageHelper.getSinglePayloadBean(wsMessage);
				ClientWebSocketContext.broadcastEvent(evt);
			} else {
				Collection<BaseBean> payload = wsMessage.getPayload();
				for (BaseBean baseBean : payload) {
					String msg = String.format("[msg] %s",
							String.valueOf(baseBean));
					MessageHelper.displayMessage(this, msg);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			MessageHelper.displayMessage(this, String.format(
					"an error occured when retrieving message: %s", data));
		}
	}

}
