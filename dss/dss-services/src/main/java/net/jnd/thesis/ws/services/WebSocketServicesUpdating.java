/**
 * 
 */
package net.jnd.thesis.ws.services;

import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public interface WebSocketServicesUpdating extends WebSocketServicesListing {

	/**
	 * @param user
	 * @return
	 * @throws WSException
	 */
	Long createUser(DSSUser user) throws WSException;

	/**
	 * @param user
	 * @return
	 * @throws WSException
	 */
	Boolean updateUser(DSSUser user) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Long createDataSource(DSSDataSource bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Boolean updateDataSource(DSSDataSource bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Long createDynamicReconfiguration(DSSDynamicReconfiguration bean)
			throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Boolean updateDynamicReconfiguration(DSSDynamicReconfiguration bean)
			throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Long createEsperExpression(DSSEsperExpression bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Boolean updateEsperExpression(DSSEsperExpression bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Long createInteractionModel(DSSInteractionModel bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Boolean updateInteractionModel(DSSInteractionModel bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Long createSessionCondition(DSSSessionCondition bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Boolean updateSessionCondition(DSSSessionCondition bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Long createUserSessionConnection(DSSUserSessionConnection bean)
			throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Boolean updateUserSessionConnection(DSSSession bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Long createSession(DSSSession bean) throws WSException;

	/**
	 * @param bean
	 * @return
	 * @throws WSException
	 */
	Boolean updateSession(DSSSession bean) throws WSException;

}
