/**
 * 
 */
package net.jnd.thesis.ws.services;

import net.jnd.thesis.common.bean.EventBean;

/**
 * @author João Domingos
 * @since Jan 27, 2013
 */
public interface SessionEventHandler {

	/**
	 * @param evt
	 */
	void handleEvent(EventBean evt);

}
