/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.util.Collection;

import net.jnd.thesis.common.interfaces.DSSDataSource;
import net.jnd.thesis.common.interfaces.DSSDynamicReconfiguration;
import net.jnd.thesis.common.interfaces.DSSEsperExpression;
import net.jnd.thesis.common.interfaces.DSSInteractionModel;
import net.jnd.thesis.common.interfaces.DSSSession;
import net.jnd.thesis.common.interfaces.DSSSessionCondition;
import net.jnd.thesis.common.interfaces.DSSUser;
import net.jnd.thesis.common.interfaces.DSSUserSessionConnection;
import net.jnd.thesis.ws.services.exception.WSException;

/**
 * @author João Domingos
 * @since Dec 10, 2012
 */
public interface WebSocketServicesListing extends WebSocketServicesCore {

	/**
	 * @return
	 * @throws WSException
	 */
	Collection<DSSUser> listUsers() throws WSException;

	/**
	 * @return
	 */
	Collection<DSSDataSource> listDataSources() throws WSException;

	/**
	 * @return
	 */
	Collection<DSSSession> listSessions() throws WSException;

	/**
	 * @return
	 */
	Collection<DSSSessionCondition> listSessionConditions() throws WSException;

	/**
	 * @return
	 */
	Collection<DSSInteractionModel> listInteractionModels() throws WSException;

	/**
	 * @return
	 */
	Collection<DSSDynamicReconfiguration> listDynamicReconfigurations()
			throws WSException;

	/**
	 * @return
	 */
	Collection<DSSEsperExpression> listEsperExpressions() throws WSException;

	/**
	 * @return
	 */
	Collection<DSSSession> listOwnedSessions() throws WSException;

	/**
	 * @return
	 */
	Collection<DSSUserSessionConnection> listUserSessionConnections()
			throws WSException;

	/**
	 * @return
	 */
	DSSSession getSession(Long sessionId) throws WSException;

	/**
	 * @return
	 */
	DSSUserSessionConnection getConnection(Long connectionId)
			throws WSException;

}
