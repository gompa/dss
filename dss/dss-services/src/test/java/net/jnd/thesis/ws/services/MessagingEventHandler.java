/**
 * 
 */
package net.jnd.thesis.ws.services;

import net.jnd.thesis.common.bean.EventBean;

/**
 * @author João Domingos
 * @since Mar 23, 2013
 */
public class MessagingEventHandler implements SessionEventHandler {

	private enum EventName {
		ALERT, EMERGENCY, DISASTER, AFTERMATH
	}

	/**
	 * This method would broadcasts messages based on the received event
	 */
	@Override
	public void handleEvent(EventBean evt) {

		// if the event is of type SESSION_DYNAMIC_RECONFIGURATION
		// and the event name is ALERT
		// broadcast alert message
		switch (evt.getEventType()) {
		case SESSION_DYNAMIC_RECONFIGURATION:
			if (EventName.ALERT.name().equals(evt.getValue())) {
				DummyEmailSendingService.broadcastAlerSignal();
			}
			break;
		default:
			break;
		}

	}

}
