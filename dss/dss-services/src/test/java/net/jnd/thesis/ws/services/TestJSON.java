/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.util.ArrayList;
import java.util.List;

import net.jnd.thesis.common.bean.AuthenticationBean;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

/**
 * @author João Domingos
 * @since Dec 9, 2012
 */
public class TestJSON {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TestJSON test = new TestJSON();
		test.testJson();
	}

	/**
	 * 
	 */
	public void testJson() {
		AuthenticationBean authBean = new AuthenticationBean("usr", "pwd");
		WebSocketMessage msg = new WebSocketMessage(null, "authentication",
				authBean);
		testWithoutClass(msg);
		testWithClass(msg);
	}

	/**
	 * @param msg
	 * @return
	 */
	private static void testWithoutClass(WebSocketMessage msg) {
		String inStr = new JSONSerializer().exclude("*.class").deepSerialize(
				msg);
		System.out.println(inStr);
		WebSocketMessage out = new JSONDeserializer<WebSocketMessage>().use(
				null, WebSocketMessage.class).deserialize(inStr);
		System.out.println(out);
	}

	/**
	 * @param msg
	 * @return
	 */
	private static void testWithClass(WebSocketMessage msg) {
		String inStr = new JSONSerializer().deepSerialize(msg);
		System.out.println(inStr);
		WebSocketMessage out = new JSONDeserializer<WebSocketMessage>().use(
				null, WebSocketMessage.class).deserialize(inStr);
		System.out.println(out);
	}

	/**
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	private static void jsonTest() {
		Object res = null;
		String wsMsg = "{\"class\":\"net.jnd.thesis.ws.services.WebSocketMessage\",\"id\":3,\"operation\":\"LIST_DATASOURCES\",\"payload\":[{\"class\":\"net.jnd.thesis.common.bean.DataSourceBean\",\"creationDate\":null,\"creationUser\":null,\"dataSourceType\":null,\"id\":1,\"name\":\"thesisdimfccs\",\"owner\":null,\"regex\":null,\"updateDate\":null,\"updateUser\":null,\"valueType\":null},{\"class\":\"net.jnd.thesis.common.bean.DataSourceBean\",\"creationDate\":null,\"creationUser\":null,\"dataSourceType\":null,\"id\":2,\"name\":\"thesis.dimfccs\",\"owner\":null,\"regex\":null,\"updateDate\":null,\"updateUser\":null,\"valueType\":null}]}";
		String arr = "[{\"class\":\"net.jnd.thesis.common.bean.DataSourceBean\",\"creationDate\":null,\"creationUser\":null,\"dataSourceType\":null,\"id\":1,\"name\":\"thesisdimfccs\",\"owner\":null,\"regex\":null,\"updateDate\":null,\"updateUser\":null,\"valueType\":null},{\"class\":\"net.jnd.thesis.common.bean.DataSourceBean\",\"creationDate\":null,\"creationUser\":null,\"dataSourceType\":null,\"id\":2,\"name\":\"thesis.dimfccs\",\"owner\":null,\"regex\":null,\"updateDate\":null,\"updateUser\":null,\"valueType\":null}]";
		try {
			res = WebSocketMessageHelper.deserializeMessage(wsMsg);
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			res = new JSONDeserializer<List>().use(null, ArrayList.class)
			/* .use("values", DataSourceBean.class) */.deserialize(arr);
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			res = new JSONDeserializer<List>().use("payload", ArrayList.class)
			/* .use("values", DataSourceBean.class) */.deserialize(wsMsg);
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			res = new JSONDeserializer<WebSocketMessage>()
					.use(null, WebSocketMessage.class)
					.use("payload", ArrayList.class).deserialize(wsMsg);
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
