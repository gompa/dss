/**
 * 
 */
package net.jnd.thesis.ws.services;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

/**
 * @author João Domingos
 * @since Jan 12, 2013
 */
public class MiscTesting {

	private static final Set<String> availableServices = new HashSet<String>();

	static {
		for (Method method : WebSocketServices.class.getDeclaredMethods()) {
			availableServices.add(method.getName());
		}
	}

	public static void main(String[] args) {
		for (String method : availableServices) {
			System.out.println(method);
		}
	}

}
