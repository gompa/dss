/**
 * 
 */
package net.jnd.thesis.ws.services;

import net.jnd.thesis.common.bean.StringBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import flexjson.JSONSerializer;

/**
 * @author João Domingos
 * @since Dec 6, 2012
 */
public class ServicesMiscTests {

	@SuppressWarnings("unused")
	private static final Log LOG = LogFactory.getLog(ServicesMiscTests.class);

	@SuppressWarnings("unused")
	private static final String CLIENT_WEBSOCKET_URL = "ws://localhost:8080/dss-server/ws/client";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		jsonTesting();
	}

	/**
	 * 
	 */
	private static void jsonTesting() {
		// String json =
		// "{\"class\":\"net.jnd.thesis.ws.services.WebSocketMessage\",\"errors\":[],\"payload\":[{\"class\":\"net.jnd.thesis.common.bean.StringBean\",\"creationDate\":null,\"creationUser\":null,\"id\":null,\"message\":\"Hello world!\",\"updateDate\":null,\"updateUser\":null}],\"requestId\":1,\"serviceName\":\"echo\"}";
		// WebSocketMessage csd =
		// WebSocketMessageHelper.deserializeMessage(json);
		// System.out.println(csd);

		try {
			// Collection<DSSBaseBean> cas = new ArrayList<DSSBaseBean>();
			// cas.add(new AuthenticationBean("usr", "pwd"));
			WebSocketMessage msg = new WebSocketMessage(1L, "echo",
					new StringBean("csdcsdcsdcscscsdc"));
			// String json = WebSocketMessageHelper.serializeMessage(msg);
			String json = new JSONSerializer().deepSerialize(msg);
			WebSocketMessage caca = WebSocketMessageHelper
					.deserializeMessage(json);
			System.out.println(caca);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
